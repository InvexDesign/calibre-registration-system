<div class="head">
    <div class="logo">
        <a class="large" href="{{ route('backend.dashboard.index') }}"><img src="{{ asset('images/logos/sidebar.svg') }}" /><span class="title">{{ App\Models\Setting::get('site-title', 'Admin Center') }}</span></a>
        <a class="small" href="{{ route('backend.dashboard.index') }}"><img src="{{ asset('images/logos/sidebar-small.png') }}" /><span class="title">{{ App\Models\Setting::get('site-title', 'Admin Center') }}</span></a>
    </div>
    <div class="user">
        <img class="icon" src="{{ Auth::user()->getGravatarImageUrl() }}" />
        <div class="details">
            <span class="username">{{ Auth::user()->username }}</span>
            <span class="role">{{ Auth::user()->getRole()->display() }}</span>
        </div>
    </div>
</div>
<ul class="navigation sidebar-nav">
    <li><a href="{{ route('backend.dashboard.index') }}"><i class="fa fa-dashboard icon"></i><span>Dashboard</span></a></li>
    <li><a href="{{ route('backend.services.create.get') }}"><i class="fa fa-cubes icon"></i>Create Service</a></li>
    <li><a href="{{ route('backend.facilities.create.get') }}"><i class="fa fa-bank icon"></i>Create Facility</a></li>
</ul>
