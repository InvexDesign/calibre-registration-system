<form class="date-form" method="get" {!! isset($target_blank) && $target_blank ? 'target="_blank"' : '' !!}>
    <div class="date-input form-group">
        {!! Form::label('date', isset($label) ? $label : 'Date', ['class'=>'control-label']) !!}
        {!! Form::text('date', $date->format('m-d-Y'), ['required', 'class'=>'form-control datepicker-dash']) !!}
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" style="margin-top:25px;">Update</button>
    </div>
</form>