<?php $payment_token = \App\Http\Controllers\SessionController::getCurrentPaymentToken(); ?>
@if($payment_token)
    <div class="alert alert-warning">
        <h4>Current Payment Request</h4>
        <ul class="errors">
            <li>Click here to begin the process of issuing payment: <a href="{{ $payment_token->getLink() }}">{{  $payment_token->display() }}</a></li>
        </ul>
    </div>
@endif