<?php $reminder_notifications = \App\Models\Notifications\Notification::active()->notSevere()->remindable()->get(); ?>
@if(count($reminder_notifications) > 0)
    <div class="alert alert-warning">
        <h4>Reminders</h4>
        <ul class="errors">
            @foreach($reminder_notifications as $reminder_notification)
                <li>{!! $reminder_notification->display() !!} {!! link_to_route('notifications.dismiss', 'Dismiss', ['id' => $reminder_notification->id]) !!}</li>
            @endforeach
        </ul>
    </div>
@endif