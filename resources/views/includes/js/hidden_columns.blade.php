hidden_columns = [
@if(isset($default_hidden_columns))
    @foreach($default_hidden_columns as $name => $garbage)
        '{{ $name }}',
    @endforeach
@endif
@if(isset($hidden_columns))
    @foreach($hidden_columns as $name => $garbage)
        '{{ $name }}',
    @endforeach
@endif
];