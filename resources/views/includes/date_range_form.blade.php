<form class="date-form" method="get" {!! isset($target_blank) && $target_blank ? 'target="_blank"' : '' !!}>
    <div class="date-input form-group">
        {!! Form::label('start', 'Start Date', ['class'=>'control-label']) !!}
        {!! Form::text('start', $start_at->format('m-d-Y'), ['required', 'class'=>'form-control datepicker-dash']) !!}
    </div>
    <div class="date-input form-group">
        {!! Form::label('end', 'End Date', ['class'=>'control-label']) !!}
        {!! Form::text('end', $end_at->format('m-d-Y'), ['required', 'class'=>'form-control datepicker-dash']) !!}
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary" style="margin-top:25px;">Update</button>
    </div>
</form>