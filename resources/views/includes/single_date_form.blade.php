<?php
$name = isset($name) ? $name : 'date';
$label = isset($label) ? $label : 'Date';
?>
<div class="row">
    <div class="col-xs-4 form-group"></div>
    <div class="col-xs-4 form-group">
        <div class="row">
            <div class="col-xs-4 form-group">
                {!! Form::label($name . '__input', $label, ['class'=>'control-label']) !!}
                {!! Form::text($name . '__input', $date->format('m/d/Y'), ['required', 'class'=>'form-control datepicker']) !!}
            </div>
            <div class="col-xs-4 form-group">
                <button onclick="changeSingleDate('#single_date_form', '{{ $name }}'); return false;" class="btn btn-primary" style="margin-top:25px;">Update Selection</button>
            </div>
            <form id="single_date_form" method="get" {!! isset($target_blank) && $target_blank ? 'target="_blank"' : '' !!}>
                <input id="{{ $name }}__submission" name="{{ $name }}" value="{{ $date->format('m/d/Y') }}" style="display:none;" />
            </form>
        </div>
    </div>
    <div class="col-xs-4 form-group"></div>
</div>