{{--<div class="shutter"></div>--}}
<div class="head">
    <div class="logo">
        <a class="large" href="{{ route('backend.dashboard.index') }}">
            <img style="width: 176px; height: 75px;" src="{{ asset(Setting::get('sidebar-logo-large', 'images/logos/sidebar.svg')) }}" />
            <span class="title">{{ Setting::get('site-title', 'Admin Center') }}</span>
        </a>
        <a class="small" href="{{ route('backend.dashboard.index') }}">
            <img src="{{ asset(Setting::get('sidebar-logo-small', 'images/logos/sidebar-small.svg')) }}" />
            <span class="title">{{ Setting::get('site-title', 'Admin Center') }}</span>
        </a>
    </div>
    <div class="user">
        <img class="icon" src="{{ Auth::user()->getGravatarImageUrl() }}" />
        <div class="details">
            <span class="username">{{ Auth::user()->username }}</span>
            <span class="role">{{ Auth::user()->getRole()->display() }}</span>
        </div>
    </div>
</div>
<ul class="navigation sidebar-nav">
    <li class="heading">Manage</li>
    <li><a href="{{ route('backend.dashboard.index') }}"><i class="fa fa-dashboard icon"></i><span>Dashboard</span></a>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-building icon"></i><span>Addresses<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Addresses</li>
            <li><a href="{{ route('backend.addresses.index') }}">View All</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-users icon"></i><span>Attendees<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Attendees</li>
            <li><a href="{{ route('backend.attendees.index') }}">View All</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-tag icon"></i><span>Discounts<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Discounts</li>
            <li><a href="{{ route('backend.discounts.index') }}">View All</a></li>
            <li><a href="{{ route('backend.discounts.active') }}">View Active</a></li>
            <li><a href="{{ route('backend.discounts.inactive') }}">View Inactive</a></li>
            <li><a href="{{ route('backend.discounts.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-graduation-cap icon"></i><span>Courses<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Courses</li>
            <li><a href="{{ route('backend.courses.index') }}">View All</a></li>
            <li><a href="{{ route('backend.courses.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-calendar icon"></i><span>Events<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Events</li>
            <li><a href="{{ route('backend.events.index') }}">View All</a></li>
            <li><a href="{{ route('backend.events.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bed icon"></i><span>Lodgings<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Lodgings</li>
            <li><a href="{{ route('backend.lodgings.index') }}">View All</a></li>
            <li><a href="{{ route('backend.lodgings.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-file-text icon"></i><span>Registrations<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Registrations</li>
            <li><a href="{{ route('backend.registrations.index') }}">View All</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-ticket icon"></i><span>Tickets<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Tickets</li>
            <li><a href="{{ route('backend.tickets.index') }}">View All</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bank icon"></i><span>Venues<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Venues</li>
            <li><a href="{{ route('backend.venues.index') }}">View All</a></li>
            <li><a href="{{ route('backend.venues.create.get') }}">Create New</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-money icon"></i><span>Payments<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Payments</li>
            <li><a href="{{ route('backend.payments.index') }}">View All</a></li>
        </ul>
    </li>
    <li class="heading">Analytics</li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-graduation-cap icon"></i><span>Courses<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Courses</li>
            <li><a href="{{ route('backend.analytics.courses.index') }}">Index</a></li>
            <li><a href="{{ route('backend.analytics.courses.timeline') }}">Timeline</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-calendar icon"></i><span>Events<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Events</li>
            <li><a href="{{ route('backend.analytics.events.index') }}">Index</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-money icon"></i><span>Payments<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Payments</li>
            <li><a href="{{ route('backend.analytics.payments.index') }}">Index</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-file-text icon"></i><span>Registrations<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Registrations</li>
            <li><a href="{{ route('backend.analytics.registrations.index') }}">Index</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-users icon"></i><span>Sales Reps<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Sales Representatives</li>
            <li><a href="{{ route('backend.analytics.sales_representatives.index') }}">Index</a></li>
        </ul>
    </li>
    <li class="heading">System</li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-lock icon"></i><span>Login Attempts<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Analytics</li>
            <li><a href="{{ route('backend.login_attempts.index') }}">View All</a></li>
            <li><a href="{{ route('backend.login_attempts.timeline') }}">Timeline</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-gears icon"></i><span>Settings<b class="caret"></b></span></a>
        <ul class="dropdown-menu">
            <li class="heading">Settings</li>
            <li><a href="{{ route('backend.users.index') }}">Users</a></li>
            <li><a href="{{ route('backend.settings.index') }}">Manage System Settings</a></li>
        </ul>
    </li>
</ul>