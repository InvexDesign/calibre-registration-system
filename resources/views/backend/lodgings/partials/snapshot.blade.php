<p>
    <strong>ID:</strong> {{ $lodging->id }}<br />
    <strong>Name:</strong> {{ $lodging->name }}<br />
    <strong>Phone:</strong> <a href="tel:+1{{ $lodging->displayTel('phone') }}">{{ $lodging->displayPhone('phone') }}</a><br />
    <strong>Email:</strong> <a href="mailto:{{ $lodging->email }}">{{ $lodging->email }}</a><br />
    <strong>Contact:</strong> {{ $lodging->contact }}<br />
    <strong>Address:</strong> {{ $lodging->address->display() }}<br />
    <strong>Description:</strong> {{ $lodging->description }}<br />
</p>