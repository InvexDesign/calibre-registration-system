<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class'=>'form-control phone-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('contact', 'Contact') !!}
        {!! Form::text('contact', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
    </div>
</div>

@include('backend.addresses.partials.form', ['address' => $address])