<?php $table_id = isset($table_id) ? $table_id : 'lodging_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Contact</th>
        <th>Address</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($lodgings as $lodging)
        <tr>
            <td>{!! link_to_route('backend.lodgings.show', $lodging->id, [$lodging->id]) !!}</td>
            <td>{!! link_to_route('backend.lodgings.show', $lodging->name, [$lodging->id]) !!}</td>
            <td><a href="tel:+1{{ $lodging->displayTel('phone') }}">{{ $lodging->displayPhone('phone') }}</a></td>
            <td><a href="mailto:{{ $lodging->email }}">{{ $lodging->email }}</a></td>
            <td>{{ $lodging->contact }}</td>
            <td>{{ $lodging->address->display() }}</td>
            <td>{{ $lodging->description }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.lodgings.show', 'fa-eye', [$lodging->id], ['title' => 'View Lodging']) !!}
                {!! Helper::icon_to_route('backend.lodgings.edit.get', 'fa-pencil', [$lodging->id], ['title' => 'Edit Lodging']) !!}
                {!! Helper::icon_to_route('backend.lodgings.destroy', 'fa-ban', [$lodging->id], ['title' => 'Delete Lodging']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'Description' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>