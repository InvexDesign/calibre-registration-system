@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Set Embedded Map')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Lodgings'            => ['backend.lodgings.index'],
        '#' . $lodging->id    => ['backend.lodgings.show', $lodging->id],
        'Set Embedded Map'    =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($lodging, ['route' => ['backend.lodgings.set_embedded_map.post', $lodging->id]]) !!}

        <div class="field form-group">
            {!! Form::label('embedded_map', 'Embedded Map HTML *') !!}
            {!! Form::textarea('embedded_map', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Update Embedded Map', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop