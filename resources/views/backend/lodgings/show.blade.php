@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Lodging')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Lodgings'            => ['backend.lodgings.index'],
        $lodging->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#lodging">Lodging</a></li>
        <li><a data-toggle="tab" href="#map">Map</a></li>
        <li><a data-toggle="tab" href="#events">Events</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="lodging" class="tab-pane fade in active">
            {!! link_to_route('backend.lodgings.edit.get', 'Edit Lodging', ['id' => $lodging->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.lodgings.destroy', 'Delete Lodging', ['id' => $lodging->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.lodgings.partials.snapshot')
        </div>
        <div id="map" class="tab-pane fade">
            {!! link_to_route('backend.lodgings.generate_embedded_map', 'Auto Generate Embedded Map', ['id' => $lodging->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.lodgings.set_embedded_map.get', 'Manually Set Embedded Map', ['id' => $lodging->id], ['class' => 'btn btn-warning']) !!}
            <br />
            <br />
            {!! $lodging->embedded_map !!}
        </div>
        <div id="events" class="tab-pane fade">
            @include('backend.events.partials.table', ['events' => $lodging->events])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $lodging])
        </div>
    </div>
@stop