@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Lodgings')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Lodgings' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.lodgings.create.get', 'Create Lodging', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.lodgings.partials.table')

@stop