@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Lodging')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Lodgings' => ['backend.lodgings.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.lodgings.create.post']) !!}

        @include('backend.lodgings.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Lodging', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop