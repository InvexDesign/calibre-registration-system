@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Lodging')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Lodgings'            => ['backend.lodgings.index'],
        '#' . $lodging->id    => ['backend.lodgings.show', $lodging->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($lodging, ['route' => ['backend.lodgings.edit.post', $lodging->id]]) !!}

        @include('backend.lodgings.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Lodging', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop