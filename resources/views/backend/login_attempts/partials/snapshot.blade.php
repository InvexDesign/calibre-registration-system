<p>
    <strong>ID:</strong> {{ $service->id }}<br />
    <strong>UID:</strong> {{ $service->uid }}<br />
    <strong>Name:</strong> {{ $service->name }}<br />
    <strong>Tagline:</strong> {{ $service->tagline }}<br />
    <strong>Description:</strong> {{ $service->description }}<br />
    <strong>Notes:</strong> {{ $service->notes }}<br />
</p>