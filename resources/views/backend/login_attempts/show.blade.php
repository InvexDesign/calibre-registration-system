@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Service')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Services'            => ['backend.services.index'],
        $service->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#service">Service</a></li>
        <li><a data-toggle="tab" href="#facilities">Facilities</a></li>
    </ul>

    <div class="tab-content">
        <div id="service" class="tab-pane fade in active">
            {!! link_to_route('backend.services.edit.get', 'Edit Service', ['id' => $service->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.services.destroy', 'Delete Service', ['id' => $service->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.services.partials.snapshot')
        </div>
        <div id="facilities" class="tab-pane fade">
            @include('backend.facilities.partials.table', ['facilities' => $service->facilities])
        </div>
    </div>
@stop