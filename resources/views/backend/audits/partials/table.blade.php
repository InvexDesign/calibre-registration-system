<?php $table_id = isset($table_id) ? $table_id : 'audit_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Event</th>
        <th>URL</th>
        <th>IP</th>
        <th>User Agent</th>
        <th>User</th>
        <th>Attribute</th>
        <th>Old</th>
        <th>New</th>
        <th>Timestamp</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($audits as $audit)
		<?php $metadata = $audit->getMetadata(); ?>
        @foreach($audit->getModified() as $attribute => $changes)
        <tr>
            <td>{!! link_to_route('backend.audits.show', $audit->id, [$audit->id]) !!}</td>
            <td>{{ class_basename($audit->auditable) . ' #' . $audit->auditable_id }}</td>
            <td>{{ $metadata['audit_event'] }}</td>
            <td>{{ $metadata['audit_url'] }}</td>
            <td>{{ $metadata['audit_ip_address'] }}</td>
            <td>{{ $metadata['audit_user_agent'] }}</td>
            <td>{!! $audit->user ? link_to_route('backend.users.show', $audit->user->display(), ['id' => $audit->user->id]) : 'N/A' !!}</td>
            <td>{{ $attribute }}</td>
            <td>{{ isset($changes['old']) ? $changes['old'] : '' }}</td>
            <td>{{ is_array($changes['new']) ? implode(',', $changes['new']) : $changes['new'] }}</td>
            <td>{{ Carbon\Carbon::parse($metadata['audit_created_at'])->format('g:iA n/j/Y') }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.audits.show', 'fa-eye', [$audit->id], ['title' => 'View Audit']) !!}
            </td>
        </tr>
        @endforeach
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['URL' => 1,'User Agent' => 1,]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, paging: false});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>