<h4>Meta Data</h4>
<?php $metadata = $audit->getMetadata(); ?>
<p>
    <strong>ID:</strong> {{ $metadata['audit_id'] }} <br />
    <strong>Event:</strong> {{ $metadata['audit_event'] }} <br />
    <strong>URL:</strong> {{ $metadata['audit_url'] }} <br />
    <strong>IP Address:</strong> {{ $metadata['audit_ip_address'] }} <br />
    <strong>User Agent:</strong> {{ $metadata['audit_user_agent'] }} <br />
    <strong>Timestamp:</strong> {{ Carbon\Carbon::parse($metadata['audit_created_at'])->format('g:iA n/j/Y') }} <br />
    <strong>User:</strong> {!! $audit->user ? link_to_route('backend.users.show', $audit->user->display(), ['id' => $audit->user->id]) : 'N/A' !!}<br />
</p>
<h4>Modifications</h4>
<?php $modifications = $audit->getModified(); ?>
<table style="width: 100%;">
    <thead>
    <tr>
        <th>Attribute</th>
        <th>Old</th>
        <th>New</th>
    </tr>
    </thead>
    <tbody>
    @foreach($modifications as $attribute => $changes)
    <tr>
        <td>{{ $attribute }}</td>
        <td>{{ isset($changes['old']) ? $changes['old'] : '' }}</td>
        <td>{{ $changes['new'] }}</td>
    </tr>
    @endforeach
    </tbody>
</table>