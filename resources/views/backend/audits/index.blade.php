@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Audits')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Audits' => null,
    ]); !!}
@stop

@section('content')

    {!! Form::open(['url' => $search_route, 'method' => 'get']) !!}
    <div class="row">
        <div class="col-xs-10 form-group">
            <div class="col-xs-6 form-group">
                {!! Form::label('event', 'Event') !!}
                {!! Form::select('event', $event_options, null, ['class'=>'select-two form-control']) !!}
            </div>
            <div class="col-xs-6 form-group">
                {!! Form::label('model', 'Model') !!}
                {!! Form::select('model', $model_options, null, ['class'=>'select-two form-control']) !!}
            </div>
        </div>
        <div class="col-xs-2" style="display: flex; justify-content: space-around; align-items: center; height: 85px;">
            {!! Form::submit('Filter', ['class' => 'btn btn-primary']) !!}
            <a href="{{ $search_route }}" class="btn btn-warning">Clear</a>
        </div>
    </div>
    {!! Form::close() !!}

	<?php $_index = ($audits->currentPage() - 1) * $audits->perPage(); ?>
    <div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 10px;">
        <p style="margin: 0;">{{ 'Showing ' . ($_index + 1) . ' to ' . ($_index + $audits->count()) . ' of ' . $audits->total() . ' entries'}}</p>
        {{ $audits->appends(Input::except('page'))->links() }}
    </div>

    @include('backend.audits.partials.table')

    {{ $audits->appends(Input::except('page'))->links() }}

@stop