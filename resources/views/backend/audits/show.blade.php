@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Audit')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Audits'            => ['backend.audits.index'],
        '#' . $audit->id    =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#audit">Audit</a></li>
    </ul>

    <div class="tab-content">
        <div id="audit" class="tab-pane fade in active">
            @include('backend.audits.partials.snapshot')
        </div>
    </div>
@stop