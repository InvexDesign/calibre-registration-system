@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Address')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Addresses' => ['backend.addresses.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.addresses.create.post']) !!}

        @include('backend.addresses.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Address', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop