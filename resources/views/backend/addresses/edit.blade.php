@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Address')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Addresses'            => ['backend.addresses.index'],
        '#' . $address->id    => ['backend.addresses.show', $address->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($address, ['route' => ['backend.addresses.edit.post', $address->id]]) !!}

        @include('backend.addresses.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Address', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop