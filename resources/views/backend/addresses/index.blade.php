@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Addresses')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Addresses' => null,
    ]); !!}
@stop

@section('content')

    @include('backend.addresses.partials.table')

@stop