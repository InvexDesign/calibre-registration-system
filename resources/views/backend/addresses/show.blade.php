@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Address')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Addresses'           => ['backend.addresses.index'],
        $address->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#address">Address</a></li>
        <li><a data-toggle="tab" href="#venues">Venues</a></li>
        <li><a data-toggle="tab" href="#lodgings">Lodgings</a></li>
    </ul>

    <div class="tab-content">
        <div id="address" class="tab-pane fade in active">
            {!! link_to_route('backend.addresses.edit.get', 'Edit Address', ['id' => $address->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.addresses.destroy', 'Delete Address', ['id' => $address->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.addresses.partials.snapshot')
        </div>
        <div id="venues" class="tab-pane fade">
            @include('backend.venues.partials.table', ['venues' => $address->venues])
        </div>
        <div id="lodgings" class="tab-pane fade">
            @include('backend.lodgings.partials.table', ['lodgings' => $address->lodgings])
        </div>
    </div>
@stop