<?php $table_id = isset($table_id) ? $table_id : 'address_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Line 1</th>
        <th>Line 2</th>
        <th>City</th>
        <th>State</th>
        <th>Zipcode</th>
        <th>Country</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($addresses as $address)
        <tr>
            <td>{!! link_to_route('backend.addresses.show', $address->id, [$address->id]) !!}</td>
            <td>{!! link_to_route('backend.addresses.show', $address->line1, [$address->id]) !!}</td>
            <td>{{ $address->line2 }}</td>
            <td>{{ $address->city }}</td>
            <td>{{ $address->state->display() }}</td>
            <td>{{ $address->zipcode }}</td>
            <td>{{ $address->country }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.addresses.show', 'fa-eye', [$address->id], ['title' => 'View Address']) !!}
                {!! Helper::icon_to_route('backend.addresses.edit.get', 'fa-pencil', [$address->id], ['title' => 'Edit Address']) !!}
                {!! Helper::icon_to_route('backend.addresses.destroy', 'fa-ban', [$address->id], ['title' => 'Delete Address']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>