@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Course')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Courses'            => ['backend.courses.index'],
        $course->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#course">Course</a></li>
        <li><a data-toggle="tab" href="#events">Events</a></li>
        <li><a data-toggle="tab" href="#analytics">Analytics</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="course" class="tab-pane fade in active">
            {!! link_to_route('backend.courses.edit.get', 'Edit Course', ['id' => $course->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.courses.destroy', 'Delete Course', ['id' => $course->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.courses.partials.snapshot')
        </div>
        <div id="events" class="tab-pane fade">
            @include('backend.events.partials.table', ['events' => $course->events])
        </div>
        <div id="analytics" class="tab-pane fade">
            @include('backend.courses.partials.analytics', ['course' => $course])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $course])
        </div>
    </div>
@stop