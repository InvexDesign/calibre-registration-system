@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Course')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Courses'            => ['backend.courses.index'],
        '#' . $course->id    => ['backend.courses.show', $course->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($course, ['route' => ['backend.courses.edit.post', $course->id]]) !!}

        @include('backend.courses.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Course', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop