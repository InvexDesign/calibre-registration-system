<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('short_name', 'Short Name') !!}
        {!! Form::text('short_name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        <p class="instruction">This will be publicly visible.</p>
    </div>
</div>