<?php $table_id = isset($table_id) ? $table_id : 'course_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Short</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($courses as $course)
        <tr>
            <td>{!! link_to_route('backend.courses.show', $course->id, [$course->id]) !!}</td>
            <td>{!! link_to_route('backend.courses.show', $course->uid, [$course->id]) !!}</td>
            <td>{!! link_to_route('backend.courses.show', $course->name, [$course->id]) !!}</td>
            <td>{{ $course->getShortName() }}</td>
            <td>{{ $course->description }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.courses.show', 'fa-eye', [$course->id], ['title' => 'View Course']) !!}
                {!! Helper::icon_to_route('backend.courses.edit.get', 'fa-pencil', [$course->id], ['title' => 'Edit Course']) !!}
                {!! Helper::icon_to_route('backend.courses.destroy', 'fa-ban', [$course->id], ['title' => 'Delete Course']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['Description' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>