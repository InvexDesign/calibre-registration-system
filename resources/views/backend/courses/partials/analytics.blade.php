<strong>Events:</strong> {{ number_format($course->events()->count(), 0) }}<br />
<strong>Tickets:</strong> {{ number_format($course->getTicketsQuery()->count(), 0) }}<br />
<strong>Registrations:</strong> {{ number_format($course->getRegistrationsQuery()->count(), 0) }}<br />
<strong>Revenue:</strong> ${{ number_format($course->getPaymentsQuery()->sum('amount_in_cents') / 100, 2) }}<br />
