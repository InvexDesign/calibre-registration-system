<p>
    <strong>ID:</strong> {{ $course->id }}<br />
    <strong>UID:</strong> {{ $course->uid }}<br />
    <strong>Name:</strong> {{ $course->name }}<br />
    <strong>Short Name:</strong> {{ $course->short_name }}<br />
    <strong>Description:</strong> {{ $course->description }}<br />
</p>