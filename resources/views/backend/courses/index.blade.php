@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Courses')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Courses' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.courses.create.get', 'Create Course', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.courses.partials.table')

@stop