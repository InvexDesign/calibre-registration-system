@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Course')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Courses' => ['backend.courses.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.courses.create.post']) !!}

        @include('backend.courses.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Course', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop