@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Feature Searches Monthly')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Analytics'     => null,
        'Features'      => null,
        'Monthly'       => null
    ]); !!}
@stop

@section('content')

    @include('includes.date_form', ['label' => 'Month Of'])

    <h4 style="text-align: center;">Date Range: {{ $start_at->format('m/d/Y') }} - {{ $end_at->format('m/d/Y') }}</h4>
    <canvas id="chart" width="800" height="450"></canvas>

    <script>
        $(document).ready(function()
        {
            new Chart(document.getElementById("chart"), {
                type: 'pie',
                data: {
                    labels: ['{!! implode("','", $data['labels']) !!}'],
                    datasets: [{
                        label: '{{ $data['datasets']['label'] }}',
                        backgroundColor: ["{!! implode('","', $data['datasets']['backgroundColor']) !!}"],
                        data: [{{ implode(',', $data['datasets']['data']) }}],
                    }]
                },
                options: {
                    title: {
                        display: true,
                        text: '{{ $title }}'
                    }
                }
            });
        });
    </script>
@stop

