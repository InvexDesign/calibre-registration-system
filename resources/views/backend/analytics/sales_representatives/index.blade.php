@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Sales Representatives')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Sales Representatives' => null,
    ]); !!}
@stop

@section('content')

    @include('includes.date_range_form')

    <?php $table_id = isset($table_id) ? $table_id : 'sales_representative_table'; ?>

    <table id="{{ $table_id }}" class="datatable">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Events</th>
            <th>Registrations</th>
            <th>Tickets</th>
            <th>Revenue</th>
            <th>Discounts</th>
            <th>Goal</th>
            <th>Goal %</th>
            <th>+ / -</th>
        </tr>
        </thead>

        <tbody>
        @foreach($sales_representatives as $sales_representative)
            <tr>
                <td>{!! link_to_route('backend.users.show', $sales_representative->id, [$sales_representative->id]) !!}</td>
                <td>{!! link_to_route('backend.users.show', $sales_representative->display(), [$sales_representative->id]) !!}</td>
                <td class="align-right">{{ number_format($sales_representative['event_count'], 0) }}</td>
                <td class="align-right">{{ number_format($sales_representative['registration_count'], 0) }}</td>
                <td class="align-right">{{ number_format($sales_representative['ticket_count'], 0) }}</td>
                <td class="align-right">${{ number_format($sales_representative['revenue'], 2) }}</td>
                <td class="align-right">${{ number_format($sales_representative['discounts'], 2) }}</td>
                <td class="align-right">${{ number_format($sales_representative['goal'], 2) }}</td>
                <td class="align-right">{{ number_format($sales_representative['goal_percent'], 2) }}%</td>
                <td class="align-right">${{ number_format($sales_representative['over_under'], 2) }}</td>
            </tr>
        @endforeach
        </tbody>

    </table>

    <script>
        $(document).ready(function()
        {
            var hidden_columns;
            @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1]])

            var table_id = '{{ $table_id }}';
            datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, sortOrder: [[ 9, "desc" ]]});

            @if(isset($has_filtering) && $has_filtering)
            $('.filter').change(function()
            {
                datatables[table_id].draw();
            });
            @endif
        });
    </script>

@stop