@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Events')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Events' => null,
    ]); !!}
@stop

@section('content')

    @include('backend._includes.date_range_form')

<?php $table_id = isset($table_id) ? $table_id : 'event_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Date</th>
        <th>Course</th>
        <th>Tickets</th>
        <th>Registrations</th>
        <th>Collected</th>
        <th>Uncollected</th>
        <th>Goal</th>
    </tr>
    </thead>

    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{!! link_to_route('backend.events.show', $event->id, [$event->id]) !!}</td>
            <td>{!! link_to_route('backend.events.show', $event->uid, [$event->id]) !!}</td>
            <td>{!! link_to_route('backend.events.show', $event->name, [$event->id]) !!}</td>
            <td>{!! Helper::displayCalendarDate($event->start_at) !!}</td>
            <td>{!! link_to_route('backend.courses.show', $event->course->display(), [$event->course_id]) !!}</td>
            <td class="align-right">{{ number_format($event->tickets()->count(), 0) }}</td>
            <td class="align-right">{{ number_format($event->registrations()->count(), 0) }}</td>
            <td class="align-right">${{ number_format($event->getPaymentsQuery()->sum('amount_in_cents') / 100, 2) }}</td>
            <td class="align-right">${{ number_format(($event->tickets()->sum('price_in_cents') - $event->getPaymentsQuery()->sum('amount_in_cents')) / 100, 2) }}</td>
            <td class="align-right">${{ number_format($event->revenue_goal / 100, 2) }}</td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1,'Name' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>

@stop