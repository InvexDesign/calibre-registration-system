@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Facility Clicks Timeline')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Analytics'     => null,
        'Facilities'    => null,
        'Timeline'      => null
    ]); !!}
@stop

@section('content')

    @include('includes.date_range_form')

    <canvas id="chart" width="800" height="450"></canvas>

    <script>
        $(document).ready(function()
        {
            new Chart(document.getElementById("chart"), {
                type: 'line',
                data: {
                    labels: ['{!! implode("','", $data['labels']) !!}'],
                    datasets: [
                        @foreach($data['datasets'] as $dataset)
                        {
                            label: '{{ $dataset['label'] }}',
                            borderColor: '{{ $dataset['borderColor'] }}',
                            fill: '{{ $dataset['fill'] }}',
                            data: [{{ implode(',', $dataset['data']) }}],
                            hidden: {{ $dataset['hidden'] ? 'true' : 'false' }}
                        },
                        @endforeach
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: '{{ $title }}'
                    }
                }
            });
        });
    </script>

@stop

