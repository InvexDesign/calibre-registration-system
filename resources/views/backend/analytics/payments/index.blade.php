@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Payments')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Payments' => null,
    ]); !!}
@stop

@section('content')

    @include('backend._includes.date_range_form')

<?php $table_id = isset($table_id) ? $table_id : 'payment_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>Type</th>
        <th>Count</th>
        <th>Amount</th>
    </tr>
    </thead>

    <tbody>
    @foreach($types as $type)
        <tr>
            <td>{{ $type['name'] }}</td>
            <td class="align-right">{{ number_format($type['count'], 0) }}</td>
            <td class="align-right">${{ number_format($type['amount'], 2) }}</td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => []])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, sortOrder: [[ 2, "desc" ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>

@stop