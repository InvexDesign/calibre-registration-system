@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Registrations')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations' => null,
    ]); !!}
@stop

@section('content')

    @include('backend._includes.date_range_form')

    <style>
        table.analytics td:first-child {
            font-weight: 700;
        }
        table.analytics td {
            padding: 0 10px;
        }
        table.analytics td {
            text-align: right;
        }
    </style>

    <table class="analytics">
        <tbody>
        <tr>
            <td>Number Of Registrations:</td>
            <td>{{ number_format($registration_count, 0) }}</td>
        </tr>
        <tr>
            <td>Average Ticket Count:</td>
            <td>{{ number_format($average_ticket_count, 0) }}</td>
        </tr>
        <tr>
            <td>Average Cost:</td>
            <td>${{ number_format($average_cost, 2) }}</td>
        </tr>
        <tr>
            <td>Total Paid:</td>
            <td>${{ number_format($total_paid, 2) }}</td>
        </tr>
        <tr>
            <td>Total Cost:</td>
            <td>${{ number_format($total_cost, 2) }}</td>
        </tr>
        <tr>
            <td>Percent Paid:</td>
            <td>{{ number_format($percent_paid, 2) }}%</td>
        </tr>
        </tbody>
    </table>
    {{--<strong>Number Of Registrations:</strong> {{ number_format($registration_count, 0) }}<br />--}}
    {{--<strong>Average Ticket Count:</strong> {{ number_format($average_ticket_count, 0) }}<br />--}}
    {{--<strong>Average Cost:</strong> ${{ number_format($average_cost, 2) }}<br />--}}
    {{--<strong>Total Paid:</strong> ${{ number_format($total_paid, 2) }}<br />--}}
    {{--<strong>Total Cost:</strong> ${{ number_format($total_cost, 2) }}<br />--}}
    {{--<strong>Percent Paid:</strong> {{ number_format($percent_paid, 2) }}%<br />--}}

@stop