@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Courses')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Courses' => ['backend.analytics.courses.index'],
        'Timeline' => null,
    ]); !!}
@stop

@section('content')

    @include('includes.date_range_form')

    <canvas id="registrations_chart" width="800" height="450"></canvas>
    <canvas id="tickets_chart" width="800" height="450"></canvas>
    <canvas id="payments_chart" width="800" height="450"></canvas>

    <script>
        $(document).ready(function()
        {
            new Chart(document.getElementById("registrations_chart"), {
                type: 'line',
                data: {
                    labels: ['{!! implode("','", $registrations_data['labels']) !!}'],
                    datasets: [
                        @foreach($registrations_data['datasets'] as $dataset)
                        {
                            label: '{{ $dataset['label'] }}',
                            borderColor: '{{ $dataset['borderColor'] }}',
                            fill: '{{ $dataset['fill'] }}',
                            data: [{{ implode(',', $dataset['data']) }}],
                            hidden: {{ $dataset['hidden'] ? 'true' : 'false' }}
                        },
                        @endforeach
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Registrations By Course'
                    }
                }
            });

            new Chart(document.getElementById("tickets_chart"), {
                type: 'line',
                data: {
                    labels: ['{!! implode("','", $tickets_data['labels']) !!}'],
                    datasets: [
                        @foreach($tickets_data['datasets'] as $dataset)
                        {
                            label: '{{ $dataset['label'] }}',
                            borderColor: '{{ $dataset['borderColor'] }}',
                            fill: '{{ $dataset['fill'] }}',
                            data: [{{ implode(',', $dataset['data']) }}],
                            hidden: {{ $dataset['hidden'] ? 'true' : 'false' }}
                        },
                        @endforeach
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Tickets By Course'
                    }
                }
            });

            new Chart(document.getElementById("payments_chart"), {
                type: 'line',
                data: {
                    labels: ['{!! implode("','", $payments_data['labels']) !!}'],
                    datasets: [
                        @foreach($payments_data['datasets'] as $dataset)
                        {
                            label: '{{ $dataset['label'] }}',
                            borderColor: '{{ $dataset['borderColor'] }}',
                            fill: '{{ $dataset['fill'] }}',
                            data: [{{ implode(',', $dataset['data']) }}],
                            hidden: {{ $dataset['hidden'] ? 'true' : 'false' }}
                        },
                        @endforeach
                    ]
                },
                options: {
                    title: {
                        display: true,
                        text: 'Payments By Course (in dollars)'
                    }
                }
            });
        });
    </script>

@stop