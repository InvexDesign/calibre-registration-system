@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Courses')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Courses' => null,
    ]); !!}
@stop

@section('content')

<?php $table_id = isset($table_id) ? $table_id : 'course_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Events</th>
        <th>Tickets</th>
        <th>Registrations</th>
        <th>Revenue</th>
    </tr>
    </thead>

    <tbody>
    @foreach($courses as $course)
        <tr>
            <td>{!! link_to_route('backend.courses.show', $course->id, [$course->id]) !!}</td>
            <td>{!! link_to_route('backend.courses.show', $course->uid, [$course->id]) !!}</td>
            <td>{!! link_to_route('backend.courses.show', $course->name, [$course->id]) !!}</td>
            <td class="align-right">{{ number_format($course->events()->count(), 0) }}</td>
            <td class="align-right">{{ number_format($course->getTicketsQuery()->count(), 0) }}</td>
            <td class="align-right">{{ number_format($course->getRegistrationsQuery()->count(), 0) }}</td>
            <td class="align-right">${{ number_format($course->getPaymentsQuery()->sum('amount_in_cents') / 100, 2) }}</td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, sortOrder: [[ 6, "desc" ]]});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>

@stop