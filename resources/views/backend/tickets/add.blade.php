@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Add Tickets')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Tickets'               => ['backend.tickets.index'],
        'Registration #' . $registration->id => ['backend.registrations.show', $registration->id],
        'Add'                   =>  null,
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => ['backend.registrations.add_tickets.post', $registration->id]]) !!}

        <h2>Attendees</h2>
        <div class="field-container col-1">
            <div class="field form-group">
                {!! Form::label('attendee_count', 'Number of Attendees *') !!}
                {!! Form::select('attendee_count', ['' => 'Please Select A Value'] + $attendee_count_options, isset($attendee_count) ?  $attendee_count : null, ['id' => 'attendee_count', 'class'=>'form-control']) !!}
            </div>
        </div>

        @for($i = 0; $i < $max_attendee_count; $i++)
            @include('frontend.attendees.partials.form')
        @endfor

        <div class="form-group">
            {!! Form::submit('Create Ticket', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

    <script>
        function showHideAttendees()
        {
            var attendee_count = parseInt($('#attendee_count').val());
            for(var i = 0; i < attendee_count; i++)
            {
                $('#attendee_form_' + i).show();
            }
            for(var j = attendee_count; i < {{ $max_attendee_count }}; i++)
            {
                $('#attendee_form_' + i).hide();
            }
        }

        $(document).ready(function()
        {
            $('#attendee_count').change(showHideAttendees);

            showHideAttendees();
        });
    </script>
@stop