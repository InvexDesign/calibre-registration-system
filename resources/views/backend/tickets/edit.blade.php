@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Ticket')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Tickets'            => ['backend.tickets.index'],
        '#' . $ticket->id    => ['backend.tickets.show', $ticket->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($ticket, ['route' => ['backend.tickets.edit.post', $ticket->id]]) !!}

        @include('backend.tickets.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Ticket', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop