@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Tickets')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Tickets' => null,
    ]); !!}
@stop

@section('content')

    <style>
        .dataTables_info {
            display: none;
        }
    </style>


    @include('backend._includes.date_range_form')

    <?php $date_range_parameters = ['start' => $start_at->format('m-d-Y'), 'end' => $end_at->format('m-d-Y')]; ?>

	<?php $_index = ($tickets->currentPage() - 1) * $tickets->perPage(); ?>
    <div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 10px;">
        <p style="margin: 0;">{{ 'Showing ' . ($_index + 1) . ' to ' . ($_index + $tickets->count()) . ' of ' . $tickets->total() . ' entries'}}</p>
        {{ $tickets->appends($date_range_parameters)->links() }}
    </div>

    @include('backend.tickets.partials.table')

    {{ $tickets->appends($date_range_parameters)->links() }}

@stop