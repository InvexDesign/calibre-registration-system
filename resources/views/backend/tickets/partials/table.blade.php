<?php $table_id = isset($table_id) ? $table_id : 'ticket_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Event</th>
        <th>Registration</th>
        <th>Attendee</th>
        <th>Price</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($tickets as $ticket)
        <tr>
            <td>{!! link_to_route('backend.tickets.show', $ticket->id, [$ticket->id]) !!}</td>
            <td>{!! link_to_route('backend.tickets.show', $ticket->uid, [$ticket->id]) !!}</td>
            <td>{!! link_to_route('backend.events.show', $ticket->event->display(), [$ticket->event_id]) !!}</td>
            <td>{!! link_to_route('backend.registrations.show', $ticket->registration->display(), [$ticket->registrations_id]) !!}</td>
            <td>{!! link_to_route('backend.attendees.show', $ticket->attendee->display(), [$ticket->attendee_id]) !!}</td>
            <td class="align-right">${{ number_format($ticket->price_in_dollars, 2) }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.tickets.show', 'fa-eye', [$ticket->id], ['title' => 'View Ticket']) !!}
{{--                {!! Helper::icon_to_route('backend.tickets.edit.get', 'fa-pencil', [$ticket->id], ['title' => 'Edit Ticket']) !!}--}}
                {!! Helper::icon_to_route('backend.tickets.destroy', 'fa-ban', [$ticket->id], ['title' => 'Delete Ticket']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => []])

        var table_id = '{{ $table_id }}';

        @if(isset($_searchable) && $_searchable)
            datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, dom: '', paging: false});
        @else
            datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, paging: false});
        @endif

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>