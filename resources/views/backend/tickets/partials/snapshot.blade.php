<p>
    <strong>ID:</strong> {{ $ticket->id }}<br />
    <strong>UID:</strong> {{ $ticket->uid }}<br />
    <strong>Name:</strong> {{ $ticket->name }}<br />
    <strong>Tagline:</strong> {{ $ticket->tagline }}<br />
    <strong>Description:</strong> {{ $ticket->description }}<br />
</p>