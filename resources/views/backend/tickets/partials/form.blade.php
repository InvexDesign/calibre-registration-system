<div class="field-container col-1">
    <div class="field form-group">
        {!! Form::label('event', 'Event #' . $registration->event_id . ': ' . $registration->event->displayClass()) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration', 'Registration #' . $registration->id . ': ' . $registration->display()) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('price', 'Price: $' . number_format($registration->event->priceInDollars, 2)) !!}
    </div>
</div>
<h4>Attendee</h4>
<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('attendee[first_name]', 'First Name *') !!}
        {!! Form::text('attendee[first_name]', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('attendee[last_name]', 'Last Name *') !!}
        {!! Form::text('attendee[last_name]', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('attendee[email]', 'Email') !!}
        {!! Form::text('attendee[email]', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('attendee[rank]', 'Rank *') !!}
        {!! Form::text('attendee[rank]', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('attendee[pid]', 'PID *') !!}
        {!! Form::text('attendee[pid]', null, ['class'=>'form-control']) !!}
    </div>
</div>