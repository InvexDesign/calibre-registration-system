@foreach($tickets as $ticket)
    <tr>
        <td>{!! link_to_route('backend.tickets.show', $ticket->id, [$ticket->id]) !!}</td>
        <td>{!! link_to_route('backend.tickets.show', $ticket->uid, [$ticket->id]) !!}</td>
        <td>{!! link_to_route('backend.events.show', $ticket->event->display(), [$ticket->event_id]) !!}</td>
        <td>{!! link_to_route('backend.registrations.show', $ticket->registration->display(), [$ticket->registrations_id]) !!}</td>
        <td>{!! link_to_route('backend.attendees.show', $ticket->attendee->display(), [$ticket->attendee_id]) !!}</td>
        <td class="align-right">${{ number_format($ticket->price_in_dollars, 2) }}</td>
        <td class="icon-td">
            {!! Helper::icon_to_route('backend.tickets.show', 'fa-eye', [$ticket->id], ['title' => 'View Ticket']) !!}
            {{--                {!! Helper::icon_to_route('backend.tickets.edit.get', 'fa-pencil', [$ticket->id], ['title' => 'Edit Ticket']) !!}--}}
            {{--                {!! Helper::icon_to_route('backend.tickets.destroy', 'fa-ban', [$ticket->id], ['title' => 'Delete Ticket']) !!}--}}
        </td>
    </tr>
@endforeach