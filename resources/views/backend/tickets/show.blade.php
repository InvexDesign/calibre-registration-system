@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Ticket')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Tickets'            => ['backend.tickets.index'],
        $ticket->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#ticket">Ticket</a></li>
        <li><a data-toggle="tab" href="#event">Event</a></li>
        <li><a data-toggle="tab" href="#registration">Registration</a></li>
        <li><a data-toggle="tab" href="#attendee">Attendee</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="ticket" class="tab-pane fade in active">
            {!! link_to_route('backend.tickets.edit.get', 'Edit Ticket', ['id' => $ticket->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.tickets.destroy', 'Delete Ticket', ['id' => $ticket->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.tickets.partials.snapshot')
        </div>
        <div id="event" class="tab-pane fade">
            {!! link_to_route('backend.events.show', 'View Event', ['id' => $ticket->event_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.events.partials.snapshot', ['event' => $ticket->event])
        </div>
        <div id="registration" class="tab-pane fade">
            {!! link_to_route('backend.registrations.show', 'View Registration', ['id' => $ticket->registration_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.registrations.partials.snapshot', ['registration' => $ticket->registration])
        </div>
        <div id="attendee" class="tab-pane fade">
            {!! link_to_route('backend.attendees.show', 'View Attendee', ['id' => $ticket->attendee_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.attendees.partials.snapshot', ['attendee' => $ticket->attendee])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $ticket])
        </div>
    </div>
@stop