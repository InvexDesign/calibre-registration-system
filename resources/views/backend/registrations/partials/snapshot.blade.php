<p>
    <strong>ID:</strong> {{ $registration->id }}<br />
    <strong>UID:</strong> {{ $registration->uid }}<br />
    <strong>Agency:</strong> {{ $registration->agency }}<br />
    <strong>Contact:</strong> {{ $registration->contact }}<br />
    <strong>Email:</strong> <a href="mailto:{{ $registration->email }}">{{ $registration->email }}</a><br />
    <strong>Department Size:</strong> {{ $registration->departmentSize->display() }}<br />
    <strong>Phone:</strong> <a href="tel:+1{{ $registration->displayTel('phone') }}">{{ $registration->displayPhone('phone') }}</a><br />
    <strong>Fax:</strong> {{ $registration->fax }}<br />
    <strong>Address:</strong> {{ $registration->address->display() }}<br />
    <strong>Passcode:</strong> {{ $registration->passcode }}<br />
    <strong>Payment Status:</strong> {{ $registration->payment_status }}<br />
    <strong>Created:</strong> {{ $registration->created_at->format('g:iA n/j/Y') }}<br />
</p>