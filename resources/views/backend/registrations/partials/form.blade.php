<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('agency', 'Agency *') !!}
        {!! Form::text('agency', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('contact', 'Contact *') !!}
        {!! Form::text('contact', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('email', 'Email *') !!}
        {!! Form::text('email', null, ['class'=>'email-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('department_size_id', 'Department Size *') !!}
        {!! Form::select('department_size_id', $department_size_options, null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('phone', 'Phone *') !!}
        {!! Form::text('phone', null, ['class'=>'phone-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('fax', 'Fax *') !!}
        {!! Form::text('fax', null, ['class'=>'phone-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('payment_status', 'Payment Status') !!}
        {!! Form::select('payment_status', $payment_status_options, null, ['class'=>'form-control']) !!}
    </div>
</div>