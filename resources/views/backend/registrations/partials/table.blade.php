<?php $table_id = isset($table_id) ? $table_id : 'registration_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Event</th>
        <th>Agency</th>
        <th>Contact</th>
        <th>Email</th>
        <th>Dept Size</th>
        <th>Phone</th>
        <th>Fax</th>
        <th>Address</th>
        <th>Passcode</th>
        <th>Total</th>
        <th>Paid?</th>
        <th>Created</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($registrations as $registration)
        <tr>
            <td>{!! link_to_route('backend.registrations.show', $registration->id, [$registration->id]) !!}</td>
            <td>{!! link_to_route('backend.registrations.show', $registration->uid, [$registration->id]) !!}</td>
            <td>{!! link_to_route('backend.events.show', $registration->event->display(), [$registration->event_id]) !!}</td>
            <td>{{ $registration->agency }}</td>
            <td>{{ $registration->contact }}</td>
            <td><a href="mailto:{{ $registration->email }}">{{ $registration->email }}</a></td>
            <td>{{ $registration->department_size }}</td>
            <td><a href="tel:+1{{ $registration->displayTel('phone') }}">{{ $registration->displayPhone('phone') }}</a></td>
            <td>{{ $registration->displayPhone('fax') }}</td>
            <td>{{ $registration->address->display() }}</td>
            <td>{{ $registration->passcode }}</td>
            <td class="align-right">${{ number_format($registration->totalPrice, 2) }}</td>
            <td>{{ $registration->payment_status }}</td>
            <td>{!! Helper::displayCalendarDate($registration->created) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.registrations.show', 'fa-eye', [$registration->id], ['title' => 'View Registration']) !!}
                {!! Helper::icon_to_route('backend.registrations.edit.get', 'fa-pencil', [$registration->id], ['title' => 'Edit Registration']) !!}
                {!! Helper::icon_to_route('backend.registrations.destroy', 'fa-ban', [$registration->id], ['title' => 'Delete Registration']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1,'Email' => 1,'Phone' => 1,'Fax' => 1,'Passcode' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, paging: false});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>