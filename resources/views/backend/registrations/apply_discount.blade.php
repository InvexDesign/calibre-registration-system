@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Apply Discount')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'             => ['backend.registrations.index'],
        $registration->display()    => ['backend.registrations.show', $registration->id],
        'Apply Discount'              => null
    ]); !!}
@stop

@section('content')

    <h4>Registration</h4>
    <table>
        <tr>
            <td><strong>Total Cost:</strong></td>
            <td class="align-right">${{ number_format($registration->getTotalCostInDollars(), 2) }}</td>
        </tr>
        <tr>
            <td><strong>Total Paid:</strong></td>
            <td class="align-right">${{ number_format($registration->getTotalPaidInDollars(), 2) }}</td>
        </tr>
        <tr>
            <td><strong>Balance:</strong></td>
            <td class="align-right">${{ number_format($registration->getBalanceInDollars(), 2) }}</td>
        </tr>
    </table>

    <h4>Selected Discount</h4>
    <table>
        <tr>
            <td><strong>ID:</strong></td>
            <td class="align-right"><span id="selected_discount_id"></span></td>
        </tr>
        <tr>
            <td><strong>Name:</strong></td>
            <td class="align-right"><span id="selected_discount_name"></span></td>
        </tr>
        <tr>
            <td><strong>Code:</strong></td>
            <td class="align-right"><span id="selected_discount_code"></span></td>
        </tr>
        <tr>
            <td><strong>Value:</strong></td>
            <td class="align-right"><span id="selected_discount_value"></span></td>
        </tr>
        <tr>
            <td><strong>Effect:</strong></td>
            <td class="align-right"><span id="selected_discount_effect"></span></td>
        </tr>
    </table>

    {!! Form::open(['route' => ['backend.registrations.apply_discount.post', $registration->id]]) !!}

        <div class="field-container col-2">
            <div class="field form-group">
                {!! Form::label('discount_id', 'Discount *') !!}
                {!! Form::select('discount_id', $discount_options, null, ['class'=>'form-control']) !!}
            </div>
            <div class="field form-group">
                {!! Form::label('note', 'Note') !!}
                {!! Form::text('note', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Apply Discount', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

    <script>
        function updateSelectedDiscountDisplay()
        {
            var discount_id = $('#discount_id').val();
            var discount = discounts[discount_id];
            if(typeof discount !== 'undefined')
            {
                $('#selected_discount_id').html(discount.id);
                $('#selected_discount_name').html(discount.name);
                $('#selected_discount_code').html(discount.code);
                $('#selected_discount_value').html(discount._value);
                $('#selected_discount_effect').html(discount.effect);
            }
        }

        $(document).ready(function()
        {
            updateSelectedDiscountDisplay();
            $('#discount_id').change(function()
            {
                updateSelectedDiscountDisplay();
            });
        });
    </script>
@stop