@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Registration')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Registrations' => ['backend.registrations.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.registrations.create.post']) !!}

        @include('backend.registrations.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Registration', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop