@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Registration')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'            => ['backend.registrations.index'],
        $registration->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#registration">Registration</a></li>
        <li><a data-toggle="tab" href="#tickets">Tickets</a></li>
        <li><a data-toggle="tab" href="#invoice">Invoice</a></li>
        <li><a data-toggle="tab" href="#payments">Payments</a></li>
        <li><a data-toggle="tab" href="#event">Event</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
        <li><a data-toggle="tab" href="#audits">Audits</a></li>
    </ul>

    <div class="tab-content">
        <div id="registration" class="tab-pane fade in active">
            {!! link_to_route('backend.registrations.edit.get', 'Edit Registration', ['id' => $registration->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.registrations.change_address.get', 'Change Address', ['id' => $registration->id], ['class' => 'btn btn-warning']) !!}
            {!! link_to_route('backend.registrations.destroy', 'Delete Registration', ['id' => $registration->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.registrations.partials.snapshot')
        </div>
        <div id="tickets" class="tab-pane fade">
            {!! link_to_route('backend.registrations.add_tickets.get', 'Add Tickets', ['registration_id' => $registration->id], ['class' => 'btn btn-primary']) !!}
            @include('backend.tickets.partials.table', ['tickets' => $registration->tickets])
        </div>
        <div id="invoice" class="tab-pane fade">
            {!! link_to_route('backend.registrations.invoice', 'View Invoice', ['id' => $registration->id], ['target' => '_blank', 'class' => 'btn btn-success']) !!}
            @include('frontend.registrations.invoice')
        </div>
        <div id="payments" class="tab-pane fade">
            Balance: ${{ number_format($registration->getBalanceInDollars(), 2) }}<br /><br />
            {!! link_to_route('frontend.workflows.payment.get', 'Make Credit Card Payment', ['uid' => $registration->uid, 'passcode' => $registration->passcode], ['target' => '_blank', 'class' => 'btn btn-success']) !!}
            {!! link_to_route('backend.registrations.apply_check_payment.get', 'Apply Check Payment', ['id' => $registration->id], ['class' => 'btn btn-success']) !!}
            {!! link_to_route('backend.registrations.apply_discount.get', 'Apply Discount', ['id' => $registration->id], ['class' => 'btn btn-warning']) !!}
            {!! link_to_route('backend.registrations.apply_adjustment.get', 'Apply Adjustment', ['id' => $registration->id], ['class' => 'btn btn-warning']) !!}
            @include('backend.payments.partials.table', ['payments' => $registration->payments, 'hidden_columns' => ['Registration' => 1]])
        </div>
        <div id="event" class="tab-pane fade">
            {!! link_to_route('backend.events.show', 'View Event', ['id' => $registration->event_id], ['target' => '_blank', 'class' => 'btn btn-success']) !!}
            @include('backend.events.partials.snapshot', ['event' => $registration->event])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $registration])
        </div>
        <div id="audits" class="tab-pane fade">
            {!! link_to_route('backend.registrations.audits', 'View All Audits', ['id' => $registration->id], ['target' => '_blank', 'class' => 'btn btn-success']) !!}
            @include('backend.audits.partials.table', ['audits' => $audits])
        </div>
    </div>
@stop