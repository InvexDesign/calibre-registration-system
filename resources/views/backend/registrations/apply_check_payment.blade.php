@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Apply Check Payment')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'             => ['backend.registrations.index'],
        $registration->display()    => ['backend.registrations.show', $registration->id],
        'Apply Check Payment'       => null
    ]); !!}
@stop

@section('content')

    <table style="width: 150px;">
        <tbody>
        <tr>
            <td>Cost:</td>
            <td class="align-right">${{ number_format($registration->getTotalCostInDollars(), 2) }}</td>
        </tr>
        <tr>
            <td>Paid:</td>
            <td class="align-right">${{ number_format($registration->getTotalPaidInDollars(), 2) }}</td>
        </tr>
        </tbody>
        <tfoot>
        <tr style="border-top: 2px solid black;">
            <td>Balance:</td>
            <td class="align-right">${{ number_format($registration->getBalanceInDollars(), 2) }}</td>
        </tr>
        </tfoot>
    </table>

    {!! Form::open(['route' => ['backend.registrations.apply_check_payment.post', $registration->id]]) !!}
        <div class="field-container col-2">
            <div class="field form-group">
                {!! Form::label('eid', 'EID') !!}
                {!! Form::text('eid', null, ['class'=>'form-control']) !!}
                <p class="instruction">External ID: possibly check number ie #1352</p>
            </div>
            <div class="field form-group">
                {!! Form::label('amount', 'Amount *') !!}
                {!! Form::text('amount', $registration->getBalanceInDollars(), ['class'=>'dollar-mask form-control']) !!}
            </div>
            <div class="field form-group">
                {!! Form::label('paid_at', 'Paid At') !!}
                {!! Form::text('paid_at', null, ['class'=>'datepicker form-control']) !!}
                <p class="instruction">If left blank, this will default to NOW.</p>
            </div>
            <div class="field form-group">
                {!! Form::label('note', 'Note') !!}
                {!! Form::text('note', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Apply Check Payment', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop