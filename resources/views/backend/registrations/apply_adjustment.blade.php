@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Apply Adjustment')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'             => ['backend.registrations.index'],
        $registration->display()    => ['backend.registrations.show', $registration->id],
        'Apply Adjustment'          => null
    ]); !!}
@stop

@section('content')

    {!! Form::open(['route' => ['backend.registrations.apply_adjustment.post', $registration->id]]) !!}

        <div class="field-container col-2">
            <div class="field form-group">
                {!! Form::label('type', 'Type *') !!}
                {!! Form::select('type', $type_options, null, ['class'=>'form-control']) !!}
            </div>
            <div class="field form-group">
                {!! Form::label('amount', 'Amount *') !!}
                {!! Form::text('amount', null, ['class'=>'dollar-mask form-control']) !!}
            </div>
            <div class="field form-group">
                {!! Form::label('note', 'Note') !!}
                {!! Form::text('note', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Apply Adjustment', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop