@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Registration')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'            => ['backend.registrations.index'],
        '#' . $registration->id    => ['backend.registrations.show', $registration->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($registration, ['route' => ['backend.registrations.edit.post', $registration->id]]) !!}

        @include('backend.registrations.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Registration', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop