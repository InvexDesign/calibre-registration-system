@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Change Registration Address')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Registrations'         => ['backend.registrations.index'],
        '#' . $registration->id => ['backend.registrations.show', $registration->id],
        'Change Address'        =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($registration, ['route' => ['backend.registrations.change_address.post', $registration->id]]) !!}

        @include('backend.addresses.partials.form', ['address' => $registration->address])

        <div class="form-group">
            {!! Form::submit('Update Address', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop