<?php $table_id = isset($table_id) ? $table_id : 'user_table'; ?>
<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Username</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Created</th>
    </tr>
    </thead>

    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! link_to_route('backend.users.show', $user->id, ['id' => $user->id]) !!}</td>
            <td>{!! link_to_route('backend.users.show', $user->username, ['id' => $user->id]) !!}</td>
            <td>{{ $user->first_last }}</td>
            <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
            <td>{{ $user->getRole() ? $user->getRole()->display_name : 'None' }}</td>
            <td>{!! Helper::displayCalendarDate($user->created_at) !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => []])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>