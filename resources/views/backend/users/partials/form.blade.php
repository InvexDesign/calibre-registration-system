<div class="field-container col-1">
    @if(!isset($allow_role_selection) || $allow_role_selection)
        <div class="field form-group">
            {!! Form::label('role_id', 'Role *') !!}
            {!! Form::select('role_id', $role_options, isset($default_role_id) ? $default_role_id : null, ['class'=>'form-control']) !!}
        </div>
    @endif
    <div class="field form-group">
        {!! Form::label('username', 'Username *') !!}
        {!! Form::text('username', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('email', 'Email *') !!}
        {!! Form::email('email', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('first_name', 'First Name') !!}
        {!! Form::text('first_name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('last_name', 'Last Name') !!}
        {!! Form::text('last_name', null, ['class'=>'form-control']) !!}
    </div>
</div>
