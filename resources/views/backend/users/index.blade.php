@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Users')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Users' => null,
    ]); !!}
@stop

@section('content')

    @if(\Auth::user()->isExecutive())
        {!! link_to_route('backend.users.create.get', 'Create User', null, ['class' => 'btn btn-primary']) !!}
        <br />
        <br />
    @endif

    @include('backend.users.partials.table')
@stop