@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Change Password')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'My Account'        => ['backend.users.show', \Auth::user()->id],
        'Change Password'   => null,
    ]); !!}
@stop

@section('content')

    {!! Form::open(['route' => 'backend.my_account.change_password.post']) !!}

        <h2>Change Password</h2>
        <div class="field-container col-1">
            @if($check_current_password)
                <div class="field form-group">
                    {!! Form::label('current_password', 'Current Password *') !!}
                    {!! Form::password('current_password', ['class'=>'form-control']) !!}
                </div>
            @endif
            <div class="field form-group">
                {!! Form::label('password', 'New Password *') !!}
                {!! Form::password('password', ['class'=>'form-control']) !!}
            </div>
            <div class="field form-group">
                {!! Form::label('password_confirmation', 'Password Confirmation *') !!}
                {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
            </div>

        {!! Form::submit('Update Password', ['class' => 'btn btn-primary']) !!}

        </div>
    {!! Form::close() !!}
@stop
