@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Profile')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Users'         => ['backend.users.index'],
        $user->username => ['backend.my_account.show'],
        'Edit'          => null,
    ]); !!}
@stop

@section('content')
    {!! Form::model($user, ['route' => ['backend.my_account.update']]) !!}

        @include('backend.users.partials.form')

        {!! Form::submit('Update User', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop


