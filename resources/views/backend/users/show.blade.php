@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'User #' . $user->id)

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Users' => ['backend.users.index'],
        $user->username => null
    ]); !!}
@stop

@section('content')
{{--    {!! link_to_route('backend.my_account.change_password.get', 'Change Password', [], ['class' => 'btn btn-primary']) !!}--}}
    {!! link_to_route('backend.users.edit.get', 'Edit User', ['id' => $user->id], ['class' => 'btn btn-primary']) !!}
    {!! link_to_route('backend.users.destroy', 'Delete User', ['id' => $user->id], ['class' => 'btn btn-danger']) !!}
    <br />
    <br />

    @include('backend.users.partials.snapshot')
@stop