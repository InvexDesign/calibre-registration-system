@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Event')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Events'            => ['backend.events.index'],
        $event->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#event">Event</a></li>
        <li><a data-toggle="tab" href="#registrations">Registrations</a></li>
        <li><a data-toggle="tab" href="#tickets">Tickets</a></li>
        <li><a data-toggle="tab" href="#lodgings">Lodgings</a></li>
        <li><a data-toggle="tab" href="#course">Course</a></li>
        <li><a data-toggle="tab" href="#venue">Venue</a></li>
        <li><a data-toggle="tab" href="#payments">Payments</a></li>
        <li><a data-toggle="tab" href="#analytics">Analytics</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="event" class="tab-pane fade in active">
            @if($event->settled_at)
                {!! link_to_route('backend.events.mark_unsettled', 'Mark Unsettled', ['id' => $event->id], ['class' => 'btn btn-warning']) !!}
            @else
                {!! link_to_route('backend.events.mark_settled', 'Mark Settled', ['id' => $event->id], ['class' => 'btn btn-warning']) !!}
            @endif
            {!! link_to_route('backend.events.edit.get', 'Edit Event', ['id' => $event->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.events.attendees', 'See Attendees', ['id' => $event->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.events.destroy', 'Delete Event', ['id' => $event->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.events.partials.snapshot')
        </div>
        <div id="registrations" class="tab-pane fade">
            @include('backend.registrations.partials.table', ['registrations' => $event->registrations])
        </div>
        <div id="tickets" class="tab-pane fade">
            @include('backend.tickets.partials.table', ['tickets' => $event->tickets])
        </div>
        <div id="lodgings" class="tab-pane fade">
            @include('backend.lodgings.partials.table', ['lodgings' => $event->lodgings])
        </div>
        <div id="course" class="tab-pane fade">
            {!! link_to_route('backend.courses.show', 'View Course', ['id' => $event->course_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.courses.partials.snapshot', ['course' => $event->course])
        </div>
        <div id="venue" class="tab-pane fade">
            {!! link_to_route('backend.venues.show', 'View Venue', ['id' => $event->venue_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.venues.partials.snapshot', ['venue' => $event->venue])
        </div>
        <div id="payments" class="tab-pane fade">
            @include('backend.payments.partials.table', ['payments' => $event->getPayments()])
        </div>
        <div id="analytics" class="tab-pane fade">
            @include('backend.events.partials.analytics')
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $event])
        </div>
    </div>
@stop