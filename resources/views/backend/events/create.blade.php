@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Event')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Events' => ['backend.events.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.events.create.post']) !!}

        @include('backend.events.partials.form')

        <div class="form-group">
            <input type="submit" name="submit_draft" value="Save Draft" class="btn btn-warning">
            <input type="submit" name="submit_publish" value="Publish" class="btn btn-primary">
        </div>

    {!! Form::close() !!}
@stop