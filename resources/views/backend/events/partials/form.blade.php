<div class="field-container col-3">
    <div class="field full-width form-group">
        {!! Form::label('course_id', 'Course *') !!}
        {!! Form::select('course_id', $course_options, isset($initial_course) ? $initial_course : null, ['class'=>'select-two form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('venue_id', 'Venue *') !!}
        {!! Form::select('venue_id', $venue_options, isset($initial_venue_id) ? $initial_venue_id : null, ['data-featherlight' => '#venue_search_container', 'class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('sales_representative_id', 'Sales Representative') !!}
        {!! Form::select('sales_representative_id', $sales_representative_options, isset($initial_sales_representative_id) ? $initial_sales_representative_id : null, ['class'=>'select-two form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('start_at', 'Start Date *', ['class'=>'control-label']) !!}
        {!! Form::text('start_at', $start_at_default, ['class'=>'lockable form-control datepicker']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('end_at', 'End Date', ['class'=>'control-label']) !!}
        {!! Form::text('end_at', $end_at_default, ['class'=>'lockable form-control datepicker']) !!}
    </div>
    {{--<div class="field form-group">--}}
{{--        {!! Form::label('timezone', 'Timezone') !!}--}}
{{--        {!! Form::select('timezone', $timezone_options, isset($initial_timezone) ? $initial_course : null, ['class'=>'select-two form-control']) !!}--}}
    {{--</div>--}}
    <div class="field full-width form-group">
        {!! Form::label('times', 'Times *') !!}
        {!! Form::textarea('times', null, ['placeholder' => 'For example...&#10;7/9: 5PM - 9PM&#10;7/10: 8AM - 5PM', 'class'=>'form-control']) !!}
        <p class="instruction">Use this section to specify event times corresponding to the days.</p>
    </div>
    <div class="field form-group">
        {!! Form::label('is_private', 'Private?') !!}
        {!! Form::select('is_private', [0 => 'Public', 1 => 'Private'], null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('deadline_at', 'Deadline Date', ['class'=>'control-label']) !!}
        {!! Form::text('deadline_at', $deadline_at_default, ['class'=>'form-control datepicker']) !!}
        <p class="instruction">Deadlines currently occur at {{ Setting::get('event-deadline-time') }}.</p>
    </div>
    <div class="field form-group">
        {!! Form::label('ticket_limit', 'Ticket Limit', ['class'=>'control-label']) !!}
        {!! Form::text('ticket_limit', null, ['class'=>'form-control positive-integer-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('price', 'Price *') !!}
        {!! Form::text('price', isset($price_default) ? $price_default : null, ['class'=>'form-control dollar-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('revenue_goal', 'Revenue Goal') !!}
        {!! Form::text('revenue_goal', null, ['class'=>'form-control dollar-mask']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('lodging_ids[]', 'Lodgings') !!}
        {!! Form::select('lodging_ids[]', $lodging_options, isset($initial_lodging_ids) ? $initial_lodging_ids : null, ['multiple', 'class'=>'select-two form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        <p class="instruction">Use this section to specify any additional event details.</p>
    </div>
</div>

@include('backend.venues.partials.search')


<script>
    $(document).ready(function()
    {
        @if(isset($event) && $event->isPublished())
        $('.lockable').attr('disabled', true);
        @endif

        $('#start_at, #end_at').change(function()
        {
            var start = $('#start_at').val();
            if(start)
            {
                var times = '';
                var start_at = moment(start, 'MM/DD/YYYY', true);

                var end = $('#end_at').val();
                if(!end)
                {
                    times = start_at.format('M/D') + ': 8AM - 5PM';
                }
                else
                {
                    var current = start_at.clone();
                    var end_at = moment(end, 'MM/DD/YYYY', true);
                    while(current <= end_at)
                    {
                        times += current.format('M/D') + ': 8AM - 5PM\n';
                        current.add({days: 1});
                    }
                }

                $('#times').val(times);
            }
        });
    });
</script>
