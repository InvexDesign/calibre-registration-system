@foreach($events as $event)
    <tr>
        <td>{!! link_to_route('backend.events.show', $event->id, [$event->id]) !!}</td>
        <td>{!! link_to_route('backend.events.show', $event->uid, [$event->id]) !!}</td>
        <td>{!! link_to_route('backend.venues.show', $event->venue->display(), [$event->venue_id]) !!}</td>
        <td>{!! link_to_route('backend.courses.show', $event->course->display(), [$event->course_id]) !!}</td>
        <td>{{ $event->timezone }}</td>
        <td>{!! Helper::displayCalendarDate($event->start_at) !!}</td>
        <td>{!! Helper::displayCalendarDate($event->end_at) !!}</td>
        <td>{!! Helper::displayCalendarDate($event->deadline_at) !!}</td>
        <td class="align-right">{{ number_format($event->ticket_limit, 0) }}</td>
        <td class="align-right">${{ number_format($event->price_in_dollars, 2) }}</td>
        <td class="align-right">${{ number_format($event->revenue_goal, 2) }}</td>
        <td>{{ $event->published_status }}</td>
        <td>{{ $event->is_finalized ? 'Yes' : 'No' }}</td>
        <td>{{ $event->notes }}</td>
        <td class="icon-td">
            {!! Helper::icon_to_route('backend.events.show', 'fa-eye', [$event->id], ['title' => 'View Event']) !!}
            {!! Helper::icon_to_route('backend.events.edit.get', 'fa-pencil', [$event->id], ['title' => 'Edit Event']) !!}
            {!! Helper::icon_to_route('backend.events.destroy', 'fa-ban', [$event->id], ['title' => 'Delete Event']) !!}
        </td>
    </tr>
@endforeach