<strong>Tickets:</strong> {{ number_format($event->tickets()->count(), 0) }}<br />
<strong>Registrations:</strong> {{ number_format($event->registrations()->count(), 0) }}<br />
<strong>Payments:</strong> {{ number_format($event->getPaymentsQuery()->count(), 0) }}<br />
<strong>Balance:</strong> ${{ number_format($event->getPaymentsQuery()->sum('amount_in_cents') / 100, 2) }} / ${{ number_format($event->tickets()->sum('price_in_cents') / 100, 2) }}<br />
@if($event->tickets()->count() > 0)
    <strong>Percent Paid:</strong> {{ number_format(($event->getPaymentsQuery()->sum('amount_in_cents') / $event->tickets()->sum('price_in_cents')) * 100, 2) }}%<br />
@else
    <strong>Percent Paid:</strong> 0%<br />
@endif
@if($event->revenue_goal)
    <strong>Revenue Goal:</strong> ${{ number_format($event->tickets()->sum('price_in_cents') / 100, 2) }} / ${{ number_format($event->revenue_goal / 100, 2) }} ({{ number_format( ($event->tickets()->sum('price_in_cents') / $event->revenue_goal) * 100, 2) }}% Fulfilled)<br />
@else
    <strong>Revenue Goal:</strong> N/A<br />
@endif
<strong>Credit Payments:</strong> ${{ number_format($event->getPaymentsQuery()->creditCardPayments()->sum('amount_in_cents') / 100, 2) }} ({{ number_format(($event->getPaymentsQuery()->creditCardPayments()->count()), 0) }} Payments)<br />
<strong>Check Payments:</strong> ${{ number_format($event->getPaymentsQuery()->checkPayments()->sum('amount_in_cents') / 100, 2) }} ({{ number_format(($event->getPaymentsQuery()->checkPayments()->count()), 0) }} Payments)<br />
<strong>Discount Payments:</strong> ${{ number_format($event->getPaymentsQuery()->discountPayments()->sum('amount_in_cents') / 100, 2) }} ({{ number_format(($event->getPaymentsQuery()->discountPayments()->count()), 0) }} Payments)<br />

<style>
    .chart-container {
        display: flex;
        flex-wrap: wrap;
    }
    .chart-container > canvas {
        width: 50%;
    }
</style>
<div class="chart-container">
    <canvas id="payments_chart" width="400" height="225"></canvas>
    @if($event->revenue_goal)
        <canvas id="goal_chart" width="400" height="225"></canvas>
    @endif
    <canvas id="timeline_chart" width="400" height="225"></canvas>
</div>

<script>
    $(document).ready(function()
    {
        var payments_chart = new Chart(document.getElementById("payments_chart"), {
            type: 'pie',
            data: {
                labels: ["Credit", "Check", "Discount"],
                datasets: [
                    {
                        label: 'Dollars',
                        backgroundColor: ['green', 'blue', 'red'],
                        data: [
                            '{{ number_format($event->getPaymentsQuery()->creditCardPayments()->sum('amount_in_cents') / 100, 2) }}',
                            '{{ number_format($event->getPaymentsQuery()->checkPayments()->sum('amount_in_cents') / 100, 2) }}',
                            '{{ number_format($event->getPaymentsQuery()->discountPayments()->sum('amount_in_cents') / 100, 2) }}',
                        ]
                    },
                    {
                        label: 'Count',
                        backgroundColor: ['green', 'blue', 'red'],
                        data: [
                            '{{ number_format($event->getPaymentsQuery()->creditCardPayments()->count(), 0) }}',
                            '{{ number_format($event->getPaymentsQuery()->checkPayments()->count(), 0) }}',
                            '{{ number_format($event->getPaymentsQuery()->discountPayments()->count(), 0) }}',
                        ]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Payments'
                }
            }
        });
        @if($event->revenue_goal)
        var goal_chart = new Chart(document.getElementById("goal_chart"), {
            type: 'pie',
            data: {
                labels: ["Paid", "Promised", "Goal"],
                datasets: [
                    {
                        label: 'Amount',
                        backgroundColor: ['green', 'blue', 'red'],
                        data: [
                            '{{ $event->getPaymentsQuery()->sum('amount_in_cents') / 100 }}',
                            '{{ ($event->tickets()->sum('price_in_cents') - $event->getPaymentsQuery()->sum('amount_in_cents')) / 100 }}',
                            '{{ ($event->revenue_goal - $event->tickets()->sum('price_in_cents')) / 100 }}'
                        ]
                    },
                    {
                        label: 'Percent',
                        backgroundColor: ['green', 'blue', 'red'],
                        data: [
                            '{{ $event->getPaymentsQuery()->sum('amount_in_cents') / $event->revenue_goal }}',
                            '{{ ($event->tickets()->sum('price_in_cents') - $event->getPaymentsQuery()->sum('amount_in_cents')) / $event->revenue_goal }}',
                            '{{ ($event->revenue_goal - $event->tickets()->sum('price_in_cents')) / $event->revenue_goal }}'
                        ]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Revenue Goal'
                }
            }
        });
        @endif
        var timeline_chart = new Chart(document.getElementById("timeline_chart"), {
            type: 'line',
            data: {
                fill: false,
                labels: ['{!! implode("','", $timeline_chart_labels) !!}'],
                datasets: [
                    {
                        label: 'Registrations',
                        fill: false,
                        borderColor: ['blue'],
                        data: [{{ implode(',', $timeline_chart_data['registrations']) }}]
                    },
                    {
                        label: 'Payments',
                        fill: false,
                        borderColor: ['green'],
                        data: [{{ implode(',', $timeline_chart_data['payments']) }}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Registrations'
                }
            }
        });
    });
</script>