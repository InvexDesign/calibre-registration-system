<p>
    <strong>ID:</strong> {{ $event->id }}<br />
    <strong>UID:</strong> {{ $event->uid }}<br />
    <strong>Name:</strong> {{ $event->name }}<br />
    <strong>Tagline:</strong> {{ $event->tagline }}<br />
    <strong>Description:</strong> {{ $event->description }}<br />
</p>