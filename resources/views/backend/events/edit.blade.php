@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Event')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Events'            => ['backend.events.index'],
        '#' . $event->id    => ['backend.events.show', $event->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    @if($event->isPublished())
        <h4>Warning: This event has already been published!</h4>
    @endif

    {!! Form::model($event, ['route' => ['backend.events.edit.post', $event->id]]) !!}

        @include('backend.events.partials.form')

        @if($event->isPublished())
            <div class="form-group">
                <input type="submit" name="submit_draft" value="Update" class="btn btn-primary">
            </div>
        @else
            <div class="form-group">
                <input type="submit" name="submit_draft" value="Save Draft" class="btn btn-warning">
                <input type="submit" name="submit_publish" value="Publish" class="btn btn-primary">
            </div>
        @endif

    {!! Form::close() !!}

@stop