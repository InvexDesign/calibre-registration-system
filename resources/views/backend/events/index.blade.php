@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Events')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Events' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.events.create.get', 'Create Event', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend._includes.date_range_form')

    @include('backend.events.partials.table')

@stop