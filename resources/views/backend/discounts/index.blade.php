@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Discounts')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Discounts' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.discounts.create.get', 'Create Discount', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.discounts.partials.table')

@stop