@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Discount')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Discounts' => ['backend.discounts.index'],
            'Create'    =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.discounts.create.post']) !!}

        @include('backend.discounts.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Discount', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop