@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Discount')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Discounts'            => ['backend.discounts.index'],
        '#' . $discount->id    => ['backend.discounts.show', $discount->id],
        'Edit'                 =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($discount, ['route' => ['backend.discounts.edit.post', $discount->id]]) !!}

        @include('backend.discounts.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Discount', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop