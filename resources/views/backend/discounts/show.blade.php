@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Discount')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Discounts'            => ['backend.discounts.index'],
        $discount->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#discount">Discount</a></li>
        <li><a data-toggle="tab" href="#payments">Payments</a></li>
        <li><a data-toggle="tab" href="#analytics">Analytics</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="discount" class="tab-pane fade in active">
            {!! link_to_route('backend.discounts.edit.get', 'Edit Discount', ['id' => $discount->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.discounts.destroy', 'Delete Discount', ['id' => $discount->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.discounts.partials.snapshot')
        </div>
        <div id="payments" class="tab-pane fade">
            @include('backend.payments.partials.table', ['payments' => $discount->payments])
        </div>
        <div id="analytics" class="tab-pane fade">
            @include('backend.discounts.partials.analytics', ['discount' => $discount])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $discount])
        </div>
    </div>
@stop