<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('code', 'Code *') !!}
        {!! Form::text('code', null, ['class'=>'form-control']) !!}
        <button id="generate_code_button" class="btn btn-danger" type="button">Generate Code</button>
    </div>
</div>
{!! Form::label('type', 'Discount Type *') !!}<br />
<div class="field-container col-2" style="justify-content: space-around;">
    <label class="radio-label">
        @if($type == 'dollar')
            {!! Form::radio('type', 'percent', null, ['id' => 'percent_discount_radio', 'disabled' => true, 'class' => 'form-control']) !!}
        @else
            {!! Form::radio('type', 'percent', null, ['id' => 'percent_discount_radio', 'class' => 'form-control']) !!}
        @endif
        <span>Percent</span>
    </label>
    <label class="radio-label">
        @if($type == 'percent')
            {!! Form::radio('type', 'dollar', null, ['id' => 'dollar_discount_radio', 'disabled' => true, 'class'=>'form-control']) !!}
        @else
            {!! Form::radio('type', 'dollar', null, ['id' => 'dollar_discount_radio', 'class'=>'form-control']) !!}
        @endif
        <span>Dollar</span>
    </label>
</div>
<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('value', 'Percent Discount *') !!}
        {!! Form::text('value', ($type == 'percent') ? $discount->value : '', ['class'=>'percent-mask form-control', 'id' => 'percent_discount']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('value', 'Dollar Discount *') !!}
        {!! Form::text('value', ($type == 'dollar') ? $discount->value / 100 : '', ['class'=>'dollar-mask form-control', 'id' => 'dollar_discount']) !!}
    </div>
</div>
<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('start_at', 'Start') !!}
        {!! Form::text('start_at', isset($discount) && $discount->start_at ? $discount->start_at->format('m/d/Y') : null, ['class'=>'datepicker form-control']) !!}
        <p class="instruction">If set, discount use will be disabled before this date.</p>
    </div>
    <div class="field form-group">
        {!! Form::label('end_at', 'End') !!}
        {!! Form::text('end_at', isset($discount) && $discount->end_at ? $discount->end_at->format('m/d/Y') : null, ['class'=>'datepicker form-control']) !!}
        <p class="instruction">If set, discount use will be disabled after this date.</p>
    </div>
    <div class="field form-group">
        {!! Form::label('is_active', 'Active?') !!}
        {!! Form::select('is_active', [0 => 'Inactive', 1 => 'Active'], null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('max_uses', 'Max Uses') !!}
        {!! Form::text('max_uses', null, ['class'=>'integer-mask form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('auto_attendee_min', 'Auto Attendee Min') !!}
        {!! Form::text('auto_attendee_min', null, ['class'=>'integer-mask form-control']) !!}
        <p class="instruction">If set, this discount will automatically apply to any registration with attendee count larger than the specified value.</p>
    </div>
</div>
<script>
    function randomString(length) {
        var charSet = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789';
        var randomString = '';
        for (var i = 0; i < length; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz,randomPoz+1);
        }
        return randomString;
    }

    $(document).ready(function()
    {
        $('#percent_discount_radio').click(function()
        {
            $('#percent_discount').prop('disabled', false);
            $('#dollar_discount').prop('disabled', true);
        });
        $('#dollar_discount_radio').click(function()
        {
            $('#dollar_discount').prop('disabled', false);
            $('#percent_discount').prop('disabled', true);
        });

        $('#generate_code_button').click(function()
        {
            var code = randomString(16);
            $('#code').val(code);
        });

        @if(isset($discount) && $discount->class == 'PercentDiscount')
            $('#percent_discount_radio').trigger('click');
        @elseif(isset($discount) && $discount->class == 'DollarDiscount')
            $('#dollar_discount_radio').trigger('click');
        @else
            $('#percent_discount_radio').trigger('click');
        @endif
    });
</script>
<style>
    input:focus {
        outline: none !important;
    }
</style>