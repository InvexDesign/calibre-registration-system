<strong>Uses:</strong> {{ number_format($discount->payments()->count(), 0) }}<br />
<strong>Amount:</strong> ${{ number_format($discount->payments()->sum('amount_in_cents') / 100, 2) }}<br />