<p>
    <strong>ID:</strong> {{ $discount->id }}<br />
    <strong>Type:</strong> {{ $discount->class }}<br />
    <strong>Name:</strong> {{ $discount->name }}<br />
    <strong>Code:</strong> {{ $discount->code }}<br />
    <strong>Discount:</strong> {{ $discount->displayDiscount() }}<br />
    <strong>Start:</strong> {{ $discount->start_at ? $discount->start_at->format('n/j/Y') : 'N/A' }}<br />
    <strong>End:</strong> {{ $discount->end_at ? $discount->end_at->format('n/j/Y') : 'N/A' }}<br />
    <strong>Active?</strong> {{ $discount->is_active ? 'Yes' : 'No' }}<br />
    <strong>Max Uses:</strong> {{ number_format($discount->max_uses, 0) }}<br />
    <strong>Auto Attendee Min:</strong> {{ number_format($discount->auto_attendee_min, 0) }}<br />
</p>