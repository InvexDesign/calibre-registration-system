<?php $table_id = isset($table_id) ? $table_id : 'discount_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Code</th>
        <th>Discount</th>
        <th>Start</th>
        <th>End</th>
        <th>Active?</th>
        <th>Max Uses</th>
        <th>Attendee Min</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($discounts as $discount)
        <tr>
            <td>{!! link_to_route('backend.discounts.show', $discount->id, [$discount->id]) !!}</td>
            <td>{!! link_to_route('backend.discounts.show', $discount->name, [$discount->id]) !!}</td>
            <td>{!! link_to_route('backend.discounts.show', $discount->code, [$discount->id]) !!}</td>
            <td class="align-right">{{ $discount->displayDiscount() }}</td>
            <td>{!! Helper::displayCalendarDate($discount->start_at) !!}</td>
            <td>{!! Helper::displayCalendarDate($discount->end_at) !!}</td>
            <td>{{ $discount->is_active ? 'Y' : 'N' }}</td>
            <td>{{ number_format($discount->max_uses, 0) }}</td>
            <td>{{ number_format($discount->auto_attendee_min, 0) }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.discounts.show', 'fa-eye', [$discount->id], ['title' => 'View Discount']) !!}
                {!! Helper::icon_to_route('backend.discounts.edit.get', 'fa-pencil', [$discount->id], ['title' => 'Edit Discount']) !!}
                {!! Helper::icon_to_route('backend.discounts.destroy', 'fa-ban', [$discount->id], ['title' => 'Delete Discount']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['Attendee Min' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>