@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Account')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'My Account'    => ['backend.my_account.show'],
        'Edit'          => null,
    ]); !!}
@stop

@section('content')
    {!! Form::model($user, ['route' => 'backend.my_account.edit.post']) !!}

        @include('backend.users.partials.form', ['allow_role_selection' => false])

        {!! Form::submit('Update Account', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@stop


