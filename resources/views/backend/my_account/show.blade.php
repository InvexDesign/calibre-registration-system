@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'My Account')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'My Account' => null
    ]); !!}
@stop

@section('content')
    {!! link_to_route('backend.my_account.change_password.get', 'Change Password', [], ['class' => 'btn btn-primary']) !!}
    {!! link_to_route('backend.my_account.edit.get', 'Edit Account', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.users.partials.snapshot')
@stop