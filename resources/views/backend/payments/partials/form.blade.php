<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('tagline', 'Tagline') !!}
        {!! Form::text('tagline', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
    </div>
</div>