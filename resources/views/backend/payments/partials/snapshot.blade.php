<p>
    <strong>ID:</strong> {{ $payment->id }}<br />
    <strong>UID:</strong> {{ $payment->uid }}<br />
    <strong>Name:</strong> {{ $payment->name }}<br />
    <strong>Tagline:</strong> {{ $payment->tagline }}<br />
    <strong>Description:</strong> {{ $payment->description }}<br />
</p>