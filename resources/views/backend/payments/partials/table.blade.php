<?php $table_id = isset($table_id) ? $table_id : 'payment_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Type</th>
        <th>EID</th>
        <th>Registration</th>
        <th>Amount</th>
        <th>Last Note</th>
        <th>Paid</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($payments as $payment)
        <tr>
            <td>{!! link_to_route('backend.payments.show', $payment->id, [$payment->id]) !!}</td>
            <td>{!! link_to_route('backend.payments.show', $payment->uid, [$payment->id]) !!}</td>
            <td>{{ $payment->class }}</td>
            <td>{!! $payment->getEidLink() !!}</td>
            <td>{!! link_to_route('backend.registrations.show', $payment->registration->display(), [$payment->registration_id]) !!}</td>
            @if($payment->amount_in_dollars < 0)
                <td class="align-right debit">- ${{ number_format(abs($payment->amount_in_dollars), 2) }}</td>
            @else
                <td class="align-right credit">${{ number_format($payment->amount_in_dollars, 2) }}</td>
            @endif
            <td>{{ ($note = $payment->getLatestNote()) ? $note->content : '' }}</td>
            <td>{!! Helper::displayCalendarDate($payment->paid_at) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.payments.show', 'fa-eye', [$payment->id], ['title' => 'View Payment']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, paging: false});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>