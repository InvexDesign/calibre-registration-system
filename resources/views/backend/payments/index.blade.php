@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Payments')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Payments' => null,
    ]); !!}
@stop

@section('content')

    <style>
        .dataTables_info {
            display: none;
        }
    </style>

    @include('backend._includes.date_range_form')

	<?php $date_range_parameters = ['start' => $start_at->format('m-d-Y'), 'end' => $end_at->format('m-d-Y')]; ?>

	<?php $_index = ($payments->currentPage() - 1) * $payments->perPage(); ?>
    <div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 10px;">
        <p style="margin: 0;">{{ 'Showing ' . ($_index + 1) . ' to ' . ($_index + $payments->count()) . ' of ' . $payments->total() . ' entries'}}</p>
        {{ $payments->appends($date_range_parameters)->links() }}
    </div>

    @include('backend.payments.partials.table')

    {{ $payments->appends($date_range_parameters)->links() }}

@stop