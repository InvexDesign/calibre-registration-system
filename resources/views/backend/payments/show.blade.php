@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Payment')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Payments'            => ['backend.payments.index'],
        $payment->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#payment">Payment</a></li>
        <li><a data-toggle="tab" href="#event">Event</a></li>
        <li><a data-toggle="tab" href="#registration">Registration</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="payment" class="tab-pane fade in active">
            @include('backend.payments.partials.snapshot')
        </div>
        <div id="registration" class="tab-pane fade">
            {!! link_to_route('backend.registrations.show', 'View Registration', ['id' => $payment->registration_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.registrations.partials.snapshot', ['registration' => $payment->registration])
        </div>
        <div id="event" class="tab-pane fade">
            {!! link_to_route('backend.events.show', 'View Event', ['id' => $payment->registration->event_id], ['class' => 'btn btn-primary']) !!}
            @include('backend.events.partials.snapshot', ['event' => $payment->registration->event])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $payment])
        </div>
    </div>
@stop