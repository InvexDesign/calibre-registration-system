<p>
    <strong>ID:</strong> {{ $venue->id }}<br />
    <strong>UID:</strong> {{ $venue->uid }}<br />
    <strong>Name:</strong> {{ $venue->name }}<br />
    <strong>Phone:</strong> <a href="tel:+1{{ $venue->displayTel('phone') }}">{{ $venue->displayPhone('phone') }}</a><br />
    <strong>Email:</strong> <a href="mailto:{{ $venue->email }}">{{ $venue->email }}</a><br />
    <strong>Address:</strong> {{ $venue->address->display() }}<br />
</p>