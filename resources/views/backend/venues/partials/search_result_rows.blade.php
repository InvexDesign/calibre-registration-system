@foreach($venues as $venue)
<tr>
    <td>{!! link_to_route('backend.venues.show', $venue->name, [$venue->id]) !!}</td>
    <td>{{ $venue->address->display() }}</td>
    <td><a href="tel:+1{{ $venue->displayTel('phone') }}">{{ $venue->displayPhone('phone') }}</a></td>
    <td><a href="mailto:{{ $venue->email }}">{{ $venue->email }}</a></td>
    <td>{!! Helper::displayCalendarDate($venue->created_at) !!}</td>
    <td><button onclick="selectVenue('{{ $venue->id }}', '{{ addslashes($venue->name_and_address) }}')" class="btn btn-primary">Select</button></td>
</tr>
@endforeach