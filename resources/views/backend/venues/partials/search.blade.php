<div id="venue_search_container" class="featherlight-hide">
    <h4>Search For A Venue</h4>
    <div id="venue_search_form" class="search-form field-container col-3">
        <div class="field form-group">
            {!! Form::label('search[venue][name]', 'Name') !!}
            {!! Form::text('search[venue][name]', null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label('search[venue][line1]', 'Address') !!}
            {!! Form::text('search[venue][line1]', null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label('search[venue][city]', 'City') !!}
            {!! Form::text('search[venue][city]', null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label('search[venue][state_id]', 'State') !!}
            {!! Form::select('search[venue][state_id]', ['' => 'Select A State'] + $state_options, null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label('search[venue][zipcode]', 'Zipcode') !!}
            {!! Form::text('search[venue][zipcode]', null, ['class'=>'form-control']) !!}
        </div>
    </div>
    <div id="venue_search_results" class="search-results">
        <table id="venue_search_results_table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Created</th>
                <th>Actions</th>
            </tr>
            </thead>

            <tbody>
            @foreach($default_venue_results as $venue)
                <tr>
                    <td>{!! link_to_route('backend.venues.show', $venue->name, [$venue->id]) !!}</td>
                    <td>{{ $venue->address->display() }}</td>
                    <td><a href="tel:+1{{ $venue->displayTel('phone') }}">{{ $venue->displayPhone('phone') }}</a></td>
                    <td><a href="mailto:{{ $venue->email }}">{{ $venue->email }}</a></td>
                    <td>{!! Helper::displayCalendarDate($venue->created_at) !!}</td>
                    <td><button onclick="selectVenue('{{ $venue->id }}', '{{ addslashes($venue->name_and_address) }}')" class="btn btn-primary">Select</button></td>
                </tr>
            @endforeach
            </tbody>

        </table>
    </div>
</div>

<style>
    .featherlight-hide {
        display: none;
    }
    .search-results table {
        width: 100%;
    }
</style>

<script>
//    var venue_search_lightbox;
    function selectVenue(id, display)
    {
        var option = $('<option></option>').attr("value", id).text(display);
        $('#venue_id').empty().append(option);
//        venue_search_lightbox.close();
        $.featherlight.current().close();
    }

    $(document).ready(function()
    {
        $('#venue_id').featherlight({targetAttr: 'data-featherlight'});

        $('#venue_search_form .form-control').change(function()
        {
            $.get('{{ route('backend.venues.search') }}', {
                name: $('.featherlight-content [name="search[venue][name]').val(),
                line1: $('.featherlight-content [name="search[venue][line1]').val(),
                city: $('.featherlight-content [name="search[venue][city]').val(),
                state_id: $('.featherlight-content [name="search[venue][state_id]').val(),
                zipcode: $('.featherlight-content [name="search[venue][zipcode]').val()
            }).done(function(data) {
                var rows = $.parseJSON(data);
                var tbody = $('#venue_search_results_table tbody');
                $('#venue_search_results_table tbody tr').remove();
                var tbody = $('#venue_search_results_table tbody').append(rows);

                /*
                for(var i = 0; i < rows.length; i++)
                {
                    var row = $('<tr></tr>')
                    var name = $('<td>' + rows[i]['name'] + '</td>');
                    var address = $('<td>' + rows[i]['address'] + '</td>');
                    var phone = $('<td>' + rows[i]['phone'] + '</td>');
                    var email = $('<td>' + rows[i]['email'] + '</td>');
                    var created = $('<td>' + rows[i]['created'] + '</td>');
//                    var actions = $('<button>Select</button>').click(function(){
//                        selectVenue(rows[i]['id'], rows[i]['name_and_address']);
//                    }).addClass('btn btn-primary');
                    var actions = $('<button>Select</button>').attr('onclick', 'selectVenue(' + rows[i]['id'] + ', "' + rows[i]['name_and_address'] + '")').addClass('btn btn-primary');

                    row.append(name).append(address).append(phone).append(email).append(created).append(actions);
                    tbody.append(row);
                }
                 */
            });
        });
    });
</script>