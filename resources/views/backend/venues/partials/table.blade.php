<?php $table_id = isset($table_id) ? $table_id : 'venue_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>Name</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Events</th>
        <th>Created</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($venues as $venue)
        <tr>
            <td>{!! link_to_route('backend.venues.show', $venue->id, [$venue->id]) !!}</td>
            <td>{!! link_to_route('backend.venues.show', $venue->uid, [$venue->id]) !!}</td>
            <td>{!! link_to_route('backend.venues.show', $venue->name, [$venue->id]) !!}</td>
            <td>{!! link_to_route('backend.addresses.show', $venue->address->display(), [$venue->address_id]) !!}</td>
            <td><a href="tel:+1{{ $venue->displayTel('phone') }}">{{ $venue->displayPhone('phone') }}</a></td>
            <td><a href="mailto:{{ $venue->email }}">{{ $venue->email }}</a></td>
            <td class="align-right">{{ number_format($venue->events()->count(), 0) }}</td>
            <td>{!! Helper::displayCalendarDate($venue->created_at) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.venues.show', 'fa-eye', [$venue->id], ['title' => 'View Attendee']) !!}
                {!! Helper::icon_to_route('backend.venues.edit.get', 'fa-pencil', [$venue->id], ['title' => 'Edit Attendee']) !!}
                {!! Helper::icon_to_route('backend.venues.destroy', 'fa-ban', [$venue->id], ['title' => 'Delete Attendee']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1,'UID' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, paging: false});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>