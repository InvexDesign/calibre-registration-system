<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('phone', 'Phone') !!}
        {!! Form::text('phone', null, ['class'=>'form-control phone-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
    </div>
</div>

@include('backend.addresses.partials.form')

<div class="field full-width form-group">
    {!! Form::label('auto_gps', 'Auto GPS') !!}
    {!! Form::checkbox('auto_gps', 1, isset($auto_gps) ? $auto_gps : true, ['class'=>'form-control']) !!}
</div>
<div class="field half-width form-group lat-long">
    {!! Form::label('latitude', 'Latitude *') !!}
    {!! Form::text('latitude', null, ['class'=>'form-control']) !!}
</div>
<div class="field half-width form-group lat-long">
    {!! Form::label('longitude', 'Longitude *') !!}
    {!! Form::text('longitude', null, ['class'=>'form-control']) !!}
</div>