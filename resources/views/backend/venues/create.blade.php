@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Venue')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Venues' => ['backend.venues.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.venues.create.post']) !!}

        @include('backend.venues.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Venue', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop