@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Venue')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Venues'            => ['backend.venues.index'],
        '#' . $venue->id    => ['backend.venues.show', $venue->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($venue, ['route' => ['backend.venues.edit.post', $venue->id]]) !!}

        @include('backend.venues.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Venue', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop