@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Venues')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Venues' => null,
    ]); !!}
@stop

@section('content')

    <style>
        .dataTables_info {
            display: none;
        }
    </style>

    {!! Form::open(['route' => 'backend.venues.index', 'method' => 'get']) !!}
    <div class="row">
        <div class="col-xs-3">
            {!! link_to_route('backend.venues.create.get', 'Create Venue', [], ['class' => 'btn btn-primary']) !!}
        </div>
        <div class="col-xs-1">
        </div>
        <div class="col-xs-6">
            {!! Form::submit('Filter', ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.venues.index', 'Clear', [], ['class' => 'btn btn-warning']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-2 form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', isset($name) ? $name : null, ['class'=>'form-control']) !!}
        </div>
        <div class="col-xs-2 form-group">
            {!! Form::label('line1', 'Address') !!}
            {!! Form::text('line1', isset($line1) ? $line1 : null, ['class'=>'form-control']) !!}
        </div>
        <div class="col-xs-2 form-group">
            {!! Form::label('city', 'City') !!}
            {!! Form::text('city', isset($city) ? $city : null, ['class'=>'form-control']) !!}
        </div>
        <div class="col-xs-2 form-group">
            {!! Form::label('state_id', 'State') !!}
            {!! Form::select('state_id', ['' => 'Select A State'] + $state_options, null, ['class'=>'form-control']) !!}
        </div>
        <div class="col-xs-2 form-group">
            {!! Form::label('zipcode', 'Zipcode') !!}
            {!! Form::text('zipcode', isset($zipcode) ? $zipcode : null, ['class'=>'form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}


	<?php $_index = ($venues->currentPage() - 1) * $venues->perPage(); ?>
    <div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 10px;">
        <p style="margin: 0;">{{ 'Showing ' . ($_index + 1) . ' to ' . ($_index + $venues->count()) . ' of ' . $venues->total() . ' entries'}}</p>
        {{ $venues->appends(Input::except('page'))->links() }}
    </div>
    @include('backend.venues.partials.table')

    {{ $venues->appends(Input::except('page'))->links() }}

@stop