@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Venue')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Venues'            => ['backend.venues.index'],
        $venue->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#venue">Venue</a></li>
        <li><a data-toggle="tab" href="#map">Map</a></li>
        <li><a data-toggle="tab" href="#events">Events</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="venue" class="tab-pane fade in active">
            {!! link_to_route('backend.venues.edit.get', 'Edit Venue', ['id' => $venue->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.venues.destroy', 'Delete Venue', ['id' => $venue->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.venues.partials.snapshot')
        </div>
        <div id="map" class="tab-pane fade">
            {!! link_to_route('backend.venues.generate_embedded_map', 'Auto Generate Embedded Map', ['id' => $venue->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.venues.set_embedded_map.get', 'Manually Set Embedded Map', ['id' => $venue->id], ['class' => 'btn btn-warning']) !!}
            <br />
            <br />
            {!! $venue->embedded_map !!}
        </div>
        <div id="events" class="tab-pane fade">
            @include('backend.events.partials.table', ['events' => $venue->events])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $venue])
        </div>
    </div>
@stop