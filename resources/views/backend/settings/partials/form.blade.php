<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('key', 'Key *') !!}
        {!! Form::text('key', null, ['readonly' => true, 'class' => 'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('type', 'Type *') !!}
        {!! Form::select('type', $type_options, null, ['class' => 'form-control']) !!}
    </div>
    <div class="field full-width non-wysiwyg form-group">
        {!! Form::label('value', 'Value') !!}
        {!! Form::text('value', isset($setting) && $setting->type == 'html' ? '' : null, ['class' => 'form-control']) !!}
    </div>
    <div class="field full-width wysiwyg">
        <div class="disabled-cover">&nbsp;</div>
        {!! Form::label('html_value', 'HTML Value') !!}
        {!! Form::textarea('html_value', isset($setting) && $setting->type == 'html' ? $setting->value : null, ['rowspan' => 10, 'disabled' => true, 'class' => 'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('scope', 'Scope *') !!}
        {!! Form::select('scope', $scope_options, null, ['class' => 'form-control']) !!}
    </div>
    <div class="field full-width form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
</div>

<style>
    .wysiwyg {
        /*display: none;*/
        margin-right: 0;
        margin-left: 0;
        /*position: absolute;*/
        /*left: -999999999px;*/
        position: relative;
    }

    /*.wysiwyg textarea {*/
    /**/
    /*}*/
    .wysiwyg label {
        display: block;
    }

    .wysiwyg .disabled-cover {
        width: 100%;
        height: 100%;
        position: absolute;
        z-index: 100;
        background-color: #999;
        cursor: not-allowed;
        opacity: 0.6;
    }
</style>
<script>
    $(document).ready(function()
    {
        $('#html_value').cazary({
            commands: "FULL"
        });

        $('#type').change(function()
        {
            if($(this).val() == 'html')
            {
                $('#value').attr('disabled', true);
                $('#html_value').attr('disabled', false);
                $('.wysiwyg .disabled-cover').css('z-index', '-100');
//                $('.wysiwyg').show();
//                $('.non-wysiwyg').hide();
            }
            else
            {
                $('#html_value').attr('disabled', true);
                $('#value').attr('disabled', false);
                $('.wysiwyg .disabled-cover').css('z-index', '100');
//                $('.wysiwyg').hide();
//                $('.non-wysiwyg').show();
            }
        });

        $('#type').trigger('change');
    });
</script>