<?php $table_id = isset($table_id) ? $table_id : 'setting_table'; ?>

<table id="{{ $table_id }}" class="no-wrap datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Key</th>
        <th>Value</th>
        <th>Type</th>
        <th>Description</th>
        <th>User</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($settings as $setting)
        <tr>
            <td>{!! link_to_route('backend.settings.edit.get', $setting->id, ['id' => $setting->id]) !!}</td>
            <td title="{{ $setting->key }}">{!! link_to_route('backend.settings.edit.get', $setting->key, ['id' => $setting->id]) !!}</td>
            <td title="{{ $setting->value }}">{!! link_to_route('backend.settings.edit.get', $setting->value, ['id' => $setting->id]) !!}</td>
            <td>{{ $setting->type }}</td>
            <td title="{{ $setting->description }}">{{ $setting->description }}</td>
            <td>{{ $setting->user_id == null ? 'Global' : $setting->user->display() }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.settings.edit.get', 'fa-pencil', ['id' => $setting->id], ['title' => 'Edit Setting']) !!}
{{--                {!! Helper::icon_to_route('backend.settings.destroy', 'fa-trash-o', ['id' => $setting->id], ['title' => 'Delete Setting']) !!}--}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1, 'Notes' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});
    });
</script>