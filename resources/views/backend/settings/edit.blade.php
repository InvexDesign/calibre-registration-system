@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Setting')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Settings'          => ['backend.settings.index'],
        '#' . $setting->id  => ['backend.settings.show', $setting->id],
        'Edit'              => null,
    ]); !!}
@stop

@section('content')
    {!! Form::model($setting, ['route' => ['backend.settings.edit.post', $setting->id]]) !!}

        {!! Form::hidden('setting_id', $setting->id) !!}
        @include('backend.settings.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Setting', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop