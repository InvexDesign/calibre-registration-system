@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Create Note')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
            'Notes' => ['backend.notes.index'],
            'Create'   =>  null
    ]); !!}
@stop

@section('content')
    {!! Form::open(['route' => 'backend.notes.create.post']) !!}

        @include('backend.notes.partials.form')

        <div class="form-group">
            {!! Form::submit('Create Note', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop