@include('backend.notes.partials.form', ['id' => $model->id, 'class' => get_class($model)])
@include('backend.notes.partials.table', ['notes' => $model->notes()->get()])