<?php $table_id = isset($table_id) ? $table_id : 'note_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Model</th>
        <th>Content</th>
        <th>Author</th>
        <th>Timestamp</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($notes as $note)
        <tr>
            <td>{!! link_to_route('backend.notes.show', $note->id, [$note->id]) !!}</td>
            <td>{!! link_to_route($note->noteable->getBackendRoutes()['show'], $note->noteable->display(), [$note->noteable->id]) !!}</td>
            <td>{{ $note->content }}</td>
            <td>{!! $note->author ? link_to_route('backend.users.show', $note->author->username, ['id' => $note->author->id]) : 'System' !!}</td>
            <td>{!! Helper::displayCalendarDate($note->created_at) !!}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.notes.show', 'fa-eye', [$note->id], ['title' => 'View Note']) !!}
                @if(\Auth::user()->isOverlord())
                {!! Helper::icon_to_route('backend.notes.destroy', 'fa-ban', [$note->id], ['title' => 'Delete Note']) !!}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['Model' => 1]])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>