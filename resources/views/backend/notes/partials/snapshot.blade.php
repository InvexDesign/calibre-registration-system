<p>
    <strong>ID:</strong> {{ $note->id }}<br />
    <strong>Content:</strong> {{ $note->content }}<br />
    <strong>Author:</strong> {!! $note->author ? link_to_route('backend.users.show', $note->author->username, ['id' => $note->author->id]) : 'System' !!}<br />
    <strong>Timestamp:</strong> {{ $note->created_at->format('g:iA n/j/Y') }}<br />
</p>