<?php $form_id = isset($form_id) ? $form_id : 'note_form'; ?>
<a href="#" class="btn btn-success" data-featherlight="#{{ $form_id }}">Add Note</a>
<div id="{{ $form_id }}" class="featherlight-target">
    <h4>Add A Note</h4>
    {!! Form::open(['route' => 'backend.notes.create.post']) !!}

        {!! Form::hidden('id', $id) !!}
        {!! Form::hidden('class', $class) !!}

        <div class="field-container col-1">
            <div class="field form-group">
                {!! Form::label('content', 'Content *') !!}
                {!! Form::textarea('content', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Add Note', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
</div>
