@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Notes')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Notes' => null,
    ]); !!}
@stop

@section('content')

    {!! link_to_route('backend.notes.create.get', 'Create Note', [], ['class' => 'btn btn-primary']) !!}
    <br />
    <br />

    @include('backend.notes.partials.table')

@stop