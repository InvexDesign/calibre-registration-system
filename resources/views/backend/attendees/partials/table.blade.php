<?php $table_id = isset($table_id) ? $table_id : 'attendee_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>ID</th>
        <th>UID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Rank</th>
        <th>PID</th>
        <th>Tickets</th>
        <th>Actions</th>
    </tr>
    </thead>

    <tbody>
    @foreach($attendees as $attendee)
        <tr>
            <td>{!! link_to_route('backend.attendees.show', $attendee->id, [$attendee->id]) !!}</td>
            <td>{!! link_to_route('backend.attendees.show', $attendee->uid, [$attendee->id]) !!}</td>
            <td>{!! link_to_route('backend.attendees.show', $attendee->first_name, [$attendee->id]) !!}</td>
            <td>{!! link_to_route('backend.attendees.show', $attendee->last_name, [$attendee->id]) !!}</td>
            <td><a href="mailto:{{ $attendee->email }}">{{ $attendee->email }}</a></td>
            <td>{{ $attendee->rank }}</td>
            <td>{{ $attendee->pid }}</td>
            <td class="align-right">{{ number_format($attendee->tickets()->count(), 0) }}</td>
            <td class="icon-td">
                {!! Helper::icon_to_route('backend.attendees.show', 'fa-eye', [$attendee->id], ['title' => 'View Attendee']) !!}
                {!! Helper::icon_to_route('backend.attendees.edit.get', 'fa-pencil', [$attendee->id], ['title' => 'Edit Attendee']) !!}
                {!! Helper::icon_to_route('backend.attendees.destroy', 'fa-ban', [$attendee->id], ['title' => 'Delete Attendee']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => ['ID' => 1]])

        var table_id = '{{ $table_id }}';

        @if(isset($_searchable) && $_searchable)
            datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns, dom: '', paging: false});
        @else
            datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});
        @endif

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>