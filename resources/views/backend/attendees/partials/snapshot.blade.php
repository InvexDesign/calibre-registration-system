<p>
    <strong>ID:</strong> {{ $attendee->id }}<br />
    <strong>UID:</strong> {{ $attendee->uid }}<br />
    <strong>First Name:</strong> {{ $attendee->first_name }}<br />
    <strong>Last Name:</strong> {{ $attendee->last_name }}<br />
    <strong>Email:</strong> {{ $attendee->email }}<br />
    <strong>Rank:</strong> {{ $attendee->rank }}<br />
    <strong>PID:</strong> {{ $attendee->pid }}<br />
</p>