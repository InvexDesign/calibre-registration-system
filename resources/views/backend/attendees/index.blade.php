@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Attendees')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Attendees' => null,
    ]); !!}
@stop

@section('content')

    <style>
        .dataTables_info {
            display: none;
        }
    </style>

    {!! Form::open(['route' => $search_route, 'method' => 'get']) !!}
    <div class="row">
        <div class="col-xs-10 form-group">
            <div class="col-xs-3 form-group">
                {!! Form::label('first_name', 'First Name') !!}
                {!! Form::text('first_name', isset($first_name) ? $first_name : null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-3 form-group">
                {!! Form::label('last_name', 'Last Name') !!}
                {!! Form::text('last_name', isset($last_name) ? $last_name : null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-3 form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', isset($email) ? $email : null, ['class'=>'form-control']) !!}
            </div>
            <div class="col-xs-3 form-group">
                {!! Form::label('rank', 'Rank') !!}
                {!! Form::text('rank', isset($rank) ? $rank : null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-2" style="display: flex; justify-content: space-around; align-items: center; height: 85px;">
            {!! Form::submit('Filter', ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.attendees.index', 'Clear', [], ['class' => 'btn btn-warning']) !!}
        </div>
    </div>
    {!! Form::close() !!}

	<?php $_index = ($attendees->currentPage() - 1) * $attendees->perPage(); ?>
    <div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 10px;">
        <p style="margin: 0;">{{ 'Showing ' . ($_index + 1) . ' to ' . ($_index + $attendees->count()) . ' of ' . $attendees->total() . ' entries'}}</p>
        {{ $attendees->appends(Input::except('page'))->links() }}
    </div>

    @include('backend.attendees.partials.table', ['_searchable' => true])

    {{ $attendees->appends(Input::except('page'))->links() }}

@stop