@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'View Attendee')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Attendees'            => ['backend.attendees.index'],
        $attendee->display()   =>  null
    ]); !!}
@stop

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#attendee">Attendee</a></li>
        <li><a data-toggle="tab" href="#tickets">Tickets</a></li>
        <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
        <div id="attendee" class="tab-pane fade in active">
            {!! link_to_route('backend.attendees.edit.get', 'Edit Attendee', ['id' => $attendee->id], ['class' => 'btn btn-primary']) !!}
            {!! link_to_route('backend.attendees.destroy', 'Delete Attendee', ['id' => $attendee->id], ['class' => 'btn btn-danger']) !!}
            <br />
            <br />
            @include('backend.attendees.partials.snapshot')
        </div>
        <div id="tickets" class="tab-pane fade">
            @include('backend.tickets.partials.table', ['tickets' => $attendee->tickets])
        </div>
        <div id="notes" class="tab-pane fade">
            @include('backend.notes.partials.tab_content', ['model' => $attendee])
        </div>
    </div>
@stop