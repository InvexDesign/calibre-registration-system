@extends('templates.master')

@section('page-title', isset($page_title) ? $page_title : 'Edit Attendee')

@section('breadcrumbs')
    {!! App\Helpers\Breadcrumbs::generate([
        'Attendees'            => ['backend.attendees.index'],
        '#' . $attendee->id    => ['backend.attendees.show', $attendee->id],
        'Edit'                =>  null
    ]); !!}
@stop

@section('content')

    {!! Form::model($attendee, ['route' => ['backend.attendees.edit.post', $attendee->id]]) !!}

        @include('backend.attendees.partials.form')

        <div class="form-group">
            {!! Form::submit('Update Attendee', ['class' => 'updatable btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop