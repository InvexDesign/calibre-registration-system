<p>Thank you for registering with Calibre Press!
   We've moved! Please download our updated W9 with our new address {!! link_to(Setting::get('w9-form-url'), 'here') !!}
   .</p>

<p>This email is your confirmation that we have received your registration
   for {{ $registration->displayCourseAndLocation() }}
   on {{ $registration->event->start_at->format('M j, Y') }}</p>

<p>We are providing you with a downloadable invoice here: Download PDF Invoice If you are unable to download your
   invoice, please contact Kristin at <a href="tel:+16302768736">630-276-8736</a> or
    <a href="mailto:kristin@calibrepress.com">kristin@calibrepress.com</a> with your name, course, and the date of
   the course.</p>

<p>The Seminar will be held at:
    {{ $registration->event->venue->name }}
    {{ $registration->event->venue->address->display() }}
   Google Map: {!! link_to("www.maps.google.com", 'Maps And Directions') !!}</p>

<p>Registration for this class begins at 7:30am and the class will be held between the hours of 8:00am – 5:00pm.</p>

<p>Requirements: Please dress comfortably. There is no equipment necessary for this class. You will be required to
   present this confirmation and Law Enforcement ID at registration. All course material will be provided. If you notice
   a misspelled name, please let us know so that we may have corrected information on your certificates.</p>

<p>Cancellation Policy: Cancellation and full refund will be honored until two weeks prior to the seminar. Any
   cancellation within two weeks of the course will receive a credit for any future seminar.</p>

<p>If you have any questions or concerns, please contact Kristin at
    <a href="mailto:kristin@calibrepress.com">kristin@calibrepress.com</a>.
   Click {!! link_to(Setting::get('w9-form-url'), 'here') !!} to download our
   W-9</p>

<p>Thank You</p>