@extends('templates.external')

@section('content')

    <h3>Reset Password</h3>

    {!! Form::open(['route' => 'password.reset.post']) !!}

        {!! Form::hidden('token', $token)!!}

        <div class="form-group">
            {!! Form::label('email', 'Email Address') !!}
            {!! Form::email('email', null, ['class'=>'form-control']) !!}
        </div>
        <br />
        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class'=>'form-control']) !!}
        </div>
        <br />
        <div class="form-group">
            {!! Form::label('password_confirmation', 'Password Confirmation') !!}
            {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
        </div>
        <br />
        <div class="flex-container form-group">
            <span style="flex: 1;">&nbsp;</span>
            {!! Form::submit('Password Reset', ['class' => 'btn btn-primary', 'style' => 'flex: 1;']) !!}
        </div>

    {!! Form::close() !!}

@stop
