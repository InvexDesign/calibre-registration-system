<div class="field-container col-2">
    <div class="field form-group">
        {!! Form::label('line1', 'Line 1 *') !!}
        {!! Form::text('line1', isset($address) ? $address->line1 : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('line2', 'Line 2') !!}
        {!! Form::text('line2', isset($address) ? $address->line2 : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('city', 'City *') !!}
        {!! Form::text('city', isset($address) ? $address->city : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('state_id', 'State *') !!}
        {!! Form::select('state_id', $state_options, isset($address) ? $address->state_id : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('zipcode', 'Zipcode *') !!}
        {!! Form::text('zipcode', isset($address) ? $address->zipcode : null, ['class'=>'form-control zipcode-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('country', 'Country *') !!}
        {!! Form::text('country', isset($address) ? $address->country : 'USA', ['class'=>'form-control']) !!}
    </div>
</div>