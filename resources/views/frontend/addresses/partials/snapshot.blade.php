<p>
    <strong>ID:</strong> {{ $address->id }}<br />
    <strong>UID:</strong> {{ $address->uid }}<br />
    <strong>Name:</strong> {{ $address->name }}<br />
    <strong>Tagline:</strong> {{ $address->tagline }}<br />
    <strong>Description:</strong> {{ $address->description }}<br />
    <strong>Notes:</strong> {{ $address->notes }}<br />
</p>