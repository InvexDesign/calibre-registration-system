<?php
header("content-type:application/json");

if(isset($_POST['g-recaptcha-response']))
{
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$secret_key = Setting::get('recaptcha-secret-key');
	$fields = [
		'secret'   => urlencode($secret_key),
		'response' => urlencode($_POST['g-recaptcha-response'])
	];

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($fields));
	curl_exec($curl);
	curl_close($curl);
}
else
{
    echo json_encode(['success' => false]);
}

