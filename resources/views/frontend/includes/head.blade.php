<link href="{{ asset('css/layout.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/select-two-custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/featherlight.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/interactive-map.css') }}" rel="stylesheet">
<link href="{{ asset('css/tympanus-ming.css') }}" rel="stylesheet">

<script src="{{ asset('js/jquery.min.js') }}"></script>
