<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/datatables_utilities.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/featherlight.min.js') }}"></script>
<script src="{{ asset('js/helpers.js') }}"></script>
<script src="{{ asset('js/utilities.js') }}"></script>

<script>
    function initializeInputs(scope)
    {
        if(typeof scope === 'undefined')
        {
            scope = '';
        }

        $('.lightbox-trigger').click(function()
        {
            var id = $(this).data('lightbox-id');
            $.featherlight('#' + id, {});
        });

        $(scope + '.email-mask').inputmask("email");
        $(scope + ".decimal-mask").inputmask("decimal", {rightAlign: false});
        $(scope + '.phone-mask').mask("999-999-9999?x99999");
        $(scope + '.zipcode-mask').mask("99999");

        $(scope + ".integer-mask").inputmask("decimal", {
            groupSeparator: ",",
            digits: 0,
            autoGroup: true,
            rightAlign: false
        });

        $(scope + ".positive-integer-mask").inputmask("decimal", {
            groupSeparator: ",",
            digits: 0,
            autoGroup: true,
            rightAlign: false,
            allowMinus: false
        });

        $(scope + ".dollar-mask").inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            rightAlign: false,
            autoUnmask: true,
            prefix: '$'
        });

        $(scope + ".datepicker").datepicker().mask("99/99/9999", {placeholder: "mm/dd/yyyy"});
        $(scope + ".datepicker-dash").datepicker({dateFormat: "mm-dd-yy"}).mask("99-99-9999", {placeholder: "mm-dd-yyyy"});

        $(scope + '.zipcode-mask').each(function()
        {
            $(this).mask("99999").blur(forceValidZipcode);
        });

        $(scope + '.select-two').select2();
        $(scope + '.select-two-tags').select2({
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    }

    $(document).ready(function()
    {
        initializeInputs();
    });
</script>