<table>
    <thead>
    <tr>
        <th>UID</th>
        <th>Type</th>
        <th>Amount</th>
        <th>Paid</th>
    </tr>
    </thead>

    <tbody>
    @foreach($payments as $payment)
        <tr>
            <td>{{ $payment->uid }}</td>
            <td>{{ $payment->class }}</td>
            @if($payment->amount_in_dollars < 0)
                <td class="align-right debit">- ${{ number_format(abs($payment->amount_in_dollars), 2) }}</td>
            @else
                <td class="align-right credit">${{ number_format($payment->amount_in_dollars, 2) }}</td>
            @endif
            <td>{{ $payment->paid_at->format('g:iA n/j/Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>