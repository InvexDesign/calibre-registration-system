<h2 style="text-align: center;">Card Details</h2>
<div class='card-wrapper'></div>
<div class="field-container col-1" style="width: 350px; margin: 0 auto;">
    <div class="field form-group">
        {!! Form::label('name', 'Name *') !!}
        {!! Form::text('name', null, ['class'=>'card-input form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('card_number', 'Card Number *') !!}
        {!! Form::text('card_number', null, ['class'=>'card-input form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('expiration', 'Expiration *') !!}
        {!! Form::text('expiration', null, ['class'=>'card-input form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('card_code', 'Card Code *') !!}
        {!! Form::text('card_code', null, ['class'=>'card-input form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('amount', 'Amount *') !!}
        {!! Form::text('amount', null, ['class'=>'dollar-mask card-input form-control']) !!}
    </div>
    @if($allow_discounts)
        @if($discount)
            <div class="automatically-applied discount-container field form-group">
                {!! Form::label('discount_id', 'Automatically Applied Discount') !!}<br />
                {{--{!! Form::hidden('discount_id', $discount->id) !!}<br />--}}
                <strong>Promotion:</strong> {{ $discount->name }}<br />
                <strong>Code:</strong> {{ $discount->code }}<br />
                <strong>Original Cost:</strong> ${{ number_format($registration->getTotalCostInDollars(), 2) }}<br />
                <strong>Savings:</strong> You save ${{ number_format($discount->calculateDiscount($registration) / 100, 2) }}<br />
                <strong>Final Cost:</strong> ${{ number_format($amount, 2) }}<br />
            </div>
        @endif
        <div class="manually-applied discount-container field form-group">
            {!! Form::label('discount_id', 'Manually Applied Discount') !!}
            {{--{!! Form::hidden('discount_id', $discount->id) !!}<br />--}}
            <strong>Promotion:</strong> <span class="name"></span><br />
            <strong>Code:</strong> <span class="code"></span><br />
            <strong>Original Cost:</strong> <span class="original-cost" data-cost="{{ $registration->getTotalCostInDollars() }}">${{ number_format($registration->getTotalCost(), 2) }}</span><br />
            <strong>Savings:</strong> You save <span class="savings"></span><br />
            <strong>Final Cost:</strong> <span class="final-cost"></span><br />
        </div>
        <div class="field form-group">
            {!! Form::label('discount_code', 'Discount') !!}
            {!! Form::text('discount_code', null, ['data-registration-uid' => $registration->id, 'data-registration-passcode' => $registration->passcode, 'class'=>'form-control']) !!}
            {!! Form::button('Apply', ['id' => 'apply_discount_button', 'class' => 'btn btn-warning']) !!}
        </div>
    @endif
</div>
<h2 style="text-align: center;">Billing Details</h2>
<div class="field-container col-1" style="width: 350px; margin: 0 auto;">
    <div class="field form-group">
        {!! Form::label('address', 'Address *') !!}
        {!! Form::text('address', isset($address) ? $address : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('city', 'City *') !!}
        {!! Form::text('city', isset($city) ? $city : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('state_id', 'State *') !!}
        {!! Form::select('state_id', $state_options, isset($state_id) ? $state_id : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('zipcode', 'Zipcode *') !!}
        {!! Form::text('zipcode', isset($zipcode) ? $zipcode : null, ['class'=>'form-control']) !!}
    </div>
</div>

<style>
    .manually-applied.discount-container {
        display: none;
    }
</style>
<script>
    $(document).ready(function()
    {
        $('#payment_form').card({
            container: '.card-wrapper', // *required*

            formSelectors: {
                numberInput: 'input#card_number', // optional — default input[name="number"]
                expiryInput: 'input#expiration', // optional — default input[name="expiry"]
                cvcInput: 'input#card_code', // optional — default input[name="cvc"]
                nameInput: 'input#name' // optional - defaults input[name="name"]
            },

            width: 350, // optional — default 350px
            formatting: true, // optional - default true

            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },

            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Full Name',
                expiry: '••/••',
                cvc: '•••'
            },

            masks: {
                cardNumber: '•' // optional - mask card number
            },

            debug: true // if true, will log helpful messages for setting up Card
        });

        @if(isset($name))
        $('#name').val('{{ $name }}');
        @endif
        @if(isset($card_number))
        $('#card_number').val('{{ $card_number }}');
        @endif
        @if(isset($expiration))
        $('#expiration').val('{{ $expiration }}');
        @endif
        @if(isset($card_code))
        $('#card_code').val('{{ $card_code }}');
        @endif
        @if(isset($amount))
        $('#amount').val('{{ $amount }}');
        @endif

        $('#apply_discount_button').click(function()
        {
            $.post("{{ route('frontend.workflows.payment.apply_discount') }}",
            {
                registration_uid: '{{ $registration->uid }}',
                registration_passcode: '{{ $registration->passcode }}',
                code: $('#discount_code').val()
            },
            function(data, status){
                console.log(data);
                var response = $.parseJSON(data);
                if(response.success)
                {
                    $('.automatically-applied.discount-container').hide();

                    var discount = response.discount;
                    $('.manually-applied.discount-container .name').html(discount.name);
                    $('.manually-applied.discount-container .code').html(discount.code);
                    $('.manually-applied.discount-container .savings').html('$' + number_format(discount.discount, 2));
                    $('.manually-applied.discount-container .final-cost').html('$' + number_format(response.amount, 2));
                    $('.manually-applied.discount-container').show();
                    $('#amount').val(response.amount);
                }
            });
        });
    });
</script>