<p>
{{--    <strong>ID:</strong> {{ $registration->id }}<br />--}}
    <strong>UID:</strong> {{ $registration->uid }}<br />
    <strong>Agency:</strong> {{ $registration->agency }}<br />
    <strong>Contact:</strong> {{ $registration->contact }}<br />
    <strong>Email:</strong> {{ $registration->email }}<br />
    <strong>Department Size:</strong> {{ $registration->department_size }}<br />
    <strong>Phone:</strong> {{ $registration->phone }}<br />
    <strong>Fax:</strong> {{ $registration->fax }}<br />
    <strong>Address:</strong> {{ $registration->address->display() }}<br />
    {{--<strong>Passcode:</strong> {{ $registration->passcode }}<br />--}}
    <strong>Payment Status:</strong> {{ $registration->payment_status }}<br />
    {{--<strong>Notes:</strong> {{ $registration->notes }}<br />--}}
</p>