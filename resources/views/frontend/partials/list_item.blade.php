<li id="facility_list_{{ $facility->id }}" data-lightbox-id="lightbox_{{ $facility->id }}" class="tympanus image effect-ming lightbox-trigger">
    <img src="{{ $APP_URL_BASE . $facility->getImageUrl() }}" />
    <div class="overlay">
        <h2 class="title">{{ $facility->name }}</h2>
        <p class="content">{{ $facility->description }}</p>
    </div>
</li>