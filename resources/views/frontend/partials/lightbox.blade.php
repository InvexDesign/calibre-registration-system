<div id="lightbox_{{ $facility->id }}">
    <h4>{{ $facility->name }}</h4>
    <p>{{ $facility->tagline }}</p>
    <p>{{ $facility->description}}</p>
    <p>{{ $facility->address1 }}<br />
        {{ $facility->address2 }}<br />
        {{ $facility->city }}, {{ $facility->state->display() }} {{ $facility->zipcode}}<br />
    </p>
    <strong>Serves: </strong> {{ $facility->type }}<br />
    <strong>Email: </strong> {{ $facility->email }}<br />
    <strong>Website: </strong> {{ $facility->website }}<br />
    <strong>Phone: </strong> {{ $facility->phone }}<br />
    <strong>Hours: </strong> {{ $facility->hours }}<br />
    <strong>Parking / Transport: </strong> {{ $facility->parking_transport }}<br />
    <strong>LGBTQ+ Services: </strong> {{ $facility->lgbtq_services }}<br />
    <strong>Minor Access: </strong> {{ $facility->minor_access }}<br />
    <strong>Appointments: </strong> {{ $facility->appointments }}<br />
    <strong>Contact: </strong> {{ $facility->contact }}<br />
</div>