<?php /* Template Name: Interactive Map */ ?>

<?php get_header(); ?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php
	$account = 'mundeleinparks';
	$APP_URL_BASE = 'https://mundeleinparks.org/wp-admin/interactive-map-admin/';

	require "/home/$account/git/interactive-map/bootstrap/autoload.php";
	$app = require_once "/home/$account/git/interactive-map/bootstrap/app.php";
	$kernel = $app->make('Illuminate\Contracts\Http\Kernel');
	$kernel->handle($request = Illuminate\Http\Request::capture());
	$__env = $app->make('Illuminate\View\Factory');

	$features = \App\Models\Feature::all();
	$feature_options = \App\Models\Feature::all()->pluck('name', 'id')->all();
	$facilities = \App\Models\Facility::all();

	$FILTER_URL = $APP_URL_BASE . 'filter';
	?>

	<link href="<?php echo $APP_URL_BASE . 'css/bootstrap.min.css'; ?>" rel="stylesheet">
	<link href="<?php echo $APP_URL_BASE . 'css/select2.min.css'; ?>" rel="stylesheet">
	<link href="<?php echo $APP_URL_BASE . 'css/select-two-custom.css'; ?>" rel="stylesheet">
	<link href="<?php echo $APP_URL_BASE . 'css/featherlight.min.css'; ?>" rel="stylesheet">
	<link href="<?php echo $APP_URL_BASE . 'css/interactive-map.css'; ?>" rel="stylesheet">
	<link href="<?php echo $APP_URL_BASE . 'css/tympanus-ming.css'; ?>" rel="stylesheet">

	<script src="<?php echo $APP_URL_BASE . 'js/jquery.min.js'; ?>"></script>
	<script src="<?php echo $APP_URL_BASE . 'js/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo $APP_URL_BASE . 'js/select2.full.min.js'; ?>"></script>
	<script src="<?php echo $APP_URL_BASE . 'js/featherlight.min.js'; ?>"></script>
	<script src="<?php echo $APP_URL_BASE . 'js/helpers.js'; ?>"></script>
	<script src="<?php echo $APP_URL_BASE . 'js/utilities.js'; ?>"></script>

	<div class="search-pane">
		<div class="search-parameter" style="flex: 1;">
			<h4>Name</h4>
		<?php echo Form::text('name', null, ['id' => 'name_filter', 'class' => 'filter form-control']); ?>
		</div>
		<div class="search-parameter" style="flex: 2;">
			<h4>Services</h4>
		<?php echo Form::select('feature_ids[]', $feature_options, null, ['id' => 'features_filter', 'multiple' => true, 'class' => 'filter select-two-custom form-control']); ?>
		</div>
	</div>
	<div class="results-pane">
		<div id="map_view"
				 class="map-view">    <?php echo $__env->make('frontend.partials.map', compact('facilities'), array_except(get_defined_vars(), [
			'__data',
			'__path'
		]))->render(); ?>    </div>
		<div id="list_view"
				 class="list-view">    <?php echo $__env->make('frontend.partials.list', compact('facilities', 'APP_URL_BASE'), array_except(get_defined_vars(), [
			'__data',
			'__path'
		]))->render(); ?>    </div>
	</div>
	<script>
      var FACILITIES = {};
      var OPEN_INFO_WINDOW = null;
      function closeOpenInfoWindow()
      {
          if(OPEN_INFO_WINDOW)
          {
              OPEN_INFO_WINDOW.close();
          }
      }

      function openInfoWindow(map, marker, infowindow)
      {
          closeOpenInfoWindow();
          infowindow.open(map, marker);
          OPEN_INFO_WINDOW = infowindow;
      }

      function filterFacilities()
      {
          closeOpenInfoWindow();
          $('div.loader').show();
          console.log("filterFacilities");

          var data = {
              name: $('#name_filter').val(),
              feature_ids: $('#features_filter').val(),
          };
          var jqxhr = $.post("<?php echo $FILTER_URL; ?>", data).done(function(response)
          {
              console.log("Success!");
              var json = JSON.parse(response);
              $.each(json.facilities, function(id, is_visible)
              {
                  FACILITIES[id].marker.setVisible(is_visible);
                  $('#facility_list_' + id).toggle(is_visible);
              });
          }).fail(function(response)
          {
              console.error("ERROR!");
              console.error(response);
          }).always(function()
          {
              $('div.loader').hide();
          });
      }
      function initializeInputs(scope)
      {
          if(typeof scope === 'undefined')
          {
              scope = '';
          }
          $(scope + '.select-two').select2();
          $(scope + '.select-two-tags').select2({
              tags: true, createTag: function(params)
              {
                  return {id: params.term, text: params.term, newOption: true}
              }
          });
      }
      $(document).ready(function()
      {
          initializeInputs();
          $('.select-two-custom').select2({placeholder: 'All'});
          $('.filter').change(function()
          {
              filterFacilities();
          });
      });
	</script>
<?php endwhile; endif; ?>

<?php get_footer(); ?>