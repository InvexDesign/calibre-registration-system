@extends('templates.frontend')

@section('content')

    <h4>Frontend Index</h4>

    {!! link_to_route('frontend.events.index', 'See Full Calendar') !!}

    <br />
    <br />
    <table>
        <thead>
        <tr>
            <th>Upcoming Events</th>
        </tr>
        </thead>
        <tbody>
        @foreach($events as $event)
            <tr>
                <td><a href="{{ $event->url }}">{{ $event->display }}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop