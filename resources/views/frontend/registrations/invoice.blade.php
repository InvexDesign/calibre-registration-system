<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
    .page {
        page-break-after: always;
    }
</style>

<div>
    <h2>Registration #{{ $registration->uid }}</h2>
    <strong>Agency:</strong> {{ $registration->agency }}<br />
    <strong>Contact:</strong> {{ $registration->contact }}<br />
    <strong>Email:</strong> {{ $registration->email }}<br />
    <strong>Department Size:</strong> {{ $registration->department_size }}<br />
    <strong>Phone:</strong> {{ $registration->phone }}<br />
    <strong>Fax:</strong> {{ $registration->fax }}<br />
    <strong>Address:</strong> {{ $registration->address->display() }}<br />
    <strong>Payment Status:</strong> {{ $registration->payment_status }}<br />
    <strong>Registration Date:</strong> {{ $registration->created_at->format('n/j/Y') }}<br />
    <strong>Balance:</strong> ${{ number_format($registration->getBalanceInDollars(), 2) }}<br />
</div>
<br />

<h2>Tickets / Attendees</h2>
<table>
    <thead>
    <tr>
        <th>UID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Rank</th>
        <th>PID</th>
        <th>Price</th>
    </tr>
    </thead>

    <tbody>
    @foreach($registration->tickets as $index => $ticket)
		<?php $attendee = $ticket->attendee; ?>
        <tr>
            <td>Ticket #{{ $ticket->uid }}</td>
            <td>{{ $attendee->first_name . ' ' . $attendee->last_name }}</td>
            <td>{{ $attendee->email }}</td>
            <td>{{ $attendee->rank }}</td>
            <td>{{ $attendee->pid }}</td>
            <td>${{ number_format($ticket->price_in_dollars, 2) }}</td>
        </tr>
    @endforeach
    </tbody>

    <tfoot>
    <tr>
        <th>{{ number_format($registration->tickets()->count(), 0) }} Tickets</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>${{ number_format($registration->tickets()->sum('price_in_cents') / 100, 2) }}</th>
    </tr>
    </tfoot>
</table>

<h2>Payments</h2>
@include('frontend.payments.partials.table', ['payments' => $registration->payments])