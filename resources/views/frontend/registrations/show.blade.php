@extends('templates.frontend')

@section('content')

    <div>
        @include('frontend.registrations.partials.snapshot')

        {!! link_to_route('frontend.registrations.invoice', 'View Invoice', [$registration->uid, $registration->passcode], ['class' => 'btn btn-primary']) !!}

        @if(!$registration->isPaid())
            {!! link_to_route('frontend.workflows.payment.get', 'Pay Now', [$registration->uid, $registration->passcode], ['class' => 'btn btn-primary']) !!}
        @endif
    </div>

@stop