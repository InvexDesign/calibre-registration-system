<h2>Department Information</h2>
<div class="field-container col-3">
    {!! Form::hidden('registration[event_id]', $event->id) !!}
    <div class="field form-group">
        {!! Form::label('registration[agency]', 'Agency *') !!}
        {!! Form::text('registration[agency]', isset($registration) ? $registration->agency : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration[contact]', 'Department Contact (Invoicing) *') !!}
        {!! Form::text('registration[contact]', isset($registration) ? $registration->contact : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration[email]', 'Department Contact Email *') !!}
        {!! Form::text('registration[email]', isset($registration) ? $registration->email : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration[phone]', 'Phone Number *') !!}
        {!! Form::text('registration[phone]', isset($registration) ? $registration->phone : null, ['class'=>'form-control phone-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration[fax]', 'Fax Number *') !!}
        {!! Form::text('registration[fax]', isset($registration) ? $registration->fax : null, ['class'=>'form-control phone-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('registration[department_size_id]', 'Department Size') !!}
        {!! Form::select('registration[department_size_id]', $department_size_options, isset($registration) ? $registration->department_size_id : null, ['class'=>'form-control']) !!}
    </div>
</div>
<h4>Address</h4>
<div class="field-container col-3">
    <div class="field form-group">
        {!! Form::label('address[line1]', 'Address Line 1 *') !!}
        {!! Form::text('address[line1]', isset($address) ? $address->line1 : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('address[line2]', 'Address Line 2') !!}
        {!! Form::text('address[line2]', isset($address) ? $address->line2 : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('address[city]', 'City *') !!}
        {!! Form::text('address[city]', isset($address) ? $address->city : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('address[state_id]', 'State *') !!}
        {!! Form::select('address[state_id]', $state_options, isset($address) ? $address->state_id : null, ['class'=>'form-control']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('address[zipcode]', 'Zipcode *') !!}
        {!! Form::text('address[zipcode]', isset($address) ? $address->zipcode : null, ['class'=>'form-control zipcode-mask']) !!}
    </div>
    <div class="field form-group">
        {!! Form::label('address[country]', 'Country *') !!}
        {!! Form::text('address[country]', isset($address) ? $address->country : 'USA', ['class'=>'form-control']) !!}
    </div>
</div>
<h2>Attendees</h2>
<div class="field-container col-1">
    <div class="field form-group">
        {!! Form::label('attendee_count', 'Number of Attendees *') !!}
        {!! Form::select('attendee_count', ['' => 'Please Select A Value'] + $attendee_count_options, isset($attendee_count) ?  $attendee_count : null, ['id' => 'attendee_count', 'class'=>'form-control']) !!}
    </div>
</div>
@for($i = 0; $i < $max_attendee_count; $i++)
    @include('frontend.attendees.partials.form')
@endfor

<script>
    function showHideAttendees()
    {
        var attendee_count = parseInt($('#attendee_count').val());
        for(var i = 0; i < attendee_count; i++)
        {
            $('#attendee_form_' + i).show();
        }
        for(var j = attendee_count; i < {{ $max_attendee_count }}; i++)
        {
            $('#attendee_form_' + i).hide();
        }
    }

    $(document).ready(function()
    {
        $('#attendee_count').change(showHideAttendees);

        showHideAttendees();
    });
</script>

