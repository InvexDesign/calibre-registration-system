<h2>Registration #{{ $registration->uid }}</h2>
<p><strong>Agency:</strong> {{ $registration->agency }}</p>
<p><strong>Contact:</strong> {{ $registration->contact }}</p>
<p><strong>Email:</strong> {{ $registration->email }}</p>
<p><strong>Department Size:</strong> {{ $registration->department_size }}</p>
<p><strong>Phone:</strong> {{ $registration->phone }}</p>
<p><strong>Fax:</strong> {{ $registration->fax }}</p>
<p><strong>Address:</strong> {{ $registration->address->display() }}</p>
<p><strong>Payment Status:</strong> {{ $registration->payment_status }}</p>

<h4>Tickets / Attendees:</h4>
<table>
    <thead>
    <tr>
        <th>UID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Rank</th>
        <th>PID</th>
        <th>Price</th>
    </tr>
    </thead>

    <tbody>
    @foreach($registration->tickets as $index => $ticket)
		<?php $attendee = $ticket->attendee; ?>
        <tr>
            <td>Ticket #{{ $ticket->uid }}</td>
            <td>{{ $attendee->first_name . ' ' . $attendee->last_name }}</td>
            <td>{{ $attendee->email }}</td>
            <td>{{ $attendee->rank }}</td>
            <td>{{ $attendee->pid }}</td>
            <td>${{ number_format($ticket->price_in_dollars, 2) }}</td>
        </tr>
    @endforeach
    </tbody>

    <tfoot>
    <tr>
        <th>{{ number_format($registration->tickets()->count(), 0) }} Tickets</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>${{ number_format($registration->tickets()->sum('price_in_cents') / 100, 2) }}</th>
    </tr>
    </tfoot>
</table>