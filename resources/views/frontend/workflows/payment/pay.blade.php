@extends('templates.frontend')

@section('includes')
    <link href="{{ asset('css/card.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.card.js') }}"></script>
    <style>
        .card-wrapper {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .jp-card .jp-card-front, .jp-card .jp-card-back {
            background: black;
        }
    </style>
@stop

@section('content')
    @include('frontend.events.partials.snapshot', ['event' => $registration->event])
    @include('frontend.registrations.partials.snapshot')

    {!! Form::open(['route' => ['frontend.workflows.payment.post', $registration->uid, $registration->passcode], 'id' => 'payment_form']) !!}

        @include('frontend.payments.partials.form', compact('registration'))

        <div class="form-group">
            {!! Form::submit('Pay Now', ['id' => 'payment_submit_button', 'class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop