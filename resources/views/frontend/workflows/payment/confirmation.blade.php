@extends('templates.frontend')

@section('content')
	<?php $registration = $payment->registration; ?>

    <h2>Payment #{{ $payment->uid }}</h2>
    <p><strong>Amount:</strong> ${{ number_format($payment->amount_in_dollars, 2) }}</p>

    @include('frontend.events.partials.snapshot', ['event' => $payment->registration->event])
    @include('frontend.registrations.partials.snapshot', ['registration' => $payment->registration])
@stop