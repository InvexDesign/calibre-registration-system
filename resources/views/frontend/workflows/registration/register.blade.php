@extends('templates.frontend')

@section('content')
    @include('frontend.events.partials.snapshot')

    {!! Form::open(['url' => '', 'id' => 'registration_form']) !!}

        @include('frontend.registrations.partials.form')

        <div class="form-group">
            <div id="registration_recaptcha" class="g-recaptcha" data-sitekey="{{ Setting::get('recaptcha-site-key') }}"></div>
            {!! Form::submit('Register', ['id' => 'registration_submit_button', 'disabled' => 'true', 'class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop

@section('javascript')
<script>
    var verify_recaptcha_url = '{{ route('frontend.recaptcha.verify') }}';
    var recaptcha_site_key = '{{ Setting::get('recaptcha-site-key') }}';
    var recaptcha_success_input_name = 'recaptcha-success-token';
    var recaptcha_render_callbacks = [];

    var renderAllRecaptchas = function()
    {
        for(var i = 0; i < recaptcha_render_callbacks.length; i++)
        {
            recaptcha_render_callbacks[i]();
        }
    };

    var registration_form_id = 'registration_form';
    var registration_form_selector = '#' + registration_form_id;
    var registration_submit_button_selector = '#registration_submit_button';
    var registration_recaptcha_id = 'registration_recaptcha';
    var registration_form_action = "{{ route('frontend.workflows.registration.register.post', $event->uid) }}";
    var registration_form_action_fail = '{{ route('frontend.recaptcha.fail') }}';
    var registration_recaptcha_widget_id = '';//Leave this blank

    var renderRegistrationRecaptcha = function()
    {
        registration_recaptcha_widget_id = grecaptcha.render(registration_recaptcha_id, {
            'sitekey': recaptcha_site_key,
            'callback': verifyRegistrationRecaptcha,
            'theme': 'dark'
        });
    };
    recaptcha_render_callbacks.push(renderRegistrationRecaptcha);

    var verifyRegistrationRecaptcha = function(response)
    {
        $.post(verify_recaptcha_url,
            {'g-recaptcha-response': response},
            function(raw, textStatus, jqXHR)
            {
                var data = $.parseJSON(raw);
                if(data && data.success === true)
                {
                    console.log('You are not a robot!');

                    $(registration_form_selector).attr('action', registration_form_action);
                    $('<input>').attr({
                        type: 'hidden',
                        name: recaptcha_success_input_name,
                        value: '{{ Setting::get('recaptcha-success-token') }}'
                    }).appendTo(registration_form_selector);
                    $('#registration_recaptcha').addClass('hide');
                    log_registration_analytics();
                }
                else
                {
                    $(registration_form_selector).attr('action', registration_form_action_fail);
                    $(registration_form_selector).submit();
                }

                $(registration_submit_button_selector).attr('disabled', false);
//                grecaptcha.reset(registration_recaptcha_widget_id);
            }
        );
    };

    function log_registration_analytics()
    {
//        ga('send', {
//            hitType: 'event',
//            eventCategory: 'Subscriptions',
//            eventAction: 'registration',
//            eventLabel: email,
//            eventValue: 1
//        });
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=renderAllRecaptchas&render=explicit" async defer></script>
@stop