<?php $table_id = isset($table_id) ? $table_id : 'event_table'; ?>

<table id="{{ $table_id }}" class="datatable">
    <thead>
    <tr>
        <th>Date</th>
        <th>Class</th>
        <th>Location</th>
        <th>Sign Up</th>
    </tr>
    </thead>

    <tbody>
    @foreach($events as $event)
        <tr>
            <td>{{ $event->displayDate() }}</td>
            <td>{!! link_to_route('frontend.events.show', $event->displayClass(), [$event->uid]) !!}</td>
            <td>{{ $event->displayLocation() }}</td>
            <td>{!! link_to_route('frontend.workflows.registration.register.get', $event->is_private ? 'Private Class' : 'Register Here', [$event->uid]) !!}</td>
        </tr>
    @endforeach
    </tbody>

</table>

<script>
    $(document).ready(function()
    {
        var hidden_columns;
        @include('includes.js.hidden_columns', ['default_hidden_columns' => []])

        var table_id = '{{ $table_id }}';
        datatables[table_id] = init_datatable_i(table_id, {hiddenColumns: hidden_columns});

        @if(isset($has_filtering) && $has_filtering)
        $('.filter').change(function()
        {
            datatables[table_id].draw();
        });
        @endif
    });
</script>