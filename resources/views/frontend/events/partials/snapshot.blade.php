<h2>{{ $event->course->display() }}</h2>
@if($event->is_private)
    <p><strong>PRIVATE EVENT</strong></p>
@endif
<p><strong>Event Description:</strong><br />{{ $event->description }}</p>
<p><strong>Course Description:</strong><br />{{ $event->course->description }}</p>
<p><strong>Price:</strong><br />${{ number_format($event->price_in_dollars, 2) }}</p>
<p><strong>Start Date:</strong><br />{{ $event->start_at->format('M j, Y') }}</p>
@if($event->end_at)
    <p><strong>End Date:</strong><br />{{ $event->end_at->format('M j, Y') }}</p>
@endif
@if($event->deadline_at)
    <p><strong>Registration Deadline:</strong><br />{{ $event->deadline_at->format('M j, Y') }}</p>
@endif
<p><strong>Times:</strong><br />{!! $event->displayTimes() !!}</p>
<p><strong>Venue:</strong><br />{{ $event->venue->name }}<br />{!! $event->venue->address->display('<br />') !!}</p>
{!! $event->venue->embedded_map !!}
<br />