@extends('templates.frontend')

@section('page-title', isset($page_title) ? $page_title : 'Events Map')

@section('content')

    <style>
        .gm-style-iw a {
            display: block;
            margin-top: 10px;
            text-align: center;
        }
    </style>
    <div id="map" style="width: 800px; height: 600px;"></div>

    <script>
        var map, markers = [], markerCluster, spiderfier, infoWindow;
        var events = JSON.parse('{!! json_encode($markers) !!}');
        var mapMarkerBaseUrl = '{{ asset('images/maps') }}/';

        function renderContent(event)
        {
            return '<h4>' + event.course + '</h4>' +
                '<strong>Start Date:</strong> ' + event.start + '<br />' +
                '<strong>Location:</strong> ' + event.location + '<br />' +
                '<strong>Venue:</strong> ' + event.venue + '<br />' +
                '<a href="' + event.link + '">View Event</a>';
        }

        function initializeMap()
        {
            var center = new google.maps.LatLng({{ Setting::get('initial-map-latitude', 38.527542) }}, {{ Setting::get('initial-map-longitude', -98.050784) }});

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                maxZoom: 14,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            spiderfier = new OverlappingMarkerSpiderfier(map, {
                keepSpiderfied: true
            });

            infoWindow = new google.maps.InfoWindow();
            function closeInfoWindow() { infoWindow.close(); }
            google.maps.event.addListener(map, 'click', closeInfoWindow);

            spiderfier.addListener('format', function(marker, status) {
                var icon = status == OverlappingMarkerSpiderfier.markerStatus.SPIDERFIED ? 'marker-highlight.svg' :
                    status == OverlappingMarkerSpiderfier.markerStatus.SPIDERFIABLE ? 'marker-plus.svg' :
                        status == OverlappingMarkerSpiderfier.markerStatus.UNSPIDERFIABLE ? 'marker.svg' :
                            null;
                marker.setIcon({
                    url: mapMarkerBaseUrl + icon,
                    scaledSize: new google.maps.Size(23, 32)  // makes SVG icons work in IE
                });
            });

            for(var i = 0; i < events.length; i++)
            {
                (function() {  // make a closure over the marker and marker data
                    var event = events[i];
                    var position = new google.maps.LatLng(event.latitude, event.longitude);
                    var marker = new google.maps.Marker({
                        position: position,
                        title: event.title
                    });
                    marker.desc = event.display;
                    marker.addListener('click', function() {
                        infoWindow.setContent(renderContent(event));
                        infoWindow.open(map, marker);
                    });
                    markers.push(marker);
                    spiderfier.addMarker(marker);
                })();
            }
            markerCluster = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
//            markerCluster.setMaxZoom(7);
            markerCluster.setMaxZoom(6);
        }
    </script>
@stop

@section('javascript')

    <script src="{{ asset('js/markerclusterer.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ Setting::get('google-maps-api-key') }}&callback=initializeMap"></script>

@stop