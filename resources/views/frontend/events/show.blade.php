@extends('templates.frontend')

@section('content')

    <div>
        @include('frontend.events.partials.snapshot')

        {!! link_to_route('frontend.workflows.registration.register.get', 'Register Now', [$event->uid], ['class' => 'btn btn-primary']) !!}
    </div>

@stop