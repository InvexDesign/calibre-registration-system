@extends('templates.frontend')

@section('page-title', isset($page_title) ? $page_title : 'Events')

@section('content')

    @include('frontend.events.partials.table')

@stop