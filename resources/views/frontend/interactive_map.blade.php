@extends('templates.frontend')

@section('content')

    <script>
        function filterFacilities()
        {
            closeOpenInfoWindow();

            $('div.loader').show();

            console.log("filterFacilities");
            var data = {
                name: $('#name_filter').val(),
                feature_ids: $('#features_filter').val(),
            };
            var jqxhr = $.post("{{ route('frontend.filter') }}", data)
                .done(function(response)
                {
                    console.log("Success!");
                    var json = JSON.parse(response);
                    $.each(json.facilities, function(id, is_visible)
                    {
                        FACILITIES[id].marker.setVisible(is_visible)
                        $('#facility_list_' + id).toggle(is_visible);
                    });
                }).fail(function(response)
                {
                    console.error("ERROR!");
                    console.error(response);
                }).always(function()
                {
                    $('div.loader').hide();
                });
        }

        $(document).ready(function()
        {
            $('.select-two-custom').select2({
                placeholder: 'All'
            });

            $('.filter').change(function ()
            {
                filterFacilities();
            });
        });
    </script>

    {{-- TEXT: Search --}}
    {{-- MULTISELECT: Services (AND logic) --}}
    <div class="search-pane">
        <div class="search-parameter" style="flex: 1;">
            <h4>Name</h4>
			<?php echo Form::text('name', null, ['id' => 'name_filter', 'class' => 'filter form-control']); ?>
        </div>
        <div class="search-parameter" style="flex: 2;">
            <h4>Services</h4>
			<?php echo Form::select('feature_ids[]', $feature_options, null, ['id' => 'features_filter', 'multiple' => true, 'class' => 'filter select-two-custom form-control']); ?>
        </div>
    </div>
    <div class="results-pane">
        <div id="map_view" class="map-view">
            <?php $process_facility_click_url = route('frontend.process_facility_click'); ?>
            @include('frontend.partials.map', compact('facilities', 'process_facility_click_url'))
        </div>
        <div id="list_view" class="list-view">
            <?php $APP_URL_BASE = url('/'); ?>
            @include('frontend.partials.list', compact('facilities', 'APP_URL_BASE'))
        </div>
    </div>
    <div class="lightboxes">
        @foreach($facilities as $facility)
            @include('frontend.partials.lightbox')
        @endforeach
    </div>
@stop