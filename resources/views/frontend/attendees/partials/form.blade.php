<div id="attendee_form_{{ $i }}" style="display: none;">
    <h4>Attendee #{{ ($i + 1) }}</h4>
    <div class="field-container col-2">
        <div class="field form-group">
            {!! Form::label("attendees[$i][first_name]", 'First Name *') !!}
            {!! Form::text("attendees[$i][first_name]", isset($attendees) ? (isset($attendees[$i]) ? $attendees[$i]->first_name : null): null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label("attendees[$i][last_name]", 'Last Name *') !!}
            {!! Form::text("attendees[$i][last_name]", isset($attendees) ? (isset($attendees[$i]) ? $attendees[$i]->last_name : null): null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label("attendees[$i][email]", 'Email') !!}
            {!! Form::text("attendees[$i][email]", isset($attendees) ? (isset($attendees[$i]) ? $attendees[$i]->email : null): null, ['class'=>'email-mask form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label("attendees[$i][rank]", 'Rank') !!}
            {!! Form::text("attendees[$i][rank]", isset($attendees) ? (isset($attendees[$i]) ? $attendees[$i]->rank : null): null, ['class'=>'form-control']) !!}
        </div>
        <div class="field form-group">
            {!! Form::label("attendees[$i][pid]", 'PID/POST (Badge or ID Number)') !!}
            {!! Form::text("attendees[$i][pid]", isset($attendees) ? (isset($attendees[$i]) ? $attendees[$i]->pid : null): null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>