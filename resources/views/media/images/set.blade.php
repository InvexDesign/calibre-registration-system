@extends('templates.master')

@section('content-header')
    <div class="header-content">
        <h2>
            <i class="fa fa-{{ Setting::get('item-fa-icon', 'cube') }}"></i>
            {{ isset($page_title) ? $page_title : 'Set Image' }}
            <span>{{ isset($page_description) ? $page_description : 'Upload a new image' }}</span>
        </h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            {!! $breadcrumbs !!}
        </div>
    </div>
@stop

@section('content')

@stop