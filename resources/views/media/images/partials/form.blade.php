
<div class="col-xs-6">
    {!! Form::label('image', 'Image') !!}
    <div class="input-group">
            <span class="input-group-btn">
                <span class="btn btn-primary btn-file">
                    Browse… {!! Form::file('image', ['onchange' => 'readURL(this);']) !!}
                </span>
            </span>
        <input id="for_image" type="text" class="form-control" readonly="">
    </div>
    {!! $errors->first('image', '<span class="error">:message</span>') !!}
</div>

@if(isset($image_resource_path))
    <div class="col-xs-3">
        <h4>Current Image</h4>
        <img src="{{ asset($image_resource_path) }}" width="250" />
    </div>
@endif

<div class="col-xs-3">
    <h4>New Image Preview</h4>
    <img id="{{ isset($image_preview_id) ? $image_preview_id : 'image_preview' }}" src="" width="250" />
</div>
<div class="clearer"></div>

<script>
    function readURL(input)
    {
        if(input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function (e)
            {
                $('#{{ isset($image_preview_id) ? $image_preview_id : 'image_preview' }}').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>