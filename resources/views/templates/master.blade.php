<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">--}}

    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic" rel="stylesheet" type="text/css">
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">--}}

    <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/footer.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendar-display.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    {{--<link href="/css/style.css?v={{ time() }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/footer.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
{{--    <link href="{{ asset('css/pills.css') }}" rel="stylesheet" media="all">--}}
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/breadcrumbs.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select-two-custom.css') }}" rel="stylesheet">
    <link href="{{ asset('cazary/themes/flat/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tympanus-file-input.css') }}" rel="stylesheet">
    <link href="{{ asset('css/featherlight.min.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('css/calendar-display.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/jquery.timepicker.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/fullcalendar.min.css') }}" rel="stylesheet">--}}

    <style>
        ul#sidebar li a {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
    </style>

    <script src="{{ asset('js/jquery.min.js') }}"></script>

    @yield('includes')
</head>

@yield('style')

<body class="dark">
<aside id="sidebar" class="sidebar">
    @include('backend._templates.' . Auth::user()->getTemplate() . '.sidebar')
</aside>
<section id="wrapper" class="wrapper">
    <section id="header_wrapper" class="header-wrapper">
        <header>
            @include('backend._templates.' . Auth::user()->getTemplate() . '.header')
        </header>
    </section>
    <section id="content_wrapper" class="content-wrapper">
        <section id="content" class="content">
            @yield('content-header')
            @include('backend._includes.messages')
            @yield('content')
        </section>
        <div class="clearer"></div>
    </section>
    <section id="footer_wrapper" class="footer-wrapper">
        <footer>
            @include('backend._templates.footer')
        </footer>
    </section>
</section>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
{{--<script src="{{ asset('js/jquery.dataTables.rowGrouping.js') }}"></script>--}}
<script src="{{ asset('js/datatables_utilities.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.bundle.min.js') }}"></script>
{{--<script src="{{ asset('js/jquery.inputmask.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/jquery.inputmask.numeric.extensions.js') }}"></script>--}}
{{--<script src="{{ asset('js/jquery.inputmask.regex.extensions.js') }}"></script>--}}
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/tympanus-file-input.js') }}"></script>
{{--<script src="{{ asset('js/lity.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>--}}
<script src="{{ asset('cazary/cazary.min.js') }}"></script>
<script src="{{ asset('js/featherlight.min.js') }}"></script>
{{--<script src="{{ asset('plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
{{--<script src="{{ asset('js/fullcalendar.min.js') }}"></script>--}}

<script src="{{ asset('js/helpers.js') }}"></script>
<script src="{{ asset('js/utilities.js') }}"></script>
{{--<script src="{{ asset('js/custom_date_range.js') }}"></script>--}}
{{--<script src="{{ asset('js/numeral.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/notifications.js') }}"></script>--}}
{{--<script src="{{ asset('js/jstz.min.js') }}"></script>--}}

@yield('javascript')

<script>
    var datatables = [];

    function initializeInputs(scope)
    {
        if(typeof scope === 'undefined')
        {
            scope = '';
        }

        /* TODO: Cazary */
        $(scope + "textarea.cazary:not(.cazary-source)").cazary({
            commands: "FULL"
        });

//        $(scope + ".integer-mask").inputmask("integer", {rightAlign: false});
        $(scope + '.email-mask').inputmask("email");
        $(scope + ".decimal-mask").inputmask("decimal", {rightAlign: false});
        $(scope + '.phone-mask').mask("(999) 999-9999?x99999");
        $(scope + '.zipcode-mask').mask("99999");

        $(scope + ".integer-mask").inputmask("decimal", {
            groupSeparator: ",",
            digits: 0,
            autoGroup: true,
            rightAlign: false
        });

        $(scope + ".positive-integer-mask").inputmask("decimal", {
            groupSeparator: ",",
            digits: 0,
            autoGroup: true,
            rightAlign: false,
            allowMinus: false
        });

        $(scope + ".dollar-mask").inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            rightAlign: false,
            prefix: '$'
        });

        $(scope + '.percent-mask').mask("99.99%");

//        $(scope + '.color-picker').colorpicker({});

        $(scope + ".datepicker").datepicker().mask("99/99/9999", {placeholder: "mm/dd/yyyy"});
        $(scope + ".datepicker-dash").datepicker({dateFormat: "mm-dd-yy"}).mask("99-99-9999", {placeholder: "mm-dd-yyyy"});
//        $(scope + ".datepicker-dash").datepicker().mask("99/99/9999", {placeholder: "mm/dd/yyyy"});

        $(scope + '.zipcode-mask').each(function()
        {
            $(this).mask("99999").blur(forceValidZipcode);
        });

        $(scope + '.select-two').select2();
        // var readonly_select_twos = $(scope + '.select-two[readonly]');
        // if(readonly_select_twos.length > 0)
        // {
        //     readonly_select_twos.select2("readonly", true);
        // }

        $('.select-two-tags').select2({
            tags: true,
            createTag: function (params) {
                return {
                    id: params.term,
                    text: params.term,
                    newOption: true
                }
            }
        });
    }
    $(document).ready(function()
    {
//        $('#sidebar .shutter').show().animate({left: "-180px"}, 500, function(){ $('#sidebar .shutter').hide(); });

        init__pillGroups();

        initializeInputs();

        $(".btn-file :file").change(function()
        {
            var input = $(this);
            var filename = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            $('#for_' + input.attr('name')).val(filename);
        });

        var hash = window.location.hash;
        if(hash && $(hash).length > 0)
        {
            $('.nav-tabs a[href="' + hash + '"]').tab('show');
        }
    });
</script>

</body>
</html>