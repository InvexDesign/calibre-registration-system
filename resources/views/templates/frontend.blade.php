<!doctype html>
<html>
<head>
    @yield('meta')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        .content {
            width: 100%;
        }
    </style>
    @include('frontend.includes.head')

    @yield('includes')
    @yield('style')
    @yield('recaptcha')
</head>

<body>
<header class="header">
    @include('frontend.partials.header')
</header>

<div class="content" id="content">
    @include('includes.messages')

    @yield('content')
</div>

<footer>
    @include('frontend.partials.footer')
</footer>
@include('frontend.includes.foot')
@yield('javascript')
</body>
</html>