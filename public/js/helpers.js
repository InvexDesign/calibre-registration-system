function roundTo(number_raw, precision_raw)
{
    var number = parseFloat(number_raw);
    var precision = parseInt(precision_raw);

    var multiplier_str = '1';
    for(var i = 0; i < precision; i++)
    {
        multiplier_str += '0';
    }

    var multiplier = parseInt(multiplier_str);

    return parseFloat(Math.round(number * multiplier) / multiplier).toFixed(precision);
}

function preDelete(model_name, delete_button_id)
{
    var question = "Are you sure you want to delete this " + model_name + "?";
    if(confirm(question))
    {
        $('#' + delete_button_id).click();
    }
}

function strstr(haystack, needle, bool)
{
    var pos = 0;

    haystack += '';
    pos = haystack.indexOf(needle);
    if(pos == -1)
    {
        return false;
    }
    else
    {
        if(bool)
        {
            return haystack.substr(0, pos);
        }
        else
        {
            return haystack.slice(pos);
        }
    }
}

function disableFormInputs(form_selector)
{
    $(form_selector).find('input:not(.updatable)').prop('disabled', true);
    $(form_selector).find('select:not(.updatable)').prop('disabled', true);
    $(form_selector).find('textarea:not(.updatable)').prop('disabled', true);
    $(form_selector).find('input[type="hidden"]').prop('disabled', false);
    //setTimeout(function() { $(form_selector).find('input[name="_token"]').prop('disabled', false); }, 100);
}

function parseFloatFromSortableDollarValue(raw_value)
{
    var value = 0;
    if(typeof raw_value === 'string')
    {
        value = parseFloat(raw_value.replace(/<span.*<\/span>/, '').replace(/[\$,]/g, ''));
    }
    else if(typeof raw_value === 'number')
    {
        value = raw_value;
    }

    return value;
}

function parseFloatFromSortablePercentValue(raw_value)
{
    var value = 0;
    if(typeof raw_value === 'string')
    {
        value = parseFloat(raw_value.replace(/<span.*<\/span>/, '').replace(/[^\d\.]/g, ''));
    }
    else if(typeof raw_value === 'number')
    {
        value = raw_value;
    }

    return value;
}

function intVal(raw_value)
{
    return typeof raw_value === 'string' ?
        parseInt(raw_value.replace(/[\$,]/g, '')) :
        typeof raw_value === 'number' ?
            raw_value : 0;
}

function intValFromStringWithParentheses(raw_value)
{
    if(typeof raw_value === "string")
    {
        var start = raw_value.lastIndexOf('>') + 1;
        var end = raw_value.indexOf('(') - 1;
        raw_value = raw_value.substr(start, end - start);
        raw_value = raw_value.replace(/[^\d]/g, '');
        raw_value = intVal(raw_value);
    }
    
    return raw_value;
}

function generateSlug(string)
{
    var stripped = string.replace(/[^A-Za-z0-9\s]/g, '');
    var dashed = stripped.replace(/\s+/g, '-');

    return dashed.toLowerCase();
}

function number_format(number, decimals, dec_point, thousands_sep)
{
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec)
        {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if(s[0].length > 3)
    {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if((s[1] || '').length < prec)
    {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function readURL(input, preview_id)
{
    if(input.files && input.files[0])
    {
        var reader = new FileReader();

        reader.onload = function (e)
        {
            $('#' + preview_id).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}