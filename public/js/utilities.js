function init__pillGroups()
{
    $('.pill-group .pill').click(function()
    {
        var selected_pill = $(this);
        selected_pill.siblings('.pill').removeClass('selected');
        selected_pill.addClass('selected');

        var value = selected_pill.data('value');
        selected_pill.closest('.pill-group').find('input').val(value).trigger('change');
    });
}
function limitCharacters(element, raw_limit)
{
    var limit = parseInt(raw_limit);
    var string = $(element).val();
    if(string.length > limit)
    {
        var trimmed = string.substring(0, limit);
        $(element).val(trimmed);
    }
}
function forceValidZipcode()
{
    var lastFour = $(this).val().substr(6, 4);
    if(lastFour != "")
    {
        if(lastFour.length != 4)
        {
            $(this).val("");
        }
    }
}
// function initializeInputs(scope)
// {
//     if(typeof scope === 'undefined')
//     {
//         scope = '';
//     }
//
//     $(scope + "textarea.cazary:not(.cazary-source)").cazary({
//         commands: "FULL"
//     });
//
//     $(scope + ".integer-mask").inputmask("integer", {rightAlign: false});
//     $(scope + ".decimal-mask").inputmask("decimal", {rightAlign: false});
//     $(scope + '.phone-mask').mask("999-999-9999?x99999");
//
//     $(scope + ".integer-mask").inputmask("decimal", {
//         groupSeparator: ",",
//         digits: 0,
//         autoGroup: true,
//         rightAlign: false
//     });
//
//     $(scope + ".positive-integer-mask").inputmask("decimal", {
//         groupSeparator: ",",
//         digits: 0,
//         autoGroup: true,
//         rightAlign: false,
//         allowMinus: false
//     });
//
//     $(scope + ".dollar-mask").inputmask("decimal", {
//         radixPoint: ".",
//         groupSeparator: ",",
//         digits: 2,
//         autoGroup: true,
//         rightAlign: false,
//         prefix: '$'
//     });
//
//     $(scope + '.color-picker').colorpicker({});
//
//     $(scope + ".datepicker").datepicker().mask("99/99/9999", {placeholder: "mm/dd/yyyy"});
//
//     $(scope + '.zipcode-mask').each(function()
//     {
//         $(this).mask("99999").blur(forceValidZipcode);
//     });
//
//     $(scope + '.select-two').select2();
//     // var readonly_select_twos = $(scope + '.select-two[readonly]');
//     // if(readonly_select_twos.length > 0)
//     // {
//     //     readonly_select_twos.select2("readonly", true);
//     // }
//
//     $('.select-two-tags').select2({
//         tags: true,
//         createTag: function (params) {
//             return {
//                 id: params.term,
//                 text: params.term,
//                 newOption: true
//             }
//         }
//     });
// }