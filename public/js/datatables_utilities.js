function init_datatables(table_ids)
{
    if(typeof table_ids !== 'undefined' && table_ids.constructor === Array)
    {
        for(var i = 0; i < table_ids.length; i++)
        {
            init_datatable(table_ids[i]);
        }
    }
    else
    {
        var datatables = $('.datatable:not(.dataTable)');
        for(var i = 0; i < datatables.length; i++)
        {
            var table_id = datatables[i].id;
            if(table_id == '')
            {
                alert('init_datatables: Table missing ID, cannot initialize!');
                console.error('init_datatables: Table missing ID, cannot initialize!');
                console.error(datatables[i]);

                continue;
            }

            init_datatable(table_id);
        }
    }

    return;
}

function init_datatable(table_id, hasToggles, hiddenColumns, displayLength, footer_callback)
{
    if(typeof table_id === 'undefined')
    {
        alert('init_datatables: table_id parameter is required!');
        return;
    }

    var table_selector = '#' + table_id;
    if($(table_selector).length <= 0)
    {
        console.error('init_datatable: Skipped table#' + table_id + ', no table found!');
        return;
    }

    if($(table_selector).hasClass('dataTable'))
    {
        console.log('init_datatable: Skipped table#' + table_id + ', already initialized.');
        return $(table_selector);
    }

    if(typeof hasToggles === 'undefined')
    {
        hasToggles = true;
    }

    if(typeof hiddenColumns === 'undefined')
    {
        hiddenColumns = [];
    }

    if(typeof displayLength === 'undefined')
    {
        displayLength = 25;
    }

    var datatable;
    if(typeof footer_callback === 'undefined')
    {
        datatable = $(table_selector).DataTable(
            {
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                displayLength: displayLength
            }
        );
    }
    else
    {
        datatable = $(table_selector).DataTable(
            {
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                displayLength: displayLength,
                footerCallback: footer_callback
            }
        );
    }

    if(hiddenColumns.length > 0)
    {
        datatable.columns().every(function()
        {
            if($.inArray(this.header().innerHTML, hiddenColumns) >= 0)
            {
                this.visible(false);
            }
        });
    }

    if(hasToggles === true)
    {
        var columnCount = 0;
        var toggleHtml =
        '<div class="clearer"></div>' +
        '<div id="' + table_id + '_toggle_collapse" class="toggle-collapse">' +
            '<h3>Click Here To Enable/Disable Columns</h3>' +
            '<ul id="' + table_id + '_column_toggles" class="column-toggles">';

        datatable.columns().every(function()
        {
            if(this.header().innerHTML.indexOf('<') < 0)
            {
                var checked = ($.inArray(this.header().innerHTML, hiddenColumns) >= 0) ? '' : 'checked="checked"';

                toggleHtml +=
                    '<li>' +
                        '<div class="checkbox">' +
                            '<label><input type="checkbox" value="' + columnCount + '" ' + checked + ' />' + this.header().innerHTML + '</label>' +
                        '</div>' +
                    '</li>';
            }

            columnCount++;
        });
        toggleHtml +=
            '</ul>' +
        '</div>';
        $('#' + table_id + '_column_toggles').html(toggleHtml);

        $(table_selector).before(toggleHtml);
        $('#' + table_id + '_toggle_collapse').accordion({
            collapsible: true,
            active: false,
            heightStyle: "content"
        });

        $('.toggle-collapse .column-toggles input[type="checkbox"]').change(function(e)
        {
            var column_index = parseInt($(this).val());
            var column = datatable.column(column_index);
            column.visible(!column.visible());
        });
    }

    return datatable;
}

function init_datatable_i(table_id, options)
{
    if(typeof table_id === 'undefined')
    {
        alert('init_datatables: table_id parameter is required!');
        return;
    }

    var table_selector = '#' + table_id;
    if($(table_selector).length <= 0)
    {
        console.error('init_datatable: Skipped table#' + table_id + ', no table found!');
        return;
    }

    if($(table_selector).hasClass('dataTable'))
    {
        console.log('init_datatable: Skipped table#' + table_id + ', already initialized.');
        return $(table_selector);
    }

    var has_options = (typeof options !== 'undefined');

    hasToggles = true;
    if(has_options && typeof options.hasToggles !== 'undefined')
    {
        hasToggles = options.hasToggles;
    }

    hiddenColumns = [];
    if(has_options && typeof options.hiddenColumns !== 'undefined')
    {
        hiddenColumns = options.hiddenColumns;
    }

    displayLength = 25;
    if(has_options && typeof options.displayLength !== 'undefined')
    {
        displayLength = options.displayLength;
    }

    var datatable;
    var datatable_options = {
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        displayLength: displayLength,
    };

    if(has_options && typeof options.sortOrder !== 'undefined')
    {
        datatable_options.order = options.sortOrder;
    }

    if(has_options && typeof options.dom !== 'undefined')
    {
        datatable_options.dom = options.dom;
    }

    if(has_options && typeof options.paging !== 'undefined')
    {
        datatable_options.paging = options.paging;
    }

    var is_using_footer_callback = has_options && typeof options['footer_callback'] !== 'undefined';
    if(is_using_footer_callback)
    {
        datatable_options.footerCallback = options.footer_callback;
    }

    var is_grouping_rows = has_options && typeof options['row_grouping'] !== 'undefined';
    if(is_grouping_rows)
    {
        datatable_options.bLengthChange = false;
        datatable_options.bPaginate = false;
        delete datatable_options.lengthMenu;
        delete datatable_options.displayLength;

        //datatable = $(table_selector).DataTable(datatable_options).rowGrouping(options.row_grouping);
        //datatable = datatable.rowGrouping()



        //datatable = $(table_selector).DataTable(datatable_options);
        //$(table_selector).dataTable().rowGrouping(options.row_grouping)
        datatable = $(table_selector).dataTable(datatable_options).rowGrouping(options.row_grouping)
    }
    else
    {
        datatable = $(table_selector).DataTable(datatable_options);
    }

    if(hiddenColumns.length > 0)
    {
        datatable.columns().every(function()
        {
            if($.inArray(this.header().innerHTML, hiddenColumns) >= 0)
            {
                this.visible(false);
            }
        });
    }

    if(hasToggles === true)
    {
        var columnCount = 0;
        var toggleHtml =
        '<div class="clearer"></div>' +
        '<div id="' + table_id + '_toggle_collapse" class="toggle-collapse">' +
            '<h3>Click Here To Enable/Disable Columns</h3>' +
            '<ul id="' + table_id + '_column_toggles" class="column-toggles">';

        datatable.columns().every(function()
        {
            if(this.header().innerHTML.indexOf('<') < 0)
            {
                var checked = ($.inArray(this.header().innerHTML, hiddenColumns) >= 0) ? '' : 'checked="checked"';

                toggleHtml +=
                    '<li>' +
                        '<div class="checkbox">' +
                            '<label><input type="checkbox" value="' + columnCount + '" ' + checked + ' />' + this.header().innerHTML + '</label>' +
                        '</div>' +
                    '</li>';
            }

            columnCount++;
        });
        toggleHtml +=
            '</ul>' +
        '</div>';
        $('#' + table_id + '_column_toggles').html(toggleHtml);

        $(table_selector).before(toggleHtml);
        $('#' + table_id + '_toggle_collapse').accordion({
            collapsible: true,
            active: false,
            heightStyle: "content"
        });

        $('.toggle-collapse .column-toggles input[type="checkbox"]').change(function(e)
        {
            var column_index = parseInt($(this).val());
            var column = datatable.column(column_index);
            column.visible(!column.visible());
        });
    }

    return datatable;
}

function getSortableDollarColumnTotal(column)
{
    var raw_total = 0;
    if(column.data().length != 0)
    {
        raw_total = column.data().reduce
        (
            function(a, b)
            { return parseFloatFromSortableDollarValue(a) + parseFloatFromSortableDollarValue(b); }
        );
    }

    var cleaned_total = parseFloatFromSortableDollarValue(raw_total);
    return numeral(cleaned_total).format('$0,0.00');
}

function getSortablePercentColumnTotal(column)
{
    var raw_total = 0;
    if(column.data().length != 0)
    {
        raw_total = column.data().reduce
        (
            function(a, b)
            { return parseFloatFromSortablePercentValue(a) + parseFloatFromSortablePercentValue(b); }
        );
    }

    var cleaned_total = parseFloatFromSortableDollarValue(raw_total);
    return numeral(cleaned_total).format('0,0.00000') + '%';
}

function getSlashPercentColumnTotal(column)
{
    var raw_total = '0 / 0 (0% / 0%)';
    if(column.data().length != 0)
    {
        raw_total = column.data().reduce(function(a, b)
        {
            var a_matches = a.replace(/[^0-9\/(]/g, '').match(/^(\d+)\/(\d+).*$/);
            var b_matches = b.replace(/[^0-9\/(]/g, '').match(/^(\d+)\/(\d+).*$/);
            var a_1 = parseInt(a_matches[1]);
            var a_2 = parseInt(a_matches[2]);
            var b_1 = parseInt(b_matches[1]);
            var b_2 = parseInt(b_matches[2]);

            var c_1 = a_1 + b_1;
            var c_2 = a_2 + b_2;
            var total = c_1 + c_2;

            var c_1p = 0;
            var c_2p = 0;
            if(total > 0)
            {
                c_1p = number_format(c_1 / total * 100, 0);
                c_2p = number_format(c_2 / total * 100, 0);
            }

            return c_1 + ' / ' + c_2 + ' (' + c_1p + '% / ' + c_2p + '%)';
        });
    }

    return raw_total;
}

function getSortableIntegerColumnTotal(column)
{
    var raw_total = 0;
    if(column.data().length != 0)
    {
        raw_total = column.data().reduce
        (
            function(a, b)
            {
                return intVal(a) + intVal(b);
            }
        );
    }

    var integer = intVal(raw_total);

    return number_format(integer, 0);
}

function getIntegerWithParenthesesColumnTotal(column)
{
    var raw_total = 0;
    if(column.data().length != 0)
    {
        raw_total = column.data().reduce
        (
            function(a, b)
            {
                return intValFromStringWithParentheses(a) + intValFromStringWithParentheses(b);
            }
        );
    }

    var integer = intVal(raw_total);

    return number_format(integer, 0);
}
