<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/* If there was an error during "up()", the migration will not have completed
		and will NOT run a "down()" before running "up()" which will error out
		since each table before the error will already exist.
		Use this to clear all tables beforehand.
		*/
		$this->down();

		Schema::create('password_resets', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at')->nullable();
		});

		Schema::create('users', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('role_id')->unsigned()->nullable();
			$table->foreign('role_id')->references('id')->on('roles');
			$table->string('username')->unique();
			$table->string('email');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('password');
			$table->dateTime('password_updated_at')->nullable();
			$table->boolean('is_active')->default(true);
			$table->rememberToken();
			$table->nullableTimestamps();
		});

		Schema::create('notes', function (Blueprint $table)
		{
			$table->increments('id');
			$table->text('content');
			$table->morphs('noteable');
			$table->integer('author_id')->unsigned()->nullable();
			$table->foreign('author_id')->references('id')->on('users');
			$table->timestamps();
		});

		Schema::create('settings', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('key')->unique();
			$table->text('value')->nullable();
			$table->string('type', 25)->default('string');
			$table->text('description')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users');
			$table->nullableTimestamps();
		});

		Schema::create('states', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('short_name');
			$table->nullableTimestamps();
		});

		Schema::create('addresses', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('line1');
			$table->string('line2')->nullable();
			$table->string('city');
			$table->integer('state_id')->unsigned();
			$table->foreign('state_id')->references('id')->on('states');
			$table->string('zipcode');
			$table->string('country');
			$table->timestamps();
		});

		Schema::create('lodgings', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('contact')->nullable();
			$table->text('embedded_map')->nullable();
			$table->integer('address_id')->unsigned();
			$table->foreign('address_id')->references('id')->on('addresses');
			$table->text('description')->nullable();
//			$table->text('notes')->nullable();
			$table->timestamps();
		});

		Schema::create('courses', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('short_name')->nullable();
			$table->text('description')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('venues', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->text('embedded_map')->nullable();
			$table->integer('address_id')->nullable();
			$table->foreign('address_id')->references('id')->on('addresses');
			$table->boolean('auto_gps')->default(false);
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('events', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->integer('venue_id')->nullable();
			$table->foreign('venue_id')->references('id')->on('venues');
			$table->string('state')->nullable();
			$table->integer('course_id')->nullable();
			$table->foreign('course_id')->references('id')->on('courses');
			$table->integer('sales_representative_id')->nullable();
			$table->foreign('sales_representative_id')->references('id')->on('users');
			$table->string('timezone')->nullable();
			$table->dateTime('start_at')->nullable();
			$table->dateTime('end_at')->nullable();
			$table->dateTime('deadline_at')->nullable();
			$table->text('times')->nullable();
			$table->integer('ticket_limit')->nullable();
			$table->integer('price_in_cents')->nullable();
			$table->integer('revenue_goal')->nullable();
			$table->dateTime('published_at')->nullable();
			$table->dateTime('settled_at')->nullable();
			$table->boolean('is_private')->default(false);
			$table->text('description')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('event_lodging', function (Blueprint $table)
		{
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events');
			$table->integer('lodging_id')->unsigned();
			$table->foreign('lodging_id')->references('id')->on('lodgings');
			$table->timestamps();
		});

		Schema::create('department_sizes', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->nullableTimestamps();
		});

		Schema::create('registrations', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->integer('event_id')->nullable();
			$table->foreign('event_id')->references('id')->on('events');
			$table->string('agency')->nullable();
			$table->string('contact')->nullable();
			$table->string('email')->nullable();
			$table->string('po_number')->nullable();
			$table->integer('department_size_id')->nullable();//TODO: Change this to string to decouple id? Lose analytics/standardization?
			$table->foreign('department_size_id')->references('id')->on('department_sizes');
			$table->string('phone')->nullable();
			$table->string('fax')->nullable();
			$table->integer('address_id')->nullable();
			$table->foreign('address_id')->references('id')->on('addresses');
			$table->string('passcode')->nullable();
			$table->string('payment_status');//TODO: foreign key?
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('attendees', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->integer('event_id')->nullable();
			$table->foreign('event_id')->references('id')->on('events');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email')->nullable();
			$table->string('rank')->nullable();
			$table->string('pid')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('tickets', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->integer('event_id')->nullable();
			$table->foreign('event_id')->references('id')->on('events');
			$table->integer('registration_id')->nullable();
			$table->foreign('registration_id')->references('id')->on('registrations');
			$table->integer('attendee_id')->nullable();
			$table->foreign('attendee_id')->references('id')->on('attendees');
			$table->integer('price_in_cents')->nullable();
			$table->nullableTimestamps();
		});

		//TODO: Add nullable discount_id?
		Schema::create('payments', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('class')->nullable();//TODO: STI needed?
			$table->string('uid')->nullable();
			$table->string('eid')->nullable();
			$table->integer('registration_id')->nullable();
			$table->foreign('registration_id')->references('id')->on('registrations');
			$table->integer('amount_in_cents')->nullable();
			$table->dateTime('paid_at')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('discounts', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('class')->nullable();//TODO: STI needed?
			$table->string('name')->nullable();//TODO: STI needed?
			$table->text('code')->nullable();//TODO: STI needed?
			$table->integer('value')->nullable();
			$table->dateTime('start_at')->nullable();
			$table->dateTime('end_at')->nullable();
			$table->boolean('is_active')->nullable();
			$table->integer('max_uses')->nullable();
			$table->integer('auto_attendee_min')->nullable();
//			$table->text('notes')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('login_attempts', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->string('username')->nullable();
			$table->string('ip_address')->nullable();
			$table->dateTime('timestamp');
			$table->boolean('success')->nullable();
			$table->string('country')->nullable();
			$table->string('region')->nullable();
			$table->string('city')->nullable();
			$table->string('postal')->nullable();
			$table->decimal('latitude', 10, 7)->nullable();
			$table->decimal('longitude', 10, 7)->nullable();
			$table->string('organization')->nullable();
			$table->string('hostname')->nullable();
			$table->string('browser')->nullable();
			$table->string('referrer')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('analytics__events', function (Blueprint $table)
		{
			$table->increments('id')->unsigned()->index();
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events');
			$table->integer('venue_id')->unsigned();
			$table->foreign('venue_id')->references('id')->on('venues');
			$table->integer('state_id')->unsigned();
			$table->foreign('state_id')->references('id')->on('states');
			$table->dateTime('start_at')->nullable();
			$table->dateTime('end_at')->nullable();
			$table->bigInteger('tickets')->unsigned();
			$table->integer('revenue_in_cents')->nullable();
			$table->nullableTimestamps();
		});

		Schema::create('notifications', function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->default('SYSTEM');
			$table->string('class');
			$table->string('description');
			$table->integer('model_id')->unsigned()->nullable();
			$table->dateTime('remind_at')->nullable();
			$table->boolean('is_severe')->default(false);
			$table->boolean('is_email')->default(true);
			$table->boolean('is_active')->default(true);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::dropIfExists('notifications');
		Schema::dropIfExists('analytics__events');
		Schema::dropIfExists('login_attempts');
		Schema::dropIfExists('discounts');
		Schema::dropIfExists('payments');
		Schema::dropIfExists('tickets');
		Schema::dropIfExists('attendees');
		Schema::dropIfExists('registrations');
		Schema::dropIfExists('department_sizes');
		Schema::dropIfExists('event_lodging');
		Schema::dropIfExists('events');
		Schema::dropIfExists('venues');
		Schema::dropIfExists('courses');
		Schema::dropIfExists('lodgings');
		Schema::dropIfExists('addresses');
		Schema::dropIfExists('states');
		Schema::dropIfExists('settings');
		Schema::dropIfExists('notes');
		Schema::dropIfExists('users');
		Schema::dropIfExists('password_resets');

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
