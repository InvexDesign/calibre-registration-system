<?php

use App\Models\Address;
use App\Models\Lodging;
use App\Models\State;
use App\Models\User;
use App\Models\Venue;
use App\Role;
use Illuminate\Database\Seeder;

class RandomSalesRepresentativeSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$sales_representative_role = Role::get('sales-representative', true);

		$is_local_environment = config('app.env') == 'local';
		$password = $is_local_environment ? 'password' : '3fuioewh983g72fe98hqwed';

		for($i = 0; $i < 10; $i++)
		{
			$user = User::create([
				'role_id'    => $sales_representative_role->id,
				'username'   => 'sales-rep-' . ($i + 1),
				'password'   => $password,
				'first_name' => $faker->firstName,
				'last_name'  => $faker->lastName,
				'email'      => $faker->email
			]);
		}
	}
}
