<?php

use App\Models\Payments\Discounts\DollarDiscount;
use App\Models\Payments\Discounts\PercentDiscount;
use Illuminate\Database\Seeder;

class RandomDiscountSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		for($i = 0; $i < 10; $i++)
		{
			PercentDiscount::create([
				'name'              => $faker->word . ' ' . $faker->word,
				'code'              => $faker->lexify('????????????'),
				'value'             => $faker->numberBetween(1, 5) * 1000,
				'start_at'          => $faker->boolean() ? $faker->dateTime : null,
				'end_at'            => $faker->boolean() ? $faker->dateTime : null,
				'is_active'         => $faker->boolean(),
				'max_uses'          => $faker->boolean() ? $faker->numberBetween(10, 100) : null,
				'auto_attendee_min' => $faker->boolean() ? 10 : null,
//				'notes'             => $faker->sentence(),
			]);
		}

		for($i = 0; $i < 10; $i++)
		{
			DollarDiscount::create([
				'name'              => $faker->word . ' ' . $faker->word,
				'code'              => $faker->lexify('????????????'),
				'value'             => $faker->numberBetween(1, 5) * 1000,
				'start_at'          => $faker->boolean() ? $faker->dateTime : null,
				'end_at'            => $faker->boolean() ? $faker->dateTime : null,
				'is_active'         => $faker->boolean(),
				'max_uses'          => $faker->boolean() ? $faker->numberBetween(10, 100) : null,
				'auto_attendee_min' => $faker->boolean() ? 10 : null,
//				'notes'             => $faker->sentence(),
			]);
		}
	}
}
