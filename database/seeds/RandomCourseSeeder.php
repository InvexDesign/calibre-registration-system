<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class RandomCourseSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$course_options = [
			'Guns',
			'Bros',
			'Belts',
			'Body Cams',
			'Slippers',
			'Stevedores',
			'Longshoremen',
			'Hats',
			'Bullets',
			'Smoke Grenades',
			'Flashbangs',
			'AK47s',
			'Car Chases',
			'Bodyguards',
			'Tazers',
			'Handguns',
			'Utility Belts',
			'Shootouts',
			'Arrests',
			'The Shield',
			'The Badge',
			'Good Guys',
			'Bad Guys',
			'Pepper Spray',
			'Criminals',
			'Perps',
			'Combat Rolls',
			'Leadership',
			'Smelly Socks',
			'Jockstraps',
			'Poolside Meetings',
			'Burger Joints',
			'Trauma',
			'Crack Addicts',
			'Prostitutes',
			'Computers',
			'Traffic Cams',
			'Dash Cams',
			'Hunting Gear',
			'Body Armor',
			'Moonbeams',
			'Tractor Beams',
			'Life Straws',
		];

		for($i = 0; $i < 20; $i++)
		{
			$name = $faker->randomElement($course_options) . ' And ' . $faker->randomElement($course_options);
			$course = Course::create([
				'name'        => $name,
				'short_name'  => $faker->boolean() ? strtoupper($faker->randomLetter() . $faker->randomLetter() . $faker->randomLetter()) : null,
				'description' => $faker->sentence(),
//				'notes'       => $faker->sentence(),
			]);
		}
	}
}
