<?php

use App\Models\Attendee;
use App\Models\Course;
use App\Models\Event;
use App\Models\Lodging;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\Payment;
use App\Models\Registration;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Database\Seeder;

class RandomNoteSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$users = User::all()->all();
		$course_options = [
			'Guns',
			'Bros',
			'Belts',
			'Body Cams',
			'Slippers',
			'Stevedores',
			'Longshoremen',
			'Hats',
			'Bullets',
			'Smoke Grenades',
			'Flashbangs',
			'AK47s',
			'Car Chases',
			'Bodyguards',
			'Tazers',
			'Handguns',
			'Utility Belts',
			'Shootouts',
			'Arrests',
			'The Shield',
			'The Badge',
			'Good Guys',
			'Bad Guys',
			'Pepper Spray',
			'Criminals',
			'Perps',
			'Combat Rolls',
			'Leadership',
			'Smelly Socks',
			'Jockstraps',
			'Poolside Meetings',
			'Burger Joints',
			'Trauma',
			'Crack Addicts',
			'Prostitutes',
			'Computers',
			'Traffic Cams',
			'Dash Cams',
			'Hunting Gear',
			'Body Armor',
			'Moonbeams',
			'Tractor Beams',
			'Life Straws',
		];
		$prefixes = [
			'Pick up',
			'Sing about',
			'Think about',
			'Talk about',
			'Set fire to',
			'He called to talk about',
		];

		$classes = [
			Attendee::class,
			Discount::class,
			Course::class,
			Event::class,
			Lodging::class,
			Payment::class,
			Registration::class,
			Ticket::class,
			Venue::class,
		];
		foreach($classes as $class)
		{
			foreach($class::all() as $model)
			{
				for($i = 0; $i < $faker->numberBetween(0, 4); $i++)
				{
					$model->createNote($faker->randomElement($prefixes) . ' ' . $faker->randomElement($course_options), $faker->randomElement($users));
				}
			}
		}
	}
}
