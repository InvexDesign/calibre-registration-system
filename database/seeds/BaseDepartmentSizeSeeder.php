<?php

use App\Models\DepartmentSize;
use Illuminate\Database\Seeder;

class BaseDepartmentSizeSeeder extends Seeder
{
	public function run()
	{
		$sizes = [
			'Less than 50',
			'51-100',
			'100-500',
			'501 or more',
		];

		foreach($sizes as $size)
		{
			DepartmentSize::create([
				'name' => $size,
			]);
		}
	}
}
