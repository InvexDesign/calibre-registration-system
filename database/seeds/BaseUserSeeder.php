<?php

use App\Models\Addresses\CustomerAddress;
use App\Models\Kitchen;
use App\Models\User;
use App\Role;
use Illuminate\Database\Seeder;

class BaseUserSeeder extends Seeder
{
	public function run()
	{
		$faker = \Faker\Factory::create();

		$overlord_role = Role::get('overlord', true);
		$master_role = Role::get('master', true);
		$administrator_role = Role::get('administrator', true);

		$is_local_environment = config('app.env') == 'local';
		$password = $is_local_environment ? 'password' : '3fuioewh983g72fe98hqwed';

		//Create Overlord
		$user = User::create([
			'role_id'    => $overlord_role->id,
			'username'   => 'dev',
			'password'   => $password,
			'first_name' => 'Babay',
			'last_name'  => 'Lettuce',
			'email'      => 'andrew@invexdesign.com'
		]);

		$user = User::create([
			'role_id'    => $administrator_role->id,
			'username'   => 'admin',
			'password'   => $password,
			'first_name' => 'Admin',
			'last_name'  => '',
			'email'      => 'brian@invexdesign.com'
		]);
	}
}
