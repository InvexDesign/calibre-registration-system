<?php

use App\Role;
use Illuminate\Database\Seeder;

class BaseRoleSeeder extends Seeder
{
	public function run()
	{
		Role::create([
			'name'         => 'overlord',
			'display_name' => 'Overlord',
			'description'  => 'Can do everything.',
		]);

		Role::create([
			'name'         => 'master',
			'display_name' => 'Master',
			'description'  => 'Can do almost everything.',
		]);

		Role::create([
			'name'         => 'administrator',
			'display_name' => 'Administrator',
			'description'  => 'Can do lots of things.',
		]);

		Role::create([
			'name'         => 'sales-representative',
			'display_name' => 'Sales Representive',
			'description'  => 'Can do some of things.',
		]);
	}
}
