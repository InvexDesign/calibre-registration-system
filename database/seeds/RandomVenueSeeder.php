<?php

use App\Models\Address;
use App\Models\State;
use App\Models\Venue;
use Illuminate\Database\Seeder;

class RandomVenueSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$state_ids = State::query()->pluck('id')->all();
		$venue_options = [
			'Barn',
			'Backyard',
			'Lake House',
			'Apartment',
			'Studio',
			'Tower',
			'Pool House',
			'Living Room',
			'Rooftop',
			'Wheat Silo',
			'Palopa',
			'Crow\'s Nest',
		];

		for($i = 0; $i < 100; $i++)
		{
			$address = Address::create([
				'line1'    => $faker->streetAddress,
				'line2'    => null,
				'city'     => $faker->city,
				'state_id' => $faker->randomElement($state_ids),
				'zipcode'  => $faker->postcode,
				'country'  => $faker->country,
			]);

			//Center of USA
			$latitude = 49.348032 - $faker->randomFloat(16, 0, 14);
			$longitude = -122.625006 + $faker->randomFloat(16, 0, 45);
			$embedded_map = '<iframe src="https://maps.google.com/maps?q=' . $latitude . ',' . $longitude . '&hl=es;z=8&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
			$venue = Venue::create([
				'name'         => $faker->firstName . '\'s ' . $faker->randomElement($venue_options),
				'embedded_map' => $embedded_map,
				'address_id'   => $address->id,
				'auto_gps'     => true,
				'latitude'     => $latitude,
				'longitude'    => $longitude,
				'phone'        => $faker->boolean() ? $faker->numberBetween(1000000000, 9999999999) : ($faker->numberBetween(1000000000, 9999999999) . $faker->numberBetween(1, 9999)),
				'email'        => $faker->email,
//				'notes'        => $faker->sentence(),
			]);
//			$venue->uid = 'VENUE-' . $venue->id;
//			$venue->save();
		}
	}
}
