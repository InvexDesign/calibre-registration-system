<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Model::unguard();
		$is_clean_seed = config('system.is_clean_seed');

		$this->call('BaseRoleSeeder');
		$BASE_USER_SEEDER = env('BASE_USER_SEEDER', 'BaseUserSeeder');
		$this->call($BASE_USER_SEEDER);

		$BASE_SETTING_SEEDER = env('BASE_SETTING_SEEDER', 'BaseSettingSeeder');
		$this->call($BASE_SETTING_SEEDER);

		$this->call('BaseStateSeeder');
		$this->call('BaseDepartmentSizeSeeder');

//		$BASE_EVENT_SEEDER = env('BASE_EVENT_SEEDER', 'BaseEventSeeder');
//		$this->call($BASE_EVENT_SEEDER);

		if(!$is_clean_seed)
		{
			$this->call(env('RANDOM_DISCOUNT_SEEDER', 'RandomDiscountSeeder'));
			$this->call(env('RANDOM_SALES_REPRESENTATIVE_SEEDER', 'RandomSalesRepresentativeSeeder'));
			$this->call(env('RANDOM_LODGING_SEEDER', 'RandomLodgingSeeder'));
			$this->call(env('RANDOM_COURSE_SEEDER', 'RandomCourseSeeder'));
			$this->call(env('RANDOM_VENUE_SEEDER', 'RandomVenueSeeder'));
			$this->call(env('RANDOM_EVENT_SEEDER', 'RandomEventSeeder'));
			$this->call(env('RANDOM_REGISTRATION_SEEDER', 'RandomRegistrationSeeder'));
			$this->call(env('RANDOM_NOTE_SEEDER', 'RandomNoteSeeder'));
//			$this->call(env('RANDOM_PAYMENT_SEEDER', 'RandomPaymentSeeder'));
		}
    }
}
