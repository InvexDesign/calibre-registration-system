<?php

use App\Models\Address;
use App\Models\Attendee;
use App\Models\DepartmentSize;
use App\Models\Payments\CheckPayment;
use App\Models\Payments\DiscountPayment;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\CreditCardPayment;
use App\Models\Payments\Payment;
use App\Models\Registration;
use App\Models\State;
use App\Models\Statuses\RegistrationPaymentStatus;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class RandomPaymentSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$registrations = Registration::all();

		foreach($registrations as $registration)
		{
			$paid_at = Carbon::today()->subDays($faker->numberBetween(0, $registration->event->created_at->diffInDays(Carbon::today())));
			$discount = Discount::findAutomaticallyApplicableDiscounts($registration);
			if($discount)
			{
				$discount_payment = DiscountPayment::create([
					'eid'             => $discount->id,
					'registration_id' => $registration->id,
					'amount_in_cents' => $discount->calculateDiscount($registration),
					'paid_at'         => $paid_at,
//					'notes'           => 'Automatic',
				]);
				$discount_payment->created_at = $registration->created_at;
				$discount_payment->save();
			}

			$balance_in_cents = $registration->getBalanceInCents();

			$is_full_payment = $faker->boolean(75);
			if(!$is_full_payment)
			{
				$balance_in_cents *= ($faker->numberBetween(50, 90) / 100);
			}

			$class = $faker->boolean() ? CreditCardPayment::class : CheckPayment::class;
			$payment = $class::create([
				'eid'             => $faker->uuid,
				'registration_id' => $registration->id,
				'amount_in_cents' => $balance_in_cents,
				'paid_at'         => $paid_at,
//				'notes'           => "Fake Payment",
			]);
			$payment->created_at = $registration->created_at;
			$payment->save();
		}
	}
}
