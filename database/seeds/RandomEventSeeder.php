<?php

use App\Models\Course;
use App\Models\Lodging;
use App\Models\Statuses\EventPublishedStatus;
use App\Models\Venue;
use App\Role;
use Illuminate\Database\Seeder;

class RandomEventSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$venue_ids = Venue::query()->pluck('id')->all();
		$course_ids = Course::query()->pluck('id')->all();
		$all_lodging_ids = Lodging::query()->pluck('id')->all();
		$sales_representative_ids = Role::get('sales-representative')->users->pluck('id')->all();

		for($i = 0; $i < 300; $i++)
		{
			$today = Carbon::today()->setTimezone('America/Chicago');

			$days = $faker->numberBetween(0, 60);
			$start_at = $today->copy()->addDays($days);
			$end_at = $faker->boolean() ? $start_at->copy() : $start_at->copy()->addDays($faker->numberBetween(1, 2));

			if($start_at->eq($end_at))
			{
				$times = $start_at->format('n/j') . ": 8AM - 5PM\n";
				$end_at = null;
			}
			else
			{
				$times = "";
				$current = $start_at->copy();
				while($current->lte($end_at))
				{
					$times .= $current->format('n/j') . ": ";
					$time_option = $faker->numberBetween(1, 3);
					switch($time_option)
					{
						case 1:
							$times .= "8AM - 5PM";
							break;
						case 2:
							$times .= "5PM - 9PM";
							break;
						case 3:
						default:
							$times .= "8AM - 12PM";
							break;
					}

					$times .= "\n";
					$current->addDays(1);
				}
			}

			$venue_id = $faker->randomElement($venue_ids);
			$venue = Venue::find($venue_id);

			$price_in_cents = $faker->numberBetween(5, 20) * 10 * 100;
			$event = \App\Models\Event::create([
				'venue_id'                => $venue_id,
				'sales_representative_id' => $faker->randomElement($sales_representative_ids),
				'state'                   => $venue->address->state->short_name,
				'course_id'               => $faker->randomElement($course_ids),
				'start_at'                => $start_at,
				'end_at'                  => $end_at,
				'is_private'              => $faker->boolean(),
				'deadline_at'             => $faker->boolean() ? $start_at->copy()->subWeeks(1) : null,
				'times'                   => $times,
				'ticket_limit'            => $faker->boolean() ? $faker->numberBetween(1, 10) * 50 : null,
				'price_in_cents'          => $price_in_cents,
				'revenue_goal'            => $faker->boolean() ? $faker->numberBetween(5, 15) * $faker->numberBetween(5, 10) * $price_in_cents : null,
				'published_at'            => $faker->boolean() ? $start_at->copy()->subDays(30) : null,
				'settled_at'              => $faker->boolean() ? $start_at->copy()->addWeek(1) : null,
				'description'             => $faker->sentence(),
//				'notes'                   => $faker->sentence(),
			]);
			$event->created_at = $event->created_at->subWeeks($faker->numberBetween(2, 10));
			$event->save();

			$lodging_count = $faker->boolean(80) ? 1 : 2;
			$lodging_ids = $faker->randomElements($all_lodging_ids, $lodging_count);
			$event->lodgings()->sync($lodging_ids);
		}
	}
}
