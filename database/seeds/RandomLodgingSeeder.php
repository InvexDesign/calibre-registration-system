<?php

use App\Models\Address;
use App\Models\Lodging;
use App\Models\State;
use App\Models\Venue;
use Illuminate\Database\Seeder;

class RandomLodgingSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$state_ids = State::query()->pluck('id')->all();
		$names = [
			'White',
			'Black',
			'Wolf',
			'Love',
			'Thyme',
			'Noodle',
			'Bread',
			'Loaf',
			'Slime',
			'Pool',
			'Great',
			'Loop',
			'Jehda',
			'7-Gee',
		];

		$gmaps_embed_codes = [
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11891.752701719637!2d-87.82549965!3d41.829629399999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1b60a35fd648cf95!2sBrookfield+Zoo!5e0!3m2!1sen!2sus!4v1500648802889" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11888.330963111783!2d-87.7804491764407!3d41.84804585412523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x62a847d058a651aa!2sThe+Olympic+Theater!5e0!3m2!1sen!2sus!4v1500648991784" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11882.740421742925!2d-87.79984691203643!3d41.878121009172865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf0d991749ea75e2b!2sPleasant+Home!5e0!3m2!1sen!2sus!4v1500649029232" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11888.418602435355!2d-87.86256257911232!3d41.84757424494382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x39f2f0cf4eb1bfee!2sLA+Fitness!5e0!3m2!1sen!2sus!4v1500649052178" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11886.077905950633!2d-87.90165845771345!3d41.86016863222343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3db61b3fabba4ff4!2sMount+Carmel+Cemetery!5e0!3m2!1sen!2sus!4v1500649067711" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11891.795431963854!2d-87.89973058414672!3d41.829399375813814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6b75032f16bc00bc!2sMeadowlark+Golf+Course!5e0!3m2!1sen!2sus!4v1500649078978" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11896.62871003468!2d-87.89910614490508!3d41.803374353299226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5e194a8eda4393fb!2sLou+Malnati&#39;s+Pizzeria!5e0!3m2!1sen!2sus!4v1500649104935" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22358.124112043814!2d-87.76175241274294!3d41.857150294797265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc34a1a2ef891e525!2sThe+Town+Of+Cicero+Community+Center!5e0!3m2!1sen!2sus!4v1500649129362" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22355.053482269548!2d-87.65992952437422!3d41.86593279588825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x56ae5a42865db386!2sAl&#39;s+Beef!5e0!3m2!1sen!2sus!4v1500649161089" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22343.197738214094!2d-87.66130178882868!3d41.899828068553035!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8adf7134e29760ce!2sForbidden+Root+Restaurant+%26+Brewery!5e0!3m2!1sen!2sus!4v1500649180595" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22339.813105435474!2d-87.65992952437426!3d41.909500547625484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf4c181b949512136!2sHomeslice!5e0!3m2!1sen!2sus!4v1500649190564" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22339.813105435474!2d-87.65992952437426!3d41.909500547625484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x719a19cc064f264a!2sKingston+Mines!5e0!3m2!1sen!2sus!4v1500649200390" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22340.044357581013!2d-87.70319621540862!3d41.90883974165625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xafa2d5ca873079ba!2sRamen+Wasabi!5e0!3m2!1sen!2sus!4v1500649212392" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22346.834503363396!2d-87.80910274271666!3d41.88943302616394!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc2cb12b6aee7148!2sConcordia+University!5e0!3m2!1sen!2sus!4v1500649236581" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22344.12270943766!2d-87.80877985696267!3d41.897184403172325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc24ce96030c3461e!2sJohnnie&#39;s+Beef!5e0!3m2!1sen!2sus!4v1500649247154" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22344.12270943766!2d-87.80877985696267!3d41.897184403172325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x47a889bc8b5b1d7f!2sAlpine+Food+Shop!5e0!3m2!1sen!2sus!4v1500649256078" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22330.100950128643!2d-87.81736766617693!3d41.93724549423231!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4f6953eeb3821448!2sForno+Rosso+Pizzeria+Napoletana!5e0!3m2!1sen!2sus!4v1500649269763" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22314.88257309308!2d-87.6850087951621!3d41.98069014930016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6b2cb79773c77fa8!2sLincoln+Karaoke!5e0!3m2!1sen!2sus!4v1500649297934" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22314.88257309308!2d-87.6850087951621!3d41.98069014930016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19730f34181f16d1!2sOra!5e0!3m2!1sen!2sus!4v1500649308719" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22297.4657373545!2d-87.74338590065969!3d42.03036597227776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9cee42efe50e0cc2!2sEvanston+Golf+Club!5e0!3m2!1sen!2sus!4v1500649332858" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22272.094236869016!2d-87.77381177815991!3d42.10264450153192!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1c2a1abf346acf2!2sSunset+Ridge+Country+Club!5e0!3m2!1sen!2sus!4v1500649346043" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
			'<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22281.41997766514!2d-87.90839012571259!3d42.07608899801514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0706667b9df1295!2sShrine+of+Our+Lady+of+Guadalupe!5e0!3m2!1sen!2sus!4v1500649360822" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
		];

		for($i = 0; $i < 40; $i++)
		{
			$address = Address::create([
				'line1'    => $faker->streetAddress,
				'line2'    => null,
				'city'     => $faker->city,
				'state_id' => $faker->randomElement($state_ids),
				'zipcode'  => $faker->postcode,
				'country'  => $faker->country,
			]);
			$lodging = Lodging::create([
				'name'         => $faker->randomElement($names) . ' ' . $faker->randomElement($names) . ' Lodge',
				'phone'        => $faker->boolean() ? $faker->numberBetween(1000000000, 9999999999) : ($faker->numberBetween(1000000000, 9999999999) . $faker->numberBetween(1, 9999)),
				'email'        => $faker->email,
				'contact'      => $faker->firstName . ' ' . $faker->lastName,
				'embedded_map' => $faker->randomElement($gmaps_embed_codes),
				'address_id'   => $address->id,
				'description'  => $faker->sentence(),
//				'notes'        => $faker->sentence(),
			]);
		}
	}
}
