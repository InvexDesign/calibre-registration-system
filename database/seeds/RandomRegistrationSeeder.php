<?php

use App\Models\Address;
use App\Models\Attendee;
use App\Models\DepartmentSize;
use App\Models\Payments\CheckPayment;
use App\Models\Payments\DiscountPayment;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\CreditCardPayment;
use App\Models\State;
use App\Models\Statuses\RegistrationPaymentStatus;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class RandomRegistrationSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker\Factory::create();
		$state_ids = State::query()->pluck('id')->all();
		$event_ids = \App\Models\Event::query()->pluck('id')->all();
		$department_sizes = DepartmentSize::pluck('id')->all();
		$ranks = [
			'Captain',
			'Detective',
			'Patrolman',
			'Lieutenant',
			'Officer',
			'Inspector',
			'Forensic Investigator',
		];

		for($i = 0; $i < count($event_ids) * 5; $i++)
		{
			$attempts = 1;
			$attendee_count = $faker->numberBetween(1, 10);

			while($attempts > 0)
			{
				$event_id = $faker->randomElement($event_ids);
				$event = \App\Models\Event::find($event_id);
				if($event->getAvailableTicketCount() == null || $event->getAvailableTicketCount() > $attendee_count)
				{
					break;
				}

				$attempts--;
			}

			if($attempts == 0)
			{
				echo "Failed to create registration #$i, not enough tickets available\n";
				continue;
			}

			$address = Address::create([
				'line1'    => $faker->streetAddress,
				'line2'    => null,
				'city'     => $faker->city,
				'state_id' => $faker->randomElement($state_ids),
				'zipcode'  => $faker->postcode,
				'country'  => $faker->country,
			]);
			$agency = $faker->domainName;
			$registration = \App\Models\Registration::create([
				'event_id'           => $event_id,
				'agency'             => $agency,
				'contact'            => $faker->firstName . ' ' . $faker->lastName,
				'email'              => $faker->email,
				'department_size_id' => $faker->randomElement($department_sizes),
				'phone'              => $faker->boolean() ? ($faker->numberBetween(1000000000, 9999999999) . $faker->numberBetween(1, 9999)) : $faker->numberBetween(1000000000, 9999999999),
				'fax'                => $faker->boolean() ? $faker->numberBetween(1000000000, 9999999999) : null,
				'address_id'         => $address->id,
				'passcode'           => $faker->password,
				'payment_status'     => RegistrationPaymentStatus::UNPAID,
//				'notes'           => $faker->sentence(),
			]);
			$registration->created_at = Carbon::today()->subDays($faker->numberBetween(0, $event->created_at->diffInDays(Carbon::today())));
			$registration->save();

			for($j = 0; $j < $attendee_count; $j++)
			{
				$attendee = Attendee::create([
//					'registration_id' => $registration->id,
					'event_id'   => $event_id,
					'first_name' => $faker->firstName,
					'last_name'  => $faker->lastName,
					'email'      => $faker->email,
					'rank'       => $faker->randomElement($ranks),
					'pid'        => $faker->numberBetween(11111, 99999),
//					'notes'           => $faker->sentence(),
				]);
				$attendee->created_at = $registration->created_at;
				$attendee->save();

				$ticket = Ticket::create([
					'event_id'        => $event_id,
					'registration_id' => $registration->id,
					'attendee_id'     => $attendee->id,
					'price_in_cents'  => $event->price_in_cents,
				]);
				$ticket->created_at = $registration->created_at;
				$ticket->save();
			}

			$paid_at = Carbon::today()->subDays($faker->numberBetween(0, $registration->event->created_at->diffInDays(Carbon::today())));
			$discount = Discount::findAutomaticallyApplicableDiscounts($registration);
			if($discount)
			{
				$discount_payment = DiscountPayment::create([
					'eid'             => $discount->id,
					'registration_id' => $registration->id,
					'amount_in_cents' => $discount->calculateDiscount($registration),
					'paid_at'         => $paid_at,
//					'notes'           => 'Automatic',
				]);
				$discount_payment->created_at = $registration->created_at;
				$discount_payment->save();
			}

			$balance_in_cents = $registration->getBalanceInCents();

			$is_full_payment = $faker->boolean(75);
			if(!$is_full_payment)
			{
				$balance_in_cents *= ($faker->numberBetween(50, 90) / 100);
			}

			$type = $faker->boolean();
			$class = $type ? CreditCardPayment::class : CheckPayment::class;
			$uid = $type ? $faker->uuid : '#' . $faker->numberBetween(100, 9999);
			$payment = $class::create([
				'eid'             => $uid,
				'registration_id' => $registration->id,
				'amount_in_cents' => $balance_in_cents,
				'paid_at'         => $paid_at,
//				'notes'           => 'Fake Payment',
			]);
			$payment->created_at = $registration->created_at;
			$payment->save();

			if($registration->getTotalPaidInCents() > 0)
			{
				$registration->payment_status = ($registration->getBalanceInCents() == 0)
					? RegistrationPaymentStatus::PAID : RegistrationPaymentStatus::PARTIALLY_PAID;
				$registration->save();
			}
		}
	}
}
