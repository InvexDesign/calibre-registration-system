<?php namespace App\Mailers;

use Mail;

class CustomerMailer
{
	public static function emailRegistrationConfirmation($registration)
	{
		Mail::send('emails.new_registration', ['registration' => $registration], function ($message) use ($registration)
		{
			$message->from(env('EMAIL'), env('COMPANY'))
					->to($registration->email, $registration->contact)
					->subject(env('COMPANY') . ' - ' . $registration->displayCourseAndLocation() . ' Invoice And Registration Confirmation');
		});
	}

	public static function emailPaymentConfirmation($payment)
	{
		Mail::send('emails.payment_receipt', ['payment' => $payment], function ($message) use ($payment)
		{
			$message->from(env('EMAIL'), env('COMPANY'))
					->to($payment->registration->email, $payment->registration->contact)
					->subject(env('COMPANY') . ' - ' . $payment->registration->displayCourseAndLocation() . ' Payment Receipt');
		});
	}
}