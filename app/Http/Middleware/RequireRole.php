<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RequireRole
{
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
	protected $error_message = 'Access Denied: Invalid Role!';

	/**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * @param          $request
	 * @param callable $next
	 * @param          $required_roles_string
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function handle($request, Closure $next, $required_roles_string)
	{
		$required_roles = explode(';', $required_roles_string);
        if($this->auth->guest())
        {
            return redirect('/')->with('errors', [$this->error_message]);
        }

		$user = $this->auth->user();
		//if(!in_array($user->role, $required_roles))
		if(!$user->hasRole($required_roles) )
        {
			if($user->isAdministrator())
			{
				return \Redirect::route('backend.dashboard.index')->with('errors', [$this->error_message]);
			}
			else
			{
				return \Redirect::route('errors.unauthorized')->with('errors', [$this->error_message]);
//				return redirect('/')->with('errors', [$this->error_message]);
			}
        }

		return $next($request);
	}

}
