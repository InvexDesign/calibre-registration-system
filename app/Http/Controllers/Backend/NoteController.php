<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Notes\DestroyNoteRequest;
use App\Http\Requests\Backend\Notes\PostCreateNoteRequest;
use App\Models\Notes\Note;
use Illuminate\Support\Facades\Redirect;

class NoteController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$notes = Note::all();
		$vars = compact('notes');

		return view('backend.notes.index', $vars);
	}

	public function postCreate(PostCreateNoteRequest $request)
	{
		$model = $request->model;
		$model->createNote($request->get('content'), \Auth::user(), false);

		return Redirect::route($model->getBackendRoutes()['show'], $request->id)->with('messages', ['Note was successfully created!']);
	}

	public function destroy(DestroyNoteRequest $request, $id)
	{
		$note = $request->note;
		$note->delete();

		$message = 'Note (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.notes.index')->with('messages', [$message]);
	}
}