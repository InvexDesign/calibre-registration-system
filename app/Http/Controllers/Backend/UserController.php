<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Users\GetChangePasswordRequest;
use App\Http\Requests\Backend\Users\GetCreateUserRequest;
use App\Http\Requests\Backend\Users\GetEditUserRequest;
use App\Http\Requests\Backend\Users\PostChangePasswordRequest;
use App\Http\Requests\Backend\Users\PostCreateUserRequest;
use App\Http\Requests\Backend\Users\PostEditUserRequest;
use App\Models\User;
use App\Role;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . EXECUTIVES);
	}

	public static function composeViews()
	{
		view()->composer('backend.users.partials.form', function ($view)
		{
			if(isset($view->getData()['user']))
			{
				$user = $view->getData()['user'];
				$view->with('default_role_id', $user->getRole() ? $user->getRole()->id : null);
			}
			else
			{
				$view->with('default_role_id', Role::get('administrator', true)->id);
			}

			$view->with('role_options', Role::all()->pluck('display_name', 'id')->all());
		});
	}

	public function index()
	{
		if(\Auth::user()->isExecutive())
		{
			$users = User::all();
		}
		else
		{
			$overlord_role = Role::get('overlord');
			$overlord_ids = $overlord_role->users->pluck('id');
			$users = User::query()->whereNotIn('id', $overlord_ids)->get();
		}

		$vars = compact('users');

		return view('backend.users.index', $vars);
	}

	public function getCreate(GetCreateUserRequest $request)
	{
		$vars = [
			'role_options' => $request->role_options
		];

		return view('backend.users.create', $vars);
	}

	public function postCreate(PostCreateUserRequest $request)
	{
		$user = User::create($request->all());
		$user->save();

		return Redirect::route('backend.users.show', $user->id)->with('messages', ['User was successfully created!']);
	}

	public function show($id)
	{
		$user = User::findOrFail($id);
		$vars = compact('user');

		return view('backend.users.show', $vars);
	}

	public function getEdit(GetEditUserRequest $request, $id)
	{
		$user = $request->user;
		$default_role_id = $user->getRole()->id;
		$vars = compact('user', 'default_role_id');

		return view('backend.users.edit', $vars);
	}

	public function postEdit(PostEditUserRequest $request, $id)
	{
		$user = $request->user;
		$user->fill($request->all());
		$user->save();

		return Redirect::route('backend.users.edit.get', $id)->with('messages', ['User was successfully updated!']);
	}
	
	public function getChangePassword(GetChangePasswordRequest $request, $id)
	{
		$user = $request->user;
		$default_role_id = $user->roles()->first()->id;
		$vars = compact('user', 'default_role_id');

		return view('backend.users.edit', $vars);
	}

	//TODO: Implement this - deactivate user
	public function postChangePassword(PostChangePasswordRequest $request, $id)
	{
		$user = $request->user;
		$user->fill($request->only(['password']));
		$user->save();

		return Redirect::route('backend.users.edit.get', $id)->with('messages', ['User was successfully updated!']);
	}
	
	public function destroy($id)
	{
		$user = User::find($id);
		
		$user->delete();

		return Redirect::route('backend.users.index')->with('messages', ['User (ID: ' . $id . ') successfully deleted!']);
	}
}