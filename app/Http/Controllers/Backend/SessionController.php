<?php namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Models\LoginAttempt;
use Carbon;
use Input;
use Redirect;
use Session;

class SessionController extends Controller
{
	protected $redirect_path = '/';
	protected $default_route = 'backend.dashboard.index';

	function __construct()
	{
		$this->middleware('auth', ['except' => ['create', 'store', 'setTimezone']]);
	}

	public function create()
	{
		if(\Auth::user())
		{
			return Redirect::route($this->default_route)->with('messages', ['You are already logged in.']);
		}

		//TODO: FLAG: ip_address has X failed logins within Y minutes
		//TODO: FLAG: ip_address from outside US
		//TODO: Limit 6 months

		$title = 'User Login';
		$form_route = 'session.store';
		$vars = compact('form_route', 'title');

		return view('session.login', $vars);
	}

	public function getClientIp()
	{
		$ipaddress = '';
		if(isset($_SERVER['HTTP_CLIENT_IP']))
		{
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
		{
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		}
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		{
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		}
		else if(isset($_SERVER['HTTP_FORWARDED']))
		{
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		}
		else if(isset($_SERVER['REMOTE_ADDR']))
		{
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		}
		else
		{
			$ipaddress = 'UNKNOWN';
		}

		return $ipaddress;
	}

	public function recordLoginAttempt($username, $user_id, $success)
	{
		$timestamp = \Carbon::now();
		$ip_address = $this->getClientIp();
		$ip_details = (array) json_decode(file_get_contents("http://ipinfo.io/{$ip_address}/json"));
		$country = isset($ip_details['country']) ? $ip_details['country'] : null;
		$region = isset($ip_details['region']) ? $ip_details['region'] : null;
		$city = isset($ip_details['city']) ? $ip_details['city'] : null;
		$postal = isset($ip_details['postal']) ? $ip_details['postal'] : null;
		$organization = isset($ip_details['organization']) ? $ip_details['organization'] : null;
		$hostname = isset($ip_details['hostname']) ? $ip_details['hostname'] : null;

		$latitude = null;
		$longitude = null;
		if(isset($ip_details['loc']))
		{
			$lat_long = $ip_details['loc'];
			list($latitude, $longitude) = explode(',', $lat_long);
			$latitude = floatval($latitude);
			$longitude = floatval($longitude);
		}

		$browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
		$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

		LoginAttempt::create(compact(
			'username',
			'ip_address',
			'timestamp',
			'success',
			'country',
			'region',
			'city',
			'postal',
			'latitude',
			'longitude',
			'organization',
			'hostname',
			'browser',
			'referrer',
			'user_id'
		));
	}

	public function store(UserLoginRequest $request)
	{
		$username = $request->get('username');
		$password = $request->get('password');
		$credentials = [
			'username' => $username,
			'password' => $password,
		];
		$remember = $request->has('remember') ? true : false;

		$is_authenticated = \Auth::attempt($credentials, $remember);

		if(!$is_authenticated)
		{
			$this->recordLoginAttempt($username, null, false);

			return Redirect::back()
						   ->withInput($request->only('username', 'remember'))
						   ->with('errors', ['Invalid credentials.']);
		}

		$this->recordLoginAttempt($username, \Auth::id(), true);

		LoginAttempt::removeRecordsOlderThan(Carbon::now()->subMonths(6));

		return Redirect::intended(route($this->default_route))->with('messages', ['You are now logged in.']);
	}

	public function destroy()
	{
		Session::flush();
		\Auth::logout();

		return Redirect::route('session.login')->with('messages', ['You are now logged out.']);
	}
}