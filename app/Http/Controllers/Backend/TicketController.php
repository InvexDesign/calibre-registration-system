<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Tickets\DestroyTicketRequest;
use App\Http\Requests\Backend\Tickets\PostAddTicketsRequest;
use App\Http\Requests\Backend\Tickets\PostCreateTicketRequest;
use App\Http\Requests\Backend\Tickets\PostEditTicketRequest;
use App\Http\Requests\Backend\Tickets\PostSetImageRequest;
use App\Models\Attendee;
use App\Models\Registration;
use Redirect;
use Setting;
use App\Models\State;
use App\Models\Ticket;
use Input;

class TicketController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.tickets.partials.form', function ($view)
		{
			if(isset($view->getData()['ticket']))
			{
				$ticket = $view->getData()['ticket'];
//				$view->with('initial_feature_ids', $ticket->features->pluck('id')->all());
			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$tickets = Ticket::query()
						 ->whereBetween('created_at', [$start_at, $end_at])
						 ->paginate(Setting::get('ticket-page-length', Setting::get('default-page-length', 50)));

		$vars = compact(
			'tickets',
			'start_at',
			'end_at'
		);

		return view('backend.tickets.index', $vars);
	}

	public function indexByEvent($event_id)
	{
		list($start_at, $end_at) = $this->getDateRange();
		$tickets = Ticket::where('event_id', $event_id)
						 ->whereBetween('created_at', [$start_at, $end_at])
						 ->get();
		$page_title = "Event #$event_id's Tickets";
		$vars = compact(
			'tickets',
			'page_title',
			'start_at',
			'end_at'
		);

		return view('backend.tickets.index', $vars);
	}

	public function indexJson()
	{
		$tickets_query = Ticket::query();
		if($event_id = Input::get('event_id', false))
		{
			$tickets_query->where('event_id', $event_id);
		}
		$tickets = $tickets_query->get();
		$vars = compact('tickets');

		return json_encode(['' => view('backend.tickets.partials.table', $vars)->render()]);
	}

	public function getAdd($registration_id)
	{
		$registration = Registration::find($registration_id);
		$max_attendee_count = Setting::get('max-attendee-count', 30);
		$attendee_count_options = [];
		for($i = 1; $i <= $max_attendee_count; $i++)
		{
			$attendee_count_options[$i] = $i;
		}

		$vars = compact(
			'registration',
			'attendee_count_options',
			'max_attendee_count'
		);

		return view('backend.tickets.add', $vars);
	}

	public function postAdd(PostAddTicketsRequest $request, $registration_id)
	{
		$registration = $request->registration;
		$event = $registration->event;

		$attendee_count = $request->attendee_count;
		for($i = 0; $i < $attendee_count; $i++)
		{
			$attendee_attributes = $request->attendees[$i];

			$attendee = Attendee::create($attendee_attributes);
			Ticket::create([
				'event_id'        => $registration->event_id,
				'registration_id' => $registration->id,
				'attendee_id'     => $attendee->id,
				'price_in_cents'  => $event->price_in_cents,
			]);
		}

		return Redirect::route('backend.registrations.show', $registration_id)->with('messages', [$attendee_count . ' Ticket(s) successfully added!']);
	}

	public function show($id)
	{
		$ticket = Ticket::findOrFail($id);
		$vars = compact('ticket');

		return view('backend.tickets.show', $vars);
	}

//	public function getEdit($id)
//	{
//	    $ticket = Ticket::findOrFail($id);
//	    $vars = compact('ticket');
//
//	    return view('backend.tickets.edit', $vars);
//	}
//
//	public function postEdit(PostEditTicketRequest $request, $id)
//	{
//		$ticket = $request->ticket;
//		$ticket->fill($request->all());
//		$ticket->save();
//
//		$message = 'Ticket was successfully updated!';
//
//		return Redirect::route('backend.tickets.edit.get', $id)->with('messages', [$message]);
//	}

	public function destroy(DestroyTicketRequest $request, $id)
	{
		$ticket = $request->ticket;
		$ticket->delete();

		$message = 'Ticket (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.tickets.index')->with('messages', [$message]);
	}
}