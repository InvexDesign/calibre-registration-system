<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Addresses\Address;
use App\Models\Category;
use App\Models\Course;
use App\Models\Events\Event;
use App\Models\Instructor;
use App\Models\Setting;
use App\Models\Statuses\Events\EventActivityStatus;
use Carbon;
use DB;
use Exception;
use Helper;
use Input;
use Log;
use Response;

class DashboardController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$vars = [];

		return view('backend.dashboard.index', $vars);
	}

	public function listView()
	{
		$events = [];
		$categories = Category::all();
		$category_options = Category::all()->pluck('name', 'id')->all();
		$address_options = Address::all()->pluck('name', 'id')->all();
		$course_options = Course::all()->pluck('name', 'id')->all();
		$instructor_options = Instructor::all()->pluck('display', 'id')->all();
		$weekday_options = [
			'Sunday'    => 'Sunday',
			'Monday'    => 'Monday',
			'Tuesday'   => 'Tuesday',
			'Wednesday' => 'Wednesday',
			'Thursday'  => 'Thursday',
			'Friday'    => 'Friday',
			'Saturday'  => 'Saturday',
		];

		$vars = compact(
			'events',
			'categories',
			'category_options',
			'address_options',
			'course_options',
			'instructor_options',
			'weekday_options'
		);

		return view('backend.dashboard.events.list', $vars);
	}

	public function listViewAjax()
	{
		$event_query = DB::table('events')
						 ->join('instructors', 'instructors.id', '=', 'events.instructor_id')
						 ->join('courses', 'courses.id', '=', 'events.course_id')
						 ->join('categories', 'categories.id', '=', 'courses.category_id')
						 ->select('events.id')
						 ->where('events.activity_status', '=', EventActivityStatus::ACTIVE)
						 ->whereBetween('start_at', [Carbon::now(), Carbon::now()->addWeeks(2)]);

		$course_ids = \Input::get('course_ids', []);
		if(!empty($course_ids))
		{
			$event_query->whereIn('course_id', $course_ids);
		}

		$instructor_ids = \Input::get('instructor_ids', []);
		if(!empty($instructor_ids))
		{
			$event_query->whereIn('instructor_id', $instructor_ids);
		}

		$category_ids = \Input::get('category_ids', []);
		if(!empty($category_ids))
		{
			$event_query->whereIn('category_id', $category_ids);
		}

		$weekdays = \Input::get('weekdays', []);
		if(!empty($weekdays))
		{
			$event_query->whereIn('weekday', $weekdays);
		}

		$event_ids = $event_query->pluck('events.id');

		$draw = (int)Input::get('draw');
		$start = (int)Input::get('start');
		$length = (int)Input::get('length');

		$all_events = Event::query()->whereIn('id', $event_ids)->orderBy('start_at', 'asc')->get();

		if(count($all_events) == 0)
		{
			$paged_events = [];
		}
		else
		{
			$paged_events = $all_events->slice($start, $length);
		}

		$timezone = Setting::get('timezone');
		$currently_displayed_date = null;
		$events = [];
		foreach($paged_events as $event)
		{
			$start_at = $event->start_at;
			$timezone_adjusted_start_at = $start_at->setTimezone($timezone);

			$events[] = [
//				$event->id,
				Helper::sortTime($timezone_adjusted_start_at),
				$event->weekday,
				Helper::sortDay($timezone_adjusted_start_at),
				$event->course ? $event->course->display() : null,
				$event->course ? $event->course->description : null,
				$event->course ? Helper::displayCategory($event->course->category) : null,
				Helper::sortFirstLastName($event->instructor->last_name, $event->instructor->first_name, $event->instructor->display()),
				Helper::sortInteger($event->getDurationInMinutes()) . ' min',
			];
		}
		$records_filtered = count($all_events);
		$records_total = count($all_events);

		$json = json_encode([
			'draw'            => $draw,
			'recordsTotal'    => $records_total,
			'recordsFiltered' => $records_filtered,
			'data'            => $events,
		]);

		return $json;
	}

	public function calendarView()
	{
		$categories = Category::all();
		$category_options = Category::all()->pluck('name', 'id')->all();
		$course_options = Course::all()->pluck('name', 'id')->all();
		$instructor_options = Instructor::all()->pluck('display', 'id')->all();
		$weekday_options = [
			'Sunday'    => 'Sunday',
			'Monday'    => 'Monday',
			'Tuesday'   => 'Tuesday',
			'Wednesday' => 'Wednesday',
			'Thursday'  => 'Thursday',
			'Friday'    => 'Friday',
			'Saturday'  => 'Saturday',
		];

		$vars = compact(
			'categories',
			'category_options',
			'course_options',
			'instructor_options',
			'weekday_options'
		);

		return view('backend.dashboard.events.calendar', $vars);
	}

	public function calendarViewAjax()
	{
		try
		{
			$start = Carbon::parse(Input::get('start'));
			$end = Carbon::parse(Input::get('end'));
		}
		catch(Exception $e)
		{
			Log::error($e);
			Log::error('DashboardController@events: Error parsing start/end dates');

			return Response::json(['success' => false]);
		}
		$dates = [$start, $end];

		$event_query = DB::table('events')
						 ->join('instructors', 'instructors.id', '=', 'events.instructor_id')
						 ->join('courses', 'courses.id', '=', 'events.course_id')
						 ->join('categories', 'categories.id', '=', 'courses.category_id')
						 ->select('events.id')
						 ->where('events.activity_status', '=', EventActivityStatus::ACTIVE)
						 ->where('start_at', '>', $start)
						 ->where('start_at', '<', $end);

		$course_ids = \Input::get('course_ids', []);
		if(!empty($course_ids))
		{
			$event_query->whereIn('course_id', $course_ids);
		}

		$instructor_ids = \Input::get('instructor_ids', []);
		if(!empty($instructor_ids))
		{
			$event_query->whereIn('instructor_id', $instructor_ids);
		}

		$category_ids = \Input::get('category_ids', []);
		if(!empty($category_ids))
		{
			$event_query->whereIn('category_id', $category_ids);
		}

		$weekdays = \Input::get('weekdays', []);
		if(!empty($weekdays))
		{
			$event_query->whereIn('weekday', $weekdays);
		}

		$event_ids = $event_query->pluck('events.id');
		$unprocessed_events = Event::query()->whereIn('id', $event_ids)->get();
		$timezone = Setting::get('timezone');

		$events = [];
		foreach($unprocessed_events as $key => $event)
		{
			$course = $event->course;
			if($course)
			{
				$category = $course->category;
				$event['background_color'] = $category->primary_color;
				$event['text_color'] = $category->text_color;
				$event['_course'] = $course->display();
				$event['_category'] = $category->display();

				$event['classes'] = 'category-' . $course->category->id;
				if($course->is_new)
				{
					$event['classes'] .= ' dashed-border';
				}
			}
			else
			{
				$event['background_color'] = $event->background_color;
				$event['text_color'] = $event->text_color;
				$event['_course'] = null;
				$event['_category'] = null;
			}

//			$event['title'] = $event->course->display() . ' with ' . $event->instructor->display();
//			$event['url'] = route('backend.events.edit.choose_type', [$event->id]);
			$event['url'] = route('backend.events.show', [$event->id]);
			$event['_instructor'] = $event->instructor ? $event->instructor->display() : null;
			$event['location'] = $event->location;
			$event['_address'] = $event->address ? $event->address->line1 : null;
			$event['start'] = $event->start_at->timezone($timezone)->format('c');
			$event['end'] = $event->end_at->timezone($timezone)->format('c');

			$events[$key] = $event;
		}

		return Response::json([
			'success' => true,
			'range'   => $dates,
			'events'  => $events
		]);
	}
}