<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Venues\DestroyVenueRequest;
use App\Http\Requests\Backend\Venues\PostCreateVenueRequest;
use App\Http\Requests\Backend\Venues\PostEditVenueRequest;
use App\Http\Requests\Backend\Venues\PostSetEmbeddedMapRequest;
use App\Models\Address;
use App\Models\Venue;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;
use Setting;

class VenueController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.venues.partials.form', function ($view)
		{
			if(isset($view->getData()['venue']))
			{
				$venue = $view->getData()['venue'];
//				$view->with('initial_feature_ids', $venue->features->pluck('id')->all());
			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function search()
	{
		$venues = $this->filter()->get();

		return json_encode(view('backend.venues.partials.search_result_rows', compact('venues'))->render());
	}

	public function index()
	{
		$venues = $this->filter()
					   ->paginate(Setting::get('venue-page-length', Setting::get('default-page-length', 50)));
		$state_options = State::all()->pluck('short_name', 'id')->all();
		$vars = compact('venues', 'state_options');

		return view('backend.venues.index', $vars);
	}

	public function getCreate()
	{
		$vars = [
			'address' => null
		];

		return view('backend.venues.create', $vars);
	}

	public function postCreate(PostCreateVenueRequest $request)
	{
		$address = Address::create($request->all());
		$request->merge(['address_id' => $address->id]);

		$venue = Venue::create($request->all());
		$venue->uid = 'VENUE-' . $venue->id;
		$venue->save();

		return Redirect::route('backend.venues.show', $venue->id)->with('messages', ['Venue was successfully created!']);
	}

	public function show($id)
	{
		$venue = Venue::findOrFail($id);
		$vars = compact('venue');

		return view('backend.venues.show', $vars);
	}

	public function getEdit($id)
	{
		$venue = Venue::findOrFail($id);
		$address = $venue->address;
		$vars = compact(
			'venue',
			'address'
		);

		return view('backend.venues.edit', $vars);
	}

	public function postEdit(PostEditVenueRequest $request, $id)
	{
		$venue = $request->venue;
		$venue->fill($request->all());
		$venue->save();

		$address = $venue->address;
		$address->fill($request->all());
		$address->save();

		$message = 'Venue was successfully updated!';

		return Redirect::route('backend.venues.edit.get', $id)->with('messages', [$message]);
	}

	public function getSetEmbeddedMap($id)
	{
		$venue = Venue::findOrFail($id);
		$vars = compact(
			'venue'
		);

		return view('backend.venues.set_embedded_map', $vars);
	}

	public function postSetEmbeddedMap(PostSetEmbeddedMapRequest $request, $id)
	{
		$venue = $request->venue;
		$venue->embedded_map = $request->embedded_map;
		$venue->save();

		$message = 'Embedded Map was successfully updated!';

		return Redirect::route('backend.venues.show', $id)->with('messages', [$message]);
	}

	public function generateEmbeddedMap($id)
	{
		$venue = Venue::findOrFail($id);
		$venue->generateEmbeddedMapAttribute(true);
		$venue->save();

		$message = 'Embedded Map successfully generated!';

		return Redirect::route('backend.venues.show', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyVenueRequest $request, $id)
	{
		$venue = $request->venue;
		$address = $venue->address;
		$venue->delete();
		$address->delete();

		$message = 'Venue (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.venues.index')->with('messages', [$message]);
	}

	protected function filter()
	{
		$venue_query = Venue::query();
		$address_query = Address::query();
		if(($name = \Input::get('name')) != '')
		{
			$venue_query->where('name', 'LIKE', "%$name%");
		}
		if(($line1 = \Input::get('line1')) != '')
		{
			$address_query->where('line1', 'LIKE', "%$line1%");
		}
		if(($city = \Input::get('city')) != '')
		{
			$address_query->where('city', 'LIKE', "%$city%");
		}
		if(($state_id = \Input::get('state_id')) != '')
		{
			$address_query->where('state_id', $state_id);
		}
		if(($zipcode = \Input::get('zipcode')) != '')
		{
			$address_query->where('zipcode', 'LIKE', "%$zipcode%");
		}
		$address_ids = $address_query->pluck('id')->all();

		return $venue_query->whereIn('address_id', $address_ids);
	}
}