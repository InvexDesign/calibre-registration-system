<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Registration;
use OwenIt\Auditing\Models\Audit;

class AuditController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$audits = Audit::orderBy('id', 'desc')->paginate(10);
		$vars = compact('audits');

		return view('backend.audits.index', $vars);
	}

	public function show($id)
	{
		$audit = Audit::findOrFail($id);
		$vars = compact('audit');

		return view('backend.audits.show', $vars);
	}
}