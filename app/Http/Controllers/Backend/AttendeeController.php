<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Attendees\DestroyAttendeeRequest;
use App\Http\Requests\Backend\Attendees\PostCreateAttendeeRequest;
use App\Http\Requests\Backend\Attendees\PostEditAttendeeRequest;
use App\Http\Requests\Backend\Attendees\PostSetImageRequest;
use App\Models\State;
use App\Models\Attendee;
use App\Models\Ticket;
use Setting;

class AttendeeController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.attendees.partials.form', function ($view)
		{
			if(isset($view->getData()['attendee']))
			{
				$attendee = $view->getData()['attendee'];
//				$view->with('initial_feature_ids', $attendee->features->pluck('id')->all());
			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		$attendee_query = Attendee::query();
		$vars = $this->_index($attendee_query);
		$vars['search_route'] = 'backend.attendees.index';

		return view('backend.attendees.index', $vars);
	}

	public function indexByEvent($event_id)
	{
		$attendee_ids = Ticket::where('event_id', $event_id)->pluck('attendee_id')->all();
		$attendee_query = Attendee::query()->whereIn('id', $attendee_ids);
		$vars = $this->_index($attendee_query);
		$vars['search_route'] = ['backend.events.attendees', $event_id];
		$vars['page_title'] = "Event #$event_id's Attendees";

		return view('backend.attendees.index', $vars);
	}

	protected function _index($attendee_query)
	{
//		if($uid = request('uid'))
//		{
//			$attendee_query = $attendee_query->where('uid', 'LIKE', "%$uid%");
//		}
		if($first_name = request('first_name'))
		{
			$attendee_query = $attendee_query->where('first_name', 'LIKE', "%$first_name%");
		}
		if($last_name = request('last_name'))
		{
			$attendee_query = $attendee_query->where('last_name', 'LIKE', "%$last_name%");
		}
		if($email = request('email'))
		{
			$attendee_query = $attendee_query->where('email', 'LIKE', "%$email%");
		}
		if($rank = request('rank'))
		{
			$attendee_query = $attendee_query->where('rank', 'LIKE', "%$rank%");
		}
//		if($pid = request('pid'))
//		{
//			$attendee_query = $attendee_query->where('pid', 'LIKE', "%$pid%");
//		}

		$attendees = $attendee_query->paginate(Setting::get('attendee-page-length', Setting::get('default-page-length', 50)));
		$vars = compact(
			'first_name',
			'last_name',
			'email',
			'rank',
			'attendees'
		);

		return $vars;
	}

//	public function getCreate()
//	{
//		$vars = [];
//
//		return view('backend.attendees.create', $vars);
//	}
//
//	public function postCreate(PostCreateAttendeeRequest $request)
//	{
//		$attendee = Attendee::create($request->all());
//
//		return Redirect::route('backend.attendees.show', $attendee->id)->with('messages', ['Attendee was successfully created!']);
//	}

	public function show($id)
	{
		$attendee = Attendee::findOrFail($id);
		$vars = compact('attendee');

		return view('backend.attendees.show', $vars);
	}

//	public function getEdit($id)
//	{
//	    $attendee = Attendee::findOrFail($id);
//	    $vars = compact('attendee');
//
//	    return view('backend.attendees.edit', $vars);
//	}
//
//	public function postEdit(PostEditAttendeeRequest $request, $id)
//	{
//		$attendee = $request->attendee;
//		$attendee->fill($request->all());
//		$attendee->save();
//
//		$message = 'Attendee was successfully updated!';
//
//		return Redirect::route('backend.attendees.edit.get', $id)->with('messages', [$message]);
//	}
//
//	public function destroy(DestroyAttendeeRequest $request, $id)
//	{
//		$attendee = $request->attendee;
//		$attendee->delete();
//
//		$message = 'Attendee (ID: ' . $id . ') successfully deleted!';
//
//		return Redirect::route('backend.attendees.index')->with('messages', [$message]);
//	}
}