<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Settings\PostEditSettingRequest;
use App\Http\Requests\CreateSettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use App\Models\Setting;
use Auth;
use Log;
use Redirect;

class SettingController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.settings.partials.form', function ($view)
		{
			$view->with('scope_options', Setting::getScopeOptions(Auth::user()))
				 ->with('type_options', Setting::getTypeOptions());
		});
	}

	public function index()
	{
		$settings = Setting::all();
		$vars = compact('settings');

		return view('backend.settings.index', $vars);
	}

//	public function getSetMarkerIcon()
//	{
//		return view('backend.settings.set_marker_icon');
//	}
//
//	public function postSetMarkerIcon(PostSetMarkerIconRequest $request)
//	{
//		//TODO: Implement using Media
//		$setting = $request->setting;
//		$setting->clearMediaCollection();
//
//		$image_file = $request->file('image');
//		$setting->addMedia($image_file)->toMediaCollection('marker');
//
//		$setting->value = $request->image;
//		$setting->save();
//
//		return Redirect::route('backend.settings.set_marker_icon.get', $setting->id)
//					   ->with('messages', ['Marker Icon was successfully updated!']);
//	}

	public function show($id)
	{
		return Redirect::route('backend.settings.edit.get', $id);
	}

	public function getEdit($id)
	{
		$setting = Setting::findOrFail($id);
		$vars = compact('setting');

		return view('backend.settings.edit', $vars);
	}

	public function postEdit(PostEditSettingRequest $request, $id)
	{
		$setting = $request->setting;

		if($request->get('type') == 'html')
		{
			$request->merge(['value' => $request->get('html_value')]);
		}

		$version = intval($setting->version) + 1;
		$request->merge(['version' => $version]);

		$setting->fill($request->all());
		$setting->save();

		return Redirect::route('backend.settings.edit.get', $setting->id)->with('messages', ['Setting was successfully updated!']);
	}

	//TODO: Test
//	public function restore($id)
//	{
//		$setting = Setting::findOrFail($id);
//		if(!$setting)
//		{
//			Log::error('Setting with ID (' . $id . ') does not exist!');
//
//			return Redirect::to('/')->with('errors', ['Setting with ID (' . $id . ') does not exist!']);
//		}
//
//		$latest_version = $setting->getLatestVersion();
//		if($latest_version->id == $setting->id)
//		{
//			return Redirect::to('/')->with('errors', ['Cannot restore Setting with ID (' . $id . ') since it is already the current version!']);
//		}
//
//		$restored_setting = new Setting();
//		$attributes = $setting->attributes;
//		$attributes['version'] = intval($latest_version->version) + 1;
//		$restored_setting->fill($attributes);
//		$restored_setting->save();
//
//		return Redirect::route('backend.settings.index')->with('messages', ['Setting (ID: ' . $id . ' -> ' . $restored_setting->id . ') successfully restored!']);
//	}
//
//	public function destroy($id)
//	{
//		$setting = Setting::find($id);
//		if(!$setting)
//		{
//			return Redirect::back()
//						   ->with('errors', ['Setting with ID (' . $id . ') does not exist and can not be deleted!']);
//		}
//
//		$setting->delete();
//
//		return Redirect::to('/')->with('messages', ['Setting successfully deleted!']);
//	}
}