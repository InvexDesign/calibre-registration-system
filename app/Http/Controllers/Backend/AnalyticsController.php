<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Event;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\MonthlyFeatureSearchRecord;
use App\Models\Payments\Payment;
use App\Models\Registration;
use App\Models\Ticket;
use App\Role;
use Carbon;
use Illuminate\Database\Query\Builder;

class AnalyticsController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function courses()
	{
		$courses = Course::all();

		$title = 'Course Analytics';
		$vars = compact(
			'courses',
			'title'
		);

		return view('backend.analytics.courses.index', $vars);
	}

	public function coursesTimeline()
	{
		$faker = \Faker\Factory::create();
		$courses = Course::all();
		list($start_at, $end_at) = $this->getDateRange([
			'start_default' => Carbon::today()->firstOfYear(),
			'end_default'   => Carbon::today()->endOfMonth()
		]);

		$course_ids = ($visible_course_ids = \Input::get('courses', false)) ? explode(',', $visible_course_ids) : [];

		$registrations_data = [
			'labels'   => [],
			'datasets' => []
		];
		$tickets_data = [
			'labels'   => [],
			'datasets' => []
		];
		$payments_data = [
			'labels'   => [],
			'datasets' => []
		];

		foreach($courses as $course)
		{
			$event_ids = Event::where('course_id', $course->id)->pluck('id')->all();

			$registrations = Registration::whereIn('event_id', $event_ids)
										 ->whereBetween('created_at', [$start_at, $end_at])
										 ->orderBy('created_at', 'asc')
										 ->get();
			foreach($registrations as $registration)
			{
				$formatted_month = $registration->created_at->format('m/Y');
				if(!isset($registrations_data['labels'][$formatted_month]))
				{
					$registrations_data['labels'][$formatted_month] = $formatted_month;
				}

				if(!isset($registrations_data['datasets'][$course->id]))
				{
					$registrations_data['datasets'][$course->id] = [
						'label'       => $course->name,
						'borderColor' => $faker->hexColor,
						'fill'        => false,
						'data'        => [],
						'hidden'      => $course_ids ? !in_array($course->id, $course_ids) : false
					];
				}

				if(!isset($registrations_data['datasets'][$course->id]['data'][$formatted_month]))
				{
					$registrations_data['datasets'][$course->id]['data'][$formatted_month] = 1;
				}
				else
				{
					$registrations_data['datasets'][$course->id]['data'][$formatted_month] += 1;
				}
			}

			$tickets = Ticket::whereIn('event_id', $event_ids)
							 ->whereBetween('created_at', [$start_at, $end_at])
							 ->orderBy('created_at', 'asc')
							 ->get();
			foreach($tickets as $ticket)
			{
				$formatted_month = $ticket->created_at->format('m/Y');
				if(!isset($tickets_data['labels'][$formatted_month]))
				{
					$tickets_data['labels'][$formatted_month] = $formatted_month;
				}

				if(!isset($tickets_data['datasets'][$course->id]))
				{
					$tickets_data['datasets'][$course->id] = [
						'label'       => $course->name,
						'borderColor' => $faker->hexColor,
						'fill'        => false,
						'data'        => [],
						'hidden'      => $course_ids ? !in_array($course->id, $course_ids) : false
					];
				}

				if(!isset($tickets_data['datasets'][$course->id]['data'][$formatted_month]))
				{
					$tickets_data['datasets'][$course->id]['data'][$formatted_month] = 1;
				}
				else
				{
					$tickets_data['datasets'][$course->id]['data'][$formatted_month] += 1;
				}
			}

			$payments = Payment::whereIn('registration_id', $registrations->pluck('id')->all())
							   ->whereBetween('created_at', [$start_at, $end_at])
							   ->orderBy('created_at', 'asc')
							   ->get();
			foreach($payments as $payment)
			{
				$formatted_month = $payment->created_at->format('m/Y');
				if(!isset($payments_data['labels'][$formatted_month]))
				{
					$payments_data['labels'][$formatted_month] = $formatted_month;
				}

				if(!isset($payments_data['datasets'][$course->id]))
				{
					$payments_data['datasets'][$course->id] = [
						'label'       => $course->name,
						'borderColor' => $faker->hexColor,
						'fill'        => false,
						'data'        => [],
						'hidden'      => $course_ids ? !in_array($course->id, $course_ids) : false
					];
				}

				if(!isset($payments_data['datasets'][$course->id]['data'][$formatted_month]))
				{
					$payments_data['datasets'][$course->id]['data'][$formatted_month] = $payment->amount_in_dollars;
				}
				else
				{
					$payments_data['datasets'][$course->id]['data'][$formatted_month] += $payment->amount_in_dollars;
				}
			}
		}

		$title = 'Course Analytics Timeline';
		$vars = compact(
			'registrations_data',
			'tickets_data',
			'payments_data',
			'start_at',
			'end_at',
			'title'
		);

		return view('backend.analytics.courses.timeline', $vars);
	}

	public function events()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$events = Event::query()
					   ->whereBetween('start_at', [$start_at, $end_at])
					   ->get();

		$title = 'Event Analytics';
		$vars = compact(
			'events',
			'title',
			'start_at',
			'end_at'
		);

		return view('backend.analytics.events.index', $vars);
	}

	public function payments()
	{
		$types = [];
		list($start_at, $end_at) = $this->getDateRange();
		foreach(Payment::getSingleTableTypeMap() as $name => $class)
		{
			$type = [];
			$type['name'] = $name;
			$type['count'] = $class::query()
								   ->whereBetween('paid_at', [$start_at, $end_at])
								   ->count();
			$type['amount'] = $class::query()
									->whereBetween('paid_at', [$start_at, $end_at])
									->sum('amount_in_cents');
			$types[] = $type;
		}

		$title = 'Payment Analytics';
		$vars = compact(
			'types',
			'title',
			'start_at',
			'end_at'
		);

		return view('backend.analytics.payments.index', $vars);
	}

	public function registrations()
	{
		list($start_at, $end_at) = $this->getDateRange();

		$registration_count = Registration::query()->whereBetween('created_at', [$start_at, $end_at])->count();
		$registration_ids = Registration::query()->whereBetween('created_at', [$start_at, $end_at])->pluck('id')->all();
		$total_paid = Payment::whereIn('registration_id', $registration_ids)->sum('amount_in_cents');
		$total_cost = Ticket::whereIn('registration_id', $registration_ids)->sum('price_in_cents');
		$total_ticket_count = Ticket::whereIn('registration_id', $registration_ids)->count();

		$average_ticket_count = ($registration_count > 0) ? $total_ticket_count / $registration_count : 0;
		$average_cost = ($registration_count > 0) ? $total_cost / $registration_count : 0;
		$percent_paid = ($total_cost > 0) ? $total_paid / $total_cost : 0;

		$title = 'Registration Analytics';
		$vars = compact(
			'title',
			'average_cost',
			'average_ticket_count',
			'total_paid',
			'total_cost',
			'percent_paid',
			'registration_count',
			'start_at',
			'end_at'
		);

		return view('backend.analytics.registrations.index', $vars);
	}

	public function salesRepresentatives()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$date_range = [$start_at, $end_at];

		$sales_representatives = [];
		$sales_representative_role = Role::get('sales-representative', true);
		foreach($sales_representative_role->users as $representative)
		{
			$events_query = $representative->events()->whereBetween('created_at', $date_range);
			$event_ids_query = clone $events_query;
			$event_count_query = clone $events_query;
			$goal_query = clone $events_query;
			$event_ids = $event_ids_query->pluck('id')->all();

			$registrations_query = Registration::query()
//											   ->whereBetween('created_at', $date_range)//TODO: Limit by date range? Or only events?
											   ->whereIn('event_id', $event_ids);
			$registration_count_query = clone $registrations_query;
			$registration_id_query = clone $registrations_query;

			$representative->event_count = $event_count_query->count();
			$representative->registration_count = $registration_count_query->count();
			$representative->ticket_count = Ticket::query()
//												  ->whereBetween('created_at', $date_range)//TODO: Limit by date range? Or only events?
												  ->whereIn('event_id', $event_ids)
												  ->count();
			$representative->revenue = Payment::query()
											  ->income()
//											  ->whereBetween('created_at', $date_range)//TODO: Limit by date range? Or only events?
											  ->whereIn('registration_id', $registration_id_query->pluck('id')->all())
											  ->sum('amount_in_cents') / 100;
			$representative->discounts = Payment::query()
												->discounts()
//											  ->whereBetween('created_at', $date_range)//TODO: Limit by date range? Or only events?
												->whereIn('registration_id', $registration_id_query->pluck('id')->all())
												->sum('amount_in_cents') / 100;
			$representative->goal = $goal_query->sum('revenue_goal') / 100;
			$representative->goal_percent = ($representative->goal > 0)
				? $representative->revenue / $representative->goal * 100 : 0;
			$representative->over_under = $representative->revenue - $representative->goal;

			$sales_representatives[] = $representative;
		}

		$title = 'Sales Representative Analytics';
		$vars = compact(
			'title',
			'sales_representatives',
			'start_at',
			'end_at'
		);

		return view('backend.analytics.sales_representatives.index', $vars);
	}

	public function facilityClickTimeline()
	{
		$faker = \Faker\Factory::create();
		$facility_ids = \Input::get('ids', false);
		$facility_ids = $facility_ids ? explode(',', $facility_ids) : false;
		list($start_at, $end_at) = $this->getDateRange(Carbon::today()->startOfMonth()->subMonths(6), Carbon::today()->startOfMonth());

		$data = [
			'labels'   => [],
			'datasets' => []
		];

		$records = MonthlyFacilityClickRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('month', 'asc')->get();

		foreach($records as $record)
		{
			$formatted_month = $record->month->format('m/Y');
			if(!isset($data['labels'][$formatted_month]))
			{
				$data['labels'][$formatted_month] = $formatted_month;
			}

			$facility = $record->facility;

			if(!isset($data['datasets'][$facility->id]))
			{
				$data['datasets'][$facility->id] = [
					'label'       => $facility->name,
					'borderColor' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
					'hidden'      => $facility_ids ? !in_array($facility->id, $facility_ids) : false
				];
			}

			$data['datasets'][$facility->id]['data'][] = $record->hits;
		}

		$title = 'Facility Clicks Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_timeline', $vars);
	}

	public function facilityClickMonthly()
	{
		$faker = \Faker\Factory::create();
		$date = $this->getDate(Carbon::today());
		$start_at = $date->copy()->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Clicks",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyFacilityClickRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('facility_id', 'asc')->get();

		foreach($records as $record)
		{
			$facility = $record->facility;

			$name = $facility->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Facility Clicks Monthly';
		$vars = compact(
			'date',
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}

	public function featureSearchTimeline()
	{
		$faker = \Faker\Factory::create();
		$feature_ids = \Input::get('ids', false);
		$feature_ids = $feature_ids ? explode(',', $feature_ids) : false;
		list($start_at, $end_at) = $this->getDateRange(Carbon::today()->startOfMonth()->subMonths(6), Carbon::today()->startOfMonth());

		$data = [
			'labels'   => [],
			'datasets' => []
		];

		$records = MonthlyFeatureSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('month', 'asc')->get();

		foreach($records as $record)
		{
			$formatted_month = $record->month->format('m/Y');
			if(!isset($data['labels'][$formatted_month]))
			{
				$data['labels'][$formatted_month] = $formatted_month;
			}

			$feature = $record->feature;

			if(!isset($data['datasets'][$feature->id]))
			{
				$data['datasets'][$feature->id] = [
					'label'       => $feature->name,
					'borderColor' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
					'hidden'      => $feature_ids ? !in_array($feature->id, $feature_ids) : false
				];
			}

			$data['datasets'][$feature->id]['data'][] = $record->hits;
		}

		$title = 'Feature Searches Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.feature_searches_timeline', $vars);
	}

	public function featureSearchMonthly()
	{
		$faker = \Faker\Factory::create();
		$date = $this->getDate(Carbon::today());
		$start_at = $date->copy()->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Searches",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyFeatureSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('feature_id', 'asc')->get();

		foreach($records as $record)
		{
			$feature = $record->feature;

			$name = $feature->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Feature Searches Monthly';
		$vars = compact(
			'date',
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}
}