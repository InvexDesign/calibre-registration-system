<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Addresses\DestroyAddressRequest;
use App\Http\Requests\Backend\Addresses\PostCreateAddressRequest;
use App\Http\Requests\Backend\Addresses\PostEditAddressRequest;
use App\Models\Address;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class AddressController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.addresses.partials.form', function ($view)
		{
			$view->with('state_options', State::getSelectOptions());
		});
	}

	public function index()
	{
		$addresses = Address::all();
		$vars = compact('addresses');

		return view('backend.addresses.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.addresses.create', $vars);
	}

	public function postCreate(PostCreateAddressRequest $request)
	{
		$address = Address::create($request->all());

		return Redirect::route('backend.addresses.show', $address->id)->with('messages', ['Address was successfully created!']);
	}

	public function show($id)
	{
		$address = Address::findOrFail($id);
		$vars = compact('address');

		return view('backend.addresses.show', $vars);
	}

	public function getEdit($id)
	{
	    $address = Address::findOrFail($id);
	    $vars = compact('address');

	    return view('backend.addresses.edit', $vars);
	}

	public function postEdit(PostEditAddressRequest $request, $id)
	{
		$address = $request->address;
		$address->fill($request->all());
		$address->save();

		$message = 'Address was successfully updated!';

		return Redirect::route('backend.addresses.edit.get', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyAddressRequest $request, $id)
	{
		$address = $request->address;
		$address->delete();

		$message = 'Address (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.addresses.index')->with('messages', [$message]);
	}
}