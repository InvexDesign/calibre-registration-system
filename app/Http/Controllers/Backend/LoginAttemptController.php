<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\LoginAttempt;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\MonthlyServiceSearchRecord;
use Carbon;

class LoginAttemptController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public function index()
	{
		$page_size = 50;
		$login_attempts = LoginAttempt::query()->orderBy('timestamp', 'desc')->paginate($page_size);

		$vars = compact(
			'login_attempts',
			'page_size'
		);

		return view('backend.login_attempts.index', $vars);
	}

	public function timeline()
	{
		$faker = \Faker\Factory::create();
		$start_at = \Input::get('start', false);
		$start_at = $start_at ? Carbon::parse($start_at) : Carbon::now()->subMonths(6);
		$end_at = \Input::get('end', false);
		$end_at = $end_at ? Carbon::parse($end_at) : Carbon::now();

		$data = [
			'labels'   => [],
			'datasets' => [
				'successes' => [
					'label'       => 'Successful Logins',
					'color' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
				],
				'failures' => [
					'label'       => 'Failed Logins',
					'color' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
				],
			]
		];

		$records = LoginAttempt::query()->whereBetween('timestamp', [$start_at, $end_at])
							   ->orderBy('timestamp', 'asc')
							   ->get()
							   ->groupBy(function ($val)
							   {
								   return Carbon::parse($val->timestamp)->format('m/Y');
							   });
		foreach($records as $month => $monthly_records)
		{
			$data['labels'][] = $month;
			$data['datasets']['successes']['data'][] = $monthly_records->where('success', '=', true)->count();
			$data['datasets']['failures']['data'][] = $monthly_records->where('success', '=', false)->count();
		}

		$title = 'Login Attempts Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.login_attempts_timeline', $vars);
	}

	public function facilityClickMonthly()
	{
		$faker = \Faker\Factory::create();
		$start_at = \Input::get('month', false);
		$start_at = $start_at ? Carbon::parse($start_at) : Carbon::today()->subMonth();
		$start_at = $start_at->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Clicks",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyFacilityClickRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('facility_id', 'asc')->get();

		foreach($records as $record)
		{
			$facility = $record->facility;

			$name = $facility->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Facility Clicks Monthly';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}

	public function serviceSearchTimeline()
	{
		$faker = \Faker\Factory::create();
		$start_at = \Input::get('start', false);
		$start_at = $start_at ? Carbon::parse($start_at) : Carbon::today()->startOfMonth()->subMonths(6);
		$end_at = \Input::get('end', false);
		$end_at = $end_at ? Carbon::parse($end_at) : Carbon::today()->startOfMonth();
		$service_ids = \Input::get('ids', false);
		$service_ids = $service_ids ? explode(',', $service_ids) : false;

		$data = [
			'labels'   => [],
			'datasets' => []
		];

		$records = MonthlyServiceSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('month', 'asc')->get();

		foreach($records as $record)
		{
			$formatted_month = $record->month->format('m/Y');
			if(!isset($data['labels'][$formatted_month]))
			{
				$data['labels'][$formatted_month] = $formatted_month;
			}

			$service = $record->service;

			if(!isset($data['datasets'][$service->id]))
			{
				$data['datasets'][$service->id] = [
					'label'       => $service->name,
					'borderColor' => $faker->hexColor,
					'fill'        => false,
					'data'        => [],
					'hidden'      => $service_ids ? !in_array($service->id, $service_ids) : false
				];
			}

			$data['datasets'][$service->id]['data'][] = $record->hits;
		}

		$title = 'Service Searches Timeline';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.service_searches_timeline', $vars);
	}

	public function serviceSearchMonthly()
	{
		$faker = \Faker\Factory::create();
		$start_at = \Input::get('month', false);
		$start_at = $start_at ? Carbon::parse($start_at) : Carbon::today()->subMonth();
		$start_at = $start_at->startOfMonth();
		$end_at = $start_at->copy()->endOfMonth();

		$data = [
			'labels'   => [],
			'datasets' => [
				'label'           => "Number of Searches",
				'backgroundColor' => [],
				'data'            => []
			]
		];

		$records = MonthlyServiceSearchRecord::query()->whereBetween('month', [
			$start_at,
			$end_at
		])->orderBy('service_id', 'asc')->get();

		foreach($records as $record)
		{
			$service = $record->service;

			$name = $service->name;
			if(!isset($data['labels'][$name]))
			{
				$data['labels'][$name] = $name;
			}

			$data['datasets']['backgroundColor'][] = $faker->hexColor;
			$data['datasets']['data'][] = $record->hits;
		}

		$title = 'Service Searches Monthly';
		$vars = compact(
			'start_at',
			'end_at',
			'data',
			'title'
		);

		return view('backend.analytics.facility_clicks_monthly', $vars);
	}
}