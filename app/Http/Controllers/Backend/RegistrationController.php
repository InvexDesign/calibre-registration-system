<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Registrations\DestroyRegistrationRequest;
use App\Http\Requests\Backend\Registrations\PostChangeAddressRequest;
use App\Http\Requests\Backend\Registrations\PostCreateRegistrationRequest;
use App\Http\Requests\Backend\Registrations\PostEditRegistrationRequest;
use App\Models\Address;
use App\Models\Attendee;
use App\Models\Audit;
use App\Models\DepartmentSize;
use App\Models\Registration;
use App\Models\State;
use App\Models\Statuses\RegistrationPaymentStatus;
use Illuminate\Support\Facades\Redirect;
use Setting;

class RegistrationController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.registrations.partials.form', function ($view)
		{
//			if(isset($view->getData()['registration']))
//			{
//				$registration = $view->getData()['registration'];
//				$view->with('initial_feature_ids', $registration->features->pluck('id')->all());
//			}

			$view->with('department_size_options', DepartmentSize::pluck('name', 'id')->all());
			$view->with('payment_status_options', RegistrationPaymentStatus::getSelectOptions());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$registrations = Registration::query()
									 ->whereBetween('created_at', [$start_at, $end_at])
									 ->paginate(Setting::get('registration-page-length', Setting::get('default-page-length', 50)));

		$vars = compact(
			'registrations',
			'start_at',
			'end_at'
		);

		return view('backend.registrations.index', $vars);
	}

//	public function getCreate()
//	{
//		$vars = [];
//
//		return view('backend.registrations.create', $vars);
//	}
//
//	public function postCreate(PostCreateRegistrationRequest $request)
//	{
//		$registration = Registration::create($request->all());
//
//		return Redirect::route('backend.registrations.show', $registration->id)->with('messages', ['Registration was successfully created!']);
//	}

	public function show($id)
	{
		$registration = Registration::findOrFail($id);
		$audits = $registration->audits()->orderBy('id', 'desc')->take(5)->get();
		$vars = compact(
			'registration',
			'audits'
		);

		return view('backend.registrations.show', $vars);
	}

	public function audits($id)
	{
		$model_is_specified = isset($_GET['model']) && $_GET['model'] != '';
		$registration = Registration::findOrFail($id);
		$attendee_audit_ids = $payment_audit_ids = $registration_audit_ids = $ticket_audit_ids = [];

		if(!$model_is_specified || 'attendees' == $_GET['model'])
		{
			$attendee_ids = $registration->tickets()->pluck('attendee_id')->all();
			$attendee_audit_ids = Audit::attendees($attendee_ids)->pluck('id')->all();
		}

		if(!$model_is_specified || 'payments' == $_GET['model'])
		{
			$payment_ids = $registration->payments()->pluck('payments.id')->all();
			$payment_audit_ids = Audit::payments($payment_ids)->pluck('id')->all();
		}

		if(!$model_is_specified || 'registrations' == $_GET['model'])
		{
			$registration_audit_ids = $registration->audits()->pluck('id')->all();
		}

		if(!$model_is_specified || 'tickets' == $_GET['model'])
		{
			$ticket_ids = $registration->tickets()->pluck('id')->all();
			$ticket_audit_ids = Audit::tickets($ticket_ids)->pluck('id')->all();
		}

		$audit_ids = array_merge($attendee_audit_ids, $payment_audit_ids, $registration_audit_ids, $ticket_audit_ids);
		$audits = Audit::whereIn('id', $audit_ids);
		if(isset($_GET['event']) && $_GET['event'] != '')
		{
			$audits = $audits->where('event', $_GET['event']);
		}
		$audits = $audits->paginate(10);

		$search_route = route('backend.registrations.audits', [$registration->id]);
		$event_options = [
			''         => 'All',
			'created'  => 'Created',
			'updated'  => 'Updated',
			'deleted'  => 'Deleted',
			'restored' => 'Restored'
		];
		$model_options = [
			''              => 'All',
			'attendees'     => 'Attendees',
			'payments'      => 'Payments',
			'registrations' => 'Registrations',
			'tickets'       => 'Tickets'
		];
		$page_title = 'Registration #' . $registration->id . ' Audits';
		$vars = compact(
			'page_title',
			'registration',
			'audits',
			'search_route',
			'clear_route',
			'event_options',
			'model_options'
		);

		return view('backend.audits.index', $vars);
	}

	//TODO: Do we need this in the backend? just use the frontend method?
	public function invoice($id)
	{
		$registration = Registration::findOrFail($id);
		$vars = compact('registration');

		return view('frontend.registrations.invoice', $vars);
	}

	public function getEdit($id)
	{
		$registration = Registration::findOrFail($id);
		$vars = compact('registration');

		return view('backend.registrations.edit', $vars);
	}

	public function postEdit(PostEditRegistrationRequest $request, $id)
	{
		$registration = $request->registration;
		$registration->fill($request->all());
		$registration->save();

		$message = 'Registration was successfully updated!';

		return Redirect::route('backend.registrations.edit.get', $id)->with('messages', [$message]);
	}

	public function getChangeAddress($id)
	{
		$registration = Registration::findOrFail($id);
		$vars = compact('registration');

		return view('backend.registrations.change_address', $vars);
	}

	public function postChangeAddress(PostChangeAddressRequest $request, $id)
	{
		$registration = $request->registration;
		$address = Address::create($request->all());
		$registration->address_id = $address->id;
		$registration->save();

		$message = 'Registration Address was successfully updated!';

		return Redirect::route('backend.registrations.show', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyRegistrationRequest $request, $id)
	{
		$registration = $request->registration;
		$registration->delete();

		$message = 'Registration (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.registrations.index')->with('messages', [$message]);
	}
}