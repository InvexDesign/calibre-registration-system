<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Lodgings\DestroyLodgingRequest;
use App\Http\Requests\Backend\Lodgings\PostCreateLodgingRequest;
use App\Http\Requests\Backend\Lodgings\PostEditLodgingRequest;
use App\Http\Requests\Backend\Lodgings\PostSetEmbeddedMapRequest;
use App\Models\Address;
use App\Models\Lodging;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class LodgingController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.lodgings.partials.form', function ($view)
		{
			if(isset($view->getData()['lodging']))
			{
				$lodging = $view->getData()['lodging'];
//				$view->with('initial_feature_ids', $lodging->features->pluck('id')->all());
			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		$lodgings = Lodging::all();
		$vars = compact('lodgings');

		return view('backend.lodgings.index', $vars);
	}

	public function getCreate()
	{
		$vars = [
			'address' => null
		];

		return view('backend.lodgings.create', $vars);
	}

	public function postCreate(PostCreateLodgingRequest $request)
	{
		$address = Address::create($request->all());
		$request->merge(['address_id' => $address->id]);

		$lodging = Lodging::create($request->all());

		return Redirect::route('backend.lodgings.show', $lodging->id)->with('messages', ['Lodging was successfully created!']);
	}
	
	public function show($id)
	{
		$lodging = Lodging::findOrFail($id);
		$vars = compact('lodging');

		return view('backend.lodgings.show', $vars);
	}

	public function getEdit($id)
	{
		$lodging = Lodging::findOrFail($id);
		$address = $lodging->address;
		$vars = compact(
			'lodging',
			'address'
		);

		return view('backend.lodgings.edit', $vars);
	}

	public function postEdit(PostEditLodgingRequest $request, $id)
	{
		$lodging = $request->lodging;
		$lodging->fill($request->all());
		$lodging->save();

		$address = $lodging->address;
		$address->fill($request->all());
		$address->save();

		$message = 'Lodging was successfully updated!';

		return Redirect::route('backend.lodgings.edit.get', $id)->with('messages', [$message]);
	}

	public function getSetEmbeddedMap($id)
	{
		$lodging = Lodging::findOrFail($id);
		$vars = compact(
			'lodging'
		);

		return view('backend.lodgings.set_embedded_map', $vars);
	}

	public function postSetEmbeddedMap(PostSetEmbeddedMapRequest $request, $id)
	{
		$lodging = $request->lodging;
		$lodging->embedded_map = $request->embedded_map;
		$lodging->save();

		$message = 'Embedded Map was successfully updated!';

		return Redirect::route('backend.lodgings.show', $id)->with('messages', [$message]);
	}

	public function generateEmbeddedMap($id)
	{
		$lodging = Lodging::findOrFail($id);
		$lodging->generateEmbeddedMapAttribute(true);
		$lodging->save();

		$message = 'Embedded Map successfully generated!';

		return Redirect::route('backend.lodgings.show', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyLodgingRequest $request, $id)
	{
		$lodging = $request->lodging;
		$address = $lodging->address;
		$lodging->delete();
		$address->delete();

		$message = 'Lodging (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.lodgings.index')->with('messages', [$message]);
	}
}