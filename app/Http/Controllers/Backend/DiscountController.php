<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Discounts\DestroyDiscountRequest;
use App\Http\Requests\Backend\Discounts\PostCreateDiscountRequest;
use App\Http\Requests\Backend\Discounts\PostEditDiscountRequest;
use App\Models\Payments\Discounts\Discount;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class DiscountController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.discounts.partials.form', function ($view)
		{
//			if(isset($view->getData()['discount']))
//			{
//				$discount = $view->getData()['discount'];
//				$view->with('initial_feature_ids', $discount->features->pluck('id')->all());
//			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		$discounts = Discount::all();
		$vars = compact('discounts');

		return view('backend.discounts.index', $vars);
	}

	public function active()
	{
		$discounts = Discount::active()->get();
		$vars = compact('discounts');

		return view('backend.discounts.index', $vars);
	}

	public function inactive()
	{
		$discounts = Discount::inactive()->get();
		$vars = compact('discounts');

		return view('backend.discounts.index', $vars);
	}

	public function getCreate()
	{
		$vars = [
			'type' => null
		];

		return view('backend.discounts.create', $vars);
	}

	public function postCreate(PostCreateDiscountRequest $request)
	{
		$class = $request->class;
		$discount = $class::create($request->all());
		$discount->save();

		return Redirect::route('backend.discounts.show', $discount->id)->with('messages', ['Discount was successfully created!']);
	}

	public function show($id)
	{
		$discount = Discount::findOrFail($id);
		$vars = compact('discount');

		return view('backend.discounts.show', $vars);
	}

	public function getEdit($id)
	{
	    $discount = Discount::findOrFail($id);
		if($discount->payments()->count() > 0)
		{
			//TODO:
			return Redirect::route('backend.discounts.show', $discount->id)->with('messages', ['This Discount has already been used and cannot be altered!']);
		}
		$type = $discount->getType();
	    $vars = compact('discount', 'type');

	    return view('backend.discounts.edit', $vars);
	}

	public function postEdit(PostEditDiscountRequest $request, $id)
	{
		$discount = $request->discount;
		$discount->fill($request->all());
		$discount->save();

		$message = 'Discount was successfully updated!';

		return Redirect::route('backend.discounts.edit.get', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyDiscountRequest $request, $id)
	{
		$discount = $request->discount;

		if($discount->payments()->count() > 0)
		{
			return Redirect::route('backend.discounts.show', $discount->id)->with('messages', ['This Discount has already been used and cannot be deleted! Please mark it INACTIVE instead.']);
		}
		$discount->delete();

		$message = 'Discount (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.discounts.index')->with('messages', [$message]);
	}
}