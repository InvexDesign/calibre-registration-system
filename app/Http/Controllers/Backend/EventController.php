<?php namespace App\Http\Controllers\Backend;

use App\Helpers\TimezoneHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Events\DestroyEventRequest;
use App\Http\Requests\Backend\Events\PostCreateEventRequest;
use App\Http\Requests\Backend\Events\PostEditEventRequest;
use App\Models\Audit;
use App\Models\Course;
use App\Models\Event;
use App\Models\Lodging;
use App\Models\State;
use App\Models\Venue;
use App\Role;
use Illuminate\Support\Facades\Redirect;

class EventController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS, [
//			'except' => [
//				'index',
//				'show',
//				'getEdit',
//				'postEdit',
//				'markSettled',
//				'markUnsettled'
//			]
//		]);
	}

	public static function composeViews()
	{
		view()->composer('backend.events.partials.form', function ($view)
		{
			if(isset($view->getData()['event']))
			{
				$event = $view->getData()['event'];
				$view->with('start_at_default', $event->start_at->format('m/d/Y'));
				$view->with('end_at_default', $event->end_at ? $event->end_at->format('m/d/Y') : null);
				$view->with('deadline_at_default', $event->deadline_at ? $event->deadline_at->format('m/d/Y') : null);
				$view->with('price_default', $event->price);
				$view->with('initial_sales_representative_id', $event->sales_representative_id);
				$view->with('initial_lodging_ids', $event->lodgings()->pluck('id')->all());
				$view->with('venue_options', [$event->venue_id => $event->venue->name_and_address]);
			}
			else
			{
				$view->with('start_at_default', null);
				$view->with('end_at_default', null);
				$view->with('deadline_at_default', null);
				$view->with('initial_sales_representative_id', null);
				$view->with('venue_options', [0 => 'Search For A Venue...']);
			}

			$view->with('course_options', Course::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
//			$view->with('venue_options', Venue::orderBy('name', 'asc')->get()->pluck('name_and_address', 'id')->all());
			$view->with('sales_representative_options', [0 => 'Unassigned'] + Role::get('sales-representative')->users()->orderBy('users.last_name', 'asc')->get()->pluck('display', 'id')->all());
			$view->with('lodging_options', Lodging::orderBy('name', 'asc')->get()->pluck('name', 'id')->all());
//			$view->with('timezone_options', TimezoneHelper::getOptions());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
			$view->with('default_venue_results', Venue::orderBy('created_at', 'desc')->take(10)->get());
		});
	}

	public function index()
	{
		if(\Auth::user()->isSalesRepresentative())
		{
			return $this->limitedIndex();
		}

		return $this->unlimitedIndex();
	}

	public function unlimitedIndex()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$events = Event::query()
					   ->whereBetween('start_at', [$start_at, $end_at])
					   ->get();
		$vars = compact(
			'events',
			'start_at',
			'end_at'
		);

		return view('backend.events.index', $vars);
	}

	public function limitedIndex()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$events = Event::query()
					   ->where('sales_representative_id', \Auth::user()->id)
					   ->whereBetween('start_at', [$start_at, $end_at])
					   ->get();
		$vars = compact(
			'events',
			'start_at',
			'end_at'
		);

		return view('backend.events.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.events.create', $vars);
	}

	public function postCreate(PostCreateEventRequest $request)
	{
		$event = Event::create($request->all());
		$event->uid = 'Event-' . $event->id;
		$event->state = $event->venue->address->state->short_name;
		$event->save();

		$event->lodgings()->sync($request->lodging_ids);

		return Redirect::route('backend.events.show', $event->id)->with('messages', ['Event was successfully created!']);
	}

	public function show($id)
	{
		$event = Event::findOrFail($id);
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}

		$event_creation_date = $event->created_at->format('n/j/Y');
		$timeline_chart_labels = [$event_creation_date];
		$timeline_chart_data = [
			'registrations' => [$event_creation_date => 0],
			'payments'      => [$event_creation_date => 0]
		];
		foreach($event->registrations()->orderBy('created_at', 'asc')->get() as $registration)
		{
			$timeline_date = $registration->created_at->format('n/j/Y');
			if(!isset($timeline_chart_data['registrations'][$timeline_date]))
			{
				$timeline_chart_labels[] = $timeline_date;
				$timeline_chart_data['registrations'][$timeline_date] = 1;

				if(!isset($timeline_chart_data['payments'][$timeline_date]))
				{
					$timeline_chart_data['payments'][$timeline_date] = 0;
				}
			}
			else
			{
				$timeline_chart_data['registrations'][$timeline_date] += 1;
			}
		}

		foreach($event->getPaymentsQuery()->orderBy('paid_at', 'asc')->get() as $payment)
		{
			$timeline_date = $payment->paid_at->format('n/j/Y');
			if(!isset($timeline_chart_data['payments'][$timeline_date]))
			{
				$timeline_chart_labels[] = $timeline_date;
				$timeline_chart_data['payments'][$timeline_date] = 1;

				if(!isset($timeline_chart_data['registrations'][$timeline_date]))
				{
					$timeline_chart_data['registrations'][$timeline_date] = 0;
				}
			}
			else
			{
				$timeline_chart_data['payments'][$timeline_date] += 1;
			}
		}

		$vars = compact('event', 'timeline_chart_labels', 'timeline_chart_data');

		return view('backend.events.show', $vars);
	}

	public function audits($id)
	{
		$event = Event::findOrFail($id);
		$model_is_specified = isset($_GET['model']) && $_GET['model'] != '';
		$payment_audit_ids = $event_audit_ids = $ticket_audit_ids = [];

		if(!$model_is_specified || $_GET['model'] == 'events')
		{
			$event_audit_ids = $event->audits()->pluck('id')->all();
		}

		if(!$model_is_specified || $_GET['model'] == 'payments')
		{
			$payment_ids = $event->getPaymentsQuery()->pluck('payments.id')->all();
			$payment_audit_ids = Audit::payments($payment_ids)->pluck('id')->all();
		}

		if(!$model_is_specified || $_GET['model'] == 'tickets')
		{
			$ticket_ids = $event->tickets()->pluck('id')->all();
			$ticket_audit_ids = Audit::tickets($ticket_ids)->pluck('id')->all();
		}

		$audit_ids = array_merge($event_audit_ids, $payment_audit_ids, $ticket_audit_ids);
		$audits = Audit::whereIn('id', $audit_ids);
		if(isset($_GET['event']) && $_GET['event'] != '')
		{
			$audits = $audits->where('event', $_GET['event']);
		}
		$audits = $audits->paginate(10);

		$search_route = route('backend.events.audits', [$event->id]);
		$event_options = [
			''         => 'All',
			'created'  => 'Created',
			'updated'  => 'Updated',
			'deleted'  => 'Deleted',
			'restored' => 'Restored'
		];
		$model_options = [
			''         => 'All',
			'events'   => 'Events',
			'payments' => 'Payments',
			'tickets'  => 'Tickets'
		];
		$page_title = 'Event #' . $event->id . ' Audits';
		$vars = compact(
			'page_title',
			'registration',
			'audits',
			'search_route',
			'event_options',
			'model_options'
		);

		return view('backend.audits.index', $vars);
	}

	public function getEdit($id)
	{
		$event = Event::findOrFail($id);
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}

		$vars = compact('event');

		return view('backend.events.edit', $vars);
	}

	public function postEdit(PostEditEventRequest $request, $id)
	{
		$event = $request->event;
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}
		$event->fill($request->all());
		$event->save();

		$event->state = $event->venue->address->state->short_name;
		$event->save();

		$event->lodgings()->sync($request->lodging_ids);

		$message = 'Event was successfully updated!';

		return Redirect::route('backend.events.edit.get', $id)->with('messages', [$message]);
	}

	public function markSettled($id)
	{
		$event = Event::findOrFail($id);
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}
		$event->settled_at = \Carbon::now();
		$event->save();

		return Redirect::route('backend.events.show', $event->id)->with('messages', ['Event was successfully marked Settled!']);
	}

	public function markUnsettled($id)
	{
		$event = Event::findOrFail($id);
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}
		$event->settled_at = null;
		$event->save();

		return Redirect::route('backend.events.show', $event->id)->with('messages', ['Event was successfully marked Unsettled!']);
	}

	public function destroy(DestroyEventRequest $request, $id)
	{
		$event = $request->event;
		if(\Auth::user()->isSalesRepresentative() && (\Auth::user()->id != $event->sales_representative_id))
		{
			return Redirect::route('backend.events.index')->with('messages', ['You are not allowed to view that Event!']);
		}
		$event->delete();

		$message = 'Event (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.events.index')->with('messages', [$message]);
	}
}