<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Registrations\PostApplyAdjustmentRequest;
use App\Http\Requests\Backend\Registrations\PostApplyCheckPaymentRequest;
use App\Http\Requests\Backend\Registrations\PostApplyDiscountRequest;
use App\Models\Payments\Adjustment;
use App\Models\Payments\CheckPayment;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\Payment;
use App\Models\Registration;
use App\Models\State;
use JavaScript;
use Redirect;
use Setting;

class PaymentController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.payments.partials.form', function ($view)
		{
			if(isset($view->getData()['payment']))
			{
				$payment = $view->getData()['payment'];
			}

			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		list($start_at, $end_at) = $this->getDateRange();
		$payments = Payment::query()
						   ->whereBetween('paid_at', [$start_at, $end_at])
						   ->paginate(Setting::get('payment-page-length', Setting::get('default-page-length', 50)));

		$vars = compact(
			'payments',
			'start_at',
			'end_at'
		);

		return view('backend.payments.index', $vars);
	}

	public function show($id)
	{
		$payment = Payment::findOrFail($id);
		$vars = compact('payment');

		return view('backend.payments.show', $vars);
	}

	public function getApplyDiscount($registration_id)
	{
		$registration = Registration::find($registration_id);
		if($registration->payments()->discountPayments()->count() > 0)
		{
			return Redirect::route('backend.registrations.show', $registration_id)->with('errors', ['This Registration already has a discount applied to it!']);
		}

		$discounts = [];
		$discount_options = [null => 'Please Select A Discount'];
		$_discounts = Discount::active()->get();
		foreach($_discounts as $discount)
		{
			if($discount->canBeApplied($registration))
			{
				$discount->_value = $discount->displayDiscount();
				$discount->effect = $discount->calculateDiscount($registration);
				$discounts[$discount->id] = $discount;
				$discount_options[$discount->id] = '#' . $discount->id . ' - ' . $discount->name . ' (' . $discount->code . ') - ' . $discount->_discount;
			}
		}
		JavaScript::put(['discounts' => $discounts]);

		$vars = compact(
			'registration',
			'discounts',
			'discount_options'
		);

		return view('backend.registrations.apply_discount', $vars);
	}

	public function postApplyDiscount(PostApplyDiscountRequest $request, $registration_id)
	{
		$registration = $request->registration;
		$discount = $request->discount;

		$payment = $discount->apply($registration);
		$payment->createNote($request->note, \Auth::user(), false);

		return Redirect::route('backend.registrations.show', $registration_id)->with('messages', ['Discount Payment successfully applied!']);
	}

	public function getApplyCheckPayment($registration_id)
	{
		$registration = Registration::find($registration_id);
		$vars = compact('registration');

		return view('backend.registrations.apply_check_payment', $vars);
	}

	public function postApplyCheckPayment(PostApplyCheckPaymentRequest $request, $registration_id)
	{
		$payment = CheckPayment::create($request->all());
		$payment->createNote($request->note, \Auth::user(), false);

		return Redirect::route('backend.registrations.show', $registration_id)->with('messages', ['Check Payment successfully applied!']);
	}

	public function getApplyAdjustment($registration_id)
	{
		$registration = Registration::find($registration_id);
		$type_options = [
			null     => 'Please Make A Selection',
			'credit' => 'Credit',
			'debit'  => 'Debit',
		];
		$vars = compact(
			'registration',
			'type_options'
		);

		return view('backend.registrations.apply_adjustment', $vars);
	}

	public function postApplyAdjustment(PostApplyAdjustmentRequest $request, $registration_id)
	{
		$adjustment = Adjustment::create($request->all());
		$adjustment->createNote($request->note, \Auth::user(), false);

		return Redirect::route('backend.registrations.show', $registration_id)->with('messages', ['Adjustment successfully applied!']);
	}
}