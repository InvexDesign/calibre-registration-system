<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Courses\DestroyCourseRequest;
use App\Http\Requests\Backend\Courses\PostCreateCourseRequest;
use App\Http\Requests\Backend\Courses\PostEditCourseRequest;
use App\Models\Course;
use App\Models\State;
use Illuminate\Support\Facades\Redirect;

class CourseController extends Controller
{
	function __construct()
	{
		$this->middleware('auth');
//		$this->middleware('require-role:' . ADMINISTRATORS);
	}

	public static function composeViews()
	{
		view()->composer('backend.courses.partials.form', function ($view)
		{
			if(isset($view->getData()['course']))
			{
				$course = $view->getData()['course'];
//				$view->with('initial_feature_ids', $course->features->pluck('id')->all());
			}

//			$view->with('feature_options', Feature::all()->pluck('name', 'id')->all());
			$view->with('state_options', State::all()->pluck('short_name', 'id')->all());
		});
	}

	public function index()
	{
		$courses = Course::all();
		$vars = compact('courses');

		return view('backend.courses.index', $vars);
	}

	public function getCreate()
	{
		$vars = [];

		return view('backend.courses.create', $vars);
	}

	public function postCreate(PostCreateCourseRequest $request)
	{
		$course = Course::create($request->all());
		$course->uid = 'Course-' . $course->id;
		$course->save();

		return Redirect::route('backend.courses.show', $course->id)->with('messages', ['Course was successfully created!']);
	}

	public function show($id)
	{
		$course = Course::findOrFail($id);
		$vars = compact('course');

		return view('backend.courses.show', $vars);
	}

	public function getEdit($id)
	{
	    $course = Course::findOrFail($id);
	    $vars = compact('course');

	    return view('backend.courses.edit', $vars);
	}

	public function postEdit(PostEditCourseRequest $request, $id)
	{
		$course = $request->course;
		$course->fill($request->all());
		$course->save();

		$message = 'Course was successfully updated!';

		return Redirect::route('backend.courses.edit.get', $id)->with('messages', [$message]);
	}

	public function destroy(DestroyCourseRequest $request, $id)
	{
		$course = $request->course;
		$course->delete();

		$message = 'Course (ID: ' . $id . ') successfully deleted!';

		return Redirect::route('backend.courses.index')->with('messages', [$message]);
	}
}