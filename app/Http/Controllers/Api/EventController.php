<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Input;

class EventController extends Controller
{
	public function upcomingEvents()
	{
		$count = Input::get('count', 10);
		$_events = Event::query()
					   ->published()
					   ->upcoming()
					   ->orderBy('start_at', 'asc')
					   ->take($count)
					   ->get();

		$events = [];
		foreach($_events as $_event)
		{
			$event = [];
			$event['display'] = $_event->start_at->format('M j'). ': ' . $_event->course->name . ' (' . $_event->venue->address->city . ', ' . $_event->venue->address->state->short_name . ')';
			$event['url'] = route('frontend.events.show', $_event->uid);
			$events[] = $event;
		}

		return json_encode([
			'success' => true,
			'events'  => $events
		]);
	}
}