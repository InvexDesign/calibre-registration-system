<?php

namespace App\Http\Controllers;

use Carbon;
use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Input;
use Log;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function getDate($default = null)
	{
		if($string = Input::get('date'))
		{
			try
			{
				$date = Carbon::createFromFormat('m-d-Y', $string);
			}
			catch(Exception $e)
			{
				Log::error($e);
				Log::error('Controller: Error parsing date');

				return null;
			}
		}
		else
		{
			$date = $default ?: Carbon::today();
		}

		return $date;
	}

	protected function getDateRange($parameters = [])
	{
		$defaults = [
			'start_name'    => 'start',
			'end_name'      => 'end',
			'start_default' => null,
			'end_default'   => null,
			'format'        => 'm-d-Y',
			'is_inclusive'  => true,
		];
		$parameters = $parameters + $defaults;

		$_start = Input::get($parameters['start_name'], false);
		$_end = Input::get($parameters['end_name'], false);

		if($_start && $_end)
		{

			try
			{
				$start_at = Carbon::createFromFormat($parameters['format'], $_start)->startOfDay();
				$end_at = Carbon::createFromFormat($parameters['format'], $_end)->startOfDay();

				if($parameters['is_inclusive'])
				{
					$end_at = $end_at->addHours(23)->addMinutes(59)->addSeconds(59);
				}
			}
			catch(Exception $e)
			{
				Log::error($e);
				Log::error('Controller: Error parsing start/end dates');

				return null;
			}
		}
		else
		{
			$start_at = $parameters['start_default'] ?: Carbon::today()->firstOfMonth();
			$end_at = $parameters['end_default'] ?: Carbon::today()->endOfMonth();
		}

		return [$start_at, $end_at];
	}
}
