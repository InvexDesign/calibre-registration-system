<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Registration;

class RecaptchaController extends Controller
{
	public function verify()
	{
		$vars = [];

		return view('frontend.recaptcha.verify', $vars);
	}

	public function fail()
	{
		//TODO: register failed attempt
		//TODO: google analytics
		//TODO: throw to error page

		return 'fail - todo';
	}
}