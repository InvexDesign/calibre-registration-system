<?php namespace App\Http\Controllers\Frontend\Workflows;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Registrations\PostProcessPaymentRequest;
use App\Mailers\CustomerMailer;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\Gateways\AuthorizeNetPaymentGateway;
use App\Models\Registration;
use App\Models\State;
use Input;
use Redirect;
use Session;

class PaymentWorkflowController extends Controller
{
	public function getProcessPayment($registration_uid, $passcode)
	{
		$user_is_admin = \Auth::user() ? \Auth::user()->isAdministrator() : false;

		$registration = Registration::where('uid', $registration_uid)->get()->first();
		if(!$registration)
		{
			return Redirect::route('frontend.index');
		}
		if($registration->passcode != $passcode)
		{
			return Redirect::route('frontend.index');
		}
		$state_options = State::getSelectOptions();
		$cost_in_cents = $registration->getBalanceInCents();
		if(!$user_is_admin && $cost_in_cents <= 0)
		{//Disallow frontend users from making payments beyond 100%
			return Redirect::route('frontend.registrations.show', [$registration_uid, $passcode])
						   ->with('messages', ['This Registration has already been paid in full!']);
		}
		$amount_in_cents = $cost_in_cents;

		$discount = null;
		$allow_discounts = false;
		if($registration->payments()->discounts()->count() == 0)
		{//Only allow automatic discount application if there are none already applied
			$allow_discounts = true;
			$discount = Discount::findAutomaticallyApplicableDiscounts($registration);//TODO: Uncomment this
			if($discount)
			{
				$discount_in_cents = $discount->calculateDiscount($registration);
				$amount_in_cents -= $discount_in_cents;
				Session::put('discount_id', $discount->id);
			}
		}

		$amount = $amount_in_cents / 100;
		$vars = compact(
			'registration',
			'amount',
			'allow_discounts',
			'discount',
			'state_options'
		);

		if(isset($_GET['randomize']))
		{
			$faker = \Faker\Factory::create();

			$fake_cards = [
				'American Express' => ['370000000000002'],
				'Discover'         => ['6011000000000012'],
				'JCB'              => ['3088000000000017'],
				'Diners Club'      => ['38000000000006'],
				'Visa'             => [
					'4007000000027',
					'4012888818888',
					'4111111111111111'
				],
				'MasterCard'       => [
					'5424000000000015',
					'2223000010309703',
					'2223000010309711'
				],
			];

			$brand = $faker->randomElement(array_keys($fake_cards));
			$vars['name'] = $faker->firstName . ' ' . $faker->lastName;
			$vars['card_number'] = $faker->randomElement($fake_cards[$brand]);
			$vars['card_code'] = ($brand == 'American Express') ? $faker->numberBetween(1000, 9999)
				: $faker->numberBetween(100, 999);
			$expiration_month = $faker->boolean() ? '0' . $faker->numberBetween(1, 9)
				: '' . $faker->numberBetween(10, 12);
			$expiration_year = '20' . $faker->numberBetween(18, 99);
			$vars['expiration'] = $expiration_month . $expiration_year;
			$vars['address'] = $faker->streetAddress;
			$vars['city'] = $faker->city;
			$vars['state_id'] = $faker->randomElement(State::query()->pluck('id')->all());
			$vars['zipcode'] = $faker->numberBetween(10000, 99999);
			$vars['is_fake'] = true;
		}

		return view('frontend.workflows.payment.pay', $vars);
	}

	//TODO: Review this. Should the query filter by start/end/etc?
	public function applyDiscount()
	{
		$registration_uid = Input::get('registration_uid', false);
		$registration = Registration::where('uid', $registration_uid)->get()->first();
		if(!$registration)
		{
			//This should only happen if hacking is going on...
			//TODO: Log this attempt? Alert admins?
			return json_encode([
				'success' => false,
				'message' => ''
			]);
		}

		$registration_passcode = Input::get('registration_passcode', false);
		if($registration->passcode != $registration_passcode)
		{
			//This should only happen if hacking is going on...
			//TODO: Log this attempt? Alert admins?
			return json_encode([
				'success' => false,
				'message' => ''
			]);
		}

		$code = Input::get('code', false);
		if(!$code || $code == '')
		{
			//TODO: Log this? Alert devs?
			return json_encode([
				'success' => false,
				'message' => ''
			]);
		}

		$discounts = Discount::where('code', $code)
							 ->where('is_active', true)
							 ->where(function ($query)
							 {
								 $query->whereNull('start_at');
								 $query->orWhere('start_at', '<=', \Carbon::now());
							 })
							 ->where(function ($query)
							 {
								 $query->whereNull('end_at');
								 $query->orWhere('end_at', '>', \Carbon::now());
							 })
							 ->get();
		$discounts->filter(function ($discount, $key) use ($registration)
		{
			return $discount->canBeApplied($registration);
		});

		if($discounts->count() > 1)
		{
			//TODO: Log this, inform admins
			return json_encode([
				'success' => false,
				'message' => 'Malformed Discount!'
			]);
		}

		if($discounts->count() < 1)
		{
			return json_encode([
				'success' => false,
				'message' => 'No Discounts Found!'
			]);
		}

		$discount = $discounts->first();
		if(!$discount)
		{
			return json_encode([
				'success' => false,
				'message' => 'No Discounts Found!'
			]);
		}

		$discount_in_cents = $discount->calculateDiscount($registration);
		$amount_in_cents = $registration->getTotalCostInCents() - $discount_in_cents;

		Session::put('discount_id', $discount->id);

		$discount = [
			'name'     => $discount->name,
			'code'     => $discount->code,
			'discount' => $discount_in_cents / 100,
		];

		return json_encode([
			'success'  => true,
			'message'  => 'Discount successfully applied!',
			'discount' => $discount,
			'amount'   => $amount_in_cents / 100
		]);
	}

	public function postProcessPayment(PostProcessPaymentRequest $request, $registration_uid, $passcode)
	{
		$registration = $request->registration;
		$amount_in_cents = $registration->getBalanceInCents();

		$discount = null;
		$discount_payment = null;
		$discount_id = Session::get('discount_id');
		if($discount_id)
		{
			$discount = Discount::find($discount_id);
			$discount_in_cents = $discount->calculateDiscount($registration);
			$discount_payment = $discount->apply($registration);
			$amount_in_cents -= $discount_in_cents;
		}

		$result = AuthorizeNetPaymentGateway::chargeCreditCard($registration, $request->card_number, $request->expiration_month, $request->expiration_year, $request->card_code, $amount_in_cents);
		if(!$result->success)
		{
			if($discount_payment)
			{
				$discount_payment->delete();
			}

			return Redirect::route('frontend.workflows.payment.get', [$registration_uid, $passcode])
						   ->withInput()
						   ->with('messages', ['Payment Failed!', $result->error]);
		}

		//Only Apply discount if card has been charged
//		if($discount)
//		{
//			$discount->apply($registration);
//		}

		$payment = $result->payment;
		CustomerMailer::emailPaymentConfirmation($payment);

		$vars = [
			'payment' => $payment
		];

		Session::forget(['discount_id', 'amount_in_cents']);

		return view('frontend.workflows.payment.confirmation', $vars);
	}
}