<?php namespace App\Http\Controllers\Frontend\Workflows;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Registrations\PostRegisterRequest;
use App\Mailers\CustomerMailer;
use App\Models\Address;
use App\Models\Attendee;
use App\Models\DepartmentSize;
use App\Models\Event;
use App\Models\Registration;
use App\Models\State;
use App\Models\Statuses\RegistrationPaymentStatus;
use App\Models\Ticket;
use Input;
use Redirect;
use Setting;

class RegistrationWorkflowController extends Controller
{
	public function getRegister($uid)
	{
		$event = Event::where('uid', $uid)->get()->first();
		if(!$event)
		{
			abort(404);

			return false;
		}

		$state_options = State::getSelectOptions();
		$department_size_options = DepartmentSize::pluck('name', 'id')->all();
		$max_attendee_count = Setting::get('max-attendee-count', 30);
		$attendee_count_options = [];
		for($i = 1; $i <= $max_attendee_count; $i++)
		{
			$attendee_count_options[$i] = $i;
		}

		$vars = compact(
			'event',
			'state_options',
			'department_size_options',
			'attendee_count_options',
			'max_attendee_count'
		);

		if(isset($_GET['randomize']))
		{
			$faker = \Faker\Factory::create();
			$registration = (object)[
				'agency'             => $faker->company,
				'contact'            => $faker->firstName . ' ' . $faker->lastName,
				'email'              => $faker->companyEmail,
				'phone'              => $faker->numberBetween(1000000000, 9999999999),
				'fax'                => $faker->numberBetween(1000000000, 9999999999),
				'department_size_id' => $faker->randomElement(array_keys($department_size_options)),
			];

			$vars['registration'] = $registration;

			$address = (object)[
				'line1'    => $faker->streetAddress,
				'line2'    => $faker->boolean() ? 'Suite #' . $faker->numberBetween(10, 9999) : null,
				'city'     => $faker->city,
				'state_id' => $faker->randomElement(array_keys($state_options)),
				'zipcode'  => $faker->numberBetween(10000, 99999),
				'country'  => $faker->country,
			];
			$vars['address'] = $address;

			$attendees = [];
			$attendee_count = $faker->numberBetween(1, intval($max_attendee_count / 3));
			for($j = 0; $j < $attendee_count; $j++)
			{
				$attendees[] = (object)[
					'first_name' => $faker->firstName,
					'last_name'  => $faker->lastName,
					'email'      => $faker->email,
					'rank'       => $faker->word,
					'pid'        => $faker->numberBetween(1000, 9999),
				];

				$vars['attendees'] = $attendees;
				$vars['attendee_count'] = $attendee_count;
			}
		}

		return view('frontend.workflows.registration.register', $vars);
	}

	public function postRegister(PostRegisterRequest $request)
	{
		$address = Address::create($request->address);
		$registration_attributes = $request->registration;
		$registration_attributes['address_id'] = $address->id;
		$registration_attributes['payment_status'] = RegistrationPaymentStatus::UNPAID;
		$registration = Registration::create($registration_attributes);

		$event = $registration->event;
		for($i = 0; $i < $request->attendee_count; $i++)
		{
			$attendee_attributes = $request->attendees[$i];
//			$attendee_attributes['registration_id'] = $registration->id;

			$attendee = Attendee::create($attendee_attributes);
			Ticket::create([
				'event_id'        => $registration->event_id,
				'registration_id' => $registration->id,
				'attendee_id'     => $attendee->id,
				'price_in_cents'  => $event->price_in_cents,
			]);
		}

		CustomerMailer::emailRegistrationConfirmation($registration);

		return Redirect::route('frontend.registrations.show', [
			$registration->uid,
			$registration->passcode
		])->with('messages', ['Registration Complete!']);
	}
}