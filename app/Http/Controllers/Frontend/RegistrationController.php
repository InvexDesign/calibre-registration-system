<?php namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Registration;
use Redirect;

class RegistrationController extends Controller
{
	public function show($uid, $passcode)
	{
		//TODO: Check for boolean set by admins allowing frontend viewing?
		$registration = Registration::where('uid', $uid)->get()->first();
		if(!$registration)
		{
			return Redirect::route('frontend.index');
		}
		if($registration->passcode != $passcode)
		{
			return Redirect::route('frontend.index');
		}
		$vars = compact('registration');

		return view('frontend.registrations.show', $vars);
	}

	public function invoice($uid, $passcode)
	{
		//TODO: Check for boolean set by admins allowing frontend viewing?
		$registration = Registration::where('uid', $uid)->get()->first();
		if(!$registration)
		{
			return Redirect::route('frontend.index');
		}
		if($registration->passcode != $passcode)
		{
			return Redirect::route('frontend.index');
		}
		$vars = compact('registration');

		return view('frontend.registrations.invoice', $vars);
	}
}