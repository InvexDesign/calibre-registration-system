<?php namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\MonthlyFacilityClickRecord;
use App\Models\Feature;
use Exception;
use Input;

class FrontendController extends Controller
{
	public function index()
	{
//		$ch = curl_init();
//		curl_setopt($ch, CURLOPT_URL, "http://localhost:8000/api/upcoming-events");
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		$output = curl_exec($ch);
//		curl_close($ch);

		$output = "{\"success\":true,\"events\":[{\"display\":\"Aug 10: Longshoremen And Tractor Beams (Huelsmouth, MT)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-45\"},{\"display\":\"Aug 10: AK47s And Tazers (Ilianahaven, AK)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-90\"},{\"display\":\"Aug 11: Tazers And Perps (Barneybury, VA)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-9\"},{\"display\":\"Aug 12: Tazers And Perps (West Claudiaview, ND)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-71\"},{\"display\":\"Aug 16: Body Cams And The Badge (Skilesville, HI)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-2\"},{\"display\":\"Aug 17: Tractor Beams And Smoke Grenades (Krystinashire, ID)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-39\"},{\"display\":\"Aug 18: Utility Belts And Body Armor (Domenickbury, NE)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-1\"},{\"display\":\"Aug 20: Tractor Beams And Smoke Grenades (Domenickbury, NE)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-72\"},{\"display\":\"Aug 20: AK47s And Tazers (Moenborough, MI)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-89\"},{\"display\":\"Aug 23: Tractor Beams And Smoke Grenades (Dannyborough, KS)\",\"url\":\"http:\/\/localhost:8000\/events\/EVENT-30\"}]}";
//		dd($output);
		$vars = [];
		$response = json_decode($output);
		if($response && $response->success)
		{
			$events = $response->events;
			$vars = compact('events');
		}

		return view('frontend.index', $vars);
	}

	public function w9()
	{
		return 'todo';
	}
}