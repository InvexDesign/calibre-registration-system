<?php namespace App\Http\Controllers\Frontend;

use App\Helpers\TimezoneHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Events\DestroyEventRequest;
use App\Http\Requests\Backend\Events\PostCreateEventRequest;
use App\Http\Requests\Backend\Events\PostEditEventRequest;
use App\Http\Requests\Backend\Registrations\PostRegisterRequest;
use App\Models\Address;
use App\Models\Attendee;
use App\Models\Course;
use App\Models\DepartmentSize;
use App\Models\Event;
use App\Models\Registration;
use App\Models\State;
use App\Models\Ticket;
use App\Models\Venue;
use Illuminate\Support\Facades\Redirect;
use Input;
use Setting;

class EventController extends Controller
{
	public function index()
	{
		$events_query = Event::query();
		if($course_uid = Input::get('course', false))
		{
			$course = Course::where('uid', $course_uid)->get()->first();
			if($course)
			{
				$events_query->where('course_id', $course->id);
			}
		}
		if($state = Input::get('state', false))
		{
			$events_query->where('state', $state);
		}
		$events = $events_query
			->where('start_at', '>=', \Carbon::today('UTC'))
			->orderBy('start_at', 'asc')
			->get();

		$vars = compact('events');

		return view('frontend.events.index', $vars);
	}

	public function map()
	{
		list($start_at, $end_at) = $this->getDateRange([
			'start_default' => \Carbon::today()->subMonth(),
			'end_default'   => \Carbon::today()->addMonth(),
		]);
		$events_query = Event::query()
							 ->where('start_at', '>=', \Carbon::today('UTC'))
							 ->whereBetween('start_at', [$start_at, $end_at]);
		if($course_uid = Input::get('course', false))
		{
			$course = Course::where('uid', $course_uid)->get()->first();
			if($course)
			{
				$events_query->where('course_id', $course->id);
			}
		}
		if($state = Input::get('state', false))
		{
			$events_query->where('state', $state);
		}
		$events = $events_query
			->orderBy('start_at', 'asc')
			->get();

		$markers = [];
		foreach($events as $event)
		{
			$marker = [
				'title'     => htmlentities($event->displayClass(), ENT_QUOTES),
				'course'    => htmlentities($event->course->display(), ENT_QUOTES),
				'link'      => route('frontend.events.show', [$event->uid]),
				'latitude'  => $event->venue->latitude,
				'longitude' => $event->venue->longitude,
				'start'     => $event->start_at->format('m/d/Y'),
				'location'  => htmlentities($event->venue->address->city . ', ' . $event->venue->address->state->short_name, ENT_QUOTES),
				'venue'     => htmlentities($event->venue->name, ENT_QUOTES)
			];

			$markers[] = $marker;
		}

		$vars = compact(
			'markers',
			'start_at',
			'end_at'
		);

		return view('frontend.events.map', $vars);
	}

	public function show($uid)
	{
		$event = Event::where('uid', $uid)->get()->first();
		if(!$event)
		{
			abort(404);

			return false;
		}

		$vars = compact('event');

		return view('frontend.events.show', $vars);
	}
}