<?php namespace App\Http\Requests;

use App\Models\User;

class CreateUserRequest extends BaseRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = User::getRules();
//		$rules['username'] = $rules['username'] . '|unique:users,username';
		$rules['email'] = $rules['email'] . '|unique:users,email';
		$rules['role_id'] = 'required|' . $rules['role_id'];
		$rules['kitchen_ids'] = 'required|' . $rules['kitchen_ids'];

		return $rules;
	}
}