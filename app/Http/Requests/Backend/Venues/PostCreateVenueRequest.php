<?php namespace App\Http\Requests\Backend\Venues;

use App\Http\Requests\BaseRequest;
use App\Models\State;

class PostCreateVenueRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'      => '',
			'name'     => 'required',
			'phone'    => '',
			'email'    => '',
			'line1'    => 'required',
			'line2'    => '',
			'city'     => 'required',
			'state_id' => 'required|exists:states,id',
			'zipcode'  => 'required',
			'country'  => 'required',
			'notes'    => '',
		]);

		if($this->get('auto_gps', false))
		{
			$formatted_address = str_replace([".", ","], ["", ""], $this->get('line1'));
			$formatted_address = str_replace(" ", "+", $formatted_address);

			$formatted_city = str_replace([".", ","], ["", ""], $this->get('city'));
			$formatted_city = str_replace(" ", "+", $formatted_city);

			$state = State::find($this->get('state_id'));
			$formatted_state = $state->short_name;

			$zipcode = $this->get('zipcode');

			$geocode_url = "http://maps.google.com/maps/api/geocode/json?address={$formatted_address}+{$formatted_city}+{$formatted_state}+{$zipcode}&sensor=false";
			\Log::debug("AUTO GPS: ATTEMPT - ($geocode_url)");

			$tries = 5;
			while($tries > 0)
			{
				$response = file_get_contents($geocode_url);
				$json = json_decode($response);

				if($json->status == "OK")
				{
					$this->merge([
						'latitude'  => $json->results[0]->geometry->location->lat,
						'longitude' => $json->results[0]->geometry->location->lng
					]);
					break;
				}

				\Log::error("AUTO GPS: FAILED - ($geocode_url)");
				$tries--;
			}
		}
	}

	public function authorize()
	{
		return true;
	}
}
