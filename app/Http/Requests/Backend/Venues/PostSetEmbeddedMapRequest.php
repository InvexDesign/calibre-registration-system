<?php namespace App\Http\Requests\Backend\Venues;

use App\Http\Requests\BaseRequest;
use App\Models\Venue;
use HttpResponseException;
use Redirect;

class PostSetEmbeddedMapRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$venue = Venue::find($id);
		if(!$venue)
		{
			$redirect = Redirect::route('backend.venues.index')->with('errors', ["Venue #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'embedded_map' => 'required',
		]);

		$this->merge(['venue' => $venue]);
	}

	public function authorize()
	{
		return true;
	}
}
