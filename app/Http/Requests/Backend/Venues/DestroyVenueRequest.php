<?php namespace App\Http\Requests\Backend\Venues;

use App\Http\Requests\BaseRequest;
use App\Models\Venue;
use HttpResponseException;
use Redirect;

class DestroyVenueRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$venue = Venue::find($id);
		if(!$venue)
		{
			$redirect = Redirect::route('backend.venues.index')->with('errors', ["Venue #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['venue' => $venue]);
	}

	public function authorize()
	{
		return true;
	}
}
