<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\DepartmentSize;
use App\Models\Registration;
use App\Models\Statuses\RegistrationPaymentStatus;
use HttpResponseException;
use Redirect;

class PostChangeAddressRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}
		$this->_validate($this->all(), [
			'line1'    => 'required',
			'line2'    => '',
			'city'     => 'required',
			'state_id' => 'required|exists:states,id',
			'zipcode'  => 'required',
			'country'  => 'required',
		]);

		$this->merge(['registration' => $registration]);
	}

	public function authorize()
	{
		return true;
	}
}
