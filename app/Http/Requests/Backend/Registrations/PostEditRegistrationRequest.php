<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\DepartmentSize;
use App\Models\Registration;
use App\Models\Statuses\RegistrationPaymentStatus;
use HttpResponseException;
use Redirect;

class PostEditRegistrationRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'uid'                => '',
			'agency'             => 'required',
			'contact'            => 'required',
			'email'              => 'required|email',
			'department_size_id' => 'required|exists:department_sizes,id',
			'phone'              => 'required',
			'fax'                => 'required',
			'payment_status'     => 'required',
			'notes'              => '',
		]);

		$this->merge([
			'registration' => $registration
		]);
	}

	public function authorize()
	{
		return true;
	}
}
