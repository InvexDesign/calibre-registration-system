<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Registration;
use HttpResponseException;
use Redirect;

class DestroyRegistrationRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['registration' => $registration]);
	}

	public function authorize()
	{
		return true;
	}
}
