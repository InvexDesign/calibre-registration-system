<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\State;

class PostCreateRegistrationRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'event_id'        => 'required|exists:events,id',
			'agency'          => 'required',
			'contact'         => 'required',
			'email'           => 'required|email',
			'department_size' => 'required',
			'phone'           => 'required',
			'fax'             => 'required',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
