<?php namespace App\Http\Requests\Backend\Settings;

use App\Http\Requests\BaseRequest;
use App\Models\Setting;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class PostEditSettingRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$setting = Setting::find($id);
		if(!$setting)
		{
			$redirect = Redirect::route('backend.settings.index')->with('errors', ["Setting #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'key'         => 'required|regex:/^[a-zA-Z0-9\-_]*$/|unique:settings,key,' . $this->get('setting_id'),
			'value'       => 'required_without:html_value',
			'html_value'  => 'required_without:value',
			'type'        => 'regex:/^[a-zA-Z0-9\-_ ]*$/',
			'description' => '',
			'user_id'     => 'exists:users,id',
		]);

		$this->merge(['setting' => $setting]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
