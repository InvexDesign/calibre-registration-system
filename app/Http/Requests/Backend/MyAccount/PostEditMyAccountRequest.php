<?php namespace App\Http\Requests\Backend\MyAccount;

use App\Http\Requests\BaseRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class PostEditMyAccountRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$user = \Auth::user();

		$this->_validate($this->all(), [
			'username'   => 'required|unique:users,username,' . $user->id,
			'email'      => 'required|email|unique:users,email,' . $user->id,
			'first_name' => '',
			'last_name'  => '',
		]);

		$this->merge([
			'user' => $user,
		]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
