<?php namespace App\Http\Requests\Backend\Addresses;

use App\Http\Requests\BaseRequest;
use App\Models\State;

class PostCreateAddressRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'notes'       => '',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
