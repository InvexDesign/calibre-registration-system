<?php namespace App\Http\Requests\Backend\Addresses;

use App\Http\Requests\BaseRequest;
use App\Models\Address;
use HttpResponseException;
use Redirect;

class DestroyAddressRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$address = Address::find($id);
		if(!$address)
		{
			$redirect = Redirect::route('backend.addresses.index')->with('errors', ["Address #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['address' => $address]);
	}

	public function authorize()
	{
		return true;
	}
}
