<?php namespace App\Http\Requests\Backend\Lodgings;

use App\Http\Requests\BaseRequest;
use App\Models\Lodging;
use HttpResponseException;
use Redirect;

class PostEditLodgingRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$lodging = Lodging::find($id);
		if(!$lodging)
		{
			$redirect = Redirect::route('backend.lodgings.index')->with('errors', ["Lodging #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'name'        => 'required',
			'phone'       => 'required_without:email',
			'email'       => 'required_without:phone',
			'contact'     => '',
			'line1'       => 'required',
			'line2'       => '',
			'city'        => 'required',
			'state_id'    => 'required|exists:states,id',
			'zipcode'     => 'required',
			'country'     => 'required',
			'description' => '',
			'notes'       => '',
		]);

		$this->merge(['lodging' => $lodging]);
	}

	public function authorize()
	{
		return true;
	}
}
