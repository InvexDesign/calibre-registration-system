<?php namespace App\Http\Requests\Backend\Lodgings;

use App\Http\Requests\BaseRequest;

class PostCreateLodgingRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'name'        => 'required',
			'phone'       => 'required_without:email',
			'email'       => 'required_without:phone',
			'contact'     => '',
			'line1'       => 'required',
			'line2'       => '',
			'city'        => 'required',
			'state_id'    => 'required|exists:states,id',
			'zipcode'     => 'required',
			'country'     => 'required',
			'description' => '',
			'notes'       => '',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
