<?php namespace App\Http\Requests\Backend\Lodgings;

use App\Http\Requests\BaseRequest;
use App\Models\Lodging;
use HttpResponseException;
use Redirect;

class DestroyLodgingRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$lodging = Lodging::find($id);
		if(!$lodging)
		{
			$redirect = Redirect::route('backend.lodgings.index')->with('errors', ["Lodging #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['lodging' => $lodging]);
	}

	public function authorize()
	{
		return true;
	}
}
