<?php namespace App\Http\Requests\Backend\Notes;

use App\Http\Requests\BaseRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostCreateNoteRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'id'      => 'required',
			'class'   => 'required',
			'content' => 'required',
		]);

		$class = $this->class;
		$model = $class::find($this->id);
		if(!$model)
		{
			$redirect = Redirect::back()->with('errors', ["Noteable Model does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['model' => $model]);
	}

	public function authorize()
	{
		return true;
	}
}
