<?php namespace App\Http\Requests\Backend\Notes;

use App\Http\Requests\BaseRequest;
use App\Models\Notes\Note;
use HttpResponseException;
use Redirect;

class DestroyNoteRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$note = Note::find($id);
		if(!$note)
		{
			$redirect = Redirect::route('backend.notes.index')->with('errors', ["Note #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['note' => $note]);
	}

	public function authorize()
	{
		return \Auth::user()->isOverlord();
	}
}
