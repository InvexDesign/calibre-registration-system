<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Registration;
use Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostApplyCheckPaymentRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'amount'  => 'required',
			'paid_at' => 'datepicker',
		]);

		if(!$this->get('paid_at', false))
		{
			$this->merge(['paid_at' => Carbon::now()]);
		}

		$amount_in_cents = intval(floatval(preg_replace('/[^\d\.]/', '', $this->amount)) * 100);
		if($amount_in_cents <= 0)
		{
			$redirect = Redirect::back()->withInput()->with('errors', ['Amount must be greater than zero!']);
			throw new HttpResponseException($redirect);
		}

		$this->merge([
			'registration'    => $registration,
			'registration_id' => $registration->id,
			'amount_in_cents' => $amount_in_cents,
			'paid_at'         => Carbon::now()
		]);

	}

	public function authorize()
	{
		return true;
	}
}
