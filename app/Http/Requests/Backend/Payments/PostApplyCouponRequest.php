<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Payments\Discounts\Discount;
use App\Models\Registration;
use Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostApplyDiscountRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$discount = Discount::find($this->discount_id);
		if(!$discount)
		{
			$redirect = Redirect::back()->withInput()->with('errors', ["Discount #" . $this->discount_id . " does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		if(!$discount->canBeApplied($registration))
		{
			$redirect = Redirect::back()->withInput()->with('errors', ["Discount cannot be applied!"]);
			throw new HttpResponseException($redirect);
		}

		if($registration->getBalanceInCents() <= 0)
		{
			$redirect = Redirect::back()->withInput()->with('errors', ["Registration has a balance of zero!"]);
			throw new HttpResponseException($redirect);
		}

		if($discount->calculateDiscount($registration) <= 0)
		{
			$redirect = Redirect::back()->withInput()->with('errors', ["Discount could have no effect!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge([
			'registration' => $registration,
			'discount'       => $discount,
		]);

	}

	public function authorize()
	{
		return true;
	}
}
