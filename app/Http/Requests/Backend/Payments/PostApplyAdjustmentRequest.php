<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Registration;
use Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostApplyAdjustmentRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$registration = Registration::find($id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'amount' => 'required',
			'note'  => '',
		]);

		switch($this->type)
		{
			case 'debit':
				$sign = -1;
				break;
			case 'credit':
				$sign = 1;
				break;
			default:
				$redirect = Redirect::back()->withInput()->with('errors', ["Type is invalid!"]);
				throw new HttpResponseException($redirect);
		}

		$this->merge([
			'registration_id' => $registration->id,
			'amount_in_cents' => $sign * intval(floatval(preg_replace('/[^\d\.]/', '', $this->amount)) * 100),
			'paid_at'         => Carbon::now()
		]);
	}

	public function authorize()
	{
		return true;
	}
}
