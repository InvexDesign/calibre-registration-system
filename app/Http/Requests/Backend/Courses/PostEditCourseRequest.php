<?php namespace App\Http\Requests\Backend\Courses;

use App\Http\Requests\BaseRequest;
use App\Models\Course;
use HttpResponseException;
use Redirect;

class PostEditCourseRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$course = Course::find($id);
		if(!$course)
		{
			$redirect = Redirect::route('backend.courses.index')->with('errors', ["Course #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'description' => '',
			'notes'       => '',
		]);

		$this->merge(['course' => $course]);
	}

	public function authorize()
	{
		return true;
	}
}
