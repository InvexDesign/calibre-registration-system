<?php namespace App\Http\Requests\Backend\Courses;

use App\Http\Requests\BaseRequest;
use App\Models\State;

class PostCreateCourseRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'         => '',
			'name'        => 'required',
			'description' => '',
			'notes'       => '',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
