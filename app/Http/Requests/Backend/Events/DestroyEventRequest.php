<?php namespace App\Http\Requests\Backend\Events;

use App\Http\Requests\BaseRequest;
use App\Models\Event;
use HttpResponseException;
use Redirect;

class DestroyEventRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$event = Event::find($id);
		if(!$event)
		{
			$redirect = Redirect::route('backend.events.index')->with('errors', ["Event #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['event' => $event]);
	}

	public function authorize()
	{
		return true;
	}
}
