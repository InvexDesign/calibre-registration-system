<?php namespace App\Http\Requests\Backend\Events;

use App\Http\Requests\BaseRequest;

class PostCreateEventRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'uid'            => '',
			'venue_id'       => 'required|exists:venues,id',
			'course_id'      => 'required|exists:courses,id',
			'lodging_ids.*'  => 'required|exists:lodgings,id',
//			'timezone'       => 'required',
			'start_at'       => 'required|date',
			'end_at'         => 'nullable|date',
			'deadline_at'    => 'nullable|date',
			'times'          => 'required',
			'ticket_limit'   => '',
			'price'          => 'required|regex:/^[\$]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{1,2})?$/',
			'submit_draft'   => 'required_without:submit_publish',
			'submit_publish' => 'required_without:submit_draft',
			'notes'          => '',
		]);

		$revenue_goal = $this->get('revenue_goal', '');
		$stripped = preg_replace('/[^\d\.]/', '', $revenue_goal);
		$this->merge(['revenue_goal' => $stripped]);
		$this->_validate($this->all(), [
			'revenue_goal'   => 'regex:/^[\$]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{1,2})?$/',
		]);

		$is_draft = $this->has('submit_draft');
		$is_published = $this->has('submit_published');
		if(!$is_draft && $is_published)
		{
			$this->merge(['published_at' => \Carbon::now()]);
		}

		$this->merge([
			'price_in_cents' => floatval(preg_replace('/[^\d]/', '', $this->price)) * 100,
		]);
	}

	public function authorize()
	{
		return true;
	}
}
