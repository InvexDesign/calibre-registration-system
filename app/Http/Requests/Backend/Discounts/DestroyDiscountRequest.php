<?php namespace App\Http\Requests\Backend\Discounts;

use App\Http\Requests\BaseRequest;
use App\Models\Payments\Discounts\Discount;
use HttpResponseException;
use Redirect;

class DestroyDiscountRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$discount = Discount::find($id);
		if(!$discount)
		{
			$redirect = Redirect::route('backend.discounts.index')->with('errors', ["Discount #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['discount' => $discount]);
	}

	public function authorize()
	{
		return true;
	}
}
