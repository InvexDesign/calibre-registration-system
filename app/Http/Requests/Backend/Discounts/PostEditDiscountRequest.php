<?php namespace App\Http\Requests\Backend\Discounts;

use App\Http\Requests\BaseRequest;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\Discounts\DollarDiscount;
use App\Models\Payments\Discounts\PercentDiscount;
use HttpResponseException;
use Redirect;

class PostEditDiscountRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$discount = Discount::find($id);
		if(!$discount)
		{
			$redirect = Redirect::route('backend.discounts.index')->with('errors', ["Discount #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'name'              => 'required',
			'code'              => 'required',
			'value'             => 'required',
			'start_at'          => 'nullable|date',
			'end_at'            => 'nullable|date',
			'is_active'         => 'required',
			'max_uses'          => 'nullable|regex:/^[0-9]{1,3}(?:,?[0-9]{3})*$/',
			'auto_attendee_min' => 'nullable|regex:/^[0-9]{1,3}(?:,?[0-9]{3})*$/',
			'notes'             => '',
		]);

		switch($discount->class)
		{
			case 'PercentDiscount':
				$class = PercentDiscount::class;
				$float_value = floatval(preg_replace('/[^\d\.]/', '', $this->value));
				$value = intval($float_value * 100);
				break;
			case 'DollarDiscount':
				$class = DollarDiscount::class;
				$float_value = floatval(preg_replace('/[^\d\.]/', '', $this->value));
				$value = intval($float_value * 100);
				break;
			default:
				//TODO: fail
		}

		$is_active = boolval($this->is_active);

		//TODO: handle max_uses
		//TODO: handle auto_attendee_min

		$this->merge(compact(
			'class',
			'discount',
			'value',
			'is_active'
		));
	}

	public function authorize()
	{
		return true;
	}
}
