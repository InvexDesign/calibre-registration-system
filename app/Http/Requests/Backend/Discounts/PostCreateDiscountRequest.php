<?php namespace App\Http\Requests\Backend\Discounts;

use App\Http\Requests\BaseRequest;
use App\Models\Payments\Discounts\DollarDiscount;
use App\Models\Payments\Discounts\PercentDiscount;
use App\Models\State;

class PostCreateDiscountRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'type'              => 'required|in:percent,dollar',
			'name'              => 'required',
			'code'              => 'required',
			'value'             => 'required',
			'start_at'          => 'nullable|date',
			'end_at'            => 'nullable|date',
			'is_active'         => 'required',
			'max_uses'          => 'nullable|regex:/^[0-9]{1,3}(?:,?[0-9]{3})*$/',
			'auto_attendee_min' => 'nullable|regex:/^[0-9]{1,3}(?:,?[0-9]{3})*$/',
			'notes'             => '',
		]);

		switch($this->type)
		{
			case 'percent':
				$class = PercentDiscount::class;
				$float_value = floatval(preg_replace('/[^\d\.]/', '', $this->value));
				$value = intval($float_value * 100);
				break;
			case 'dollar':
				$class = DollarDiscount::class;
				$float_value = floatval(preg_replace('/[^\d\.]/', '', $this->value));
				$value = intval($float_value * 100);
				break;
			default:
				//TODO: fail
		}

		$is_active = boolval($this->is_active);

		//TODO: handle max_uses
		//TODO: handle auto_attendee_min

		$this->merge(compact(
			'class',
			'value',
			'is_active'
		));
	}

	public function authorize()
	{
		return true;
	}
}
