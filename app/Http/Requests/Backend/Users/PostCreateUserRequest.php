<?php namespace App\Http\Requests\Backend\Users;

use App\Http\Requests\BaseRequest;
use App\Role;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class PostCreateUserRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$this->_validate($this->all(), [
			'username'   => 'required|unique:users,username',
			'email'      => 'required|email|unique:users,email',
			'password'   => 'required|min:5|confirmed',
			'role_id'    => 'required|exists:roles,id',
			'first_name' => '',
			'last_name'  => '',
		]);

		$current_user_role = \Auth::user()->getRole();
		if(!$current_user_role)
		{
			$redirect = Redirect::back()->with('errors', ["Your User is not assigned a Role!"]);
			throw new HttpResponseException($redirect);
		}

		$role = Role::find($this->role_id);
		$backend_role_hierarchy = Role::getBackendHierarchy('name');
		$index = array_search($current_user_role->name, $backend_role_hierarchy);
		$available_roles = array_slice($backend_role_hierarchy, $index);
		if(count($available_roles) < 1)
		{
			$redirect = Redirect::back()->with('errors', ["There are no roles that you are allowed to create!"]);
			throw new HttpResponseException($redirect);
		}
		if(!in_array($role->name, $available_roles))
		{
			$role_display = $role->display();
			$redirect = Redirect::back()->withInput()->with('errors', ["You do not have permission to create users with role '$role_display'!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge([
			'username' => $this->email,
			'role'     => $role
		]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
