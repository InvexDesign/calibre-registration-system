<?php namespace App\Http\Requests\Backend\Users;

use App\Http\Requests\BaseRequest;
use App\Models\User;
use App\Role;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class PostChangePasswordRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$user = User::find($id);
		if(!$user)
		{
			$redirect = Redirect::route('backend.users.index')->with('errors', ["User #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}
		$role = $user->getRole();

		$current_user = \Auth::user();
		if(!$current_user->isExecutive())
		{
			$backend_role_hierarchy = Role::getBackendHierarchy('name');

			$current_user_role = $current_user->getRole();
			$current_user_role_index = array_search($current_user_role->name, $backend_role_hierarchy);

			$user_role_index = array_search($role->name, $backend_role_hierarchy);
			if($current_user_role_index >= $user_role_index)
			{
				$redirect = Redirect::route('backend.dashboard.index')->with('errors', ["You do not have sufficient privileges to edit users of this type!"]);
				throw new HttpResponseException($redirect);
			}
		}

		$this->_validate($this->all(), [
			'password' => 'required|min:5|confirmed',
		]);

		$this->merge([
			'user' => $user,
			'role' => $role
		]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
