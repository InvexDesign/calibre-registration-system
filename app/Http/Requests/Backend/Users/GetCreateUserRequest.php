<?php namespace App\Http\Requests\Backend\Users;

use App\Http\Requests\BaseRequest;
use App\Role;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Validator;

class GetCreateUserRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$current_user_role = \Auth::user()->getRole();
		if(!$current_user_role)
		{
			$redirect = Redirect::back()->with('errors', ["Your User is not assigned a Role!"]);
			throw new HttpResponseException($redirect);
		}

		$backend_role_hierarchy = Role::getBackendHierarchy('name');
		$index = array_search($current_user_role->name, $backend_role_hierarchy);
		$available_roles = array_slice($backend_role_hierarchy, $index);
		if(count($available_roles) < 1)
		{
			$redirect = Redirect::back()->with('errors', ["There are no roles that you are allowed to create!"]);
			throw new HttpResponseException($redirect);
		}

		//Prepare role_id => display for select options
		$role_options = [];
		foreach($available_roles as $available_role)
		{
			$role = Role::get($available_role, true);
			$role_options[$role->id] = $role->display();
		}
		$this->merge(['role_options' => $role_options]);
	}

	public function __validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}
	}

	public function _validate(array $data, array $rules, array $messages = [], array $custom_attributes = [])
	{
		$validator = Validator::make($data, $rules, $messages, $custom_attributes);
		if($validator->fails())
		{
			$redirect = Redirect::back()
								->withInput($this->input())
								->withErrors($validator->errors()->getMessages(), 'default');

			throw new HttpResponseException($redirect);
		}
	}

	public function authorize()
	{
		return true;
	}
}
