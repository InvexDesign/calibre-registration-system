<?php namespace App\Http\Requests\Backend\Tickets;

use App\Http\Requests\BaseRequest;
use App\Models\Registration;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;
use Setting;

class PostAddTicketsRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$registration_id = $this->route()->parameters()['id'];
		$registration = Registration::find($registration_id);
		if(!$registration)
		{
			$redirect = Redirect::route('backend.registrations.index')->with('errors', ["Registration #$registration_id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->all(), [
			'attendee_count' => 'required',
		]);
		$attendees = $this->attendees;
		$attendee_count = intval($this->attendee_count);
		for($i = $attendee_count; $i < Setting::get('max-attendee-count', 30); $i++)
		{
			unset($attendees[$i]);
		}
		$this->merge(['attendees' => $attendees]);
		$this->_validate($this->all(), [
			'attendees.*.first_name' => 'required',
			'attendees.*.last_name'  => 'required',
			'attendees.*.email'      => '',
			'attendees.*.rank'       => 'required',
			'attendees.*.pid'        => 'required',
		]);

		$this->merge(['registration' => $registration]);
	}

	public function authorize()
	{
		return true;
	}
}
