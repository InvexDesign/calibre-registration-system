<?php namespace App\Http\Requests\Backend\Tickets;

use App\Http\Requests\BaseRequest;
use App\Models\Ticket;
use HttpResponseException;
use Redirect;

class DestroyTicketRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$id = $this->route()->parameters()['id'];
		$ticket = Ticket::find($id);
		if(!$ticket)
		{
			$redirect = Redirect::route('backend.tickets.index')->with('errors', ["Ticket #$id does not exist!"]);
			throw new HttpResponseException($redirect);
		}

		$this->merge(['ticket' => $ticket]);
	}

	public function authorize()
	{
		return true;
	}
}
