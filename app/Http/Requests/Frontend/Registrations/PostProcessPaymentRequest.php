<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Registration;
use App\Models\Setting;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostProcessPaymentRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		$uid = $this->route()->parameters()['uid'];
		$registration = Registration::where('uid', $uid)->get()->first();
		if(!$registration)
		{
			$redirect = Redirect::route('frontend.index')->with('errors', ["Registration #$uid does not exist!"]);
			throw new HttpResponseException($redirect);
		}
		$passcode = $this->route()->parameters()['passcode'];
		if($registration->passcode != $passcode)
		{
			return Redirect::route('frontend.index')->with('errors', ["Unauthorized!"]);
		}

		$this->_validate($this->all(), [
			'name'        => 'required',
			'card_number' => 'required',
			'expiration'  => 'required',
			'card_code'   => 'required',
			'address'     => 'required',
			'city'        => 'required',
			'state_id'    => 'required|exists:states,id',
			'zipcode'     => 'required',
		]);

		$card_number = preg_replace('/[\s]/', '', $this->card_number);
		$expiration = preg_replace('/[\s]/', '', $this->expiration);
		$expiration_month = substr($expiration, 0, 2);
		$expiration_year = substr($expiration, 3);

		$this->merge(compact('registration','card_number', 'expiration_month', 'expiration_year'));
	}

	public function authorize()
	{
		return true;
	}
}
