<?php namespace App\Http\Requests\Backend\Registrations;

use App\Http\Requests\BaseRequest;
use App\Models\Setting;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class PostRegisterRequest extends BaseRequest
{
	public function validate()
	{
		$this->__validate();

		if(!$this->has('recaptcha-success-token') || ($this->get('recaptcha-success-token') != Setting::get('recaptcha-success-token')))
		{
			$redirect = Redirect::route('frontend.recaptcha.fail');
			throw new HttpResponseException($redirect);
		}

		$this->_validate($this->address, [
			'line1'    => 'required',
			'line2'    => '',
			'city'     => 'required',
			'state_id' => 'required|exists:states,id',
			'zipcode'  => 'required',
			'country'  => 'required',
		]);

		$this->_validate($this->registration, [
			'event_id'           => 'required|exists:events,id',
			'agency'             => 'required',
			'contact'            => 'required',
			'email'              => 'required|email',
			'department_size_id' => 'required|exists:department_sizes,id',
			'phone'              => 'required',
			'fax'                => 'required',
		]);

		$this->_validate($this->all(), [
			'attendee_count' => 'required',
		]);
		$attendees = $this->attendees;
		$attendee_count = intval($this->attendee_count);
		for($i = $attendee_count; $i < Setting::get('max-attendee-count', 30); $i++)
		{
			unset($attendees[$i]);
		}
		$this->merge(['attendees' => $attendees]);
		$this->_validate($this->all(), [
			'attendees.*.first_name' => 'required',
			'attendees.*.last_name'  => 'required',
			'attendees.*.email'      => '',
			'attendees.*.rank'       => 'required',
			'attendees.*.pid'        => 'required',
		]);
	}

	public function authorize()
	{
		return true;
	}
}
