<?php namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Redirect;

class ChangePasswordRequest extends BaseRequest
{
	public function validate()
	{
		if(!$this->passesAuthorization())
		{
			$this->failedAuthorization();
		}

		if(\Auth::user()->requiresCurrentPasswordToUpdate())
		{
			$current_password_matches = \Auth::validate([
				'username' => \Auth::user()->username,
				'password' => $this->current_password
			]);
			if(!$current_password_matches)
			{
				$redirect = Redirect::back()->with('errors', ["Your current password is incorrect!"]);
				throw new HttpResponseException($redirect);
			}
		}

		$instance = $this->getValidatorInstance();
		if(!$instance->passes())
		{
			$this->failedValidation($instance);
		}
	}

	public function rules()
	{
		$rules = [
			'password' => 'required|' . User::getRules()['password'],
		];

		return $rules;
	}

	public function authorize()
	{
		return true;
	}
}
