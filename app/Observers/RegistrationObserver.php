<?php namespace App\Observers;


use App\Models\Registration;

class RegistrationObserver
{
	public function created(Registration $registration)
	{
		$registration->syncOriginal();
		$registration->uid = 'REGISTRATION-' . $registration->id;
		$registration->passcode = Registration::generatePasscode();
		$registration->save();
	}
}