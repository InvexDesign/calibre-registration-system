<?php namespace App\Observers;


use App\Models\Event;

class EventObserver
{
	public function created(Event $event)
	{
		$event->syncOriginal();
		$event->uid = 'EVENT-' . $event->id;
		$event->save();
	}
}