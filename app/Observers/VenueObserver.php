<?php namespace App\Observers;


use App\Models\Venue;

class VenueObserver
{
	public function created(Venue $venue)
	{
		$venue->syncOriginal();
		$venue->uid = 'VENUE-' . $venue->id;
		$venue->generateEmbeddedMapAttribute();
		$venue->save();
	}
}