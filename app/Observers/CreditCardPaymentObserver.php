<?php namespace App\Observers;


use App\Models\Payments\CreditCardPayment;

class CreditCardPaymentObserver
{
	public function created(CreditCardPayment $payment)
	{
		$payment->syncOriginal();
		$payment->uid = 'CC-PAYMENT-' . $payment->id;
		$payment->save();
	}
}