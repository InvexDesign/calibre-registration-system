<?php namespace App\Observers;


use App\Models\Lodging;

class LodgingObserver
{
	public function created(Lodging $lodging)
	{
		$lodging->syncOriginal();
		$lodging->generateEmbeddedMapAttribute();
		$lodging->save();
	}
}