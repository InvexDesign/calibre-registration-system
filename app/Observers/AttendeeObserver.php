<?php namespace App\Observers;


use App\Models\Attendee;

class AttendeeObserver
{
	public function created(Attendee $attendee)
	{
		$attendee->syncOriginal();
		$attendee->uid = 'ATTENDEE-' . $attendee->id;
		$attendee->save();
	}
}