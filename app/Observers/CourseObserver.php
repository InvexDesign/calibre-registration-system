<?php namespace App\Observers;


use App\Models\Course;

class CourseObserver
{
	public function created(Course $course)
	{
		$course->syncOriginal();
		$course->uid = 'COURSE-' . $course->id;
		$course->save();
	}
}