<?php namespace App\Observers;


use App\Models\Payments\CheckPayment;

class CheckPaymentObserver
{
	public function created(CheckPayment $payment)
	{
		$payment->syncOriginal();
		$payment->uid = 'CH-PAYMENT-' . $payment->id;
		$payment->save();
	}
}