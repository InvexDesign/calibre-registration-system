<?php namespace App\Observers;


use App\Models\Ticket;

class TicketObserver
{
	public function created(Ticket $ticket)
	{
		$ticket->syncOriginal();
		$ticket->uid = 'TICKET-' . $ticket->id;
		$ticket->save();
	}
}