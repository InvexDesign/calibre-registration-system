<?php namespace App\Observers;


use App\Models\Payments\Adjustment;

class AdjustmentObserver
{
	public function created(Adjustment $payment)
	{
		$payment->syncOriginal();
		$payment->uid = 'ADJUSTMENT-' . $payment->id;
		$payment->save();
	}
}