<?php namespace App\Observers;


use App\Models\Payments\DiscountPayment;

class DiscountPaymentObserver
{
	public function created(DiscountPayment $payment)
	{
		$payment->syncOriginal();
		$payment->uid = 'CP-PAYMENT-' . $payment->id;
		$payment->save();
	}
}