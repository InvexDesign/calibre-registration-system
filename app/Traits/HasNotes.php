<?php namespace App\Traits;

use App\Models\Notes\Note;
use ReflectionClass;

trait HasNotes
{
	public function getClassName()
	{
		$reflect = new ReflectionClass($this);

		return $reflect->getShortName();
	}

	public function getBackendRoutes()
	{
		return [
			'show' => 'backend.' . $this->table . '.show'
		];
	}

	public function notes()
	{
		return $this->morphMany(Note::class, 'noteable');
	}

	public function getLatestNote()
	{
		return $this->notes()->orderBy('id', 'desc')->take(1)->get()->first();
	}

	public function createNote($content, $author = null, $reload = true)
	{
		$note = $this->notes()->create([
			'content'   => $content,
			'author_id' => !is_null($author) ? $author->id : null
		]);

		if($reload)
		{
			$this->load(['notes']);
		}

		return $note;
	}
}