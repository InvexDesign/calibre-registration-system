<?php namespace App\Traits;

use Setting;

trait HasEmbeddedMap
{
	public function generateEmbeddedMapAttribute($force = false)
	{
		if(!$force && $this->embedded_map)
		{
			return;
		}

		$address = $this->address;
		if(!$address)
		{
			return;
		}

		$formatted_address = str_replace([".", ","], ["", ""], $address->line1);
		$formatted_address = str_replace(" ", "+", $formatted_address);
		$formatted_city = str_replace([".", ","], ["", ""], $address->city);
		$formatted_city = str_replace(" ", "+", $formatted_city);
		$state = $address->state->short_name;
		$zipcode = $address->zipcode;
		$q = "$formatted_address+$formatted_city+$state+$zipcode";

		$width = Setting::get('embedded-map-width', 400);
		$height = Setting::get('embedded-map-height', 250);
		$this->embedded_map = '<iframe src="//www.google.com/maps/embed/v1/place?q=' . $q . '&zoom=17&key=AIzaSyA9MgJaNV1hN4_PaF00LoRioaRO5dsaBVc" width="' . $width . '" height="' . $height . '" frameborder="0" style="border:0" allowfullscreen></iframe>';
	}
}