<?php namespace App\Traits;

trait DisplaysPhoneNumber
{
	public function displayPhone($attribute, $default = '')
	{
		if($phone = $this->$attribute)
		{
			$string = '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6, 4);

			if(strlen('' . $phone) > 10)
			{
				$string .= ' x' . substr($phone, 10);
			}

			return $string;
		}

		return $default;
	}


	public function displayTel($attribute, $default = '')
	{
		if($phone = $this->$attribute)
		{
			return substr($phone, 0, 10);
		}

		return $default;
	}
}