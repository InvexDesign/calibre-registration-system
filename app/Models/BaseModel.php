<?php namespace App\Models;

use App\Helpers\TimezoneHelper;
use App\Models\Notifications\GeneralNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

//TODO: Extract this to Invex Package
class BaseModel extends Model
{
	public static function getClassBaseName()
	{
		return substr(static::class, strrpos(static::class, '\\') + 1);
	}

	protected static function getAttributeMap()
	{
		return [];
	}

	protected static function getAttributeMapDefaults()
	{
		return [
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		];
	}

	public function setAttribute($key, $value)
	{
		$attribute_map = static::getAttributeMap() + self::getAttributeMapDefaults();
		if(isset($attribute_map[$key]) && $attribute_map[$key] != '')
		{
			switch($attribute_map[$key])
			{
				case 'boolean':
					$value = $this->parseBoolean($value);
					break;
				case 'boolean-nullable':
					$value = $this->parseBoolean($value, true);
					break;
				case 'datetime':
					$value = $this->parseDateTime($value, Setting::get('timezone', 'America/Chicago'));
					break;
				case 'foreign-nullable':
					$value = $this->parseForeignNullable($value);
					break;
				case 'html':
					$value = $this->escapeNonAlphaNum($value);
					break;
				case 'html-nullable':
					$value = $this->escapeNonAlphaNum($value, true);
					break;
				case 'dollar':
					$value = $this->parseDollar($value);
					break;
				case 'integer':
					$value = $this->parseInteger($value);
					break;
				case 'phone':
					$value = $this->parsePhone($value);
					break;
				case 'float':
					$value = $this->parseFloat($value);
					break;
				case 'string-escaped':
					$value = $this->escapeNonAlphaNum($value);
					break;
				case 'json':
					$value = json_encode($this->escapeNonAlphaNum($value));
					break;
				case 'string-escaped-nullable':
					$value = $this->escapeNonAlphaNum($value, true);
					break;
				case 'text-escaped':
					$value = $this->escapeNonAlphaNum($value);
					break;
				case 'text-escaped-nullable':
					$value = $this->escapeNonAlphaNum($value, true);
					break;
				default:
					$message = static::class . ": '$key' key has invalid attribute mapping!";
					if(GeneralNotification::active()->description($message)->count() == 0)
					{
						GeneralNotification::create([
							'description' => $message,
							'is_severe'   => true
						]);
					}
			}
		}

		parent::setAttribute($key, $value);
	}

	public function getAttributeValue($key)
	{
		$value = parent::getAttributeValue($key);

		$attribute_map = static::getAttributeMap() + self::getAttributeMapDefaults();
		if(isset($attribute_map[$key]))
		{
			switch($attribute_map[$key])
			{
				case 'html':
				case 'html-nullable':
				case 'string-escaped':
				case 'string-escaped-nullable':
				case 'text-escaped':
				case 'text-escaped-nullable':
					$value = $this->unescapeNonAlphaNum($value);
					break;
				case 'json':
					$value = $this->unescapeNonAlphaNum(json_decode($value));
					break;
				case 'datetime':
					$value = $this->unparseDateTime($value, Setting::get('timezone', 'America/Chicago'));
				case 'boolean':
				case 'dollar':
				case 'phone':
			}
		}

		return $value;
	}

	public static function escapeNonAlphaNum($value, $nullable = false)
	{
		if($nullable && ($value == null || $value == ''))
		{
			return null;
		}

		return preg_replace("/([^a-zA-Z0-9 ])/", "\\\\$1", $value);
	}

	public static function unescapeNonAlphaNum($value)
	{
		return preg_replace("/\\\\([^a-zA-Z0-9 ])/", "$1", $value);
	}

	//TODO: Allow 'false' => false?
	//TODO: Allow more null cases?
	public function parseBoolean($value, $nullable = false)
	{
		if($value === 'false')
		{
			return false;
		}

		if($nullable && ($value === null || $value === ''))
		{
			return null;
		}

		return boolval($value);
	}

	public function parseDateTime($value, $timezone = null)
	{
		if(is_object($value) && get_class($value) == 'Carbon\Carbon')
		{
			return $value->timezone(config('app.timezone', 'UTC'));
		}
		elseif(is_object($value) && get_class($value) == 'DateTime')
		{
			return Carbon::parse($value->format('c'), $timezone)->timezone(config('app.timezone', 'UTC'));
		}
		elseif(is_string($value) && $value != '')
		{
			return Carbon::parse($value, $timezone)->timezone(config('app.timezone', 'UTC'));
		}
		else
		{
			return null;
		}
	}

	public function parseDollar($value)
	{
		if(is_string($value) && $value != '')
		{
			$stripped = preg_replace('/[^\d\.]/', '', $value);

			return floatval($stripped);
		}
		elseif(is_float($value) || is_int($value))
		{
			return $value;
		}
		else
		{
			return null;
		}
	}

	public function parseInteger($value)
	{
		if(is_string($value) && $value != '')
		{
			$stripped = preg_replace('/[^\d\.]/', '', $value);

			return intval(floatval($stripped));
		}
		elseif(is_float($value) || is_int($value))
		{
			return intval($value);
		}
		else
		{
			return null;
		}
	}

	public function parseFloat($value)
	{
		if(is_string($value) && $value != '')
		{
			$stripped = preg_replace('/[^\d\.]/', '', $value);

			return floatval($stripped);
		}
		elseif(is_float($value) || is_int($value))
		{
			return $value;
		}
		else
		{
			return null;
		}
	}

	public function parseForeignNullable($value)
	{
		if($value === null)
		{
			return null;
		}

		$int = $this->parseInteger($value);
		if($int == 0 && $int == null)
		{
			return null;
		}

		return $int;
	}

	public function parseHtml($value, $nullable = false)
	{
		if($nullable && ($value == null || $value == ''))
		{
			return null;
		}
		else
		{
			return htmlspecialchars($value);
		}
	}

	public function unparseHtml($attribute)
	{
		return htmlspecialchars_decode($this->attributes[$attribute]);
	}

	public function unparseDateTime($value, $timezone = null)
	{
		if($value === null)
		{
			return null;
		}

		$carbon_datetime = parent::asDateTime($value);//TODO: fromDateTime($value)?

		return $carbon_datetime->timezone($timezone);
	}

	public function parsePhone($value)
	{
		if('' . $value != '')
		{
			$stripped = preg_replace('/[^\d]/', '', $value);

			return $stripped;
		}
		else
		{
			return null;
		}
	}

	//TODO: Remove everything below this line
//
//	public function setBooleanAttribute($attribute, $value)
//	{
//		$this->attributes[$attribute] = $this->parseBoolean($value);
//	}
//
//
//	public function setCarbonDateAttribute($attribute, $value)
//	{
//		$this->attributes[$attribute] = $this->parseDateTime($value);
//	}
//
//
//	public function setDollarAttribute($attribute, $value)
//	{
//		$this->attributes[$attribute] = $this->parseDollar($value);
//	}
//
//	public function setHtmlAttribute($attribute, $value, $is_nullable = false)
//	{
//		$this->attributes[$attribute] = $this->parseHtml($value, $is_nullable);
//	}
//
//	public function getHtmlAttribute($attribute)
//	{
//		return $this->unparseHtml($attribute);
//	}
}