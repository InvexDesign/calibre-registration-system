<?php namespace App\Models;

use App\Models\Payments\Payment;

class Audit extends \OwenIt\Auditing\Models\Audit
{
	public function scopeAttendees($query, $ids)
	{
		return $query->where('auditable_type', Attendee::class)->whereIn('auditable_id', $ids);
	}

	public function scopePayments($query, $ids)
	{
		return $query->where('auditable_type', 'like', '%Payment%')->whereIn('auditable_id', $ids);
	}

	public function scopeRegistrations($query, $ids)
	{
		return $query->where('auditable_type', Registration::class)->whereIn('auditable_id', $ids);
	}

	public function scopeTickets($query, $ids)
	{
		return $query->where('auditable_type', Ticket::class)->whereIn('auditable_id', $ids);
	}
}