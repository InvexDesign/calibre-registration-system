<?php namespace App\Models;

use App\Traits\DisplaysPhoneNumber;
use App\Traits\HasEmbeddedMap;
use App\Traits\HasNotes;

class Lodging extends BaseModel
{
	use DisplaysPhoneNumber, HasEmbeddedMap, HasNotes;

	protected $table    = 'lodgings';
	protected $fillable = [
		'name',
		'phone',
		'email',
		'contact',
		'embedded_map',
		'address_id',
		'description',
//		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'name'              => 'string-escaped',
			'phone'             => 'phone',
			'contact'           => 'string-escaped',
			'embedded_map' => 'html-nullable',
			'description'       => 'text-escaped',
//			'notes'             => 'text-escaped',
		];
	}


	/* ELOQUENT */
	public function address()
	{
		return $this->belongsTo(Address::class, 'address_id');
	}

	public function events()
	{
		return $this->belongsToMany(Event::class);
	}


	/* QUERIES */


	/* GETTERS / SETTERS */


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}
}
