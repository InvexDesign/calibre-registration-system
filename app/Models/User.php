<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Role;
use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract, Displayable
{
	use Authenticatable, Authorizable, CanResetPassword;

	protected $table    = 'users';
	protected $fillable = [
		'role_id',
		'username',
		'email',
		'password',
		'first_name',
		'last_name',
		'password_updated_at',
	];
	protected $hidden   = [
		'password',
		'remember_token'
	];

	public static function getAttributeMap()
	{
		return [
			'first_name' => 'string-escaped',
			'last_name'  => 'string-escaped',
		];
	}


	/* GETTERS / SETTERS */
	public function getRole()
	{
		return $this->role;
	}

	public function hasRole($accepted_roles, $require_all = false)
	{
		if(is_array($accepted_roles))
		{
			foreach($accepted_roles as $role_name)
			{
				$has_role = $this->hasRole($role_name);
				if($has_role && !$require_all)
				{
					return true;
				}
				elseif(!$has_role && $require_all)
				{
					return false;
				}
			}

			// If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
			// If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
			// Return the value of $requireAll;
			return $require_all;
		}
		else
		{
			$role = $this->getRole();

			return $role && ($role->name == $accepted_roles);
		}
	}

	public function isOverlord()
	{
		return $this->hasRole('overlord');
	}

	public function isExecutive()
	{
		return $this->isOverlord() || $this->hasRole('master');
	}

	public function isAdministrator()
	{
		return $this->isExecutive() || $this->hasRole('administrator');
	}

	public function isSalesRepresentative()
	{
		return $this->hasRole('sales-representative');
	}

	public function getTemplate()
	{
		if($this->isExecutive())
		{
			return 'executives';
		}
		elseif($this->isAdministrator())
		{
			return 'administrators';
		}
		elseif($this->isSalesRepresentative())
		{
			return 'sales_representatives';
		}

		return 'none';
	}

	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

	public function getLastCommaFirstAttribute()
	{
		return $this->display_helper(true);
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}

	public function getFirstLastAttribute()
	{
		return $this->display_helper(false);
	}

	public function getDisplayEmailAttribute()
	{
		return $this->display_helper(false) . ($this->email && $this->email != '' ? ' <' . $this->email . '>' : '');
	}

	public function getUsernameEmailAttribute()
	{
		return $this->username . ' <' . $this->email . '>';
	}

	public function requiresCurrentPasswordToUpdate()
	{
		return $this->password_updated_at != null;
	}


	/* ELOQUENT */
	public function role()
	{
		return $this->belongsTo(Role::class, 'role_id');
	}

	public function events()
	{
		return $this->hasMany(Event::class, 'sales_representative_id');
	}


	/* PRESENTATION */
	public function display_helper($is_last_comma_first = false)
	{
		$display = '';

		if($this->last_name . $this->first_name == '')
		{
			if($this->username)
			{
				$display .= $this->username;
			}
		}
		else
		{
			if($is_last_comma_first)
			{
				$display .= $this->last_name . ', ' . $this->first_name;
			}
			else
			{
				$display .= $this->first_name . ' ' . $this->last_name;
			}
		}

		return $display;
	}

	public function display()
	{
		return $this->display_helper(false);
	}

	public function getGravatarImageUrl()
	{
		return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->email))) . "?s=40&d=mm";
	}
}
