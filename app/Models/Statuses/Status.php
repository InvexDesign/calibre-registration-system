<?php namespace App\Models\Statuses;


use ReflectionClass;

abstract class Status
{
	public static function getDefault()
	{
		return null;
	}

	public static function getOptions()
	{
		$oClass = new ReflectionClass(static::class);

		return $oClass->getConstants();
	}

	public static function getSelectOptions()
	{
		$oClass = new ReflectionClass(static::class);

		$options = [];
		foreach($oClass->getConstants() as $key => $value)
		{
			$options[$value] = $value;
		}

		return $options;
	}
}