<?php namespace App\Models\Statuses;

class EventPublishedStatus extends Status
{
	const DRAFTED = "Drafted";
	const PUBLISHED = "Published";

	public static function getDefault()
	{
		return self::DRAFTED;
	}
}