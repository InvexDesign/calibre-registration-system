<?php namespace App\Models\Statuses;

class RegistrationPaymentStatus extends Status
{
	const UNPAID = "Unpaid";
	const PARTIALLY_PAID = "Partially-paid";
	const PAID = "Paid";
	const FORGIVEN = "Forgiven";

	public static function getDefault()
	{
		return self::UNPAID;
	}
}