<?php namespace App\Models;

use App\Interfaces\Displayable;
use App\Models\Notifications\GeneralNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Log;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Setting extends Model implements Displayable
{
	use HasMediaTrait;

	protected $table    = 'settings';
	protected $fillable = [
		'key',
		'value',
		'type',
		'description',
		'user_id'
	];

	public static function getScopeOptions($user)
	{
		return [
			0         => 'Global',
			$user->id => 'Personal'
		];
	}

	public static function getTypeOptions()
	{
		return [
			'string'          => 'String',
			'integer'         => 'Integer',
			'float'           => 'Float',
			'boolean'         => 'Boolean',
			'array-comma'     => 'Array (Comma Separated)',
			'array-semicolon' => 'Array (Semicolon Separated)',
			'html'            => 'HTML',
		];
	}

	public static function get($key, $default = null)
	{
		$setting = null;

		$user = \Auth::user();
		if($user)
		{
			$setting = Setting::where('key', $key)
							  ->where('user_id', $user->id)
							  ->first();
		}

		if($setting == null)
		{
			$setting = Setting::where('key', $key)
							  ->whereNull('user_id')
							  ->first();
		}

		if($setting == null)
		{
			if(config('settings.allow_defaults') == 'TRUE')
			{
				return $default;
			}
			else
			{
				$description = "Setting@get: Defaults Disabled: Please set the \"$key\" System Setting";

				Log::error($description);

				if(count(GeneralNotification::active()->where('description', $description)->get()) == 0)
				{
					GeneralNotification::create([
						'description' => $description,
						'remind_at'   => Carbon::tomorrow(),
						'is_severe'   => true,
					]);
				}

				//TODO: We appear to need a default here lol, how do we handle this?
				return false;
			}
		}

		switch($setting->type)
		{
			case 'string':
				return $setting->value;
			case 'integer':
				return intval($setting->value);
			case 'float':
				return floatval($setting->value);
			case 'boolean':
				switch($setting->value)
				{
					case '1':
					case 'true':
					case 'True':
					case 'TRUE':
						return true;
					case '0':
					case 'false':
					case 'False':
					case 'FALSE':
					default:
						return false;
				}
			case 'array-comma':
				return explode(',', $setting->value);
			case 'array-semicolon':
				return explode(';', $setting->value);
			default:
				return $setting->value;
		}
	}

	public function setValueAttribute($value)
	{
		$this->attributes['value'] = htmlspecialchars($value);
	}

	public function getValueAttribute()
	{
		return htmlspecialchars_decode($this->attributes['value']);
	}


	/* ELOQUENT */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->key . ': ' . $this->value . ' (' . $this->type . ')';
	}

	public static function getRules()
	{
		return [
			'key'         => 'required|regex:/^[a-zA-Z0-9\-_]*$/',
			'value'       => 'required_without:html_value',
			'html_value'  => 'required_without:value',
			'type'        => 'regex:/^[a-zA-Z0-9\-_ ]*$/',
			'description' => '',
			'user_id'     => 'exists:users,id',
		];
	}
}