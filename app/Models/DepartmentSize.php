<?php namespace App\Models;

use App\Models\Payments\Payment;
use App\Traits\HasNotes;
use Helper;

class DepartmentSize extends BaseModel
{
	protected $table    = 'department_sizes';
	protected $fillable = [
		'name',
	];

	public static function getAttributeMap()
	{
		return [
			'name' => 'string-escaped',
		];
	}


	/* ELOQUENT */
	public function registrations()
	{
		return $this->hasMany(Registration::class, 'department_size_id');
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}


	/* ANALYTICS*/
}