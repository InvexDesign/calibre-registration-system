<?php namespace App\Models;

use App\Interfaces\Displayable;

class EventRecord extends BaseModel implements Displayable
{
	protected $table    = 'analytics__events';
	protected $fillable = [
		'event_id',
		'venue_id',
		'state_id',
		'start_at',
		'end_at',
		'tickets',
		'revenue',
	];

	public static function getAttributeMap()
	{
		return [
			'start_at' => 'datetime',
			'end_at'   => 'datetime',
			'tickets'  => 'integer',
			'revenue'  => 'dollar',
		];
	}

	//TODO: Implement
	public static function incrementCurrentMonthsRecord($facility_id)
	{
		return;
		$month = (new \Carbon('first day of this month'))->startOfDay();
		$record = self::query()
					  ->where('facility_id', $facility_id)
					  ->where('month', $month)
					  ->get()
					  ->first();

		if(!$record)
		{
			self::create([
				'facility_id' => $facility_id,
				'month'       => $month,
				'hits'        => 1,
			]);
		}
		else
		{
			$record->hits = $record->hits + 1;
			$record->save();
		}
	}


	/* ELOQUENT */
	public function event()
	{
		return $this->belongsTo(Event::class, 'event_id');
	}

	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}

	public function state()
	{
		return $this->belongsTo(State::class, 'state_id');
	}


	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		//TODO: Implement
		return 'TODO';
	}
}