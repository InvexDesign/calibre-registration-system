<?php namespace App\Models\Notifications;

use App\Interfaces\Displayable;
use App\Models\BaseModel;
use Carbon\Carbon;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class Notification extends BaseModel implements Displayable
{
	use SingleTableInheritanceTrait;
	protected $table    = 'notifications';
	protected $fillable = [
		'type',
		'class',
		'model_id',
		'description',
		'remind_at',
		'is_severe',
		'is_email',
		'is_active',
	];

	protected static $singleTableTypeField  = 'class';
	protected static $singleTableSubclasses = [
		GeneralNotification::class,
		UserNotification::class,
	];

	public static function getRules()
	{
		return [
			'description' => 'required',
			'model_id'    => 'numeric',
			'remind_at'   => 'datetime',
			'created_at'  => 'datetime',
			'updated_at'  => 'datetime',
		];
	}

	public static function getAttributeMap()
	{
		return [
			'remind_at'   => 'datetime',
			'is_severe'   => 'boolean',
			'is_email'    => 'boolean',
			'is_active'   => 'boolean',
			'description' => 'text-escaped',
		];
	}

	public static function translateFullClassName($short)
	{
		if($short == 'notification')
		{
			return 'Notification';
		}

		return str_replace('-', '', implode('-', array_map('ucfirst', explode('-', $short)))) . 'Notification';
	}

	public function getIconClass()
	{
		return 'bell-o';
	}

	public function getIconColor()
	{
		return 'red';
	}

	public function getTimeElapsed()
	{
		return $this->created_at->timezone('UTC')->diffForHumans();
	}

	/* GETTERS / SETTERS */
	public function getModel()
	{
		return null;
	}

	public function getColor()
	{
		return 'grey';
	}
//
//	public function setRemindAtAttribute($value)
//	{
//		$this->setCarbonDateAttribute('remind_at', $value);
//	}
//
//	public function setIsSevereAttribute($value)
//	{
//		$this->setBooleanAttribute('is_severe', $value);
//	}
//
//	public function setIsEmailAttribute($value)
//	{
//		$this->setBooleanAttribute('is_email', $value);
//	}
//
//	public function setIsActiveAttribute($value)
//	{
//		$this->setBooleanAttribute('is_active', $value);
//	}


	/* ELOQUENT */
	public function scopeDescription($query, $message)
	{
		$translation = $this->escapeNonAlphaNum($message);
		
		return $query->where('description', $translation);
	}

	public function scopeActive($query)
	{
		return $query->where('is_active', true);
	}

	public function scopeInactive($query)
	{
		return $query->where('is_active', false);
	}

	public function scopeSevere($query)
	{
		return $query->where('is_severe', true);
	}

	public function scopeNotSevere($query)
	{
		return $query->where('is_severe', false);
	}

	public function scopeRemindable($query)
	{
		return $query->where('remind_at', '<', Carbon::now());
	}

	public function scopeEmail($query)
	{
		return $query->where('is_email', '=', 1);
	}


	public function dismiss()
	{
		$this->is_active = false;
		$this->save();
	}

	/* PRESENTATION */
	public function display()
	{
		return $this->type . ': ' . link_to_route($this->getModelShowRoute(), $this->displayModel(), ['id' => $this->getModel()->id]) . ' - ' . $this->description;
	}
}