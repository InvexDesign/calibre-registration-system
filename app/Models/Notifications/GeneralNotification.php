<?php namespace App\Models\Notifications;

use App\Interfaces\Displayable;

class GeneralNotification extends Notification implements Displayable
{
	protected static $singleTableType = 'GeneralNotification';

	public static function getRules()
	{
		return [
			'description' => 'required',
			'remind_at'   => 'date',
		];
	}


	/* GETTERS / SETTERS */
	public function getModelShowRoute()
	{
		dd('GeneralNotification@getModelShowRoute: TODO');
		return 'projects.show';
	}

	public function getColor()
	{
		return 'yellow';
	}


	/* PRESENTATION */
	public function displayModel()
	{
		dd('GeneralNotification@displayModel: TODO');
		return 'GENERAL';
	}

	public function display()
	{
		return $this->type . ': ' . $this->description;
	}


	/* ELOQUENT */
	public function getModel()
	{
		return null;
	}
}