<?php namespace App\Models;

use App\Interfaces\Displayable;

class Address extends BaseModel implements Displayable
{
	protected $table    = 'addresses';
	protected $fillable = [
		'line1',
		'line2',
		'city',
		'state_id',
		'zipcode',
		'country',
	];

	public static function getAttributeMap()
	{
		return [
			'line1'   => 'string-escaped',
			'line2'   => 'string-escaped-nullable',
			'city'    => 'string-escaped',
			'country' => 'string-escaped',
		];
	}


	/* ELOQUENT */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id');
	}

	public function venues()
	{
		return $this->hasMany(Venue::class, 'address_id');
	}

	public function lodgings()
	{
		return $this->hasMany(Lodging::class, 'address_id');
	}


	/* QUERIES */


	/* GETTERS / SETTERS */
	public function getInlineAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display($delimiter = ', ', $is_abbreviate_state = true)
	{
		$address = '';

		if($this->name != null)
		{
			$address .= $this->name . $delimiter;
		}

		$address = $this->line1 . $delimiter;

		if(($this->line2 != null) && ($this->line2 != ''))
		{
			$address .= $this->line2 . $delimiter;
		}

		$address .= $this->city . ', ' . $this->state->display($is_abbreviate_state) . ' ' . $this->zipcode;

		return $address;
	}
}
