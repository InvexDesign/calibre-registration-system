<?php namespace App\Models\Payments;


class CreditCardPayment extends Payment
{
	protected static $singleTableType = 'CreditCardPayment';

	public function getEidLink()
	{
		return "<a href='https://www.authorize.net/' target='_blank'>" . $this->eid . "</a>";
	}
}
