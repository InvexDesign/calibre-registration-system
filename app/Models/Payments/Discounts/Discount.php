<?php namespace App\Models\Payments\Discounts;

use App\Models\Address;
use App\Models\BaseModel;
use App\Models\Event;
use App\Models\Payments\DiscountPayment;
use App\Models\Registration;
use App\Traits\HasNotes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;

class Discount extends BaseModel
{
	use SingleTableInheritanceTrait, HasNotes;

	protected $table    = 'discounts';
	protected $fillable = [
		'class',
		'name',
		'code',
		'value',
		'start_at',
		'end_at',
		'is_active',
		'max_uses',
		'auto_attendee_min',
	];

	public static function getAttributeMap()
	{
		return [
			'name'              => 'string-escaped',
			'value'             => 'integer',
			'start_at'          => 'datetime',
			'end_at'            => 'datetime',
			'is_active'         => 'boolean-nullable',
			'max_uses'          => 'integer',
			'auto_attendee_min' => 'integer',
		];
	}

	protected static $singleTableTypeField  = 'class';
	protected static $singleTableSubclasses = [
		DollarDiscount::class,
		PercentDiscount::class,
	];

	public static function findAutomaticallyApplicableDiscounts($registration)
	{
		$discounts = Discount::query()
							 ->where('is_active', true)
							 ->where(function ($query)
							 {
								 $query->whereNull('start_at');
								 $query->orWhereNull('start_at', '<=', \Carbon::now());
							 })
							 ->where(function ($query)
							 {
								 $query->whereNull('end_at');
								 $query->orWhereNull('end_at', '>', \Carbon::now());
							 })
							 ->whereNotNull('auto_attendee_min')
							 ->get();
		$discounts->filter(function ($discount, $key) use ($registration)
		{
			return $discount->canBeAutomaticallyApplied($registration);
//			return $discount->canBeApplied($registration) && $discount->canBeAutomaticallyApplied($registration);
		});

		//TODO: If there are more than one, choose one? or present the options to user?
		$discounts = $discounts->sort(function ($a, $b) use ($registration)
		{
			return $a->calculateDiscount($registration) < $b->calculateDiscount($registration);
		});

		return $discounts->first();
	}

	public function canBeApplied(Registration $registration)
	{
		if(!$this->is_active)
		{
			return false;
		}

		if($this->max_uses && ($this->payments()->count() > $this->max_uses))
		{
			return false;
		}

		if($this->start_at && \Carbon::now()->lte($this->start_at))
		{
			return false;
		}

		if($this->end_at && \Carbon::now()->gte($this->end_at))
		{
			return false;
		}

		return true;
	}

	public function calculateDiscount(Registration $registration)
	{
		dd('Implement this in the sub class!');
	}

	public function apply(Registration $registration)
	{
		dd('Implement this in the sub class!');
	}

	public function canBeAutomaticallyApplied(Registration $registration)
	{
		if(!$this->canBeApplied($registration))
		{
			return false;
		}

		if($this->attendee_min && ($registration->tickets()->count() > $this->attendee_min))
		{
			return true;
		}

		return false;
	}


	/* ELOQUENT */
	public function address()
	{
		return $this->belongsTo(Address::class, 'address_id');
	}

	public function events()
	{
		return $this->belongsToMany(Event::class);
	}

	public function payments()
	{
		return $this->hasMany(DiscountPayment::class, 'eid');
	}


	/* QUERIES */
	public function scopeActive($query)
	{
		return $query->where('is_active', true);
	}

	public function scopeInactive($query)
	{
		return $query->where('is_active', false);
	}


	/* GETTERS / SETTERS */


	/* PRESENTATION */
	public function displayDiscount()
	{
		dd('Implement this in sub class!');
	}

	public function display()
	{
		return $this->name;
	}
}