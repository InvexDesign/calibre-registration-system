<?php namespace App\Models\Payments\Discounts;

use App\Models\Payments\DiscountPayment;
use App\Models\Registration;

class DollarDiscount extends Discount
{
	protected static $singleTableType = 'DollarDiscount';

	public function getType()
	{
		return 'dollar';
	}

//	public function canBeApplied(Registration $registration)
//	{
//		return parent::canBeApplied($registration);
//	}

	//TODO: Allow entire purchase to be discounted?
	public function calculateDiscount(Registration $registration)
	{
		$balance_in_cents = $registration->getBalanceInCents();//TODO: Should this use balance?
		$amount_in_cents = ($this->value > $balance_in_cents) ? $balance_in_cents : $this->value;

		return $amount_in_cents;
	}

	public function apply(Registration $registration)
	{
		$amount_in_cents = $this->calculateDiscount($registration);
		DiscountPayment::create([
			'eid'             => $this->id,
			'registration_id' => $registration->id,
			'amount_in_cents' => $amount_in_cents,
			'paid_at'         => \Carbon::now(),
			'notes'           => 'DollarDiscount',
		]);
	}

	public function displayDiscount()
	{
		return '$' . ($this->value / 100);
	}
}
