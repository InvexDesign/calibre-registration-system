<?php namespace App\Models\Payments\Discounts;

use App\Models\Payments\DiscountPayment;
use App\Models\Registration;

class PercentDiscount extends Discount
{
	protected static $singleTableType = 'PercentDiscount';

	public function getType()
	{
		return 'percent';
	}

//	public function canBeApplied(Registration $registration)
//	{
//		return parent::canBeApplied($registration);
//	}

	public function calculateDiscount(Registration $registration)
	{
		$total_cost_in_cents = $registration->getTotalCostInCents();
		$amount_in_cents = intval($total_cost_in_cents * ($this->value / 10000));

		return $amount_in_cents;
	}

	public function apply(Registration $registration)
	{
		$amount_in_cents = $this->calculateDiscount($registration);
		return DiscountPayment::create([
			'eid'             => $this->id,
			'registration_id' => $registration->id,
			'amount_in_cents' => $amount_in_cents,
			'paid_at'         => \Carbon::now(),
			'notes'           => 'PercentDiscount',
		]);
	}

	public function displayDiscount()
	{
		return ($this->value / 100) . '%';
	}
}
