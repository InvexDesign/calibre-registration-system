<?php namespace App\Models\Payments;


class CheckPayment extends Payment
{
	protected static $singleTableType = 'CheckPayment';

	public function getEidLink()
	{
		return $this->eid;
	}
}
