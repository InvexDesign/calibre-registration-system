<?php namespace App\Models\Payments;

use App\Models\BaseModel;
use App\Models\Registration;
use App\Traits\HasNotes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Payment extends BaseModel implements AuditableContract
{
	use SingleTableInheritanceTrait, AuditableTrait, HasNotes;

	protected $table    = 'payments';
	protected $fillable = [
		'class',
		'uid',
		'eid',
		'registration_id',
		'amount_in_cents',
		'paid_at',
//		'notes',
	];

	protected static $singleTableTypeField  = 'class';
	protected static $singleTableSubclasses = [
		Adjustment::class,
		CheckPayment::class,
		CreditCardPayment::class,
		DiscountPayment::class,
	];

	public static function getAttributeMap()
	{
		return [
			'eid'             => 'string-escaped',
			'amount_in_cents' => 'integer',
			'paid_at'         => 'datetime',
//			'notes'           => 'text-escaped',
		];
	}

	public static function getSingleTableType()
	{
		return self::$singleTableType;
	}


	/* ELOQUENT */
	public function registration()
	{
		return $this->belongsTo(Registration::class, 'registration_id');
	}

	public function getDiscount()
	{
		return null;
	}

	public function scopeType($query, $type)
	{
		return $query->where('class', $type);
	}


	/* QUERIES */
	public function scopeDiscountPayments($query)
	{
		return $query->type('DiscountPayment');
	}

	public function scopeCheckPayments($query)
	{
		return $query->type('CheckPayment');
	}

	public function scopeCreditCardPayments($query)
	{
		return $query->type('CreditCardPayment');
	}

	public function scopeIncome($query)
	{
		return $query->whereIn('class', ['CheckPayment', 'CreditCardPayment']);
	}

	public function scopeDiscounts($query)
	{
		return $query->where('class', 'DiscountPayment');
	}


	/* GETTERS / SETTERS */


	/* PRESENTATION */
	public function display()
	{
		return $this->uid;
	}

	public function getAmountAttribute()
	{
		return $this->amount_in_dollars;
	}

	public function getAmountInDollarsAttribute()
	{
		return $this->amount_in_cents / 100;
	}
}
