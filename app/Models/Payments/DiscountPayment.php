<?php namespace App\Models\Payments;


use App\Models\Payments\Discounts\Discount;

class DiscountPayment extends Payment
{
	protected static $singleTableType = 'DiscountPayment';

	public function getDiscount()
	{
		return $this->belongsTo(Discount::class, 'eid')->get()->first();
	}

	public function getEidLink()
	{
		return link_to_route('backend.discounts.show', $this->eid, [$this->eid]);
	}
}
