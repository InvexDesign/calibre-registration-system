<?php namespace App\Models\Payments\Gateways;

use App\Models\Payments\CreditCardPayment;
use App\Models\Registration;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Setting;


class AuthorizeNetPaymentGateway
{
	public static function chargeCreditCard(Registration $registration, $card_number, $expiration_month, $expiration_year, $card_code, $amount_in_cents)
	{
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(Setting::get('authorize-net-api-login-id'));
		$merchantAuthentication->setTransactionKey(Setting::get('authorize-net-transaction-key'));
		$refId = 'ref' . time();

		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($card_number);
		$creditCard->setExpirationDate($expiration_year . '-' . $expiration_month);
		$creditCard->setCardCode($card_code);//TODO: Is this needed?

		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);

		$order = new AnetAPI\OrderType();
		$order->setInvoiceNumber($registration->uid);
		$order->setDescription("Event #" . $registration->event->uid);


		$customer = new AnetAPI\CustomerDataType();
//		$customer->setId("15");
		$customer->setEmail($registration->email);

		$customerData = new AnetAPI\CustomerDataType();
		//TODO: Set contact name? company name?
		$customerData->setType("individual");//TODO: Company?
//		$customerData->setId("99999456654");//TODO: Can this be a string?
		$customerData->setEmail($registration->email);

		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($amount_in_cents / 100);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setCustomer($customerData);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);

		//TODO: Flip sandbox on/off?
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

		if($response != null)
		{

			// Since the API request was successful, look for a transaction response
			// and parse it to display the results of authorizing the card
			$tresponse = $response->getTransactionResponse();

			if($tresponse != null && $tresponse->getMessages() != null)
			{
				$notes = "Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";
				$notes .= "Transaction Response Code: " . $tresponse->getResponseCode() . "\n";
				$notes .= "Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";
				$notes .= "Auth Code: " . $tresponse->getAuthCode() . "\n";
				$notes .= "Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";

				$payment = CreditCardPayment::create([
					'eid'             => $tresponse->getTransId(),
					'registration_id' => $registration->id,
					'amount_in_cents' => $amount_in_cents,
					'paid_at'         => \Carbon::now(),
					'notes'           => $notes,
				]);

				return (object)[
					'success' => true,
					'payment' => $payment,
				];
			}
			else
			{
				$error = "Transaction Failed \n";
				if($tresponse->getErrors() != null)
				{
					$error .= " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
					$error .= " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
				}

				return (object)[
					'success' => false,
					'payment' => null,
					'error'   => $error,
				];
			}
		}
		else
		{
			$error = "No response returned \n";

			return (object)[
				'success' => false,
				'payment' => null,
				'error'   => $error,
			];
		}
	}
}
