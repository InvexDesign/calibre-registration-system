<?php namespace App\Models\Notes;

use App\Models\BaseModel;
use App\Models\User;

//TODO: Add visibility based on role? ie only viewable by overlords
class Note extends BaseModel
{
	protected $table    = 'notes';
	protected $fillable = [
		'content',
		'noteable_id',
		'noteable_type',
		'author_id',
	];

	public static function getAttributeMap()
	{
		return [
			'content' => 'string-escaped',
		];
	}


	/* ELOQUENT */
	public function noteable()
	{
		return $this->morphTo();
	}

	public function author()
	{
		return $this->belongsTo(User::class);
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->content;
	}
}