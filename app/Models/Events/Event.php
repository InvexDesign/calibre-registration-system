<?php namespace App\Models;

use App\Models\Payments\Payment;
use App\Traits\HasNotes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Event extends BaseModel implements AuditableContract
{
	use AuditableTrait, HasNotes;

	protected $table    = 'events';
	protected $fillable = [
		'uid',
		'venue_id',
		'state',
		'course_id',
		'sales_representative_id',
		'timezone',
		'start_at',
		'end_at',
		'is_private',
		'deadline_at',
		'times',
		'ticket_limit',
		'price_in_cents',
		'revenue_goal',
		'published_status',
		'published_at',
		'settled_at',
		'description',
//		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'start_at'       => 'datetime',
			'end_at'         => 'datetime',
			'is_private'     => 'boolean',
			'deadline_at'    => 'datetime',
			'times'          => 'string-escaped',
			'ticket_limit'   => 'integer',
			'price_in_cents' => 'integer',
			'revenue_goal'   => 'integer',
			'published_at'   => 'datetime',
			'settled_at'     => 'datetime',
			'description'    => 'string-escaped',
//			'notes'          => 'string-escaped',
		];
	}

	public function getAvailableTicketCount()
	{
		return ($this->ticket_limit != null) ? $this->ticket_limit - $this->tickets()->count() : null;
	}

	public function isPublished()
	{
		return $this->published_at != null;
	}


	/* ELOQUENT */
	public function venue()
	{
		return $this->belongsTo(Venue::class, 'venue_id');
	}

	public function course()
	{
		return $this->belongsTo(Course::class, 'course_id');
	}

	public function tickets()
	{
		return $this->hasMany(Ticket::class, 'event_id');
	}

	public function registrations()
	{
		return $this->hasMany(Registration::class, 'event_id');
	}

	public function lodgings()
	{
		return $this->belongsToMany(Lodging::class);
	}

	public function salesRepresentative()
	{
		return $this->belongsTo(User::class, 'sales_representative_id');
	}

	public function getPaymentsQuery()
	{
		$registration_ids = $this->registrations()->pluck('id')->all();
		return Payment::query()->whereIn('registration_id', $registration_ids);
	}

	public function getPayments()
	{
		return $this->getPaymentsQuery()->get();
	}


	/* QUERIES */
	public function scopePublished($query)
	{
		return $query->whereNotNull('published_at');
	}

	public function scopeUpcoming($query, $now = null)
	{
		$now = ($now != null) ? $now : \Carbon::now();

		return $query->where('start_at', '>=', $now);
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->uid;
	}

	public function getPriceAttribute()
	{
		return $this->price_in_dollars;
	}

	public function getPriceInDollarsAttribute()
	{
		return $this->price_in_cents / 100;
	}

	public function displayDate()
	{
		$date = $this->start_at->format('M j');
		if($this->end_at)
		{
			$date .= ' - ' . $this->end_at->format('M j, Y');
		}
		else
		{
			$date .= ', ' . $this->start_at->format('Y');
		}

		return $date;
	}

	public function displayClass()
	{
		$class = $this->course->display();

		if($this->is_private)
		{
			$class .= ' - Private';
		}

		$class .= ' (' . $this->venue->address->city . ', ' . $this->venue->address->state->display() . ')';

		return $class;
	}

	public function displayLocation()
	{
		return $this->venue->address->city . ', ' . $this->venue->address->state->display();
	}

	public function displayTimes()
	{
		return preg_replace('/[\n]/', '<br />', $this->times);
	}


	/* ANALYTICS*/
}