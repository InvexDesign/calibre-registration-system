<?php namespace App\Models;

use App\Models\Payments\Payment;
use App\Models\Statuses\RegistrationPaymentStatus;
use App\Traits\DisplaysPhoneNumber;
use App\Traits\HasNotes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Registration extends BaseModel implements AuditableContract
{
	use DisplaysPhoneNumber, AuditableTrait, HasNotes;

	protected $table    = 'registrations';
	protected $fillable = [
		'uid',
		'event_id',
		'agency',
		'contact',
		'email',
		'department_size_id',
		'phone',
		'fax',
		'address_id',
		'passcode',
		'payment_status',
	];

	public static function getAttributeMap()
	{
		return [
			'agency'          => 'string-escaped',
			'contact'         => 'string-escaped',
			'email'           => 'string-escaped',
			'phone'           => 'phone',
			'fax'             => 'phone',
		];
	}

	public static function generatePasscode()
	{
		$key = '';
		$pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

		for($i = 0; $i < 16; $i++)
		{
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}

		return $key;
	}

	public function isPaid()
	{
		return $this->payment_status == RegistrationPaymentStatus::PAID;
	}

	public function getBalanceInCents()
	{
		return $this->getTotalCostInCents() - $this->getTotalPaidInCents();
	}

	public function getBalanceInDollars()
	{
		return $this->getBalanceInCents() / 100;
	}

	public function getBalance()
	{
		return $this->getBalanceInDollars();
	}

	public function getTotalPaidInCents()
	{
		return intval($this->payments()->sum('amount_in_cents'));
	}

	public function getTotalPaidInDollars()
	{
		return $this->getTotalPaidInCents() / 100;
	}

	public function getTotalPaid()
	{
		return $this->getTotalPaidInDollars();
	}

	public function getTotalCostInCents()
	{
		return intval($this->tickets()->sum('price_in_cents'));
	}

	public function getTotalCostInDollars()
	{
		return $this->getTotalCostInCents() / 100;
	}

	public function getTotalCost()
	{
		return $this->getTotalCostInDollars();
	}

	public function getExternalUrl()
	{
		return route('frontend.registrations.show', [$this->uid, $this->passcode]);
	}


	/* ELOQUENT */
	public function event()
	{
		return $this->belongsTo(Event::class, 'event_id');
	}

	public function address()
	{
		return $this->belongsTo(Address::class, 'address_id');
	}

	public function departmentSize()
	{
		return $this->belongsTo(DepartmentSize::class, 'department_size_id');
	}

	public function tickets()
	{
		return $this->hasMany(Ticket::class, 'registration_id');
	}

	public function payments()
	{
		return $this->hasMany(Payment::class, 'registration_id');
	}


	/* QUERIES */


	/* GETTERS / SETTERS */
	public function getDepartmentSize()
	{
		return DepartmentSize::getOptions()[$this->department_size];
	}

	public function getAudits()
	{
		$audits = $this->audits;
		$audits = array_merge($audits, $this->tickets()->pluck('id')->all());
	}


	/* PRESENTATION */
	public function getCreatedAttribute()
	{
		return $this->created_at;
	}

	public function getTotalPriceAttribute()
	{
		return $this->tickets()->sum('price_in_cents') / 100;
	}

	public function display()
	{
		return $this->uid;
	}

	public function displayCourseAndLocation()
	{
		return $this->event->course->name . ' (' . $this->event->venue->address->city . ', ' . $this->event->venue->address->state->display() . ')';
	}

}
