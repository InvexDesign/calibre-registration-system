<?php namespace App\Models;

use App\Traits\DisplaysPhoneNumber;
use App\Traits\HasEmbeddedMap;
use App\Traits\HasNotes;

class Venue extends BaseModel
{
	use DisplaysPhoneNumber, HasEmbeddedMap, HasNotes;

	protected $table    = 'venues';
	protected $fillable = [
		'uid',
		'name',
		'embedded_map',
		'address_id',
		'auto_gps',
		'latitude',
		'longitude',
		'phone',
		'email',
//		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'name'         => 'string-escaped',
			'embedded_map' => 'html-nullable',
			'auto_gps'     => 'boolean',
			'latitude'     => 'float',
			'longitude'    => 'float',
			'phone'        => 'phone',
//			'notes'        => 'text-escaped',
		];
	}


	/* ELOQUENT */
	public function address()
	{
		return $this->belongsTo(Address::class, 'address_id');
	}

	public function events()
	{
		return $this->hasMany(Event::class, 'venue_id');
	}


	/* PRESENTATION */
	public function display()
	{
		return $this->name;
	}

	public function getNameAndAddressAttribute()
	{
		return $this->name . ' - ' . $this->address->display();
	}


	/* ANALYTICS*/
}