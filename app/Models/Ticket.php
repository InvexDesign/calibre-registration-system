<?php namespace App\Models;

use App\Traits\HasNotes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Ticket extends BaseModel implements AuditableContract
{
	use AuditableTrait, HasNotes;

	protected $table    = 'tickets';
	protected $fillable = [
		'uid',
		'event_id',
		'registration_id',
		'attendee_id',
		'price_in_cents',
	];

	public static function getAttributeMap()
	{
		return [
			'price_in_cents' => 'integer',
		];
	}


	/* ELOQUENT */
	public function event()
	{
		return $this->belongsTo(Event::class, 'event_id');
	}

	public function registration()
	{
		return $this->belongsTo(Registration::class, 'registration_id');
	}

	public function attendee()
	{
		return $this->belongsTo(Attendee::class, 'attendee_id');
	}


	/* QUERIES */


	/* GETTERS / SETTERS */


	/* PRESENTATION */
	public function getPriceInDollarsAttribute()
	{
		return $this->price_in_cents / 100;
	}

	public function display()
	{
		return $this->uid;
	}

	public function backendDisplayRoutes()
	{
		return [
			'show' => 'backend.tickets.show'
		];
	}
}
