<?php namespace App\Models;

use App\Traits\HasNotes;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Attendee extends BaseModel implements AuditableContract
{
	use AuditableTrait, HasNotes;

	protected $table    = 'attendees';
	protected $fillable = [
		'uid',
		'first_name',
		'last_name',
		'email',
		'rank',
		'pid',
//		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'first_name' => 'string-escaped',
			'last_name'  => 'string-escaped',
			'rank'       => 'string-escaped',
			'pid'        => 'string-escaped',
//			'notes'      => 'text-escaped',
		];
	}


	/* ELOQUENT */
	public function tickets()
	{
		return $this->hasMany(Ticket::class, 'attendee_id');
	}


	/* QUERIES */


	/* GETTERS / SETTERS */


	/* PRESENTATION */
	public function display()
	{
		return $this->first_name . ' ' . $this->last_name;
	}
}
