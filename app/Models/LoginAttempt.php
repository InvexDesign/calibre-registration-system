<?php namespace App\Models;

use App\Interfaces\Displayable;

class LoginAttempt extends BaseModel implements Displayable
{
	protected $table    = 'login_attempts';
	protected $fillable = [
		'username',
		'ip_address',
		'timestamp',
		'success',
		'country',
		'region',
		'city',
		'postal',
		'latitude',
		'longitude',
		'organization',
		'hostname',
		'browser',
		'referrer',
		'user_id',
	];

	public static function getAttributeMap()
	{
		return [
			'timestamp' => 'datetime',
		];
	}

	public static function removeRecordsOlderThan($cutoff)
	{
		self::query()
			->where('timestamp', '<', $cutoff)
			->delete();
	}


	/* ELOQUENT */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function getDisplayAttribute()
	{
		return $this->display();
	}


	/* PRESENTATION */
	public function display()
	{
		return 'TODO';
	}
}