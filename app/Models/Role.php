<?php namespace App;

use App\Interfaces\Displayable;
use Exception;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole implements Displayable
{
	protected $fillable = [
		'name',
		'display_name',
		'description'
	];

	public static function get($name, $fail_if_not_found = false)
	{
		$role = Role::where('name', $name)->get()->first();
		if(!$role && $fail_if_not_found)
		{
			throw new Exception("Role '$name' not found!");
		}

		return $role;
	}

	public static function getBackendHierarchy($display = 'name')
	{
		switch($display)
		{
			case 'display':
				return [
					'Overlord',
					'Master',
					'Administrator',
				];
				break;
			case 'name':
			default:
				return [
					'overlord',
					'master',
					'administrator',
				];
				break;
		}
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'role_id');
	}

	public function display()
	{
		return $this->display_name;
	}
}