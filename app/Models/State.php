<?php namespace App\Models;

class State extends BaseModel
{
	protected $table    = 'states';
	protected $fillable = [
		'name',
		'short_name'
	];

	public static function getSelectOptions()
	{
		$sorted_states = State::all();
//		$sorted_states = State::all()->sort(function ($a, $b)
//		{
//			if($a->short_name == 'IL')
//			{
//				return -1;
//			}
//			elseif($b->short_name == 'IL')
//			{
//				return 1;
//			}
//
//			return strcmp($a->name, $b->name);
//		});

		return $sorted_states->pluck('name', 'id')->all();
	}

	public static function get($short_name)
	{
		return State::short($short_name)->get()->first();
	}


	/* ELOQUENT */
	public function addresses()
	{
		return $this->hasMany(Address::class, 'state_id');
	}


	/* QUERIES */
	public function scopeShort($query, $short_name)
	{
		return $query->where('short_name', $short_name);
	}


	/* PRESENTATION */
	public function display($is_abbreviated = true)
	{
		if($is_abbreviated)
		{
			return $this->short_name;
		}
		else
		{
			return $this->name;
		}
	}
}
