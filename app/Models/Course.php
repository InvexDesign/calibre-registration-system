<?php namespace App\Models;

use App\Models\Payments\Payment;
use App\Traits\HasNotes;
use Helper;

class Course extends BaseModel
{
	use HasNotes;

	protected $table    = 'courses';
	protected $fillable = [
		'uid',
		'name',
		'short_name',
		'description',
//		'notes',
	];

	public static function getAttributeMap()
	{
		return [
			'name'        => 'string-escaped',
			'short_name'  => 'string-escaped',
			'description' => 'text-escaped',
//			'notes'       => 'text-escaped',
		];
	}


	/* ELOQUENT */
	public function events()
	{
		return $this->hasMany(Event::class, 'course_id');
	}

	public function getTicketsQuery()
	{
		$event_ids = $this->events()->pluck('id')->all();

		return Ticket::query()->whereIn('event_id', $event_ids);
	}

	public function getTickets()
	{
		return $this->getTicketsQuery()->get();
	}

	public function getRegistrationsQuery()
	{
		$event_ids = $this->events()->pluck('id')->all();

		return Registration::query()->whereIn('event_id', $event_ids);
	}

	public function getRegistrations()
	{
		return $this->getRegistrationsQuery()->get();
	}

	public function getPaymentsQuery()
	{
		$registration_ids = $this->getRegistrationsQuery()->pluck('id')->all();

		return Payment::query()->whereIn('registration_id', $registration_ids);
	}

	public function getPayments()
	{
		return $this->getPaymentsQuery()->get();
	}


	/* PRESENTATION */
	public function getShortName()
	{
		return $this->short_name ?: Helper::getCapitalLetters($this->name);
	}

	public function display()
	{
		return $this->name;
	}


	/* ANALYTICS*/
}