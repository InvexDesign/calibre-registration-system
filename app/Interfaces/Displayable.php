<?php namespace App\Interfaces;

/**
 * Created by PhpStorm.
 * User: Paranoid Android
 * Date: 5/27/2015
 * Time: 4:21 PM
 */

interface Displayable {

	public function display();
}