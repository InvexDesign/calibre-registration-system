<?php

namespace App\Providers;

use App\Models\Attendee;
use App\Models\Course;
use App\Models\Event;
use App\Models\Lodging;
use App\Models\Payments\Adjustment;
use App\Models\Payments\CheckPayment;
use App\Models\Payments\DiscountPayment;
use App\Models\Payments\CreditCardPayment;
use App\Models\Registration;
use App\Models\Ticket;
use App\Models\Venue;
use App\Observers\AdjustmentObserver;
use App\Observers\AttendeeObserver;
use App\Observers\CheckPaymentObserver;
use App\Observers\DiscountPaymentObserver;
use App\Observers\CourseObserver;
use App\Observers\CreditCardPaymentObserver;
use App\Observers\EventObserver;
use App\Observers\LodgingObserver;
use App\Observers\PaymentObserver;
use App\Observers\RegistrationObserver;
use App\Observers\TicketObserver;
use App\Observers\VenueObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Schema::defaultStringLength(191);

		Validator::extend('datepicker', function ($attribute, $value, $parameters, $validator)
		{
			if($value && $value != '')
			{
				list($dd, $mm, $yyyy) = explode('/', $value);

				return checkdate($mm, $dd, $yyyy);
			}

			return true;
		});

		Adjustment::observe(AdjustmentObserver::class);
		Attendee::observe(AttendeeObserver::class);
		Course::observe(CourseObserver::class);
		DiscountPayment::observe(DiscountPaymentObserver::class);
		CreditCardPayment::observe(CreditCardPaymentObserver::class);
		CheckPayment::observe(CheckPaymentObserver::class);
		Event::observe(EventObserver::class);
		Lodging::observe(LodgingObserver::class);
		Registration::observe(RegistrationObserver::class);
		Ticket::observe(TicketObserver::class);
		Venue::observe(VenueObserver::class);
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		if($this->app->environment() !== 'production')
		{
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}
	}
}
