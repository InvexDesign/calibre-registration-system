<?php namespace App\Providers;

use App\Http\Controllers\Backend\AddressController;
use App\Http\Controllers\Backend\AttendeeController;
use App\Http\Controllers\Backend\CourseController;
use App\Http\Controllers\Backend\EventController;
use App\Http\Controllers\Backend\LodgingController;
use App\Http\Controllers\Backend\RegistrationController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\TicketController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\VenueController;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		AddressController::composeViews();
		AttendeeController::composeViews();
		CourseController::composeViews();
		EventController::composeViews();
		LodgingController::composeViews();
		RegistrationController::composeViews();
		TicketController::composeViews();
		VenueController::composeViews();
		SettingController::composeViews();
		UserController::composeViews();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
