<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;


use App\Models\Setting;
use Carbon\Carbon;
use Faker\Factory;

class Helper
{
	public static function getCapitalLetters($str)
	{
		if(preg_match_all('#([A-Z]+)#', $str, $matches))
		{
			return implode('', $matches[1]);
		}

		return '';
	}

	public static function snakeCase($input)
	{
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];

		foreach($ret as &$match)
		{
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}

		return implode('_', $ret);
	}

	public static function cleanDollarAmount($input)
	{
		return preg_replace('/[^\d\.]/', '', $input);
	}

	public static function dateInputFormat($date)
	{
		if($date == null)
		{
			return null;
		}

		return $date->format('m/d/Y');
	}

	public static function timeInputFormat($date)
	{
		if($date == null)
		{
			return null;
		}

		return $date->format('g:iA');
	}

	public static function datetimeDisplayFormat($date)
	{
		if($date == null)
		{
			return 'N/A';
		}

		return $date->format('g:iA m/d/Y');
	}

	public static function dateDisplayFormat($date)
	{
		if($date == null)
		{
			return 'N/A';
		}

		return $date->format('m/d/Y');
	}

	public static function sortDatetime($date)
	{
		if($date == null)
		{
			$date = Carbon::now();
			$display = 'N/A';
		}
		else
		{
			$display = $date->format('g:iA m/d/Y');
		}

		return '<span class="hide">' . $date->format('Y-m-d H:i:s') . '</span>' . $display;
	}

	public static function sortTime($date)
	{
		if($date == null)
		{
			$date = Carbon::now();
			$display = 'N/A';
		}
		else
		{
			$display = $date->format('g:iA');
		}

		return '<span class="hide">' . $date->format('H:i:s') . '</span>' . $display;
	}

	public static function sortWeekday($date)
	{
		if($date == null)
		{
			$date = Carbon::now();
			$display = 'N/A';
		}
		else
		{
			$display = $date->format('l');
		}
		return '<span class="hide">' . $date->dayOfWeek . '</span>' . $display;
	}

	public static function sortFirstLastName($primary_name, $secondary_name, $display)
	{
		return '<span class="hide">' . $primary_name . $secondary_name . '</span>' . $display;
	}

	public static function sortDay($date)
	{
		if($date == null)
		{
			$date = Carbon::now();
			$display = 'N/A';
		}
		else
		{
			$display = $date->format('n/j/y');
		}

		return '<span class="hide">' . $date->format('Y-m-d H:i:s') . '</span>' . $display;
	}

	public static function sortDate($date, $default = 'now', $format = 'n/j/Y')
	{
		if($date == null)
		{
			$date = Carbon::parse($default);
			$display = 'N/A';
		}
		else
		{
			$display = $date->format($format);
		}

		return '<span class="hide">' . $date->format('Y-m-d H:i:s') . '</span>' . $display;
	}

	public static function sortDollar($amount, $default = '0.00')
	{
		if($amount == null || $amount == '')
		{
			$amount = '0.00';
			$display = $default;
		}
		else
		{
			$display = '$' . number_format($amount, 2);
		}

		$max_digits = 12 + 1;
		$leading_zeroes = '0000000000000';
		$sort_amount = substr($leading_zeroes, 0, $max_digits - strlen($amount)) . $amount;

		return '<span class="hide">' . $sort_amount . '</span>' . $display;
	}

	public static function sortInteger($amount, $default = '0')
	{
		if($amount == null || $amount == '')
		{
			$amount = 0;
			$display = $default;
		}
		else
		{
			$display = number_format($amount, 0);
		}

		$max_digits = 12 + 1;
		$leading_zeroes = '0000000000000';
		$sort_amount = substr($leading_zeroes, 0, $max_digits - strlen($amount)) . $amount;

		return '<span class="hide">' . $sort_amount . '</span>' . $display;
	}

	public static function generateUniqueColor($current_colors, $default_color = 'red')
	{
		$faker = Factory::create();

		$count = 0;
		$color = $faker->hexcolor;
		while(in_array($color, $current_colors))
		{
			if($count > 100)
			{
				$color = $default_color;
				break;
			}

			$color = $faker->hexcolor;
		}

		return $color;
	}

	public static function getCannotDeleteModelWithDependenciesMessage($model_name, $dependencies)
	{
		$message =  "Cannot delete ${model_name}s that have ";
		$i = 1;
		$num_dependencies = count($dependencies);
		foreach($dependencies as $dependency => $count)
		{
			if($i == 1)
			{
				$message .= $dependency . 's';
			}
			elseif($i == $num_dependencies)
			{
				$message .= ' or ' .$dependency . 's!';
			}
			else
			{
				$message .= ', ' .$dependency . 's';
			}

			$i++;
		}

		$message .= ". This ${model_name} has ";
		$i = 1;
		$num_dependencies = count($dependencies);
		foreach($dependencies as $dependency => $count)
		{
			if($i == 1)
			{
				$message .= $count . ' ' . $dependency . '(s)';
			}
			elseif($i == $num_dependencies)
			{
				$message .= ' and ' . $count . ' ' . $dependency . '(s) that depend on it.';
			}
			else
			{
				$message .= ', ' . $count . ' ' . $dependency . '(s)';
			}

			$i++;
		}

		return $message;
	}


	public static function populateInvoiceKeywords($invoice, $original)
	{
		$company = Setting::get('company_name', 'Invex Design');
		$pay_to_company = Setting::get('pay_to_company', 'Invex Design, LLC');
		$company_website = Setting::get('company_website', 'http://www.invexdesign.com/');
		$invoice_balance = '$' . number_format($invoice->getBalance(), 2);

		if(Setting::get('pay_to_address_line_1', null) && Setting::get('pay_to_address_line_1') != '')
		{
			$line_1 = Setting::get('pay_to_address_line_1');
			$billing_address = "<div>$line_1</div>";

			if(Setting::get('pay_to_address_line_2', null) && Setting::get('pay_to_address_line_2') != '')
			{
				$line_2 = Setting::get('pay_to_address_line_2');
				$billing_address .= "<div>$line_2</div>";
			}

			$original = str_replace('%BILLING_ADDRESS%', $billing_address, $original);
		}

		$original = str_replace('%INVOICE_ID%', $invoice->id, $original);
		$original = str_replace('%COMPANY%', $company, $original);
		$original = str_replace('%COMPANY_WEBSITE%', $company_website, $original);
		$original = str_replace('%PAY_TO_COMPANY%', $pay_to_company, $original);
		$original = str_replace('%INVOICE_BALANCE%', $invoice_balance, $original);

		return $original;
	}

	public static function escapeNonAlphaNum($value)
	{
		return preg_replace("/([^a-zA-Z0-9])/","\\\\$1", $value);
	}

	public static function unescapeNonAlphaNum($value)
	{
		return preg_replace("/\\\\([^a-zA-Z0-9])/", "$1", $value);
	}

	public static function mailTo($email, $default = 'N/A')
	{
		if($email === null)
		{
			return $default;
		}

		return "<a href=\"mailto:$email\">$email</a>";
	}

	public static function phoneTo($phone, $default = 'N/A')
	{
		if($phone === null)
		{
			return $default;
		}

		$digits = self::getNonExtensionDigits($phone);

		return "<a href=\"tel:+1$digits\">$phone</a>";
	}

	public static function displayDollar($value, $default = 'N/A')
	{
		if($value === null)
		{
			return $default;
		}

		return '$' . number_format($value, 2);
	}

	public static function displayDate($date, $default = 'N/A')
	{
		if($date == null)
		{
			return $default;
		}

		return $date->format('n/j/Y');
	}

	public static function displayDateTime($date, $default = 'N/A')
	{
		if($date == null)
		{
			return $default;
		}

		return $date->format('g:iA n/j/Y');
	}

	public static function displayInteger($value, $default = 'N/A')
	{
		if($value == null)
		{
			return $default;
		}

		return number_format($value, 0);
	}

	public static function icon_to_route($route, $icon_classes, $parameters, $attributes = [], $size = 'fa-2x')
	{
		$url = route($route, $parameters);
		$attributes['href'] = $url;

		$markup = "<a ";
		foreach($attributes as $key => $value)
		{
			$markup .= 	$key . "=\"$value\"";
		}
		$markup .= 	">";
		$markup .= 	"<i class=\"fa $size $icon_classes\" aria-hidden=\"true\"></i></a>";

		return $markup;
	}
	
	public static function faded_icon($icon_classes, $attributes = [])
	{
		$markup = "<i class=\"fa fa-2x $icon_classes faded\" aria-hidden=\"true\" ";
		foreach($attributes as $key => $value)
		{
			$markup .= 	$key . "=\"$value\"";
		}
		$markup .= 	"></i>";

		return $markup;
	}
	
	public static function displayCalendarDate($dt, $style = 'date-5')
	{
		if($dt == null)
		{
			return '';
		}

		$display = $dt->format('m/d/Y');

		switch($style)
		{
			case 'calendar-date':
				$month = $dt->format('m');
				$day = $dt->format('d');
				$year = $dt->format('Y');
				$markup = "
					<span class=\"calendar-date\" title=\"$display\">
						<i class=\"icon fa fa-calendar-o\"></i>
						<span class=\"calendar-overlay\">
							<span class=\"month-day\">
								<span class=\"month\">$month</span>
								<span class=\"day\">$day</span>
							</span>
							<span class=\"year\">$year</span>
						</span>
					</span>";
				break;
			case 'date-1':
				$month = $dt->format('M');
				$day = $dt->format('j');
				$markup = "
					<span class=\"date-1\" title=\"$display\">
						<div class=\"date-header\">$month</div>
						<div class=\"date-day\">$day</div>
					</span>";
				break;
			case 'date-2':
				$month = $dt->format('M');
				$day = $dt->format('j');
				$year = $dt->format('Y');
				$markup = "
					<span class=\"date-2\" title=\"$display\">
						<div class=\"date-header\">$month</div>
						<div class=\"date-day\">$day</div>
						<div class=\"date-footer\">$year</div>
					</span>";
				break;
			case 'date-3':
				$month = $dt->format('M');
				$day = $dt->format('j');
				$year = $dt->format('y');
				$markup = "
					<span class=\"date-3\" title=\"$display\">
						<div class=\"date-header\">
							<div class=\"month\">$month</div>
							<div class=\"year\">'$year</div>
						</div>
						<div class=\"date-day\">$day</div>
					</span>";
				break;
			case 'date-4':
				$month = $dt->format('M');
				$day = $dt->format('j');
				$year = $dt->format('y');
				$ordinal = $dt->format('S');
				$markup = "
					<span class=\"date-4\" title=\"$display\">
						<div class=\"date-header\">
							<div class=\"month\">$month</div>
							<div class=\"year\">'$year</div>
						</div>
						<div class=\"date-content\">
							<span class=\"day\">$day</span>
							<span class=\"ordinal-indicator\">$ordinal</span>
						</div>
					</span>";
				break;
			case 'date-5':
			default:
				$size_class = '';
				$years_markup = '';
				//TODO: Timezone?
				$years_in_future = Carbon::now()->diffInYears($dt, false);
				if($years_in_future != 0)
				{
					$sign = ($years_in_future > 0) ? '+' : "";
					$years_markup = "<div class=\"year\">${sign}${years_in_future}</div>";
//
//					$year_count = abs($years_in_future);
//					$year_noun = ($year_count > 1) ? 'years' : 'year';
//					$year_display = " (${year_count} ${year_noun}";
//					$year_display .= ($years_in_future > 0) ? " from now)" : " ago)";
//					$year_display = ' (' . $dt->diffForHumans() . ')';
//					$display .= $year_display;

					$size_class = 'bigger';
				}

				$display .= ' (' . $dt->diffForHumans() . ')';

				$month = $dt->format('M');
				$day = $dt->format('j');
				$ordinal = $dt->format('S');
				$markup = "
					<span class=\"date-5 $size_class\" title=\"$display\">
						<div class=\"date-header\">
							<div class=\"month\">$month</div>
							$years_markup
						</div>
						<div class=\"date-content\">
							<span class=\"day\">$day</span>
							<span class=\"ordinal-indicator\">$ordinal</span>
						</div>
					</span>";
				break;
		}
		

		return $markup;
	}
	
	public static function displayCalendarDate_old($carbon_date, $style = 'date-4')
	{
		$display = $carbon_date->format('m/d/Y');
		$month = $carbon_date->format('m');
		$day = $carbon_date->format('d');
		$year = $carbon_date->format('Y');

		$markup = "
		<span class=\"calendar-date\" title=\"$display\">
			<i class=\"icon fa fa-calendar-o\"></i>
			<span class=\"calendar-overlay\">
				<span class=\"month-day\">
					<span class=\"month\">$month</span>
					<span class=\"day\">$day</span>
				</span>
				<span class=\"year\">$year</span>
			</span>
		</span>";

		return $markup;
	}
	
	public static function sortCalendarDate($dt, $default = 'now')
	{
		if($dt == null)
		{
			$dt = Carbon::parse($default);
		}

		$searchable_dates = $dt->format('m/d/Y') . $dt->format('m-d-Y') . $dt->format('m d Y');
		$sortable_date = $dt->format('Y-m-d H:i:s');

		$markup = "<span class=\"hide\">$sortable_date</span>";
		$markup .= "<span style=\"opacity: 0; position: absolute; left: 99999px;\">$searchable_dates</span>";
		$markup .= self::displayCalendarDate($dt);

		return $markup;
	}

	public static function excerpt($string, $parameters = [])
	{
		$tail = isset($parameters['tail']) ? $parameters['tail'] : ' [...]';
		$length = isset($parameters['length']) ? $parameters['length'] : 250;
		$length = $length - strlen($tail);

		$short = substr($string, 0, $length);
		$word_endings = [' ', '.', '?', '!', ':', ';', ')'];

		$position = -1;
		foreach($word_endings as $ending)
		{
			$position = max($position, strrpos($short, $ending));
		}
		
		return substr($short, 0, $position) . $tail;
	}
	
	public static function sanitize_title($title)
	{
		$stripped = preg_replace('/[^A-Za-z0-9\s]/', '', $title);
		$dashed = preg_replace('/\s+/', '-', $stripped);
		$lowercase = strtolower($dashed);

		return $lowercase;
	}

	public static function yearsInFuture(Carbon $b, Carbon $a = null)
	{
		$a = $a ?: Carbon::now($b->tz);
		$years = $b->diffInYears($a, false);
		return intval($b->format('Y')) - intval($a->format('Y'));
	}

	public static function formatPhone($string)
	{
		$digits = preg_replace('/[^0-9]/', '', $string);
//		if(substr($digits, 0, 1) == '1')
//		{
//			$digits = substr($digits, 1);
//		}

		$length = strlen($digits);
		if($length > 10)
		{
			$phone = substr($digits, 0, 3) . '-' . substr($digits, 3, 3) . '-' . substr($digits, 6, 4) . 'x' . substr($digits, 10);
		}
		elseif($length == 10)
		{
			$phone = substr($digits, 0, 3) . '-' . substr($digits, 3, 3) . '-' . substr($digits, 6, 4);
		}
		elseif($length == 7)
		{
			$phone = substr($digits, 0, 3) . '-' . substr($digits, 3, 4);
		}
		elseif($length == 0)
		{
			$phone = '';
		}
		else
		{
			$phone = null;
		}
		
		return $phone;
	}
	
	public static function getNonExtensionDigits($phone)
	{
		return substr(preg_replace('/[^\d]/', '', $phone), 0, 10);
	}
	
	public static function displayCategory($category)
	{
		return '<span style="border-left: 30px solid ' . $category->primary_color . '; padding: 5px;">' . $category->display() . '</span>';
//		return '<span style="display: inline-block; width: calc(100% - 30px); border-right: 30px solid ' . $category->primary_color . '; padding: 5px;">' . $category->display() . '</span>';
	}
	
}