<?php
/**
 * Created by PhpStorm.
 * User: asittwo
 * Date: 7/15/2015
 * Time: 8:35 AM
 */

namespace App\Helpers;


use App\Models\Setting;
use Auth;
use Carbon\Carbon;
use Faker\Factory;
use Session;

class TimezoneHelper
{
	public static function getUserTimezone($default = null)
	{
		if(Auth::user() && Auth::user()->timezone)
		{
			return Auth::user()->timezone;
		}
		elseif(Session::has('timezone') && Session::get('timezone', false))
		{
			return Session::get('timezone');
		}
		elseif($default == null)
		{
			return Setting::get('default_timezone', 'America/Chicago');
		}

		return $default;
	}

	public static function getOptions()
	{
		return [
			'America/New_York' => 'America/New_York', //Eastern
			'America/Chicago' => 'America/Chicago', //Central
			'America/Denver' => 'America/Denver', //Mountain
			'America/Phoenix' => 'America/Phoenix', //Mountain no DST
			'America/Los_Angeles' => 'America/Los_Angeles', //Pacific
			'America/Anchorage' => 'America/Anchorage', //Alaska
			'America/Adak' => 'America/Adak', //Hawaii
			'Pacific/Honolulu' => 'Pacific/Honolulu', //Hawaii no DST
		];
	}
}