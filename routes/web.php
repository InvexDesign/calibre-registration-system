<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Payments\DiscountPayment;
use App\Models\Payments\Discounts\Discount;
use App\Models\Payments\Discounts\DollarDiscount;
use App\Models\Payments\Payment;
use App\Models\Registration;

Route::get('/', ['as' => 'frontend.index', 'uses' => 'FrontendController@index']);
//Route::post('/filter', ['as' => 'frontend.filter', 'uses' => 'FrontendController@filter']);
//Route::post('/process-facility-click', ['as' => 'frontend.process_facility_click', 'uses' => 'FrontendController@processFacilityClick']);


if(config('app.env') == 'local')
{
	Route::get('test', function ()
	{
		return 'nope';
	});
	Route::get('sheet', function ()
	{
		return view('frontend.sheet');
	});
	Route::get('debug', function ()
	{
		return 'debugging';
	});
}

Route::get('/login', ['as' => 'session.login', 'uses' => 'SessionController@create']);
Route::post('/login', ['as' => 'session.store', 'uses' => 'SessionController@store']);
Route::get('/logout', ['as' => 'session.destroy', 'uses' => 'SessionController@destroy']);

Route::get('/password/forgot', ['as' => 'password.reset.email.get', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('/password/forgot', ['as' => 'password.reset.email.post', 'uses' => 'Auth\PasswordController@postEmail']);
Route::get('/password/reset/{token}', ['as' => 'password.reset.get', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

Route::get('/unauthorized', ['as' => 'errors.unauthorized', 'uses' => 'ErrorController@unauthorized']);


Route::group(['prefix' => '/events/'], function ()
{
	Route::get('/', ['as' => 'frontend.events.index', 'uses' => 'Frontend\EventController@index']);
	Route::get('/map', ['as' => 'frontend.events.map', 'uses' => 'Frontend\EventController@map']);
	Route::get('/{uid}', ['as' => 'frontend.events.show', 'uses' => 'Frontend\EventController@show']);
	Route::get('/{uid}/register', ['as' => 'frontend.workflows.registration.register.get', 'uses' => 'Frontend\Workflows\RegistrationWorkflowController@getRegister']);
	Route::post('/{uid}/register', ['as' => 'frontend.workflows.registration.register.post', 'uses' => 'Frontend\Workflows\RegistrationWorkflowController@postRegister']);
});

Route::group(['prefix' => '/registrations/'], function ()
{
	Route::get('/{uid}/{passcode}', ['as' => 'frontend.registrations.show', 'uses' => 'Frontend\RegistrationController@show']);
	Route::get('/{uid}/invoice/{passcode}', ['as' => 'frontend.registrations.invoice', 'uses' => 'Frontend\RegistrationController@invoice']);
	Route::get('/{uid}/pay/{passcode}', ['as' => 'frontend.workflows.payment.get', 'uses' => 'Frontend\Workflows\PaymentWorkflowController@getProcessPayment']);
	Route::post('/{uid}/pay/{passcode}', ['as' => 'frontend.workflows.payment.post', 'uses' => 'Frontend\Workflows\PaymentWorkflowController@postProcessPayment']);
	Route::post('/pay/apply-promo-code', ['as' => 'frontend.workflows.payment.apply_discount', 'uses' => 'Frontend\Workflows\PaymentWorkflowController@applyDiscount']);
});

Route::group(['prefix' => '/recaptcha/'], function ()
{
	Route::post('/verify', ['as' => 'frontend.recaptcha.verify', 'uses' => 'Frontend\RecaptchaController@verify']);
	Route::get('/fail', ['as' => 'frontend.recaptcha.fail', 'uses' => 'Frontend\RecaptchaController@fail']);
});


Route::group(['prefix' => '/manage/', 'namespace' => 'Backend'], function ()
{
	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => 'DashboardController@index']);
//	Route::get('/', ['as' => 'backend.dashboard.index', 'uses' => '\App\Http\Controllers\FrontendController@interactiveMap']);

	Route::group(['prefix' => '/my-account/'], function ()
	{
		Route::get('/', ['as' => 'backend.my_account.show', 'uses' => 'MyAccountController@show']);
		Route::get('/edit', ['as' => 'backend.my_account.edit.get', 'uses' => 'MyAccountController@getEdit']);
		Route::post('/edit', ['as' => 'backend.my_account.edit.post', 'uses' => 'MyAccountController@postEdit']);
		Route::get('/change-password', ['as' => 'backend.my_account.change_password.get', 'uses' => 'MyAccountController@getChangePassword']);
		Route::post('/change-password', ['as' => 'backend.my_account.change_password.post', 'uses' => 'MyAccountController@postChangePassword']);
	});

	Route::group(['prefix' => '/addresses/'], function ()
	{
		Route::get('/', ['as' => 'backend.addresses.index', 'uses' => 'AddressController@index']);
		Route::get('/{id}', ['as' => 'backend.addresses.show', 'uses' => 'AddressController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.addresses.edit.get', 'uses' => 'AddressController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.addresses.edit.post', 'uses' => 'AddressController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.addresses.destroy', 'uses' => 'AddressController@destroy']);
	});

	Route::group(['prefix' => '/attendees/'], function ()
	{
		Route::get('/', ['as' => 'backend.attendees.index', 'uses' => 'AttendeeController@index']);
		Route::get('/{id}', ['as' => 'backend.attendees.show', 'uses' => 'AttendeeController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.attendees.edit.get', 'uses' => 'AttendeeController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.attendees.edit.post', 'uses' => 'AttendeeController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.attendees.destroy', 'uses' => 'AttendeeController@destroy']);
	});

	Route::group(['prefix' => '/courses/'], function ()
	{
		Route::get('/', ['as' => 'backend.courses.index', 'uses' => 'CourseController@index']);
		Route::get('/create', ['as' => 'backend.courses.create.get', 'uses' => 'CourseController@getCreate']);
		Route::post('/create', ['as' => 'backend.courses.create.post', 'uses' => 'CourseController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.courses.show', 'uses' => 'CourseController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.courses.edit.get', 'uses' => 'CourseController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.courses.edit.post', 'uses' => 'CourseController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.courses.destroy', 'uses' => 'CourseController@destroy']);
	});

	Route::group(['prefix' => '/discounts/'], function ()
	{
		Route::get('/', ['as' => 'backend.discounts.index', 'uses' => 'DiscountController@index']);
		Route::get('/active', ['as' => 'backend.discounts.active', 'uses' => 'DiscountController@active']);
		Route::get('/inactive', ['as' => 'backend.discounts.inactive', 'uses' => 'DiscountController@inactive']);
		Route::get('/create', ['as' => 'backend.discounts.create.get', 'uses' => 'DiscountController@getCreate']);
		Route::post('/create', ['as' => 'backend.discounts.create.post', 'uses' => 'DiscountController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.discounts.show', 'uses' => 'DiscountController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.discounts.edit.get', 'uses' => 'DiscountController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.discounts.edit.post', 'uses' => 'DiscountController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.discounts.destroy', 'uses' => 'DiscountController@destroy']);
	});

	Route::group(['prefix' => '/events/'], function ()
	{
		Route::get('/', ['as' => 'backend.events.index', 'uses' => 'EventController@index']);
		Route::get('/create', ['as' => 'backend.events.create.get', 'uses' => 'EventController@getCreate']);
		Route::post('/create', ['as' => 'backend.events.create.post', 'uses' => 'EventController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.events.show', 'uses' => 'EventController@show']);
		Route::get('/{id}/audits', ['as' => 'backend.events.audits', 'uses' => 'EventController@audits']);
		Route::get('/{id}/attendees', ['as' => 'backend.events.attendees', 'uses' => 'AttendeeController@indexByEvent']);
		Route::get('/{id}/tickets', ['as' => 'backend.events.tickets', 'uses' => 'TicketController@indexByEvent']);
		Route::get('/{id}/venues', ['as' => 'backend.events.venues', 'uses' => 'VenueController@indexByEvent']);
		Route::get('/{id}/edit', ['as' => 'backend.events.edit.get', 'uses' => 'EventController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.events.edit.post', 'uses' => 'EventController@postEdit']);
		Route::get('/{id}/mark-settled', ['as' => 'backend.events.mark_settled', 'uses' => 'EventController@markSettled']);
		Route::get('/{id}/mark-unsettled', ['as' => 'backend.events.mark_unsettled', 'uses' => 'EventController@markUnsettled']);
		Route::get('/{id}/destroy', ['as' => 'backend.events.destroy', 'uses' => 'EventController@destroy']);
	});

	Route::group(['prefix' => '/lodgings/'], function ()
	{
		Route::get('/', ['as' => 'backend.lodgings.index', 'uses' => 'LodgingController@index']);
		Route::get('/create', ['as' => 'backend.lodgings.create.get', 'uses' => 'LodgingController@getCreate']);
		Route::post('/create', ['as' => 'backend.lodgings.create.post', 'uses' => 'LodgingController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.lodgings.show', 'uses' => 'LodgingController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.lodgings.edit.get', 'uses' => 'LodgingController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.lodgings.edit.post', 'uses' => 'LodgingController@postEdit']);
		Route::get('/{id}/generate-embedded-map', ['as' => 'backend.lodgings.generate_embedded_map', 'uses' => 'LodgingController@generateEmbeddedMap']);
		Route::get('/{id}/set-embedded-map', ['as' => 'backend.lodgings.set_embedded_map.get', 'uses' => 'LodgingController@getSetEmbeddedMap']);
		Route::post('/{id}/set-embedded-map', ['as' => 'backend.lodgings.set_embedded_map.post', 'uses' => 'LodgingController@postSetEmbeddedMap']);
		Route::get('/{id}/destroy', ['as' => 'backend.lodgings.destroy', 'uses' => 'LodgingController@destroy']);
	});

	Route::group(['prefix' => '/notes/'], function ()
	{
		Route::get('/', ['as' => 'backend.notes.index', 'uses' => 'NoteController@index']);
//		Route::get('/create', ['as' => 'backend.notes.create.get', 'uses' => 'NoteController@getCreate']);
		Route::post('/create', ['as' => 'backend.notes.create.post', 'uses' => 'NoteController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.notes.show', 'uses' => 'NoteController@show']);
//		Route::get('/{id}/edit', ['as' => 'backend.notes.edit.get', 'uses' => 'NoteController@getEdit']);
//		Route::post('/{id}/edit', ['as' => 'backend.notes.edit.post', 'uses' => 'NoteController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.notes.destroy', 'uses' => 'NoteController@destroy']);
	});

	Route::group(['prefix' => '/payments/'], function ()
	{
		Route::get('/', ['as' => 'backend.payments.index', 'uses' => 'PaymentController@index']);
		Route::get('/{id}', ['as' => 'backend.payments.show', 'uses' => 'PaymentController@show']);
	});

	Route::group(['prefix' => '/registrations/'], function ()
	{
		Route::get('/', ['as' => 'backend.registrations.index', 'uses' => 'RegistrationController@index']);
		Route::get('/{id}', ['as' => 'backend.registrations.show', 'uses' => 'RegistrationController@show']);
		Route::get('/{id}/apply-check-payment', ['as' => 'backend.registrations.apply_check_payment.get', 'uses' => 'PaymentController@getApplyCheckPayment']);
		Route::post('/{id}/apply-check-payment', ['as' => 'backend.registrations.apply_check_payment.post', 'uses' => 'PaymentController@postApplyCheckPayment']);
		Route::get('/{id}/apply-discount', ['as' => 'backend.registrations.apply_discount.get', 'uses' => 'PaymentController@getApplyDiscount']);
		Route::post('/{id}/apply-discount', ['as' => 'backend.registrations.apply_discount.post', 'uses' => 'PaymentController@postApplyDiscount']);
		Route::get('/{id}/apply-adjustment', ['as' => 'backend.registrations.apply_adjustment.get', 'uses' => 'PaymentController@getApplyAdjustment']);
		Route::post('/{id}/apply-adjustment', ['as' => 'backend.registrations.apply_adjustment.post', 'uses' => 'PaymentController@postApplyAdjustment']);
		Route::post('/{id}/add-note', ['as' => 'backend.registrations.add_note', 'uses' => 'RegistrationController@addNote']);
		Route::get('/{id}/add-tickets', ['as' => 'backend.registrations.add_tickets.get', 'uses' => 'TicketController@getAdd']);
		Route::post('/{id}/add-tickets', ['as' => 'backend.registrations.add_tickets.post', 'uses' => 'TicketController@postAdd']);
		Route::get('/{id}/change-address', ['as' => 'backend.registrations.change_address.get', 'uses' => 'RegistrationController@getChangeAddress']);
		Route::post('/{id}/change-address', ['as' => 'backend.registrations.change_address.post', 'uses' => 'RegistrationController@postChangeAddress']);
		Route::get('/{id}/invoice', ['as' => 'backend.registrations.invoice', 'uses' => 'RegistrationController@invoice']);
		Route::get('/{id}/audits', ['as' => 'backend.registrations.audits', 'uses' => 'RegistrationController@audits']);
		Route::get('/{id}/edit', ['as' => 'backend.registrations.edit.get', 'uses' => 'RegistrationController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.registrations.edit.post', 'uses' => 'RegistrationController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.registrations.destroy', 'uses' => 'RegistrationController@destroy']);
	});

	Route::group(['prefix' => '/tickets/'], function ()
	{
		Route::get('/', ['as' => 'backend.tickets.index', 'uses' => 'TicketController@index']);
		Route::get('/{id}', ['as' => 'backend.tickets.show', 'uses' => 'TicketController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.tickets.edit.get', 'uses' => 'TicketController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.tickets.edit.post', 'uses' => 'TicketController@postEdit']);
		Route::get('/{id}/destroy', ['as' => 'backend.tickets.destroy', 'uses' => 'TicketController@destroy']);
	});

	Route::group(['prefix' => '/venues/'], function ()
	{
		Route::get('/', ['as' => 'backend.venues.index', 'uses' => 'VenueController@index']);
		Route::get('/search', ['as' => 'backend.venues.search', 'uses' => 'VenueController@search']);
		Route::get('/create', ['as' => 'backend.venues.create.get', 'uses' => 'VenueController@getCreate']);
		Route::post('/create', ['as' => 'backend.venues.create.post', 'uses' => 'VenueController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.venues.show', 'uses' => 'VenueController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.venues.edit.get', 'uses' => 'VenueController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.venues.edit.post', 'uses' => 'VenueController@postEdit']);
		Route::get('/{id}/generate-embedded-map', ['as' => 'backend.venues.generate_embedded_map', 'uses' => 'VenueController@generateEmbeddedMap']);
		Route::get('/{id}/set-embedded-map', ['as' => 'backend.venues.set_embedded_map.get', 'uses' => 'VenueController@getSetEmbeddedMap']);
		Route::post('/{id}/set-embedded-map', ['as' => 'backend.venues.set_embedded_map.post', 'uses' => 'VenueController@postSetEmbeddedMap']);
		Route::get('/{id}/destroy', ['as' => 'backend.venues.destroy', 'uses' => 'VenueController@destroy']);
	});

	Route::group(['prefix' => '/users/'], function ()
	{
		Route::get('/', ['as' => 'backend.users.index', 'uses' => 'UserController@index']);
		Route::get('/create', ['as' => 'backend.users.create.get', 'uses' => 'UserController@getCreate']);
		Route::post('/create', ['as' => 'backend.users.create.post', 'uses' => 'UserController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.users.show', 'uses' => 'UserController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.users.edit.get', 'uses' => 'UserController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.users.edit.post', 'uses' => 'UserController@postEdit']);
		Route::get('/{id}/change-password', ['as' => 'backend.users.change_password.get', 'uses' => 'UserController@getChangePassword']);
		Route::post('/{id}/change-password', ['as' => 'backend.users.change_password.post', 'uses' => 'UserController@postChangePassword']);
		Route::get('/{id}/destroy', ['as' => 'backend.users.destroy', 'uses' => 'UserController@destroy']);
	});

	Route::group(['prefix' => '/settings/'], function ()
	{
		Route::get('/', ['as' => 'backend.settings.index', 'uses' => 'SettingController@index']);
//		Route::get('/create', ['as' => 'backend.settings.create.get', 'uses' => 'SettingController@getCreate']);
//		Route::post('/create', ['as' => 'backend.settings.create.post', 'uses' => 'SettingController@postCreate']);
		Route::get('/{id}', ['as' => 'backend.settings.show', 'uses' => 'SettingController@show']);
		Route::get('/{id}/edit', ['as' => 'backend.settings.edit.get', 'uses' => 'SettingController@getEdit']);
		Route::post('/{id}/edit', ['as' => 'backend.settings.edit.post', 'uses' => 'SettingController@postEdit']);
//		Route::get('/{id}/destroy', ['as' => 'backend.settings.destroy', 'uses' => 'SettingController@destroy']);
	});

	Route::group(['prefix' => '/login-attempts/'], function ()
	{
		Route::get('/', ['as' => 'backend.login_attempts.index', 'uses' => 'LoginAttemptController@index']);
		Route::get('/timeline', ['as' => 'backend.login_attempts.timeline', 'uses' => 'LoginAttemptController@timeline']);
//		Route::get('/timeline', ['as' => 'backend.login_attempts.timeline', 'uses' => 'LoginAttemptController@timeline']);
		Route::get('/{id}', ['as' => 'backend.login_attempts.show', 'uses' => 'LoginAttemptController@show']);
//		Route::get('/{id}/edit', ['as' => 'backend.login_attempts.edit.get', 'uses' => 'LoginAttemptController@getEdit']);
//		Route::post('/{id}/edit', ['as' => 'backend.login_attempts.edit.post', 'uses' => 'LoginAttemptController@postEdit']);
	});

	Route::group(['prefix' => '/audits/'], function ()
	{
		Route::get('/', ['as' => 'backend.audits.index', 'uses' => 'AuditController@index']);
		Route::get('/{id}', ['as' => 'backend.audits.show', 'uses' => 'AuditController@show']);
	});

	Route::group(['prefix' => '/analytics/'], function ()
	{
		Route::group(['prefix' => '/courses/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.courses.index', 'uses' => 'AnalyticsController@courses']);
			Route::get('/timeline', ['as' => 'backend.analytics.courses.timeline', 'uses' => 'AnalyticsController@coursesTimeline']);
		});

		Route::group(['prefix' => '/events/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.events.index', 'uses' => 'AnalyticsController@events']);
		});

		Route::group(['prefix' => '/payments/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.payments.index', 'uses' => 'AnalyticsController@payments']);
		});

		Route::group(['prefix' => '/registrations/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.registrations.index', 'uses' => 'AnalyticsController@registrations']);
		});

		Route::group(['prefix' => '/sales-representatives/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.sales_representatives.index', 'uses' => 'AnalyticsController@salesRepresentatives']);
		});

		Route::group(['prefix' => '/features/'], function ()
		{
			Route::get('/', ['as' => 'backend.analytics.features.timeline', 'uses' => 'AnalyticsController@FeatureSearchTimeline']);
			Route::get('/timeline', ['as' => 'backend.analytics.features.timeline', 'uses' => 'AnalyticsController@FeatureSearchTimeline']);
			Route::get('/monthly', ['as' => 'backend.analytics.features.monthly', 'uses' => 'AnalyticsController@FeatureSearchMonthly']);
		});
	});
});